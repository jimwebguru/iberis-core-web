/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.jimwebguru.iberis.core.web

import java.io.Serializable
import java.util.ResourceBundle

import javax.annotation.PostConstruct
import javax.enterprise.context.ApplicationScoped
import javax.faces.context.FacesContext
import javax.inject.Named

import com.jimwebguru.iberis.core.web.utils.GeneralUtil

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
@Named
@ApplicationScoped
class SiteSettings : Serializable
{
	var siteUrl: String? = null

	var bundle: ResourceBundle? = null
		private set

	@PostConstruct
	fun init()
	{
		val serverUrl = GeneralUtil.request!!.scheme + "://" + GeneralUtil.request!!.serverName + ":" + (if (GeneralUtil.request!!.serverPort != 80) GeneralUtil.request!!.serverPort else "") + GeneralUtil.request!!.contextPath

		this.siteUrl = serverUrl

		this.bundle = ResourceBundle.getBundle("com.jimwebguru.iberis.core.web.bundle.DatabaseResourceBundle", FacesContext.getCurrentInstance().viewRoot.locale)
	}
}
