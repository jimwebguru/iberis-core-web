package com.jimwebguru.iberis.core.web.components

import com.jimwebguru.iberis.core.persistence.*
import com.jimwebguru.iberis.core.persistence.MenuColumn
import com.jimwebguru.iberis.core.persistence.MenuItem
import com.jimwebguru.iberis.core.utils.db.DaoUtil
import com.jimwebguru.iberis.core.web.controllers.admin.security.Authorize
import com.jimwebguru.iberis.core.web.utils.GeneralUtil
import com.ocpsoft.pretty.PrettyContext
import org.primefaces.model.menu.*

import javax.enterprise.context.RequestScoped
import javax.inject.Named
import javax.servlet.http.HttpServletRequest
import java.util.*


/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
@Named
@RequestScoped
class MenuUtil
{
	private val menuModels = HashMap<Long, DefaultMenuModel?>()
	private val smartMenuMarkups = HashMap<String, String?>()
	private val flatMenuMarkups = HashMap<Long, String?>()

	val menuTypeList: ArrayList<UnitPropertyListItem>
		get()
		{
			val unitPropertyListItems = ArrayList<UnitPropertyListItem>()

			for (menuType in MenuType.values())
			{
				val unitPropertyListItem = UnitPropertyListItem()
				unitPropertyListItem.itemValue = menuType.typeId.toString()
				unitPropertyListItem.displayValue = menuType.name

				unitPropertyListItems.add(unitPropertyListItem)
			}

			return unitPropertyListItems
		}

	fun getMenuModel(menu: Menu?): DefaultMenuModel?
	{
		if (menu != null && menu.id != null && menu.id > 0 && menu.type != 7 && menu.type != 6)
		{
			val menuModel = DefaultMenuModel()

			if (menu.menuItemCollection != null)
			{
				for (menuItem in menu.menuItemCollection)
				{
					if (this.userCanAccess(menuItem))
					{
						if (menuItem.isSubMenuGroup!! && menu.type != 6)
						{
							val defaultSubMenu = DefaultSubMenu()

							defaultSubMenu.label = menuItem.displayName
							defaultSubMenu.id = "prime-sub-" + menuItem.id!!
							defaultSubMenu.styleClass = "prime-sub-" + menuItem.id + " " + menuItem.cssClassNames

							if (menuItem.icon != null && menuItem.icon.isNotEmpty())
							{
								defaultSubMenu.icon = menuItem.icon
							}

							if (menuItem.subMenuItems != null && menuItem.subMenuItems.isNotEmpty())
							{
								for (sMenuItem in menuItem.subMenuItems)
								{
									if (this.userCanAccess(sMenuItem))
									{
										this.buildSubMenus(sMenuItem, defaultSubMenu, menu.type!!)
									}
								}
							}

							if (menuItem.separatorBefore != null && menuItem.separatorBefore!!)
							{
								menuModel.addElement(DefaultSeparator())
							}

							menuModel.addElement(defaultSubMenu)

							if (menuItem.separatorAfter != null && menuItem.separatorAfter!!)
							{
								menuModel.addElement(DefaultSeparator())
							}
						}
						else
						{
							val defaultMenuItem = DefaultMenuItem()
							defaultMenuItem.value = menuItem.displayName

							if (!menuItem.url.startsWith("/") && !menuItem.externalLink)
							{
								defaultMenuItem.url = "/" + menuItem.url
							}
							else
							{
								defaultMenuItem.url = menuItem.url
							}

							defaultMenuItem.id = "prime-menu-item-" + menuItem.id!!
							defaultMenuItem.styleClass = "prime-menu-item " + menuItem.cssClassNames

							if (menuItem.openNewTab != null && menuItem.openNewTab!!)
							{
								defaultMenuItem.target = "_blank"
							}

							if (menuItem.icon != null && menuItem.icon.isNotEmpty())
							{
								defaultMenuItem.icon = menuItem.icon
							}

							if (menuItem.separatorBefore != null && menuItem.separatorBefore!!)
							{
								menuModel.addElement(DefaultSeparator())
							}

							menuModel.addElement(defaultMenuItem)

							if (menuItem.separatorAfter != null && menuItem.separatorAfter!!)
							{
								menuModel.addElement(DefaultSeparator())
							}
						}
					}
				}
			}

			return menuModel
		}

		return null
	}

	private fun buildSubMenus(menuItem: MenuItem, defaultSubMenu: DefaultSubMenu, menuType: Int)
	{
		if (menuItem.isSubMenuGroup!! && menuType != 0)
		{
			val pmSubmenu = DefaultSubMenu()
			pmSubmenu.label = menuItem.displayName
			pmSubmenu.id = "prime-sub-" + menuItem.id!!
			pmSubmenu.styleClass = "prime-sub-group prime-sub-" + menuItem.id + " " + menuItem.cssClassNames

			if (menuItem.icon != null && menuItem.icon.isNotEmpty())
			{
				pmSubmenu.icon = menuItem.icon
			}

			if (menuItem.subMenuItems != null && menuItem.subMenuItems.isNotEmpty())
			{
				for (sMenuItem in menuItem.subMenuItems)
				{
					this.buildSubMenus(sMenuItem, pmSubmenu, menuType)
				}
			}

			if (menuItem.separatorBefore != null && menuItem.separatorBefore!!)
			{
				defaultSubMenu.addElement(DefaultSeparator())
			}

			defaultSubMenu.addElement(pmSubmenu)

			if (menuItem.separatorAfter != null && menuItem.separatorAfter!!)
			{
				defaultSubMenu.addElement(DefaultSeparator())
			}
		}
		else if (!menuItem.isSubMenuGroup)
		{
			val pmItem = DefaultMenuItem()
			pmItem.value = menuItem.displayName

			if (!menuItem.url.startsWith("/") && !menuItem.externalLink)
			{
				pmItem.url = "/" + menuItem.url
			}
			else
			{
				pmItem.url = menuItem.url
			}

			pmItem.id = "prime-menu-item-" + menuItem.id!!
			pmItem.styleClass = "prime-menu-item " + menuItem.cssClassNames

			if (menuItem.openNewTab != null && menuItem.openNewTab!!)
			{
				pmItem.target = "_blank"
			}

			if (menuItem.icon != null && menuItem.icon.isNotEmpty())
			{
				pmItem.icon = menuItem.icon
			}

			if (menuItem.separatorBefore != null && menuItem.separatorBefore!!)
			{
				defaultSubMenu.addElement(DefaultSeparator())
			}

			defaultSubMenu.addElement(pmItem)

			if (menuItem.separatorAfter != null && menuItem.separatorAfter!!)
			{
				defaultSubMenu.addElement(DefaultSeparator())
			}
		}
	}

	fun getBreadCrumbMenuModel(menu: Menu?, siteId: Long): DefaultMenuModel?
	{
		if (menu != null && menu.id != null && menu.id > 0 && menu.type == 6 && menu.menuToCrumb != null)
		{
			val menuModel = DefaultMenuModel()

			val request = GeneralUtil.request

			if (PrettyContext.getCurrentInstance().currentMapping != null)
			{
				var urlAlias = PrettyContext.getCurrentInstance().currentMapping.pattern

				if (urlAlias.indexOf("#{/.*/ urlAlias}") > -1)
				{
					val alias = request!!.getParameter("urlAlias")

					urlAlias = urlAlias.replace("#\\{/.*/ urlAlias\\}".toRegex(), alias)
				}

				if (urlAlias.indexOf("#{formUrlAlias}") > -1)
				{
					val alias = request!!.getParameter("formUrlAlias")

					urlAlias = urlAlias.replace("#\\{formUrlAlias\\}".toRegex(), alias)
				}

				if (urlAlias.indexOf("#{catalogUrlAlias}") > -1)
				{
					val alias = request!!.getParameter("catalogUrlAlias")

					urlAlias = urlAlias.replace("#\\{catalogUrlAlias\\}".toRegex(), alias)
				}

				val crumbMenu = menu.menuToCrumb

				var targetMenuItem: MenuItem? = null
				val defaultMenuItems = ArrayList<DefaultMenuItem>()

				for (item in crumbMenu.menuItemCollection)
				{
					if (targetMenuItem == null)
					{
						targetMenuItem = this.findMenuItem(item, urlAlias)

						if (targetMenuItem != null)
						{
							this.getBreadCrumbMenuItems(targetMenuItem, defaultMenuItems)
						}
					}
				}

				if(targetMenuItem == null && siteId > 0)
				{
					val daoUtil = DaoUtil()

					val redirectList = daoUtil.find<UrlRedirect>("SELECT OBJECT(o) FROM UrlRedirect o WHERE o.site.siteID = $siteId AND o.redirectTo = '$urlAlias'")

					if(redirectList != null && redirectList.isNotEmpty())
					{
						urlAlias = redirectList[0].redirectFrom
					}

					for (item in crumbMenu.menuItemCollection)
					{
						if (targetMenuItem == null)
						{
							targetMenuItem = this.findMenuItem(item, urlAlias)

							if (targetMenuItem != null)
							{
								this.getBreadCrumbMenuItems(targetMenuItem, defaultMenuItems)
							}
						}
					}
				}

				val pmItem = DefaultMenuItem()
				pmItem.value = "Home"
				pmItem.url = GeneralUtil.serverUrl
				pmItem.id = "prime-menu-item-home"
				pmItem.styleClass = "prime-menu-home"

				defaultMenuItems.add(pmItem)

				Collections.reverse(defaultMenuItems)

				for (defaultMenuItem in defaultMenuItems)
				{
					menuModel.addElement(defaultMenuItem)
				}
			}

			return menuModel
		}

		return null
	}

	private fun findMenuItem(item: MenuItem, urlAlias: String): MenuItem?
	{
		var foundMenuItem: MenuItem?

		if (!item.url.startsWith("/"))
		{
			item.url = "/" + item.url
		}

		if (urlAlias == item.url)
		{
			return item
		}

		if (item.subMenuItems != null && item.subMenuItems.isNotEmpty())
		{
			for (sItem in item.subMenuItems)
			{
				foundMenuItem = this.findMenuItem(sItem, urlAlias)

				if (foundMenuItem != null)
				{
					return foundMenuItem
				}
			}
		}

		return null
	}

	private fun getBreadCrumbMenuItems(item: MenuItem, defaultMenuItems: ArrayList<DefaultMenuItem>)
	{
		val pmItem = DefaultMenuItem()
		pmItem.value = item.displayName

		if (!item.url.startsWith("/"))
		{
			pmItem.url = "/" + item.url
		}
		else
		{
			pmItem.url = item.url
		}

		pmItem.id = "prime-menu-item-" + item.id!!
		pmItem.styleClass = "prime-menu-item " + item.cssClassNames

		/*if(item.getIcon() != null && item.getIcon().isNotEmpty())
		{
			pmItem.setIcon(item.getIcon());
		} */

		defaultMenuItems.add(pmItem)

		if (item.parentMenuItem != null)
		{
			this.getBreadCrumbMenuItems(item.parentMenuItem, defaultMenuItems)
		}
	}

	fun getFlatMenuMarkup(menu: Menu?): String?
	{
		if (menu != null && menu.id != null && menu.id > 0 && menu.type == 9)
		{
			val builder = StringBuilder()
			builder.append("<div id='flat-menu-" + menu.id + "' class='flat-menu ui-g'>")

			if (menu.menuItemCollection != null)
			{
				for (menuItem in menu.menuItemCollection)
				{
					if (this.userCanAccess(menuItem))
					{
						builder.append("<div class='ui-g-3 ui-sm-6'>")

						if (menuItem.isSubMenuGroup!!)
						{
							if (menuItem.url != null && menuItem.url.trim { it <= ' ' } != "")
							{
								builder.append("<a class='top-link' href='")

								if (!menuItem.url.startsWith("/") && !menuItem.externalLink)
								{
									builder.append("/" + menuItem.url)
								}
								else
								{
									builder.append(menuItem.url)
								}

								builder.append("'")

								if (menuItem.openNewTab != null && menuItem.openNewTab!!)
								{
									builder.append(" target='_blank'")
								}

								builder.append(">")

								if (menuItem.icon != null && menuItem.icon.isNotEmpty())
								{
									builder.append("<span class='mobile-menu-icon ")
									builder.append(menuItem.icon)
									builder.append("'></span>")
								}
								builder.append("<span class='p-menu-sub-group first-sub-group'>")
								builder.append(menuItem.displayName)
								builder.append("</span>")
								builder.append("</a>")
							}
							else
							{
								builder.append("<span class='p-menu-sub-group first-sub-group'>")

								if (menuItem.icon != null && menuItem.icon.isNotEmpty())
								{
									builder.append("<span class='mobile-menu-icon ")
									builder.append(menuItem.icon)
									builder.append("'></span>")
								}

								builder.append(menuItem.displayName)
								builder.append("</span>")
							}

							builder.append("<ul class='p-menu-sub-group-list first-sub-group-list'>")

							if (menuItem.subMenuItems != null && menuItem.subMenuItems.isNotEmpty())
							{
								for (sMenuItem in menuItem.subMenuItems)
								{
									if (this.userCanAccess(sMenuItem))
									{
										this.buildMobileSubMenusMarkup(sMenuItem, builder)
									}
								}
							}

							builder.append("</ul>")
						}
						else
						{
							builder.append("<a class='top-link' href='")

							if (!menuItem.url.startsWith("/") && !menuItem.externalLink)
							{
								builder.append("/" + menuItem.url)
							}
							else
							{
								builder.append(menuItem.url)
							}

							builder.append("'")

							if (menuItem.openNewTab != null && menuItem.openNewTab!!)
							{
								builder.append(" target='_blank'")
							}

							builder.append(">")

							if (menuItem.icon != null && menuItem.icon.isNotEmpty())
							{
								builder.append("<span class='mobile-menu-icon ")
								builder.append(menuItem.icon)
								builder.append("'></span>")
							}

							builder.append(menuItem.displayName)
							builder.append("</a>")
						}

						builder.append("</div>")
					}
				}
			}

			builder.append("</div>")

			return builder.toString()
		}

		return null
	}

	fun getSmartMenuMarkup(menu: Menu?, assignmentId: Long, zIndex: Int, vertical: Boolean): String?
	{
		if (menu != null && menu.id != null && menu.id > 0 && menu.type != 7)
		{
			val builder = StringBuilder()
			builder.append("<div id='smart-menu-container-$assignmentId-" + menu.id + "' class='smart-menu-container preload'")

			if(menu.width != null && menu.width > 0)
			{
				builder.append(" style='width: ${menu.width}px'")
			}

			builder.append(">")

			builder.append("<input id=\"smart-menu-state-$assignmentId-${menu.id}\" class=\"smart-menu-state\" type=\"checkbox\" />" +
					"<label class=\"smart-menu-btn\" for=\"smart-menu-state-$assignmentId-${menu.id}\">" +
					"<span class=\"smart-menu-btn-icon\"></span> Toggle main menu visibility</label>")

			if(menu.buttonText != null && menu.buttonText.isNotEmpty())
			{
				builder.append("<h2 class=\"nav-mobile-text\">${menu.buttonText}</h2>")
			}

			builder.append("<ul id='smart-menu-$assignmentId-" + menu.id + "' class='iberis-menu iberis-menu-$assignmentId-${menu.id} sm ")

			if(vertical)
			{
				builder.append("sm-vertical ")
			}

			builder.append("sm-simple smart-menu'")

			if(zIndex > 0 && menu.zIndex == 0)
			{
				builder.append(" style='z-index: $zIndex'")
			}

			if(menu.zIndex > 0)
			{
				builder.append(" style='z-index: ${menu.zIndex}'")
			}

			builder.append(">")

			if (menu.menuItemCollection != null)
			{
				for (menuItem in menu.menuItemCollection)
				{
					if (this.userCanAccess(menuItem))
					{
						builder.append("<li>")

						if (menuItem.isSubMenuGroup!! && menu.type != 6)
						{
							if (menuItem.url != null && menuItem.url.trim { it <= ' ' } != "")
							{
								builder.append("<a href='")

								if (!menuItem.url.startsWith("/") && !menuItem.externalLink)
								{
									builder.append("/" + menuItem.url)
								}
								else
								{
									builder.append(menuItem.url)
								}

								builder.append("'")

								if (menuItem.openNewTab != null && menuItem.openNewTab!!)
								{
									builder.append(" target='_blank'")
								}

								builder.append(">")

								if (menuItem.icon != null && menuItem.icon.isNotEmpty())
								{
									builder.append("<span class='mobile-menu-icon ")
									builder.append(menuItem.icon)
									builder.append("'></span>")
								}
								builder.append("<span class='p-menu-sub-group'>")
								builder.append(menuItem.displayName)
								builder.append("</span>")
								builder.append("</a>")
							}
							else
							{
								builder.append("<span class='p-menu-sub-group'>")

								if (menuItem.icon != null && menuItem.icon.isNotEmpty())
								{
									builder.append("<span class='mobile-menu-icon ")
									builder.append(menuItem.icon)
									builder.append("'></span>")
								}

								builder.append(menuItem.displayName)
								builder.append("</span>")
							}

							builder.append("<ul class='p-menu-sub-group-list'>")

							if (menuItem.subMenuItems != null && menuItem.subMenuItems.isNotEmpty())
							{
								for (sMenuItem in menuItem.subMenuItems)
								{
									if (this.userCanAccess(sMenuItem))
									{
										this.buildMobileSubMenusMarkup(sMenuItem, builder)
									}
								}
							}

							builder.append("</ul>")
						}
						else
						{
							builder.append("<a href='")

							if (!menuItem.url.startsWith("/") && !menuItem.externalLink)
							{
								builder.append("/" + menuItem.url)
							}
							else
							{
								builder.append(menuItem.url)
							}

							builder.append("'")

							if (menuItem.openNewTab != null && menuItem.openNewTab!!)
							{
								builder.append(" target='_blank'")
							}

							builder.append(">")

							if (menuItem.icon != null && menuItem.icon.isNotEmpty())
							{
								builder.append("<span class='mobile-menu-icon ")
								builder.append(menuItem.icon)
								builder.append("'></span>")
							}

							builder.append(menuItem.displayName)
							builder.append("</a>")
						}

						builder.append("</li>")
					}
				}
			}

			builder.append("</ul>")
			builder.append("</div>")

			builder.append("<script type=\"text/javascript\">")
			builder.append("$(document).ready(function(){")
			builder.append("$('#smart-menu-$assignmentId-" + menu.id + "').smartmenus({collapsibleBehavior: 'link'});")
			builder.append("if ($('#smart-menu-container-$assignmentId-${menu.id} .smart-menu-state').length) {" +
					"$('#smart-menu-container-$assignmentId-${menu.id} .smart-menu-state').change(function(e) {" +
					"var smartMenu = $('#smart-menu-container-$assignmentId-${menu.id} .smart-menu');" +
					"if (this.checked) {" +
					"smartMenu.hide().slideDown(250, function() { smartMenu.css('display', ''); });" +
					"} else {" +
					"smartMenu.show().slideUp(250, function() { smartMenu.css('display', ''); });" +
					"}" +
					"});" +
					"$(window).bind('beforeunload unload', function() {" +
					"if ($('#smart-menu-container-$assignmentId-${menu.id} .smart-menu-state')[0].checked) {" +
					"$('#smart-menu-container-$assignmentId-${menu.id} .smart-menu-state')[0].click();" +
					"}" +
					"});" +
					"}")
			builder.append("});")
			builder.append("</script>")

			return builder.toString()
		}

		return null
	}

	private fun buildMobileSubMenusMarkup(menuItem: MenuItem, builder: StringBuilder)
	{
		builder.append("<li>")

		if (menuItem.isSubMenuGroup!!)
		{
			if (menuItem.url != null && menuItem.url.trim { it <= ' ' } != "")
			{
				builder.append("<a href='")

				if (!menuItem.url.startsWith("/") && !menuItem.externalLink)
				{
					builder.append("/" + menuItem.url)
				}
				else
				{
					builder.append(menuItem.url)
				}

				builder.append("'")

				if (menuItem.openNewTab != null && menuItem.openNewTab!!)
				{
					builder.append(" target='_blank'")
				}

				builder.append(">")

				if (menuItem.icon != null && menuItem.icon.isNotEmpty())
				{
					builder.append("<span class='mobile-menu-icon ")
					builder.append(menuItem.icon)
					builder.append("'></span>")
				}

				builder.append("<span class='p-menu-sub-group'>")
				builder.append(menuItem.displayName)
				builder.append("</span>")
				builder.append("</a>")
			}
			else
			{
				builder.append("<span class='p-menu-sub-group'>")

				if (menuItem.icon != null && menuItem.icon.isNotEmpty())
				{
					builder.append("<span class='mobile-menu-icon ")
					builder.append(menuItem.icon)
					builder.append("'></span>")
				}

				builder.append(menuItem.displayName)
				builder.append("</span>")
			}

			builder.append("<ul class='p-menu-sub-group-list'>")

			if (menuItem.subMenuItems != null && menuItem.subMenuItems.isNotEmpty())
			{
				for (sMenuItem in menuItem.subMenuItems)
				{
					this.buildMobileSubMenusMarkup(sMenuItem, builder)
				}
			}

			builder.append("</ul>")
		}
		else
		{
			builder.append("<a href='")

			if (!menuItem.url.startsWith("/") && !menuItem.externalLink)
			{
				builder.append("/" + menuItem.url)
			}
			else
			{
				builder.append(menuItem.url)
			}

			builder.append("'")

			if (menuItem.openNewTab != null && menuItem.openNewTab!!)
			{
				builder.append(" target='_blank'")
			}

			builder.append(">")

			if (menuItem.icon != null && menuItem.icon.isNotEmpty())
			{
				builder.append("<span class='mobile-menu-icon ")
				builder.append(menuItem.icon)
				builder.append("'></span>")
			}

			builder.append(menuItem.displayName)
			builder.append("</a>")
		}

		builder.append("</li>")
	}

	private fun userCanAccess(item: MenuItem): Boolean
	{
		var canAccess = true

		if (item.anonymousOnly != null && item.anonymousOnly!! && Authorize.isAuthenticated)
		{
			return false
		}

		if (item.itemRoles.isNotEmpty())
		{
			if (Authorize.isAuthenticated)
			{
				val roleNames = ArrayList<String>()

				for (r in item.itemRoles)
				{
					roleNames.add(r.roleName)
				}

				canAccess = Authorize.isUserInRole(roleNames.toTypedArray())
			}
			else
			{
				canAccess = false
			}
		}

		return canAccess
	}

	fun getMenuWidthJquery(menu: Menu): String
	{
		val jquery = StringBuilder()

		if(menu.width > 0)
		{
			jquery.append("<script type=\"text/javascript\">")

			jquery.append("$(\".iberis-menu-" + menu.id + "\").css('width', " + menu.width + ");")

			jquery.append("</script>")
		}

		return jquery.toString()
	}

	fun getSubWidthJquery(menu: Menu): String
	{
		val jquery = StringBuilder()

		if(menu.subWidth > 0)
		{
			jquery.append("<script type=\"text/javascript\">")

			jquery.append("$(\".iberis-menu-" + menu.id + " ul.ui-menu-child\").each(function(){$(this).css('white-space' ,'nowrap');$(this).attr('style',$(this).attr('style') + ';width: " + menu.subWidth + "px !important;');});")

			jquery.append("</script>")
		}

		return jquery.toString()
	}

	fun getMenuTypeName(type: Int): String
	{
		for (menuType in MenuType.values())
		{
			if (menuType.typeId == type)
			{
				return menuType.name
			}
		}

		return type.toString()
	}

	fun getMegamenuColumnClassWidth(subMenu: MenuItem): Int
	{
		val classWidth = 12 / subMenu.subMenuColumns.size

		return if (classWidth < 3)
		{
			3
		}
		else classWidth

	}

	fun getDefaultMenuModel(menu: Menu): DefaultMenuModel?
	{
		if (this.menuModels.get(menu.id) == null)
		{
			this.menuModels.put(menu.id, this.getMenuModel(menu))
		}

		return if (this.menuModels.get(menu.id) == null)
		{
			null
		}
		else this.menuModels.get(menu.id)

	}

	fun getDefaultBreadCrumbMenuModel(menu: Menu, siteId: Long): DefaultMenuModel?
	{
		if (this.menuModels.get(menu.id) == null)
		{
			this.menuModels.put(menu.id, this.getBreadCrumbMenuModel(menu, siteId))
		}

		return if (this.menuModels.get(menu.id) == null)
		{
			null
		}
		else this.menuModels.get(menu.id)

	}

	fun getFlatMenu(menu: Menu): String?
	{
		if (this.flatMenuMarkups.get(menu.id) == null)
		{
			this.flatMenuMarkups.put(menu.id, this.getFlatMenuMarkup(menu))
		}

		return if (this.flatMenuMarkups.get(menu.id) == null)
		{
			null
		}
		else this.flatMenuMarkups.get(menu.id)
	}

	fun getSmartMarkup(menu: Menu, assignmentId: Long, zIndex: Int, vertical: Boolean): String?
	{
		if(this.smartMenuMarkups.get("$assignmentId-${menu.id}") == null)
		{
			this.smartMenuMarkups.put("$assignmentId-${menu.id}", this.getSmartMenuMarkup(menu, assignmentId, zIndex, vertical))
		}

		return if (this.smartMenuMarkups.get("$assignmentId-${menu.id}") != null)
		{
			this.smartMenuMarkups.get("$assignmentId-${menu.id}")
		}
		else null
	}
}
