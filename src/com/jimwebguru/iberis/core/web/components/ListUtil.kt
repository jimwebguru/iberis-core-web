package com.jimwebguru.iberis.core.web.components

import com.jimwebguru.iberis.core.persistence.UnitPropertyListItem

import javax.faces.view.ViewScoped
import javax.inject.Named
import java.io.Serializable
import java.util.ArrayList

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
@Named
@ViewScoped
class ListUtil : Serializable
{
	fun getUnitPropertyList(listString: String): ArrayList<UnitPropertyListItem>
	{
		val unitPropertyListItems = ArrayList<UnitPropertyListItem>()

		val listItems = listString.split("\\|".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()

		for (listItem in listItems)
		{
			val items = listItem.split(",".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()

			val unitPropertyListItem = UnitPropertyListItem()
			unitPropertyListItem.itemValue = items[0]
			unitPropertyListItem.displayValue = items[1]

			unitPropertyListItems.add(unitPropertyListItem)
		}

		return unitPropertyListItems
	}

	fun getDisplayValue(unitPropertyListItems: Collection<UnitPropertyListItem>, value: String): String
	{
		for (unitPropertyListItem in unitPropertyListItems)
		{
			if (unitPropertyListItem.itemValue == value)
			{
				return unitPropertyListItem.displayValue
			}
		}

		return ""
	}
}
