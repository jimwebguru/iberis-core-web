/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.jimwebguru.iberis.core.web.components

import java.io.Serializable

import javax.faces.view.ViewScoped
import javax.inject.Named
import javax.faces.component.UIComponent
import javax.faces.component.html.*

import com.jimwebguru.iberis.core.persistence.*
import com.jimwebguru.iberis.core.utils.db.DaoUtil
import com.jimwebguru.iberis.core.web.utils.GeneralUtil
import org.primefaces.component.calendar.Calendar
import org.primefaces.component.inputmask.InputMask
import org.primefaces.component.inputtext.InputText
import org.primefaces.component.inputtextarea.InputTextarea
import org.primefaces.component.selectonemenu.SelectOneMenu

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
@Named
@ViewScoped
class ComponentsUtil : Serializable
{
	private val daoUtil = DaoUtil()

	private val daoPackage = "com.jimwebguru.iberis.core.persistence."

	fun fakeLookupDisplayValue(unitId: Long, instanceId: Long): String
	{
		try
		{
			val fakeLookupUnit = this.daoUtil.find(SystemUnit::class.java, unitId)

			val displayProperty = this.daoUtil.find(UnitProperty::class.java, fakeLookupUnit!!.lookupDisplayPropertyId)

			val persistenceClass = Class.forName(this.daoPackage + fakeLookupUnit.persistenceClass)

			val objectInstance = this.daoUtil.find(persistenceClass, instanceId)

			if (objectInstance != null)
			{
				val field = persistenceClass.getDeclaredField(displayProperty!!.propertyName)
				field.isAccessible = true

				var fieldValue: Any? = field.get(objectInstance)

				if (displayProperty.propertyType.typeId == 3L)
				{
					if (fieldValue != null)
					{
						val lookupUnit = this.daoUtil.find(SystemUnit::class.java, displayProperty.lookupUnitId)

						val lookupDisplayProperty = this.daoUtil.find(UnitProperty::class.java, lookupUnit!!.lookupDisplayPropertyId)

						val lookupClass = Class.forName(this.daoPackage + lookupUnit.persistenceClass)

						val lookupDisplayField = lookupClass.getDeclaredField(lookupDisplayProperty!!.propertyName)

						lookupDisplayField.isAccessible = true

						fieldValue = lookupDisplayField.get(fieldValue)
					}
				}

				if (fieldValue != null)
				{
					return fieldValue.toString()
				}
			}
		}
		catch (ex: Exception)
		{
			GeneralUtil.logError(ex)
		}

		return ""
	}

	fun flexibleLookupDisplayValue(property: UnitProperty, dataBeanName: String): String
	{
		var displayName = ""

		val unitIdObj = GeneralUtil.getValueOfExpression("#{" + dataBeanName + "." + property.propertyName + "}", Long::class.java)

		if (unitIdObj != null && unitIdObj.toString().isNotEmpty() && unitIdObj.toString().toLong() > 0)
		{
			val lookupUnitId = unitIdObj as Long

			val instanceId = GeneralUtil.getValueOfExpression("#{" + dataBeanName + "." + property.propertyName + "InstanceId}", Any::class.java)

			if (instanceId != null && instanceId.toString().isNotEmpty() && instanceId.toString().toLong() > 0)
			{
				val lookupUnit = this.daoUtil.find(SystemUnit::class.java, lookupUnitId)

				val displayProperty = this.daoUtil.find(UnitProperty::class.java, lookupUnit!!.lookupDisplayPropertyId)

				try
				{
					val objectInstance = this.daoUtil.find(Class.forName(this.daoPackage + lookupUnit.persistenceClass), instanceId.toString().toLong())

					if (objectInstance != null)
					{
						try
						{
							val c = Class.forName(this.daoPackage + lookupUnit.persistenceClass)

							val field = c.getDeclaredField(displayProperty!!.propertyName)

							field.isAccessible = true

							val displayObject = field.get(objectInstance)

							if (displayObject != null)
							{
								displayName = displayObject.toString()
							}
						}
						catch (ex: ClassNotFoundException)
						{
							GeneralUtil.logError(ex)
						}
						catch (nsex: NoSuchFieldException)
						{
							GeneralUtil.logError(nsex)
						}
						catch (iex: IllegalAccessException)
						{
							GeneralUtil.logError(iex)
						}

					}
				}
				catch (ex: Exception)
				{
					GeneralUtil.logError(ex)
				}

			}
		}

		return displayName
	}

	fun getLookupProperties(property: UnitProperty): Array<Any?>
	{
		val lookupProperties = arrayOfNulls<Any>(4)

		if (property.propertyType.typeId.toInt() == PropertyType.LOOKUP.propertyTypeId)
		{
			val lookupUnit = this.daoUtil.find(SystemUnit::class.java, property.lookupUnitId)

			val displayProperty = this.daoUtil.find(UnitProperty::class.java, lookupUnit!!.lookupDisplayPropertyId)

			val primaryKeyProperty = this.daoUtil.find(UnitProperty::class.java, lookupUnit.lookupPropertyPrimaryKey)

			var primaryKeyProperty2: UnitProperty? = null

			if (lookupUnit.lookupPropertyPrimaryKey2 != null && lookupUnit.lookupPropertyPrimaryKey2 > 0)
			{
				primaryKeyProperty2 = this.daoUtil.find(UnitProperty::class.java, lookupUnit.lookupPropertyPrimaryKey2)
			}

			lookupProperties[0] = lookupUnit
			lookupProperties[1] = displayProperty
			lookupProperties[2] = primaryKeyProperty
			lookupProperties[3] = primaryKeyProperty2
		}

		return lookupProperties
	}

	fun getLookupProperties(lookupUnitId: Long, displayPropertyId: Long, primaryKeyPropertyId: Long, primaryKeyProperty2Id: Long?): Array<Any?>
	{
		val lookupProperties = arrayOfNulls<Any>(4)

		val lookupUnit = this.daoUtil.find(SystemUnit::class.java, lookupUnitId)
		val displayProperty = this.daoUtil.find(UnitProperty::class.java, displayPropertyId)
		val primaryKeyProperty = this.daoUtil.find(UnitProperty::class.java, primaryKeyPropertyId)

		var primaryKeyProperty2: UnitProperty? = null

		if (primaryKeyProperty2Id != null)
		{
			primaryKeyProperty2 = this.daoUtil.find(UnitProperty::class.java, primaryKeyProperty2Id)
		}

		lookupProperties[0] = lookupUnit
		lookupProperties[1] = displayProperty
		lookupProperties[2] = primaryKeyProperty
		lookupProperties[3] = primaryKeyProperty2

		return lookupProperties
	}

	fun getLookupDisplayValue(property: UnitProperty, displayProperty: UnitProperty, dataBeanName: String): String
	{
		var displayName = ""

		if (property.isFakeLookup!!)
		{
			val lookupIdValue = GeneralUtil.getValueOfExpression("#{" + dataBeanName + "." + property.propertyName + "}", Any::class.java)

			if (lookupIdValue != null && lookupIdValue.toString().isNotEmpty() && lookupIdValue.toString().toLong() > 0)
			{
				displayName = this.fakeLookupDisplayValue(property.lookupUnitId, lookupIdValue.toString().toLong())
			}
		}
		else
		{
			val displayTextValue: Any?

			if (displayProperty.propertyType.typeId == 3L)
			{
				val lookupUnit = this.daoUtil.find(SystemUnit::class.java, displayProperty.lookupUnitId)

				val lookupDisplayProperty = this.daoUtil.find(UnitProperty::class.java, lookupUnit!!.lookupDisplayPropertyId)

				displayTextValue = GeneralUtil.getValueOfExpression("#{" + dataBeanName + "." + property.propertyName + "." + displayProperty.propertyName + "." + lookupDisplayProperty!!.propertyName + "}", Any::class.java)
			}
			else
			{
				displayTextValue = GeneralUtil.getValueOfExpression("#{" + dataBeanName + "." + property.propertyName + "." + displayProperty.propertyName + "}", Any::class.java)
			}

			if (displayTextValue != null)
			{
				displayName = displayTextValue.toString()
			}
		}

		return displayName
	}

	fun getFlexibleLookupUnitDefaultFormPage(property: UnitProperty, dataBeanName: String): String
	{
		val unitIdObj = GeneralUtil.getValueOfExpression("#{" + dataBeanName + "." + property.propertyName + "}", Long::class.java)

		if (unitIdObj != null && unitIdObj.toString().isNotEmpty() && unitIdObj.toString().toLong() > 0)
		{
			val lookupUnitId = unitIdObj as Long

			val lookupUnit = this.daoUtil.find(SystemUnit::class.java, lookupUnitId)

			return lookupUnit!!.defaultFormPage
		}

		return ""
	}

	fun getFieldProperty(propertyId: Long): UnitProperty?
	{
		return daoUtil.find(UnitProperty::class.java, propertyId)
	}

	fun formatUnitId(format: String, unitId: Long?): String
	{
		return String.format(format, unitId)
	}

	companion object
	{

		fun getComponentValue(comp: UIComponent): String?
		{
			var value: String? = null

			if (comp is InputText)
			{
				value = comp.value.toString()
			}

			if (comp is SelectOneMenu)
			{
				value = comp.value.toString()
			}

			if (comp is Calendar)
			{
				value = comp.value.toString()
			}

			if (comp is HtmlInputHidden)
			{
				value = comp.value.toString()
			}

			if (comp is InputMask)
			{
				value = comp.value.toString()
			}

			if (comp is InputTextarea)
			{
				value = (comp as InputText).value.toString()
			}

			return value
		}
	}
}
