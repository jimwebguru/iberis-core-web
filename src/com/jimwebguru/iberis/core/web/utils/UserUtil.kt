package com.jimwebguru.iberis.core.web.utils

import com.jimwebguru.iberis.core.persistence.User
import com.jimwebguru.iberis.core.web.controllers.admin.security.Authorize
import com.jimwebguru.iberis.core.web.utils.GeneralUtil.response
import java.io.Serializable
import javax.annotation.PostConstruct
import javax.enterprise.context.SessionScoped
import javax.inject.Named
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler
import org.springframework.security.core.context.SecurityContextHolder
import javax.faces.context.FacesContext


/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
@Named
@SessionScoped
class UserUtil : Serializable
{
	var user: User? = null

	@PostConstruct
	fun postInit()
	{
		this.user = Authorize.getUser()
	}

	fun userLogout()
	{
		val auth = SecurityContextHolder.getContext().authentication

		if (auth != null)
		{
			SecurityContextLogoutHandler().logout(GeneralUtil.request, response, auth)
		}

		FacesContext.getCurrentInstance().externalContext.redirect(FacesContext.getCurrentInstance().externalContext.requestContextPath)
	}
}