package com.jimwebguru.iberis.core.web.utils

import com.jimwebguru.iberis.core.web.SiteSettings
import org.jsoup.Jsoup
import org.jsoup.safety.Whitelist
import org.primefaces.PrimeFaces

import javax.el.ELContext
import javax.el.ExpressionFactory
import javax.el.MethodExpression
import javax.el.ValueExpression
import javax.faces.application.FacesMessage
import javax.faces.component.UIComponent
import javax.faces.context.FacesContext
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse
import java.util.logging.Level
import java.util.logging.Logger
import java.util.regex.Matcher
import java.util.regex.Pattern
import javax.naming.Context
import javax.naming.InitialContext

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
object GeneralUtil
{
	val request: HttpServletRequest?
		get()
		{
			val context = FacesContext.getCurrentInstance()

			return if (context != null)
			{
				context.externalContext.request as HttpServletRequest
			}
			else null

		}

	val response: HttpServletResponse?
		get()
		{
			val context = FacesContext.getCurrentInstance()

			return if (context != null)
			{
				context.externalContext.response as HttpServletResponse
			}
			else null

		}

	val elContext: ELContext
		get()
		{
			val context = FacesContext.getCurrentInstance()
			return context.elContext
		}

	val serverUrl: String
		get()
		{

			val request = GeneralUtil.request

			return request!!.scheme + "://" + request.serverName + ":" + (if (request.serverPort != 80) request.serverPort else "") + request.contextPath
		}

	fun getComponentById(id: String): UIComponent?
	{
		return FacesContext.getCurrentInstance().viewRoot.findComponent(id)
	}

	fun getManagedBean(beanName: String): Any
	{
		val context = FacesContext.getCurrentInstance()

		return context.application.elResolver.getValue(context.elContext, null, beanName)
	}

	fun getMethodExpression(ex: String, arguments: Array<Class<*>>): MethodExpression?
	{
		val context = FacesContext.getCurrentInstance()
		val factory = context.application.expressionFactory

		return factory.createMethodExpression(context.elContext, ex, null, arguments)
	}

	fun getExpression(ex: String, c: Class<*>): ValueExpression?
	{
		val context = FacesContext.getCurrentInstance()
		val factory = context.application.expressionFactory

		return factory.createValueExpression(context.elContext, ex, c)
	}

	fun getValueOfExpression(ex: String, c: Class<*>): Any?
	{
		val context = FacesContext.getCurrentInstance()
		val factory = context.application.expressionFactory
		val expression = factory.createValueExpression(context.elContext, ex, c)

		return expression.getValue(context.elContext)
	}

	fun setValueOfExpression(ex: String, c: Class<*>, value: Any)
	{
		val context = FacesContext.getCurrentInstance()
		val factory = context.application.expressionFactory
		val expression = factory.createValueExpression(context.elContext, ex, c)

		expression.setValue(context.elContext, value)
	}

	fun getRequestParameterValue(key: String): Any?
	{
		val context = FacesContext.getCurrentInstance()
		val map = context.externalContext.requestParameterMap

		return map[key]
	}

	fun getQueryStringValue(indexName: String): String?
	{
		val request = GeneralUtil.request

		return request!!.getParameter(indexName)
	}

	fun setRequestAttributes(request: HttpServletRequest)
	{
		val pMap = request.parameterMap

		for (key in pMap.keys)
		{
			if (request.getAttribute(key.toString()) == null)
			{
				request.setAttribute(key.toString(), pMap[key])
			}
		}
	}

	fun getWebXmlContextParam(paramName: String): String?
	{
		val ctx = FacesContext.getCurrentInstance()
		return ctx.externalContext.getInitParameter(paramName)
	}

	fun getWebXmlEnvParam(paramName: String): Any?
	{
		val ctx = InitialContext().lookup("java:comp/env") as Context

		return ctx.lookup(paramName)
	}

	fun logError(ex: Exception)
	{
		Logger.getLogger(GeneralUtil::class.java.name).log(Level.SEVERE, "exception caught", ex)
	}

	fun addMessage(message: String, severity: FacesMessage.Severity)
	{
		FacesContext.getCurrentInstance().addMessage(null, FacesMessage(severity, message, message))
	}

	fun addFlashMessage(message: String, severity: FacesMessage.Severity)
	{
		FacesContext.getCurrentInstance().addMessage(null, FacesMessage(severity, message, message))
		FacesContext.getCurrentInstance().externalContext.flash.isKeepMessages = true
	}

	fun preserveMessages()
	{
		val context = FacesContext.getCurrentInstance()
		context.externalContext.flash.isKeepMessages = true
	}

	fun cleanHTML(html: String?): String
	{
		if (html == null)
		{
			return ""
		}

		val siteSettings = GeneralUtil.getManagedBean("siteSettings") as SiteSettings

		return Jsoup.clean(html, siteSettings.siteUrl!!, Whitelist.relaxed().addAttributes("img", "style"))
	}

	fun cleanHTML(html: String?, allowHtmlInlineStyles: Boolean): String
	{
		if (html == null)
		{
			return ""
		}

		val siteSettings = GeneralUtil.getManagedBean("siteSettings") as SiteSettings

		return if (allowHtmlInlineStyles)
		{
			Jsoup.clean(html, siteSettings.siteUrl, Whitelist.relaxed().preserveRelativeLinks(true).addAttributes(":all", "style","class", "id"))
		}
		else
		{
			Jsoup.clean(html, siteSettings.siteUrl, Whitelist.relaxed().preserveRelativeLinks(true).addAttributes("img", "style"))
		}

	}

	fun removeHTMLTags(html: String?) : String
	{
		if(html != null)
		{
			return Jsoup.parse(html).text()
		}

		return ""
	}

	fun makeUrlSlug(original: String): String
	{
		var original = original

		val slug: String

		original = original.toLowerCase()

		val pattern = Pattern.compile("[^a-zA-Z0-9-]")

		val slugMatcher = pattern.matcher(original)

		slug = slugMatcher.replaceAll("-")

		return slug
	}

	fun addCallbackParam(param: String, value: Any)
	{
		PrimeFaces.current().ajax().addCallbackParam(param, value)
	}
}
