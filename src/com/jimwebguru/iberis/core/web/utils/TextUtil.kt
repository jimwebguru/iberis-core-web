package com.jimwebguru.iberis.core.web.utils

import com.google.gson.JsonParser
import org.apache.commons.text.WordUtils
import java.net.URL

import javax.faces.view.ViewScoped
import javax.inject.Named
import java.io.Serializable
import java.text.Normalizer
import java.util.ArrayList
import java.util.regex.Pattern
import java.io.InputStreamReader
import java.io.BufferedReader
import java.net.HttpURLConnection
import java.nio.charset.Charset


/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
@Named
@ViewScoped
class TextUtil : Serializable
{
	fun transformToTitleCase(value: String): String?
	{
		var transformedValue = value.replace(String.format("%s|%s|%s", "(?<=[A-Z])(?=[A-Z][a-z])", "(?<=[^A-Z])(?=[A-Z])", "(?<=[A-Za-z])(?=[^A-Za-z])").toRegex(), " ")

		transformedValue = WordUtils.capitalizeFully(transformedValue)

		return transformedValue
	}

	fun getWrapperClass(title: String): String
	{
		return Normalizer.normalize(title.toLowerCase(), Normalizer.Form.NFD)
				.replace("\\p{InCombiningDiacriticalMarks}+".toRegex(), "")
				.replace("[^\\p{Alnum}]+".toRegex(), "-")
	}

	fun getCleanHtml(value: String): String
	{
		return GeneralUtil.cleanHTML(value)
	}

	fun getCleanHtml(value: String, allowHtmlInlineStyles: Boolean): String
	{
		return GeneralUtil.cleanHTML(value, allowHtmlInlineStyles)
	}

	fun getCleanHtmlJsSafe(value: String, allowHtmlInlineStyles: Boolean): String
	{
		var html = GeneralUtil.cleanHTML(value, allowHtmlInlineStyles)

		return html.replace("\"","'").replace("\r","").replace("\n", "")
	}

	fun getStrippedHtml(value: String, maxChar: kotlin.Int): String
	{
		if(maxChar > 0)
		{
			return GeneralUtil.removeHTMLTags(value).take(maxChar)
		}

		return GeneralUtil.removeHTMLTags(value)
	}

	fun convertHexToRgb(hexValue: String): String
	{
		val rgb: String = Integer.valueOf(hexValue.substring(0, 2), 16).toString() + "," + Integer.valueOf(hexValue.substring(2, 4), 16) + "," + Integer.valueOf(hexValue.substring(4, 6), 16)

		return rgb
	}
	
	fun convertVideoUrlToCode(url: String): String
	{
		var videoUrl = url.toLowerCase()

		if((videoUrl.contains("youtube") && !videoUrl.contains("/embed/")) || videoUrl.contains("youtu.be"))
		{
			val patternList = ArrayList<Pattern>()
			patternList.add(Pattern.compile("(?:https?://)?(?:www\\.)?(?:youtube\\.com/watch\\?v=|youtu\\.be/)([^& \\n<]+)(?:[^ \\n<]+)?"))
			patternList.add(Pattern.compile("^https?://.*(?:youtu.be/|v/|u/\\\\w/|embed/|watch?v=)([^#&?]*).*\$"))

			for(pattern in patternList)
			{
				val slugMatcher = pattern.matcher(videoUrl)

				if(slugMatcher.matches())
				{
					videoUrl = "https://www.youtube.com/embed/${slugMatcher.group(1)}"

					if(videoUrl.indexOf('?') > -1)
					{
						videoUrl = videoUrl.substring(0, videoUrl.indexOf('?'))
					}
				}
			}
		}

		if (videoUrl.contains("youtube") && !videoUrl.contains("?enablejsapi=1"))
		{
			videoUrl = videoUrl + "?enablejsapi=1"
		}

		if(videoUrl.contains("vimeo") && !videoUrl.contains("player.vimeo.com/video/"))
		{
			try
			{
				val netUrl = URL("https://vimeo.com/api/oembed.json?url=$videoUrl")

				val sb = StringBuilder()

				val urlConn = netUrl.openConnection() as HttpURLConnection
				urlConn.readTimeout = 60 * 1000

				if (urlConn.inputStream != null)
				{
					val `in` = InputStreamReader(urlConn.inputStream, Charset.defaultCharset())

					val bufferedReader = BufferedReader(`in`)

					var cp: Int
					cp = bufferedReader.read()

					while (cp != -1)
					{
						sb.append(cp.toChar())

						cp = bufferedReader.read()
					}

					bufferedReader.close()

					`in`.close()
				}

				if(sb.isNotEmpty())
				{
					val videoId = JsonParser().parse(sb.toString()).asJsonObject.get("video_id")

					videoUrl = "https://player.vimeo.com/video/$videoId"
				}
			}
			catch(exception: Exception)
			{

			}
		}

		return "<div class=\"embed-video-container\"><iframe class=\"video-iframe\" src=\"$videoUrl\" frameborder=\"0\" allowfullscreen=\"true\"></iframe></div>"
	}
}
