package com.jimwebguru.iberis.core.web.utils

import com.jimwebguru.iberis.core.persistence.*
import com.jimwebguru.iberis.core.utils.FileUtil
import com.jimwebguru.iberis.core.utils.PersistenceUtil
import com.jimwebguru.iberis.core.utils.db.DaoUtil
import com.jimwebguru.iberis.core.web.controllers.admin.security.Authorize
import org.primefaces.model.UploadedFile

import javax.faces.application.FacesMessage
import javax.imageio.ImageIO
import java.io.ByteArrayInputStream
import java.io.File
import java.io.InputStream
import java.nio.charset.Charset
import java.util.*

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
object UploadUtil
{
	fun uploadImage(daoUtil: DaoUtil, uploadedFile: UploadedFile)
	{
		try
		{
			val persistenceUtil = PersistenceUtil()

			val fileStorageSetting = daoUtil.find(SystemSetting::class.java, 1L)

			val directoryPath = fileStorageSetting!!.value + "/images/"

			val fileWriteName = uploadedFile.fileName.replace(" ".toRegex(), "").toLowerCase()

			FileUtil.createFileAndDirectory(directoryPath, directoryPath + fileWriteName, uploadedFile.inputstream)

			val image = Image()
			image.name = fileWriteName
			image.filePath = "/image/" + fileWriteName

			val bufferedImage = ImageIO.read(File(directoryPath + fileWriteName))
			image.width = java.lang.Long.valueOf(bufferedImage.width.toLong())
			image.height = java.lang.Long.valueOf(bufferedImage.height.toLong())
			image.mimeType = persistenceUtil.getMimeType(uploadedFile.contentType)

			val lastIndexOf = fileWriteName.lastIndexOf(".")
			image.fileExtension = persistenceUtil.getFileExtension(fileWriteName.substring(lastIndexOf))

			val user = Authorize.getUser()

			image.sites = ArrayList<Site>()

			for (membership in user!!.siteMemberships)
			{
				image.sites.add(membership.site)
			}

			daoUtil.create(image)

			GeneralUtil.addMessage("Images have been uploaded.", FacesMessage.SEVERITY_INFO)
		}
		catch (ex: Exception)
		{
			GeneralUtil.logError(ex)
		}

	}

	fun uploadImage(daoUtil: DaoUtil, fileName: String, imageData: String?)
	{
		try
		{
			val persistenceUtil = PersistenceUtil()

			val fileStorageSetting = daoUtil.find(SystemSetting::class.java, 1L)

			val directoryPath = fileStorageSetting!!.value + "/images/"

			val fileWriteName = fileName.replace(" ".toRegex(), "").toLowerCase()

			//Base64.getDecoder().decode(imageData)

			val partSeparator = ','
			var encodedImg = imageData

			if (imageData!!.contains(partSeparator, false))
			{
				encodedImg = imageData.split(partSeparator)[1]
			}

			val iStream = ByteArrayInputStream(Base64.getDecoder().decode(encodedImg)) as InputStream

			FileUtil.createFileAndDirectory(directoryPath, directoryPath + fileWriteName, iStream)

			val image = Image()
			image.name = fileWriteName
			image.filePath = "/image/" + fileWriteName

			val bufferedImage = ImageIO.read(File(directoryPath + fileWriteName))
			image.width = java.lang.Long.valueOf(bufferedImage.width.toLong())
			image.height = java.lang.Long.valueOf(bufferedImage.height.toLong())
			image.mimeType = persistenceUtil.getMimeType("image/png")

			val lastIndexOf = fileWriteName.lastIndexOf(".")
			image.fileExtension = persistenceUtil.getFileExtension(fileWriteName.substring(lastIndexOf))

			val user = Authorize.getUser()

			image.sites = ArrayList<Site>()

			for (membership in user!!.siteMemberships)
			{
				image.sites.add(membership.site)
			}

			daoUtil.create(image)

			GeneralUtil.addMessage("Image has been uploaded.", FacesMessage.SEVERITY_INFO)
		}
		catch (ex: Exception)
		{
			GeneralUtil.logError(ex)
		}

	}

	fun uploadReplacementImage(daoUtil: DaoUtil, uploadedFile: UploadedFile, image: Image)
	{
		try
		{
			val persistenceUtil = PersistenceUtil()

			val fileStorageSetting = daoUtil.find(SystemSetting::class.java, 1L)

			val directoryPath = fileStorageSetting!!.value + "/images/"

			val fileWriteName = uploadedFile.fileName.replace(" ".toRegex(), "").toLowerCase()

			FileUtil.createFileAndDirectory(directoryPath, directoryPath + fileWriteName, uploadedFile.inputstream)

			image.name = fileWriteName
			image.filePath = "/image/" + fileWriteName

			val bufferedImage = ImageIO.read(File(directoryPath + fileWriteName))
			image.width = java.lang.Long.valueOf(bufferedImage.width.toLong())
			image.height = java.lang.Long.valueOf(bufferedImage.height.toLong())
			image.mimeType = persistenceUtil.getMimeType(uploadedFile.contentType)

			val lastIndexOf = fileWriteName.lastIndexOf(".")
			image.fileExtension = persistenceUtil.getFileExtension(fileWriteName.substring(lastIndexOf))

			daoUtil.update(image)

			GeneralUtil.addMessage("Replace image has been uploaded.", FacesMessage.SEVERITY_INFO)
		}
		catch (ex: Exception)
		{
			GeneralUtil.logError(ex)
		}

	}

	fun uploadCroppedImage(daoUtil: DaoUtil, fileName: String, original: Image, sliceInfo: IntArray)
	{
		try
		{
			val randomNumber = Random().nextInt(99999)

			val fileWriteName = fileName.replace("[^\\w\\s]".toRegex(), "").replace(" ".toRegex(), "").toLowerCase() + randomNumber

			val fileStorageSetting = daoUtil.find(SystemSetting::class.java, 1L)

			val directoryPath = fileStorageSetting!!.value + "/images/"

			val originalImage = ImageIO.read(File(fileStorageSetting.value + original.filePath.replace("/image","/images")))

			val croppedImage = originalImage.getSubimage(sliceInfo[0], sliceInfo[1], sliceInfo[2], sliceInfo[3])

			val croppedFile = File(directoryPath + fileWriteName + original.fileExtension.name)

			ImageIO.write(croppedImage, original.fileExtension.name.replace(".",""), croppedFile)

			val image = Image()
			image.name = fileName
			image.filePath = "/image/" + fileWriteName + original.fileExtension.name

			val bufferedImage = ImageIO.read(File(directoryPath + fileWriteName + original.fileExtension.name))
			image.width = java.lang.Long.valueOf(bufferedImage.width.toLong())
			image.height = java.lang.Long.valueOf(bufferedImage.height.toLong())
			image.mimeType = original.mimeType

			image.fileExtension = original.fileExtension

			val user = Authorize.getUser()

			image.sites = ArrayList<Site>()

			for (membership in user!!.siteMemberships)
			{
				image.sites.add(membership.site)
			}

			daoUtil.create(image)

			GeneralUtil.addMessage("Image has been cropped and new image created.", FacesMessage.SEVERITY_INFO)
		}
		catch (ex: Exception)
		{
			GeneralUtil.logError(ex)
		}
	}

	fun uploadFile(daoUtil: DaoUtil, uploadedFile: UploadedFile)
	{
		try
		{
			val persistenceUtil = PersistenceUtil()

			val fileStorageSetting = daoUtil.find(SystemSetting::class.java, java.lang.Long.valueOf(1))

			val directoryPath = fileStorageSetting!!.value + "/files/general/"

			val fileWriteName = uploadedFile.fileName.replace(" ".toRegex(), "").toLowerCase()

			FileUtil.createFileAndDirectory(directoryPath, directoryPath + fileWriteName, uploadedFile.inputstream)

			val file = File()
			file.fileName = fileWriteName
			file.displayName = fileWriteName

			file.filePath = "/file/general/" + fileWriteName
			file.mimeType = persistenceUtil.getMimeType(uploadedFile.contentType)

			val lastIndexOf = fileWriteName.lastIndexOf(".")
			file.fileExtension = persistenceUtil.getFileExtension(fileWriteName.substring(lastIndexOf))

			val user = Authorize.getUser()

			file.sites = ArrayList<Site>()

			for (membership in user!!.siteMemberships)
			{
				file.sites.add(membership.site)
			}

			daoUtil.create(file)

			GeneralUtil.addMessage("Files have been uploaded.", FacesMessage.SEVERITY_INFO)
		}
		catch (ex: Exception)
		{
			GeneralUtil.logError(ex)
		}

	}

	fun uploadVideo(daoUtil: DaoUtil, uploadedFile: UploadedFile)
	{
		try
		{
			val persistenceUtil = PersistenceUtil()

			val fileStorageSetting = daoUtil.find(SystemSetting::class.java, 1L)

			val directoryPath = fileStorageSetting!!.value + "/videos/"

			val fileWriteName = uploadedFile.fileName.replace(" ".toRegex(), "").toLowerCase()

			FileUtil.createFileAndDirectory(directoryPath, directoryPath + fileWriteName, uploadedFile.inputstream)

			val vid = Video()
			vid.name = fileWriteName
			vid.filePath = "/video/" + fileWriteName

			vid.mimeType = persistenceUtil.getMimeType(uploadedFile.contentType)

			val user = Authorize.getUser()

			vid.sites = ArrayList<Site>()

			for (membership in user!!.siteMemberships)
			{
				vid.sites.add(membership.site)
			}

			daoUtil.create(vid)

			GeneralUtil.addMessage("Videos have been uploaded.", FacesMessage.SEVERITY_INFO)
		}
		catch (ex: Exception)
		{
			GeneralUtil.logError(ex)
		}

	}
}
