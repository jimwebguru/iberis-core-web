package com.jimwebguru.iberis.core.web.utils

import com.jimwebguru.iberis.core.persistence.Site
import com.jimwebguru.iberis.core.utils.db.DaoUtil
import javax.activation.DataHandler
import javax.activation.DataSource
import javax.activation.FileDataSource
import javax.enterprise.context.SessionScoped
import javax.faces.context.FacesContext
import javax.inject.Named
import javax.mail.*
import javax.mail.internet.*
import java.io.Serializable
import java.util.Properties
import javax.annotation.PostConstruct
import com.sun.mail.smtp.SMTPTransport
import javax.faces.view.ViewScoped


/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
@Named
@ViewScoped
class MailUtil : Serializable
{
	private val daoUtil = DaoUtil()

	var site: Site? = null

	@PostConstruct
	fun postInit()
	{
		val cmsSiteId = GeneralUtil.getWebXmlEnvParam("cmsSiteId")

		if (cmsSiteId != null)
		{
			this.site = this.daoUtil.find(Site::class.java, cmsSiteId as Long)
		}
	}

	/**
	 * Read mail (SMTP) server from web.xml. Fallback to hard-coded value if not found.
	 */
	private val mailHost: String
		get()
		{
			val host = this.site!!.mailHost

			return host ?: "127.0.0.1"
		}

	/**
	 * Read mail (SMTP) port from web.xml. Fallback to hard-coded value if not found.
	 */
	private val mailPort: kotlin.Int
		get()
		{
			val port = this.site!!.mailPort

			return port ?: 25
		}


	private val mailProperties: Properties
		get()
		{
			val mailSmtpConnectionTimeout ="mail.smtp.connectiontimeout"
			val mailSmtpTimeout = "mail.smtp.timeout"
			val mailSmtpWriteTimeout = "mail.smtp.writetimeout"
			val mailSocketTimeout = "60000"

			val properties = System.getProperties()

			if(this.site!!.mailUseSSL)
			{
				properties.setProperty("mail.smtps.host", this.mailHost)
			}
			else
			{
				properties.setProperty("mail.smtp.host", this.mailHost)
			}

			// Set a fixed timeout of 60s for all operations -
			// the default timeout is "infinite"
			properties.put(mailSmtpConnectionTimeout, mailSocketTimeout)
			properties.put(mailSmtpTimeout, mailSocketTimeout)
			properties.put(mailSmtpWriteTimeout, mailSocketTimeout)

			if (this.site!!.mailUser != null && this.site!!.mailUser.trim() != ""
					&& this.site!!.mailUserPassword != null && this.site!!.mailUserPassword.trim() != "")
			{
				if(this.site!!.mailUseSSL)
				{
					properties.setProperty("mail.smtps.auth", "true")
				}
				else
				{
					properties.setProperty("mail.smtp.auth", "true")
				}
				//properties.setProperty("mail.user", this.site!!.mailUser)
				//properties.setProperty("mail.password", this.site!!.mailUserPassword)
			}

			/*if (this.site!!.mailUseSSL)
			{
				properties.setProperty("mail.transport.protocol", "smtps")
				properties.setProperty("mail.smtps.starttls.enable", "true")
				properties.setProperty("mail.smtps.port", this.site!!.mailPort.toString())
			}
			else
			{
				properties.setProperty("mail.smtp.port", this.site!!.mailPort.toString())
			}*/

			if(this.site!!.mailPort > 0)
			{
				if(this.site!!.mailUseSSL)
				{
					properties.setProperty("mail.smtps.port", this.site!!.mailPort.toString())
				}
				else
				{
					properties.setProperty("mail.smtp.port", this.site!!.mailPort.toString())
				}
			}

			// Only cache DNS lookups for 10 seconds
			//java.security.Security.setProperty("networkaddress.cache.ttl","10")

			//System.setProperty("file.encoding", Charsets.UTF_8.name())
			//System.setProperty("mail.mime.charset", Charsets.UTF_8.name())

			return properties
		}

	fun sendEmail(toAddresses: Array<Address>, ccAddresses: Array<Address>?, bccAddresses: Array<Address>?, subject: String, content: String, isHtml: Boolean): Boolean
	{
		val properties = this.mailProperties

		// Get the default Session object.
		val session = Session.getInstance(properties, null)
		//session.debug = true
		try
		{
			// Create a default MimeMessage object.
			val message = MimeMessage(session)

			// Set From: header field of the header.
			message.setFrom(InternetAddress(this.site!!.mailFrom))

			// Set To: header field of the header.
			message.addRecipients(Message.RecipientType.TO, toAddresses)

			if (ccAddresses != null && ccAddresses.isNotEmpty())
			{
				message.addRecipients(Message.RecipientType.CC, ccAddresses)
			}

			if (bccAddresses != null && bccAddresses.isNotEmpty())
			{
				message.addRecipients(Message.RecipientType.BCC, bccAddresses)
			}

			// Set Subject: header field
			message.subject = subject

			// Send the actual HTML message, as big as you like
			if (isHtml)
			{
				message.setContent(content, "text/html")
			}
			else
			{
				message.setText(content)
			}

			var t : SMTPTransport? = null

			if(this.site!!.mailUseSSL)
			{
				t = session.getTransport("smtps") as SMTPTransport
			}
			else
			{
				t = session.getTransport("smtp") as SMTPTransport
			}

			if(this.site!!.mailUser != null && this.site!!.mailUser.trim() != ""
					&& this.site!!.mailUserPassword != null && this.site!!.mailUserPassword.trim() != "")
			{
				t.connect(this.site!!.mailHost, this.site!!.mailUser, this.site!!.mailUserPassword)
			}
			else
			{
				t.connect()
			}

			t.sendMessage(message, message.allRecipients)
			t.close()

			return true
		}
		catch (mex: MessagingException)
		{
			GeneralUtil.logError(mex)
		}

		return false
	}

	fun sendEmailWithAttachment(toAddresses: Array<Address>, ccAddresses: Array<Address>?, bccAddresses: Array<Address>?, subject: String, content: String, isHtml: Boolean, fileSource: FileDataSource): Boolean
	{
		// Get system properties
		val properties = this.mailProperties

		// Get the default Session object.
		val session = Session.getInstance(properties, null)

		try
		{
			// Create a default MimeMessage object.
			val message = MimeMessage(session)

			// Set From: header field of the header.
			message.setFrom(InternetAddress(this.site!!.mailFrom))

			// Set To: header field of the header.
			message.addRecipients(Message.RecipientType.TO, toAddresses)

			if (ccAddresses != null && ccAddresses.isNotEmpty())
			{
				message.addRecipients(Message.RecipientType.CC, ccAddresses)
			}

			if (bccAddresses != null && bccAddresses.isNotEmpty())
			{
				message.addRecipients(Message.RecipientType.BCC, bccAddresses)
			}

			// Set Subject: header field
			message.subject = subject

			// Create the message part
			var messageBodyPart: BodyPart = MimeBodyPart()

			// Send the actual HTML message, as big as you like
			if (isHtml)
			{
				messageBodyPart.setContent(content, "text/html")
			}
			else
			{
				messageBodyPart.setText(content)
			}

			// Create a multipart message
			val multipart = MimeMultipart()

			// Set text message part
			multipart.addBodyPart(messageBodyPart)

			// Part two is attachment
			messageBodyPart = MimeBodyPart()

			messageBodyPart.setDataHandler(DataHandler(fileSource))
			messageBodyPart.setFileName(fileSource.file.name)
			multipart.addBodyPart(messageBodyPart)

			// Send the complete message parts
			message.setContent(multipart)

			var t : SMTPTransport? = null

			if(this.site!!.mailUseSSL)
			{
				t = session.getTransport("smtps") as SMTPTransport
			}
			else
			{
				t = session.getTransport("smtp") as SMTPTransport
			}

			if(this.site!!.mailUser != null && this.site!!.mailUser.trim() != ""
					&& this.site!!.mailUserPassword != null && this.site!!.mailUserPassword.trim() != "")
			{
				t.connect(this.site!!.mailHost, this.site!!.mailUser, this.site!!.mailUserPassword)
			}
			else
			{
				t.connect()
			}

			t.sendMessage(message, message.allRecipients)
			t.close()

			return true
		}
		catch (mex: MessagingException)
		{
			GeneralUtil.logError(mex)
		}

		return false
	}

	companion object
	{

		val contactFormNotificationList: Array<InternetAddress?>
			@Throws(AddressException::class)
			get()
			{
				val ctx = FacesContext.getCurrentInstance()
				val notifyAddressList = ctx.externalContext.getInitParameter("contactUs.notifyList")

				val notifyAddresses = notifyAddressList.split(",".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()

				val toAddresses = arrayOfNulls<InternetAddress>(notifyAddresses.size)

				var index = 0
				for (to in notifyAddresses)
				{
					try
					{
						toAddresses[index++] = InternetAddress(to)
					}
					catch (e: AddressException)
					{
						throw e
					}

				}

				return toAddresses
			}
	}
}
