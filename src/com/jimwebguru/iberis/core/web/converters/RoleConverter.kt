package com.jimwebguru.iberis.core.web.converters

import com.jimwebguru.iberis.core.utils.db.DaoUtil
import com.jimwebguru.iberis.core.persistence.Role

import javax.faces.application.FacesMessage
import javax.faces.component.UIComponent
import javax.faces.context.FacesContext
import javax.faces.convert.Converter
import javax.faces.convert.ConverterException
import javax.faces.convert.FacesConverter

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
@FacesConverter("roleConverter")
class RoleConverter : Converter
{
	override fun getAsObject(fc: FacesContext, uic: UIComponent, value: String?): Any?
	{
		return if (value != null && value.isNotEmpty())
		{
			try
			{
				val daoUtil = DaoUtil()

				daoUtil.find(Role::class.java, value.toLong())
			}
			catch (e: NumberFormatException)
			{
				throw ConverterException(FacesMessage(FacesMessage.SEVERITY_ERROR, "Conversion Error", "Not a valid role."))
			}

		}
		else
		{
			null
		}
	}

	override fun getAsString(fc: FacesContext, uic: UIComponent, `object`: Any?): String?
	{
		return if (`object` != null)
		{
			(`object` as Role).id.toString()
		}
		else
		{
			null
		}
	}
}