package com.jimwebguru.iberis.core.web.servlet

import com.jimwebguru.iberis.core.utils.db.DaoUtil
import com.jimwebguru.iberis.core.persistence.SystemSetting

import javax.servlet.ServletException
import javax.servlet.annotation.WebServlet
import javax.servlet.http.HttpServlet
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse
import java.io.*
import java.net.URLDecoder
import java.util.ArrayList
import java.util.Arrays

/**
 *
 * IBERIS CMS PROJECT
 *
 * Created By: BalusC
 * http://balusc.omnifaces.org/2009/02/fileservlet-supporting-resume-and.html
 *
 * Modified and Converted By: James May ( jimwebguru )
 *
 */
@WebServlet(name = "videoServlet", urlPatterns = arrayOf("/video/*"))
class VideoServlet : HttpServlet()
{

	// Properties ---------------------------------------------------------------------------------

	private var filePath: String? = null

	private val daoUtil = DaoUtil()

	// Actions ------------------------------------------------------------------------------------

	@Throws(ServletException::class)
	override fun init()
	{
		val context = servletContext

		val fileStorageSetting = this.daoUtil.find(SystemSetting::class.java, 1L)

		// Define base path somehow. You can define it as init-param of the servlet.
		this.filePath = fileStorageSetting!!.value + "/videos"

		// In a Windows environment with the Applicationserver running on the
		// c: volume, the above path is exactly the same as "c:\files".
		// In UNIX, it is just straightforward "/files".
	}

	/**
	 * Process HEAD request. This returns the same headers as GET request, but without content.
	 *
	 * @see HttpServlet.doHead
	 */
	@Throws(ServletException::class, IOException::class)
	override fun doHead(request: HttpServletRequest, response: HttpServletResponse)
	{
		// Process request without content.
		processRequest(request, response, false)
	}

	/**
	 * Process GET request.
	 *
	 * @see HttpServlet.doGet
	 */
	@Throws(ServletException::class, IOException::class)
	override fun doGet(request: HttpServletRequest, response: HttpServletResponse)
	{
		// Process request with content.
		processRequest(request, response, true)
	}

	@Throws(IOException::class)
	protected fun processRequest(request: HttpServletRequest, response: HttpServletResponse, content: Boolean)
	{
		// Get requested file by path info.
		val requestedFile = request.pathInfo

		// Check if file is actually supplied to the request URI.
		if (requestedFile == null)
		{
			// Do your thing if the file is not supplied to the request URI.
			// Throw an exception, or send 404, or show default/warning page, or just ignore it.
			response.sendError(HttpServletResponse.SC_NOT_FOUND) // 404.
			return
		}

		// Decode the file name (might contain spaces and on) and prepare file object.
		val file = File(filePath, URLDecoder.decode(requestedFile, "UTF-8"))

		// Check if file actually exists in filesystem.
		if (!file.exists())
		{
			// Do your thing if the file appears to be non-existing.
			// Throw an exception, or send 404, or show default/warning page, or just ignore it.
			response.sendError(HttpServletResponse.SC_NOT_FOUND) // 404.
			return
		}

		val length = file.length()
		val lastModified = file.lastModified()
		val eTag = file.name + "_" + length + "_" + lastModified
		val expires = System.currentTimeMillis() + DEFAULT_EXPIRE_TIME

		// Validate request headers for caching ---------------------------------------------------

		// If-None-Match header should contain "*" or ETag. If so, then return 304.
		val ifNoneMatch = request.getHeader("If-None-Match")
		if (ifNoneMatch != null && matches(ifNoneMatch, eTag))
		{
			response.status = HttpServletResponse.SC_NOT_MODIFIED
			response.setHeader("ETag", eTag) // Required in 304.
			response.setDateHeader("Expires", expires) // Postpone cache with 1 week.
			return
		}

		// If-Modified-Since header should be greater than LastModified. If so, then return 304.
		// This header is ignored if any If-None-Match header is specified.
		val ifModifiedSince = request.getDateHeader("If-Modified-Since")
		if (ifNoneMatch == null && ifModifiedSince != -1L && ifModifiedSince + 1000 > lastModified)
		{
			response.status = HttpServletResponse.SC_NOT_MODIFIED
			response.setHeader("ETag", eTag) // Required in 304.
			response.setDateHeader("Expires", expires) // Postpone cache with 1 week.
			return
		}


		// Validate request headers for resume ----------------------------------------------------

		// If-Match header should contain "*" or ETag. If not, then return 412.
		val ifMatch = request.getHeader("If-Match")
		if (ifMatch != null && !matches(ifMatch, eTag))
		{
			response.sendError(HttpServletResponse.SC_PRECONDITION_FAILED)
			return
		}

		// If-Unmodified-Since header should be greater than LastModified. If not, then return 412.
		val ifUnmodifiedSince = request.getDateHeader("If-Unmodified-Since")
		if (ifUnmodifiedSince != -1L && ifUnmodifiedSince + 1000 <= lastModified)
		{
			response.sendError(HttpServletResponse.SC_PRECONDITION_FAILED)
			return
		}

		// Validate and process range -------------------------------------------------------------

		// Prepare some variables. The full Range represents the complete file.
		val full = Range(0, length - 1, length)
		val ranges = ArrayList<Range>()

		// Validate and process Range and If-Range headers.
		val range = request.getHeader("Range")
		if (range != null)
		{

			// Range header should match format "bytes=n-n,n-n,n-n...". If not, then return 416.
			if (!range.matches("^bytes=\\d*-\\d*(,\\d*-\\d*)*$".toRegex()))
			{
				response.setHeader("Content-Range", "bytes */" + length) // Required in 416.
				response.sendError(HttpServletResponse.SC_REQUESTED_RANGE_NOT_SATISFIABLE)
				return
			}

			// If-Range header should either match ETag or be greater then LastModified. If not,
			// then return full file.
			val ifRange = request.getHeader("If-Range")
			if (ifRange != null && ifRange != eTag)
			{
				try
				{
					val ifRangeTime = request.getDateHeader("If-Range") // Throws IAE if invalid.

					if (ifRangeTime != -1L && ifRangeTime + 1000 < lastModified)
					{
						ranges.add(full)
					}
				}
				catch (ignore: IllegalArgumentException)
				{
					ranges.add(full)
				}

			}

			// If any valid If-Range header, then process each part of byte range.
			if (ranges.isEmpty())
			{
				for (part in range.substring(6).split(",".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray())
				{
					// Assuming a file with length of 100, the following examples returns bytes at:
					// 50-80 (50 to 80), 40- (40 to length=100), -20 (length-20=80 to length=100).
					var start = sublong(part, 0, part.indexOf("-"))
					var end = sublong(part, part.indexOf("-") + 1, part.length)

					if (start == -1L)
					{
						start = length - end
						end = length - 1
					}
					else if (end == -1L || end > length - 1)
					{
						end = length - 1
					}

					// Check if Range is syntactically valid. If not, then return 416.
					if (start > end)
					{
						response.setHeader("Content-Range", "bytes */" + length) // Required in 416.
						response.sendError(HttpServletResponse.SC_REQUESTED_RANGE_NOT_SATISFIABLE)
						return
					}

					// Add range.
					ranges.add(Range(start, end, length))
				}
			}
		}

		// Get content type by filename.
		var contentType: String? = servletContext.getMimeType(file.name)

		// If content type is unknown, then set the default value.
		// For all content types, see: http://www.w3schools.com/media/media_mimeref.asp
		// To add new content types, add new mime-mapping entry in web.xml.
		if (contentType == null)
		{
			contentType = "application/octet-stream"
		}

		val disposition = "inline"

		// Init servlet response.
		response.reset()
		response.bufferSize = DEFAULT_BUFFER_SIZE
		response.setHeader("Content-Disposition", disposition + ";filename=\"" + file.name + "\"")
		response.setHeader("Accept-Ranges", "bytes")
		response.setHeader("ETag", eTag)
		response.setDateHeader("Last-Modified", lastModified)
		response.setDateHeader("Expires", expires)

		// Prepare streams.
		var input: RandomAccessFile? = null
		var output: OutputStream? = null

		try
		{
			// Open streams.
			input = RandomAccessFile(file, "r")
			output = response.outputStream

			if (ranges.isEmpty() || ranges[0] === full)
			{
				// Return full file.
				response.contentType = contentType

				if (content)
				{

					// Content length is not directly predictable in case of GZIP.
					// So only add it if there is no means of GZIP, else browser will hang.
					response.setHeader("Content-Length", full.length.toString())

					// Copy full range.
					copy(input, output, full.start, full.length)
				}

			}
			else if (ranges.size == 1)
			{
				// Return single part of file.
				val r = ranges[0]
				response.contentType = contentType
				response.setHeader("Content-Range", "bytes " + r.start + "-" + r.end + "/" + r.total)
				response.setHeader("Content-Length", r.length.toString())
				response.status = HttpServletResponse.SC_PARTIAL_CONTENT // 206.

				if (content)
				{
					// Copy single part range.
					copy(input, output, r.start, r.length)
				}

			}
			else
			{

				// Return multiple parts of file.
				response.contentType = "multipart/byteranges; boundary=" + MULTIPART_BOUNDARY
				response.status = HttpServletResponse.SC_PARTIAL_CONTENT // 206.

				if (content)
				{
					// Cast back to ServletOutputStream to get the easy println methods.
					val sos = output

					// Copy multi part range.
					for (r in ranges)
					{
						// Add multipart boundary and header fields for every range.
						sos.println()
						sos.println("--" + MULTIPART_BOUNDARY)
						sos.println("Content-Type: " + contentType)
						sos.println("Content-Range: bytes " + r.start + "-" + r.end + "/" + r.total)

						// Copy single part range of multi part range.
						copy(input, output, r.start, r.length)
					}

					// End with multipart boundary.
					sos.println()
					sos.println("--$MULTIPART_BOUNDARY--")
				}
			}
		}
		finally
		{
			// Gently close streams.
			close(output)
			close(input)
		}
	}

	// Inner classes ------------------------------------------------------------------------------

	/**
	 * This class represents a byte range.
	 */
	protected inner class Range
	/**
	 * Construct a byte range.
	 *
	 * @param start Start of the byte range.
	 * @param end   End of the byte range.
	 * @param total Total length of the byte source.
	 */
	(internal var start: Long, internal var end: Long, internal var total: Long)
	{
		internal var length: Long = 0

		init
		{
			this.length = end - start + 1
		}

	}

	companion object
	{
		// Constants ----------------------------------------------------------------------------------

		private val DEFAULT_BUFFER_SIZE = 10240 // 10KB.
		private val DEFAULT_EXPIRE_TIME = 604800000L // ..ms = 1 week.
		private val MULTIPART_BOUNDARY = "MULTIPART_BYTERANGES"

		// Helpers (can be refactored to public utility class) ----------------------------------------

		/**
		 * Returns true if the given accept header accepts the given value.
		 *
		 * @param acceptHeader The accept header.
		 * @param toAccept     The value to be accepted.
		 * @return True if the given accept header accepts the given value.
		 */
		private fun accepts(acceptHeader: String, toAccept: String): Boolean
		{
			val acceptValues = acceptHeader.split("\\s*(,|;)\\s*".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
			Arrays.sort(acceptValues)
			return (Arrays.binarySearch(acceptValues, toAccept) > -1
					|| Arrays.binarySearch(acceptValues, toAccept.replace("/.*$".toRegex(), "/*")) > -1
					|| Arrays.binarySearch(acceptValues, "*/*") > -1)
		}

		/**
		 * Returns true if the given match header matches the given value.
		 *
		 * @param matchHeader The match header.
		 * @param toMatch     The value to be matched.
		 * @return True if the given match header matches the given value.
		 */
		private fun matches(matchHeader: String, toMatch: String): Boolean
		{
			val matchValues = matchHeader.split("\\s*,\\s*".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
			Arrays.sort(matchValues)
			return Arrays.binarySearch(matchValues, toMatch) > -1 || Arrays.binarySearch(matchValues, "*") > -1
		}

		/**
		 * Returns a substring of the given string value from the given begin index to the given end
		 * index as a long. If the substring is empty, then -1 will be returned
		 *
		 * @param value      The string value to return a substring as long for.
		 * @param beginIndex The begin index of the substring to be returned as long.
		 * @param endIndex   The end index of the substring to be returned as long.
		 * @return A substring of the given string value as long or -1 if substring is empty.
		 */
		private fun sublong(value: String, beginIndex: Int, endIndex: Int): Long
		{
			val substring = value.substring(beginIndex, endIndex)
			return if (substring.length > 0) java.lang.Long.parseLong(substring) else -1
		}

		/**
		 * Copy the given byte range of the given input to the given output.
		 *
		 * @param input  The input to copy the given range to the given output for.
		 * @param output The output to copy the given range from the given input for.
		 * @param start  Start of the byte range.
		 * @param length Length of the byte range.
		 * @throws IOException If something fails at I/O level.
		 */
		@Throws(IOException::class)
		private fun copy(input: RandomAccessFile, output: OutputStream?, start: Long, length: Long)
		{
			val buffer = ByteArray(DEFAULT_BUFFER_SIZE)
			var read: Int

			if (input.length() == length)
			{
				// Write full range.
				read = input.read(buffer)

				while (read > 0)
				{
					output!!.write(buffer, 0, read)

					read = input.read(buffer)
				}
			}
			else
			{
				// Write partial range.
				input.seek(start)
				var toRead = length

				read = input.read(buffer)

				while (read > 0)
				{
					toRead -= read.toLong()

					if (toRead > 0)
					{
						output!!.write(buffer, 0, read)
					}
					else
					{
						output!!.write(buffer, 0, toRead.toInt() + read)
						break
					}

					read = input.read(buffer)
				}
			}
		}

		private fun close(resource: Closeable?)
		{
			if (resource != null)
			{
				try
				{
					resource.close()
				}
				catch (e: IOException)
				{
					// Do your thing with the exception. Print it, log it or mail it.
					e.printStackTrace()
				}

			}
		}
	}
}
