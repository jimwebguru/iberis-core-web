package com.jimwebguru.iberis.core.web.servlet

import com.jimwebguru.iberis.core.utils.db.DaoUtil
import com.jimwebguru.iberis.core.persistence.SystemSetting

import javax.servlet.ServletException
import javax.servlet.annotation.WebServlet
import javax.servlet.http.HttpServlet
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse
import java.io.*
import java.net.URLDecoder

/**
 *
 * IBERIS CMS PROJECT
 *
 * Created By: BalusC
 * http://balusc.omnifaces.org/2007/07/fileservlet.html
 * http://balusc.omnifaces.org/2007/04/imageservlet.html
 *
 * Modified and Converted By: James May ( jimwebguru )
 *
 */
@WebServlet(name = "imageServlet", urlPatterns = arrayOf("/image/*"))
class ImageServlet : HttpServlet()
{

	// Properties ---------------------------------------------------------------------------------

	private var imagePath: String? = null

	private val daoUtil = DaoUtil()

	// Actions ------------------------------------------------------------------------------------

	@Throws(ServletException::class)
	override fun init()
	{
		val context = servletContext

		val fileStorageSetting = this.daoUtil.find(SystemSetting::class.java, 1L)

		// Define base path somehow. You can define it as init-param of the servlet.
		this.imagePath = fileStorageSetting!!.value + "/images"

		// In a Windows environment with the Applicationserver running on the
		// c: volume, the above path is exactly the same as "c:\images".
		// In UNIX, it is just straightforward "/images".
		// If you have stored files in the WebContent of a WAR, for example in the
		// "/WEB-INF/images" folder, then you can retrieve the absolute path by:
		// this.imagePath = getServletContext().getRealPath("/WEB-INF/images");
	}

	@Throws(ServletException::class, IOException::class)
	override fun doGet(request: HttpServletRequest, response: HttpServletResponse)
	{
		// Get requested image by path info.
		val requestedImage = request.pathInfo

		// Check if file name is actually supplied to the request URI.
		if (requestedImage == null)
		{
			// Do your thing if the image is not supplied to the request URI.
			// Throw an exception, or send 404, or show default/warning image, or just ignore it.
			response.sendError(HttpServletResponse.SC_NOT_FOUND) // 404.
			return
		}

		// Decode the file name (might contain spaces and on) and prepare file object.
		val image = File(imagePath, URLDecoder.decode(requestedImage, "UTF-8"))

		// Check if file actually exists in filesystem.
		if (!image.exists())
		{
			// Do your thing if the file appears to be non-existing.
			// Throw an exception, or send 404, or show default/warning image, or just ignore it.
			response.sendError(HttpServletResponse.SC_NOT_FOUND) // 404.
			return
		}

		// Get content type by filename.
		val contentType = servletContext.getMimeType(image.name)

		// Check if file is actually an image (avoid download of other files by hackers!).
		// For all content types, see: http://www.w3schools.com/media/media_mimeref.asp
		if (contentType == null || !contentType.startsWith("image"))
		{
			// Do your thing if the file appears not being a real image.
			// Throw an exception, or send 404, or show default/warning image, or just ignore it.
			response.sendError(HttpServletResponse.SC_NOT_FOUND) // 404.
			return
		}

		// Init servlet response.
		response.reset()
		response.bufferSize = DEFAULT_BUFFER_SIZE
		response.contentType = contentType
		response.setHeader("Content-Length", image.length().toString())
		response.setHeader("Content-Disposition", "inline; filename=\"" + image.name + "\"")

		// Prepare streams.
		var input: BufferedInputStream? = null
		var output: BufferedOutputStream? = null

		try
		{
			// Open streams.
			input = BufferedInputStream(FileInputStream(image), DEFAULT_BUFFER_SIZE)
			output = BufferedOutputStream(response.outputStream, DEFAULT_BUFFER_SIZE)

			// Write file contents to response.
			val buffer = ByteArray(DEFAULT_BUFFER_SIZE)

			var length: Int = input.read(buffer)

			while (length > 0)
			{
				output.write(buffer, 0, length)

				length = input.read(buffer)
			}
		}
		finally
		{
			// Gently close streams.
			close(output)
			close(input)
		}
	}

	companion object
	{
		// Constants ----------------------------------------------------------------------------------

		private val DEFAULT_BUFFER_SIZE = 10240 // 10KB.

		// Helpers (can be refactored to public utility class) ----------------------------------------

		private fun close(resource: Closeable?)
		{
			if (resource != null)
			{
				try
				{
					resource.close()
				}
				catch (e: IOException)
				{
					// Do your thing with the exception. Print it, log it or mail it.
					e.printStackTrace()
				}

			}
		}
	}
}
