package com.jimwebguru.iberis.core.web.servlet

import com.jimwebguru.iberis.core.utils.db.DaoUtil
import com.jimwebguru.iberis.core.persistence.SystemSetting

import javax.servlet.annotation.WebServlet
import javax.servlet.http.HttpServlet
import javax.servlet.ServletException
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse
import java.io.*
import java.net.URLDecoder

/**
 *
 * IBERIS CMS PROJECT
 *
 * Created By: BalusC
 * http://balusc.omnifaces.org/2007/07/fileservlet.html
 *
 * Modified and Converted By: James May ( jimwebguru )
 *
 */
@WebServlet(name = "fileServlet", urlPatterns = arrayOf("/file/*"))
class FileServlet : HttpServlet()
{

	// Properties ---------------------------------------------------------------------------------

	private var filePath: String? = null

	private val daoUtil = DaoUtil()

	// Actions ------------------------------------------------------------------------------------

	@Throws(ServletException::class)
	override fun init()
	{
		val context = servletContext

		val fileStorageSetting = this.daoUtil.find(SystemSetting::class.java, 1L)

		// Define base path somehow. You can define it as init-param of the servlet.
		this.filePath = fileStorageSetting!!.value + "/files"

		// In a Windows environment with the Applicationserver running on the
		// c: volume, the above path is exactly the same as "c:\files".
		// In UNIX, it is just straightforward "/files".
	}

	@Throws(ServletException::class, IOException::class)
	override fun doGet(request: HttpServletRequest, response: HttpServletResponse)
	{
		// Get requested file by path info.
		val requestedFile = request.pathInfo

		// Check if file is actually supplied to the request URI.
		if (requestedFile == null)
		{
			// Do your thing if the file is not supplied to the request URI.
			// Throw an exception, or send 404, or show default/warning page, or just ignore it.
			response.sendError(HttpServletResponse.SC_NOT_FOUND) // 404.
			return
		}

		// Decode the file name (might contain spaces and on) and prepare file object.
		val file = File(filePath, URLDecoder.decode(requestedFile, "UTF-8"))

		// Check if file actually exists in filesystem.
		if (!file.exists())
		{
			// Do your thing if the file appears to be non-existing.
			// Throw an exception, or send 404, or show default/warning page, or just ignore it.
			response.sendError(HttpServletResponse.SC_NOT_FOUND) // 404.
			return
		}

		// Get content type by filename.
		var contentType: String? = servletContext.getMimeType(file.name)

		// If content type is unknown, then set the default value.
		// For all content types, see: http://www.w3schools.com/media/media_mimeref.asp
		// To add new content types, add new mime-mapping entry in web.xml.
		if (contentType == null)
		{
			contentType = "application/octet-stream"
		}

		// Init servlet response.
		response.reset()
		response.bufferSize = DEFAULT_BUFFER_SIZE
		response.contentType = contentType
		response.setHeader("Content-Length", file.length().toString())

		var downloadOnly = "false"

		val downloadOnlyParam = request.getParameter("downloadOnly")

		if (downloadOnlyParam != null)
		{
			downloadOnly = downloadOnlyParam
		}

		if (downloadOnly == "true")
		{
			response.setHeader("Content-Disposition", "attachment; filename=\"" + file.name + "\"")
		}

		// Prepare streams.
		var input: BufferedInputStream? = null
		var output: BufferedOutputStream? = null

		try
		{
			// Open streams.
			input = BufferedInputStream(FileInputStream(file), DEFAULT_BUFFER_SIZE)
			output = BufferedOutputStream(response.outputStream, DEFAULT_BUFFER_SIZE)

			// Write file contents to response.
			val buffer = ByteArray(DEFAULT_BUFFER_SIZE)

			var length: Int = input.read(buffer)

			while (length  > 0)
			{
				output.write(buffer, 0, length)

				length = input.read(buffer)
			}
		}
		finally
		{
			// Gently close streams.
			close(output)
			close(input)
		}
	}

	companion object
	{
		// Constants ----------------------------------------------------------------------------------

		private val DEFAULT_BUFFER_SIZE = 10240 // 10KB.

		// Helpers (can be refactored to public utility class) ----------------------------------------

		private fun close(resource: Closeable?)
		{
			if (resource != null)
			{
				try
				{
					resource.close()
				}
				catch (e: IOException)
				{
					// Do your thing with the exception. Print it, log it or mail it.
					e.printStackTrace()
				}

			}
		}
	}
}
