package com.jimwebguru.iberis.core.web

import com.jimwebguru.iberis.core.persistence.Video
import com.jimwebguru.iberis.core.utils.db.DaoUtil
import com.jimwebguru.iberis.core.web.utils.GeneralUtil
import java.io.Serializable
import javax.annotation.PostConstruct
import javax.enterprise.context.RequestScoped
import javax.inject.Named

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
@Named
@RequestScoped
class VideoBackground : Serializable
{
	private val daoUtil = DaoUtil()

	var video: Video? = null

	@PostConstruct
	fun postInit()
	{
		if (GeneralUtil.getRequestParameterValue("id") != null)
		{
			this.video = this.daoUtil.find(Video::class.java, GeneralUtil.getRequestParameterValue("id").toString().toLong())
		}
	}
}