package com.jimwebguru.iberis.core.web.bundle

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */

import com.jimwebguru.iberis.core.utils.db.DaoUtil
import com.jimwebguru.iberis.core.utils.db.QueryParameter
import com.jimwebguru.iberis.core.persistence.ResourceBundleItem
import java.util.*

class IberisResourceBundle (private val baseName: String, private val loc: Locale) : ListResourceBundle()
{
	private val daoUtil = DaoUtil()

	override fun getContents(): Array<Array<Any?>>
	{
		val parameters = ArrayList<QueryParameter>()
		parameters.add(QueryParameter("countryCode", this.loc.country))
		parameters.add(QueryParameter("languageCode", this.loc.language))
		parameters.add(QueryParameter("variant", this.loc.variant))
		parameters.add(QueryParameter("bundleName", this.baseName))

		val resources = this.daoUtil.find<ResourceBundleItem>("SELECT OBJECT(o) FROM ResourceBundleItem o WHERE o.bundleName = :bundleName AND o.countryCode = :countryCode AND o.languageCode = :languageCode AND o.variant = :variant", parameters)

		if (resources != null)
		{
			val all = Array<Array<Any?>>(resources.size) { arrayOfNulls(2) }

			var i = 0

			val it = resources.iterator()
			while (it.hasNext())
			{
				val resource = it.next()

				all[i] = arrayOf(resource.bundleKey, resource.bundleValue)

				i++
			}

			return all
		}

		return arrayOf()
	}
}
