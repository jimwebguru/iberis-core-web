package com.jimwebguru.iberis.core.web.bundle

import javax.faces.context.FacesContext
import java.util.Enumeration
import java.util.Locale
import java.util.ResourceBundle

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
class DatabaseResourceBundle @JvmOverloads constructor(locale: Locale = FacesContext.getCurrentInstance().viewRoot.locale) : ResourceBundle()
{
	init
	{
		setParent(ResourceBundle.getBundle(BUNDLE_NAME, locale, DBControl()))
	}

	override fun handleGetObject(key: String): Any
	{
		return parent.getObject(key)
	}

	override fun getKeys(): Enumeration<String>
	{
		return parent.keys
	}

	companion object
	{
		protected val BUNDLE_NAME = "iberisbundle"
	}
}
