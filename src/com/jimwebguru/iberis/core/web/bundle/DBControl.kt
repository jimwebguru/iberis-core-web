package com.jimwebguru.iberis.core.web.bundle

import java.io.IOException
import java.util.*

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
class DBControl : ResourceBundle.Control()
{
	override fun getFormats(baseName: String): List<String>
	{
		if (baseName == null)
		{
			throw NullPointerException()
		}

		return Arrays.asList("db")
	}

	@Throws(IllegalAccessException::class, InstantiationException::class, IOException::class)
	override fun newBundle(baseName: String?, locale: Locale?, format: String, loader: ClassLoader?, reload: Boolean): ResourceBundle?
	{
		if (baseName == null || locale == null || format == null || loader == null)
		{
			throw NullPointerException()
		}

		var bundle: ResourceBundle? = null

		if (format == "db")
		{
			bundle = IberisResourceBundle(baseName, locale)
		}

		return bundle
	}
}
