package com.jimwebguru.iberis.core.web.controllers.ecommerce

import com.jimwebguru.iberis.core.loaders.GenericLazyLoader
import com.jimwebguru.iberis.core.utils.db.DaoUtil
import com.jimwebguru.iberis.core.persistence.Manufacturer
import com.jimwebguru.iberis.core.persistence.Site
import com.jimwebguru.iberis.core.web.controllers.admin.security.Authorize
import com.jimwebguru.iberis.core.web.controllers.cms.CmsAuthUtil
import com.jimwebguru.iberis.core.web.utils.GeneralUtil

import javax.annotation.PostConstruct
import javax.faces.application.FacesMessage
import javax.faces.view.ViewScoped
import javax.inject.Named
import java.io.Serializable
import java.util.ArrayList

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
@Named
@ViewScoped
class ManufacturerManagement : Serializable
{
	private val daoUtil = DaoUtil()

	var manufacturer: Manufacturer? = null

	var lazyLoader = GenericLazyLoader(Manufacturer::class.java)

	var siteList = ArrayList<Site>()

	var siteListValue: Long? = null

	@PostConstruct
	fun postInit()
	{
		if (GeneralUtil.getRequestParameterValue("id") != null)
		{
			this.manufacturer = this.daoUtil.find(Manufacturer::class.java, GeneralUtil.getRequestParameterValue("id").toString().toLong())
		}

		this.lazyLoader.additionalQueryParams = CmsAuthUtil.getCmsSiteFilterParameters()

		val user = Authorize.getUser()

		if(user != null)
		{
			for (siteMembership in user.siteMemberships)
			{
				this.siteList.add(siteMembership.site)
			}
		}
	}

	fun newManufacturer()
	{
		this.manufacturer = Manufacturer()
	}

	fun selectManufacturer(item: Manufacturer)
	{
		this.manufacturer = item
	}

	fun saveManufacturer()
	{
		if(this.siteList.size == 1)
		{
			if (this.manufacturer!!.sites == null)
			{
				this.manufacturer!!.sites = ArrayList()
			}

			if (this.manufacturer!!.sites.isEmpty())
			{
				this.manufacturer!!.sites.add(this.siteList[0])
			}
		}

		if(this.manufacturer!!.sites == null || this.manufacturer!!.sites.isEmpty())
		{
			GeneralUtil.addMessage("A site must be associated.", FacesMessage.SEVERITY_ERROR)
		}
		else
		{
			if (this.manufacturer!!.manufacturerId != null && this.manufacturer!!.manufacturerId > 0)
			{
				if (this.daoUtil.update(this.manufacturer!!))
				{
					GeneralUtil.addMessage("Manufacturer has been updated.", FacesMessage.SEVERITY_INFO)
				}
				else
				{
					GeneralUtil.addMessage("Manufacturer could not be updated.", FacesMessage.SEVERITY_ERROR)
				}
			}
			else
			{
				if (this.daoUtil.create(this.manufacturer!!))
				{
					GeneralUtil.addMessage("Manufacturer has been created.", FacesMessage.SEVERITY_INFO)
				}
				else
				{
					GeneralUtil.addMessage("Manufacturer could not be created.", FacesMessage.SEVERITY_ERROR)
				}
			}
		}
	}

	fun deleteManufacturer(Manufacturer: Manufacturer)
	{
		if (Manufacturer.manufacturerId != null && Manufacturer.manufacturerId > 0)
		{
			this.daoUtil.delete(Manufacturer)

			this.newManufacturer()

			GeneralUtil.addMessage("Manufacturer has been deleted.", FacesMessage.SEVERITY_INFO)
		}

	}

	fun addSite()
	{
		val site = this.daoUtil.find(Site::class.java, this.siteListValue!!)

		if (site != null)
		{
			if (this.manufacturer!!.sites == null)
			{
				this.manufacturer!!.sites = ArrayList()
			}

			var siteExists = false

			for (cSite in this.manufacturer!!.sites)
			{
				if (cSite.siteID == site.siteID)
				{
					siteExists = true
				}
			}

			if (!siteExists)
			{
				this.manufacturer!!.sites.add(site)
			}
		}
	}

	fun removeSite(site: Site)
	{
		this.manufacturer!!.sites.remove(site)
	}

	fun inSiteList(siteId: Long): Boolean
	{
		for(cSite in this.siteList)
		{
			if(cSite.siteID == siteId)
			{
				return true
			}
		}

		return false
	}
}
