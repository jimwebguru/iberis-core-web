package com.jimwebguru.iberis.core.web.controllers.ecommerce

import com.jimwebguru.iberis.core.loaders.GenericLazyLoader
import com.jimwebguru.iberis.core.persistence.*
import com.jimwebguru.iberis.core.utils.db.DaoUtil
import com.jimwebguru.iberis.core.utils.db.QueryParameter

import javax.faces.view.ViewScoped
import javax.inject.Named
import java.io.Serializable
import java.util.ArrayList
import java.util.Hashtable

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
@Named
@ViewScoped
class EcommerceUtil : Serializable
{
	private val productLoaders = Hashtable<Long, GenericLazyLoader<Product>>()

	var productGallery: Gallery? = null

	private val daoUtil = DaoUtil()

	fun retrieveProductLoader(key: Long?): GenericLazyLoader<Product>?
	{
		if (!this.productLoaders.containsKey(key))
		{
			val catalog = this.daoUtil.find(ProductCatalog::class.java, key!!)

			if (catalog != null)
			{
				val lazyLoader = GenericLazyLoader(Product::class.java)

				val queryParameters = ArrayList<QueryParameter>()

				if (catalog.productGroups != null && catalog.productGroups.isNotEmpty())
				{
					var conditionValue = "("

					for (group in catalog.productGroups)
					{
						conditionValue += group.groupId!!.toString() + ","
					}

					conditionValue = conditionValue.substring(0, conditionValue.length - 1) + ")"

					val queryParameter = QueryParameter()

					queryParameter.explicitCondition = "o.productGroup.groupId IN " + conditionValue

					queryParameters.add(queryParameter)

					lazyLoader.additionalQueryParams = queryParameters
				}
				else if (catalog.products != null && catalog.products.isNotEmpty())
				{
					var conditionValue = "("

					for (product in catalog.products)
					{
						conditionValue += product.productId!!.toString() + ","
					}

					conditionValue = conditionValue.substring(0, conditionValue.length - 1) + ")"

					val queryParameter = QueryParameter()

					queryParameter.explicitCondition = "o.productId IN " + conditionValue

					queryParameters.add(queryParameter)

					lazyLoader.additionalQueryParams = queryParameters
				}

				this.productLoaders.put(key!!, lazyLoader)
			}
		}

		return this.productLoaders.get(key!!)
	}

	fun createProductGallery(product: Product)
	{
		this.productGallery = Gallery()

		this.productGallery!!.galleryImages = ArrayList()

		if (product.productImages != null && product.productImages.isNotEmpty())
		{
			for (pImage in product.productImages)
			{
				val galleryImage = GalleryImage()
				galleryImage.gallery = this.productGallery
				galleryImage.image = pImage.image

				this.productGallery!!.galleryImages.add(galleryImage)
			}
		}
	}
}
