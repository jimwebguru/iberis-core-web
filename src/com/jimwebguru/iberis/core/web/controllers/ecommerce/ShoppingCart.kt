package com.jimwebguru.iberis.core.web.controllers.ecommerce

import com.jimwebguru.iberis.core.persistence.Product
import com.jimwebguru.iberis.core.web.utils.GeneralUtil
import org.primefaces.PrimeFaces
import org.primefaces.event.CellEditEvent
import org.primefaces.util.ComponentUtils

import javax.faces.application.FacesMessage
import javax.enterprise.context.SessionScoped
import javax.inject.Named
import java.io.Serializable
import java.math.BigDecimal
import java.math.MathContext
import java.util.ArrayList

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
@Named
@SessionScoped
class ShoppingCart : Serializable
{
	var items = ArrayList<ShoppingCartItem>()

	var total = BigDecimal(0)

	fun addProduct(product: Product)
	{
		var found = false

		for (sItem in this.items)
		{
			if (sItem.product!!.productId == product.productId)
			{
				found = true

				sItem.quantity = sItem.quantity!! + 1
			}
		}

		if (!found)
		{
			val item = ShoppingCartItem()
			item.product = product
			item.quantity = 1

			this.items.add(item)
		}

		this.recalculateTotal()

		PrimeFaces.current().ajax().update(ComponentUtils.findComponentClientId("shopping-cart-dash"))
	}

	fun removeProduct(item: ShoppingCartItem)
	{
		this.items.remove(item)

		this.recalculateTotal()

		PrimeFaces.current().ajax().update(ComponentUtils.findComponentClientId("shopping-cart-dash"))
	}

	fun checkout()
	{
		//Add some logic later with payment gateway
	}

	fun onCellEdit(event: CellEditEvent)
	{
		val oldValue = event.oldValue
		val newValue = event.newValue

		if (newValue != null && newValue != oldValue)
		{

		}

		GeneralUtil.addMessage("Cart has been updated with new quantity.", FacesMessage.SEVERITY_INFO)
	}

	private fun recalculateTotal()
	{
		this.total = BigDecimal(0)

		for (item in this.items)
		{
			val mTotal = item.product!!.price.multiply(BigDecimal(item.quantity!!))

			this.total = this.total.add(mTotal)
		}
	}
}
