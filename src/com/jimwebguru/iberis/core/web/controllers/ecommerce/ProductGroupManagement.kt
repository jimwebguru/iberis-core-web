package com.jimwebguru.iberis.core.web.controllers.ecommerce

import com.jimwebguru.iberis.core.loaders.GenericLazyLoader
import com.jimwebguru.iberis.core.utils.db.DaoUtil
import com.jimwebguru.iberis.core.persistence.ProductGroup
import com.jimwebguru.iberis.core.persistence.Site
import com.jimwebguru.iberis.core.web.controllers.admin.security.Authorize
import com.jimwebguru.iberis.core.web.controllers.cms.CmsAuthUtil
import com.jimwebguru.iberis.core.web.utils.GeneralUtil

import javax.annotation.PostConstruct
import javax.faces.application.FacesMessage
import javax.faces.view.ViewScoped
import javax.inject.Named
import java.io.Serializable
import java.util.ArrayList

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
@Named
@ViewScoped
class ProductGroupManagement : Serializable
{
	private val daoUtil = DaoUtil()

	var productGroup: ProductGroup? = null

	var lazyLoader = GenericLazyLoader(ProductGroup::class.java)

	var siteList = ArrayList<Site>()

	var siteListValue: Long? = null

	@PostConstruct
	fun postInit()
	{
		if (GeneralUtil.getRequestParameterValue("id") != null)
		{
			this.productGroup = this.daoUtil.find(ProductGroup::class.java, GeneralUtil.getRequestParameterValue("id").toString().toLong())

			this.resetObjectForUIBind()
		}

		this.lazyLoader.additionalQueryParams = CmsAuthUtil.getCmsSiteFilterParameters()

		val user = Authorize.getUser()

		if(user != null)
		{
			for (siteMembership in user.siteMemberships)
			{
				this.siteList.add(siteMembership.site)
			}
		}
	}

	fun newProductGroup()
	{
		this.productGroup = ProductGroup()
		this.productGroup!!.parentGroup = ProductGroup()
	}

	fun selectProductGroup(item: ProductGroup)
	{
		this.productGroup = item
		this.productGroup!!.parentGroup = ProductGroup()
	}

	fun saveProductGroup()
	{
		if(this.siteList.size == 1)
		{
			if (this.productGroup!!.sites == null)
			{
				this.productGroup!!.sites = ArrayList()
			}

			if (this.productGroup!!.sites.isEmpty())
			{
				this.productGroup!!.sites.add(this.siteList[0])
			}
		}

		if(this.productGroup!!.sites == null || this.productGroup!!.sites.isEmpty())
		{
			GeneralUtil.addMessage("A site must be associated.", FacesMessage.SEVERITY_ERROR)
		}
		else
		{
			this.clearObjectForSave()

			if (this.productGroup!!.groupId != null && this.productGroup!!.groupId > 0)
			{
				if (this.daoUtil.update(this.productGroup!!))
				{
					GeneralUtil.addMessage("Product Group has been updated.", FacesMessage.SEVERITY_INFO)
				}
				else
				{
					GeneralUtil.addMessage("Product Group could not be updated.", FacesMessage.SEVERITY_ERROR)
				}
			}
			else
			{
				if (this.daoUtil.create(this.productGroup!!))
				{
					GeneralUtil.addMessage("Product Group has been created.", FacesMessage.SEVERITY_INFO)
				}
				else
				{
					GeneralUtil.addMessage("Product Group could not be created.", FacesMessage.SEVERITY_ERROR)
				}
			}

			this.resetObjectForUIBind()
		}
	}

	fun deleteProductGroup(ProductGroup: ProductGroup)
	{
		if (ProductGroup.groupId != null && ProductGroup.groupId > 0)
		{
			this.daoUtil.delete(ProductGroup)

			this.newProductGroup()

			GeneralUtil.addMessage("Product Group has been deleted.", FacesMessage.SEVERITY_INFO)
		}

	}

	private fun resetObjectForUIBind()
	{
		if (this.productGroup!!.parentGroup == null)
		{
			this.productGroup!!.parentGroup = ProductGroup()
		}
	}

	private fun clearObjectForSave()
	{
		if (this.productGroup!!.parentGroup != null && (this.productGroup!!.parentGroup.groupId == null || this.productGroup!!.parentGroup.groupId < 1))
		{
			this.productGroup!!.parentGroup = null
		}
	}

	fun addSite()
	{
		val site = this.daoUtil.find(Site::class.java, this.siteListValue!!)

		if (site != null)
		{
			if (this.productGroup!!.sites == null)
			{
				this.productGroup!!.sites = ArrayList()
			}

			var siteExists = false

			for (cSite in this.productGroup!!.sites)
			{
				if (cSite.siteID == site.siteID)
				{
					siteExists = true
				}
			}

			if (!siteExists)
			{
				this.productGroup!!.sites.add(site)
			}
		}
	}

	fun removeSite(site: Site)
	{
		this.productGroup!!.sites.remove(site)
	}

	fun inSiteList(siteId: Long): Boolean
	{
		for(cSite in this.siteList)
		{
			if(cSite.siteID == siteId)
			{
				return true
			}
		}

		return false
	}
}
