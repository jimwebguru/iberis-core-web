package com.jimwebguru.iberis.core.web.controllers.ecommerce

import com.jimwebguru.iberis.core.persistence.Product

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
class ShoppingCartItem
{
	var product: Product? = null
	var quantity: Int? = null
}
