package com.jimwebguru.iberis.core.web.controllers.ecommerce

import com.jimwebguru.iberis.core.loaders.GenericLazyLoader
import com.jimwebguru.iberis.core.persistence.*
import com.jimwebguru.iberis.core.utils.FileUtil
import com.jimwebguru.iberis.core.utils.PersistenceUtil
import com.jimwebguru.iberis.core.utils.db.DaoUtil
import com.jimwebguru.iberis.core.web.controllers.admin.security.Authorize
import com.jimwebguru.iberis.core.web.controllers.cms.CmsAuthUtil
import com.jimwebguru.iberis.core.web.utils.GeneralUtil
import org.primefaces.event.FileUploadEvent

import javax.annotation.PostConstruct
import javax.faces.application.FacesMessage
import javax.faces.view.ViewScoped
import javax.inject.Named
import javax.imageio.ImageIO
import java.io.Serializable
import java.util.ArrayList


/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
@Named
@ViewScoped
class ProductManagement : Serializable
{
	private val daoUtil = DaoUtil()

	var product: Product? = null

	var existingImage: Image? = Image()

	var lazyLoader = GenericLazyLoader(Product::class.java)

	var siteList = ArrayList<Site>()

	var siteListValue: Long? = null

	@PostConstruct
	fun postInit()
	{
		//Authorize.roleCheck(new String[] {"Admin"});

		if (GeneralUtil.getRequestParameterValue("id") != null)
		{
			this.product = this.daoUtil.find(Product::class.java, GeneralUtil.getRequestParameterValue("id").toString().toLong())

			this.resetObjectForUIBind()
		}

		this.lazyLoader.additionalQueryParams = CmsAuthUtil.getCmsSiteFilterParameters()

		val user = Authorize.getUser()

		if(user != null)
		{
			for (siteMembership in user.siteMemberships)
			{
				this.siteList.add(siteMembership.site)
			}
		}
	}

	fun newProduct()
	{
		this.product = Product()
		this.product!!.productGroup = ProductGroup()
		this.product!!.manufacturer = Manufacturer()
		this.product!!.mainImage = ProductImage()
		this.product!!.mainImage.image = Image()
	}

	fun selectProduct(item: Product)
	{
		this.product = item

		this.resetObjectForUIBind()
	}

	fun saveProduct()
	{
		if(this.siteList.size == 1)
		{
			if (this.product!!.sites == null)
			{
				this.product!!.sites = ArrayList()
			}

			if (this.product!!.sites.isEmpty())
			{
				this.product!!.sites.add(this.siteList[0])
			}
		}

		if(this.product!!.sites == null || this.product!!.sites.isEmpty())
		{
			GeneralUtil.addMessage("A site must be associated.", FacesMessage.SEVERITY_ERROR)
		}
		else
		{
			this.clearObjectForSave()

			if (this.product!!.productId != null && this.product!!.productId > 0)
			{
				if (this.daoUtil.update(this.product!!))
				{
					GeneralUtil.addMessage("Product has been updated.", FacesMessage.SEVERITY_INFO)
				}
				else
				{
					GeneralUtil.addMessage("Product could not be updated.", FacesMessage.SEVERITY_ERROR)
				}
			}
			else
			{
				if (this.daoUtil.create(this.product!!))
				{
					GeneralUtil.addMessage("Product has been created.", FacesMessage.SEVERITY_INFO)
				}
				else
				{
					GeneralUtil.addMessage("Product could not be created.", FacesMessage.SEVERITY_ERROR)
				}
			}

			this.resetObjectForUIBind()
		}
	}

	fun deleteProduct(prod: Product)
	{
		if (prod.productId != null && prod.productId > 0)
		{
			if (prod.productImages.isNotEmpty())
			{
				for (productImage in prod.productImages)
				{
					this.daoUtil.delete(productImage)
				}
			}

			val success = this.daoUtil.delete(prod)

			if (success)
			{
				this.newProduct()

				GeneralUtil.addMessage("Product has been deleted.", FacesMessage.SEVERITY_INFO)
			}
			else
			{
				GeneralUtil.addMessage("Could not delete product.", FacesMessage.SEVERITY_ERROR)
			}
		}

	}

	fun handleImageUpload(event: FileUploadEvent)
	{
		val uploadedFile = event.file

		try
		{
			val persistenceUtil = PersistenceUtil()

			val fileStorageSetting = this.daoUtil.find(SystemSetting::class.java, 1L)

			val directoryPath = fileStorageSetting!!.value + "/images/product/" + this.product!!.productId + "/"

			val fileWriteName = uploadedFile.fileName.replace(" ".toRegex(), "").toLowerCase()

			FileUtil.createFileAndDirectory(directoryPath, directoryPath + fileWriteName, uploadedFile.inputstream)

			val image = Image()
			image.name = fileWriteName
			image.filePath = "/image/product/" + product!!.productId + "/" + fileWriteName

			val bufferedImage = ImageIO.read(java.io.File(directoryPath + fileWriteName))
			image.width = java.lang.Long.valueOf(bufferedImage.width.toLong())
			image.height = java.lang.Long.valueOf(bufferedImage.height.toLong())
			image.mimeType = persistenceUtil.getMimeType(uploadedFile.contentType)

			val lastIndexOf = fileWriteName.lastIndexOf(".")
			image.fileExtension = persistenceUtil.getFileExtension(uploadedFile.fileName.substring(lastIndexOf))

			val success = this.daoUtil.create(image)

			if (success)
			{
				val productImage = ProductImage()
				productImage.product = this.product
				productImage.image = image

				this.daoUtil.create(productImage)

				if (this.product!!.productImages == null)
				{
					this.product!!.productImages = ArrayList()
				}

				this.daoUtil.refresh(Product::class.java, this.product!!.productId)

				this.product = this.daoUtil.find(Product::class.java, this.product!!.productId)

				this.resetObjectForUIBind()
			}

			GeneralUtil.addMessage("Images have been uploaded.", FacesMessage.SEVERITY_INFO)
		}
		catch (ex: Exception)
		{
			GeneralUtil.logError(ex)
		}

	}

	fun removeImage(pImage: ProductImage)
	{
		var objectToRemove: ProductImage? = null

		for (pImg in this.product!!.productImages)
		{
			if (pImg.id == pImage.id)
			{
				objectToRemove = pImg
			}
		}

		this.product!!.productImages.remove(objectToRemove)
		this.daoUtil.delete(objectToRemove!!)

		this.daoUtil.refresh(Product::class.java, this.product!!.productId)
	}

	fun addExistingImage()
	{
		if (this.existingImage != null && this.existingImage!!.imageid != null && this.existingImage!!.imageid > 0)
		{
			if (this.product!!.productImages == null)
			{
				this.product!!.productImages = ArrayList()
			}

			var addImage = true

			if (this.product!!.productImages.isNotEmpty())
			{
				for (productImage in this.product!!.productImages)
				{
					if (productImage.image.imageid == this.existingImage!!.imageid)
					{
						addImage = false
					}
				}
			}

			if (addImage)
			{
				val productImage = ProductImage()
				productImage.image = Image()
				productImage.image.imageid = this.existingImage!!.imageid
				productImage.product = this.product

				this.daoUtil.create(productImage)

				this.daoUtil.refresh(Product::class.java, this.product!!.productId)

				this.product = this.daoUtil.find(Product::class.java, this.product!!.productId)

				this.resetObjectForUIBind()
			}
		}
	}

	private fun resetObjectForUIBind()
	{
		if (this.product!!.productGroup == null)
		{
			this.product!!.productGroup = ProductGroup()
		}

		if (this.product!!.manufacturer == null)
		{
			this.product!!.manufacturer = Manufacturer()
		}

		if (this.product!!.mainImage == null)
		{
			this.product!!.mainImage = ProductImage()
			this.product!!.mainImage.image = Image()
		}
	}

	private fun clearObjectForSave()
	{
		if (this.product!!.productGroup != null && (this.product!!.productGroup.groupId == null || this.product!!.productGroup.groupId < 1))
		{
			this.product!!.productGroup = null
		}

		if (this.product!!.manufacturer != null && (this.product!!.manufacturer.manufacturerId == null || this.product!!.manufacturer.manufacturerId < 1))
		{
			this.product!!.manufacturer = null
		}

		if (this.product!!.mainImage != null && (this.product!!.mainImage.id == null || this.product!!.mainImage.id < 1))
		{
			this.product!!.mainImage = null
		}
	}

	fun addSite()
	{
		val site = this.daoUtil.find(Site::class.java, this.siteListValue!!)

		if (site != null)
		{
			if (this.product!!.sites == null)
			{
				this.product!!.sites = ArrayList()
			}

			var siteExists = false

			for (cSite in this.product!!.sites)
			{
				if (cSite.siteID == site.siteID)
				{
					siteExists = true
				}
			}

			if (!siteExists)
			{
				this.product!!.sites.add(site)
			}
		}
	}

	fun removeSite(site: Site)
	{
		this.product!!.sites.remove(site)
	}

	fun inSiteList(siteId: Long): Boolean
	{
		for(cSite in this.siteList)
		{
			if(cSite.siteID == siteId)
			{
				return true
			}
		}

		return false
	}
}
