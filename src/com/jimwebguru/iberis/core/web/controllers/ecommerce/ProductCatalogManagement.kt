package com.jimwebguru.iberis.core.web.controllers.ecommerce

import com.jimwebguru.iberis.core.loaders.GenericLazyLoader
import com.jimwebguru.iberis.core.persistence.*
import com.jimwebguru.iberis.core.utils.db.DaoUtil
import com.jimwebguru.iberis.core.web.controllers.admin.security.Authorize
import com.jimwebguru.iberis.core.web.controllers.cms.CmsAuthUtil
import com.jimwebguru.iberis.core.web.utils.GeneralUtil

import javax.annotation.PostConstruct
import javax.faces.application.FacesMessage
import javax.faces.view.ViewScoped
import javax.inject.Named
import java.io.Serializable
import java.util.ArrayList

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
@Named
@ViewScoped
class ProductCatalogManagement : Serializable
{
	private val daoUtil = DaoUtil()

	var productCatalog: ProductCatalog? = null
	var productGroup: ProductGroup? = null
	var product: Product? = null

	var lazyLoader = GenericLazyLoader(ProductCatalog::class.java)

	var siteList = ArrayList<Site>()

	var siteListValue: Long? = null

	@PostConstruct
	fun postInit()
	{
		if (GeneralUtil.getRequestParameterValue("id") != null)
		{
			this.productCatalog = this.daoUtil.find(ProductCatalog::class.java, GeneralUtil.getRequestParameterValue("id").toString().toLong())

			this.resetObjectForUIBind()
		}

		this.product = Product()
		this.productGroup = ProductGroup()

		this.lazyLoader.additionalQueryParams = CmsAuthUtil.getCmsSiteFilterParameters()

		val user = Authorize.getUser()

		if(user != null)
		{
			for (siteMembership in user.siteMemberships)
			{
				this.siteList.add(siteMembership.site)
			}
		}
	}

	fun newProductCatalog()
	{
		this.productCatalog = ProductCatalog()
		this.productCatalog!!.site = Site()
		this.productCatalog!!.initialRows = 5
		this.productCatalog!!.pagerTemplate = "5,20,50,100"
	}

	fun selectProductCatalog(item: ProductCatalog)
	{
		this.productCatalog = item

		if (this.productCatalog!!.site == null)
		{
			this.productCatalog!!.site = Site()
		}
	}

	fun saveProductCatalog()
	{
		if(this.siteList.size == 1)
		{
			if (this.productCatalog!!.sites == null)
			{
				this.productCatalog!!.sites = ArrayList()
			}

			if (this.productCatalog!!.sites.isEmpty())
			{
				this.productCatalog!!.sites.add(this.siteList[0])
			}
		}

		if(this.productCatalog!!.sites == null || this.productCatalog!!.sites.isEmpty())
		{
			GeneralUtil.addMessage("A site must be associated.", FacesMessage.SEVERITY_ERROR)
		}
		else
		{
			this.clearObjectForSave()

			if (this.productCatalog!!.id != null && this.productCatalog!!.id > 0)
			{
				if (this.daoUtil.update(this.productCatalog!!))
				{
					GeneralUtil.addMessage("Product Catalog has been updated.", FacesMessage.SEVERITY_INFO)
				}
				else
				{
					GeneralUtil.addMessage("Product Catalog could not be updated.", FacesMessage.SEVERITY_ERROR)
				}
			}
			else
			{
				if (this.daoUtil.create(this.productCatalog!!))
				{
					GeneralUtil.addMessage("Product Catalog has been created.", FacesMessage.SEVERITY_INFO)
				}
				else
				{
					GeneralUtil.addMessage("Product Catalog could not be created.", FacesMessage.SEVERITY_ERROR)
				}
			}

			this.resetObjectForUIBind()
		}
	}

	fun deleteProductCatalog(ProductCatalog: ProductCatalog)
	{
		if (ProductCatalog.id != null && ProductCatalog.id > 0)
		{
			this.daoUtil.delete(ProductCatalog)

			this.newProductCatalog()

			GeneralUtil.addMessage("Product Catalog has been deleted.", FacesMessage.SEVERITY_INFO)
		}

	}

	fun addProductGroup()
	{
		if (this.productGroup!!.groupId != null && this.productGroup!!.groupId > 0)
		{
			if (this.productCatalog!!.productGroups == null)
			{
				this.productCatalog!!.productGroups = ArrayList()
			}

			var addGroup = true

			for (group in this.productCatalog!!.productGroups)
			{
				if (group.groupId == this.productGroup!!.groupId)
				{
					addGroup = false
				}
			}

			if (addGroup)
			{
				val newProductGroup = ProductGroup()
				newProductGroup.groupId = this.productGroup!!.groupId
				newProductGroup.groupName = this.productGroup!!.groupName

				this.productCatalog!!.productGroups.add(newProductGroup)
				this.daoUtil.update(this.productCatalog!!)
			}
		}
	}

	fun removeProductGroup(group: ProductGroup)
	{
		var objectToRemove: ProductGroup? = null

		for (pGroup in this.productCatalog!!.productGroups)
		{
			if (pGroup.groupId == group.groupId)
			{
				objectToRemove = pGroup
			}
		}

		this.productCatalog!!.productGroups.remove(objectToRemove)

		this.daoUtil.update(this.productCatalog!!)
	}

	fun addProduct()
	{
		if (this.product!!.productId != null && this.product!!.productId > 0)
		{
			if (this.productCatalog!!.products == null)
			{
				this.productCatalog!!.products = ArrayList()
			}

			var addProduct = true

			for (product in this.productCatalog!!.products)
			{
				if (product.productId == this.product!!.productId)
				{
					addProduct = false
				}
			}

			if (addProduct)
			{
				val newProduct = Product()
				newProduct.productId = this.product!!.productId
				newProduct.productName = this.product!!.productName

				this.productCatalog!!.products.add(newProduct)
				this.daoUtil.update(this.productCatalog!!)
			}
		}
	}

	fun removeProduct(product: Product)
	{
		var objectToRemove: Product? = null

		for (prod in this.productCatalog!!.products)
		{
			if (prod.productId == product.productId)
			{
				objectToRemove = prod
			}
		}

		this.productCatalog!!.products.remove(objectToRemove)

		this.daoUtil.update(this.productCatalog!!)
	}

	private fun resetObjectForUIBind()
	{
		if (this.productCatalog!!.site == null)
		{
			this.productCatalog!!.site = Site()
		}
	}

	private fun clearObjectForSave()
	{
		if (this.productCatalog!!.site != null && (this.productCatalog!!.site.siteID == null || this.productCatalog!!.site.siteID < 1))
		{
			this.productCatalog!!.site = null
		}
	}

	fun addSite()
	{
		val site = this.daoUtil.find(Site::class.java, this.siteListValue!!)

		if (site != null)
		{
			if (this.productCatalog!!.sites == null)
			{
				this.productCatalog!!.sites = ArrayList()
			}

			var siteExists = false

			for (cSite in this.productCatalog!!.sites)
			{
				if (cSite.siteID == site.siteID)
				{
					siteExists = true
				}
			}

			if (!siteExists)
			{
				this.productCatalog!!.sites.add(site)
			}
		}
	}

	fun removeSite(site: Site)
	{
		this.productCatalog!!.sites.remove(site)
	}

	fun inSiteList(siteId: Long): Boolean
	{
		for(cSite in this.siteList)
		{
			if(cSite.siteID == siteId)
			{
				return true
			}
		}

		return false
	}
}
