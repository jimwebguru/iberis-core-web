package com.jimwebguru.iberis.core.web.controllers.form

import java.io.Serializable
import java.util.*

import javax.annotation.PostConstruct
import javax.faces.application.FacesMessage
import javax.faces.view.ViewScoped
import javax.inject.Named

import com.jimwebguru.iberis.core.web.SiteSettings
import com.jimwebguru.iberis.core.persistence.*
import com.jimwebguru.iberis.core.web.utils.GeneralUtil
import com.jimwebguru.iberis.core.layout.FormLayout
import com.jimwebguru.iberis.core.utils.db.DaoUtil
import com.jimwebguru.iberis.core.utils.db.QueryParameter
import com.jimwebguru.iberis.core.utils.db.SchemaDB
import org.eclipse.persistence.internal.helper.DatabaseField
import org.eclipse.persistence.internal.sessions.ArrayRecord
import kotlin.collections.HashMap

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
@Named
@ViewScoped
class DataForm : Serializable
{
	private var unitId: Long? = null

	var unit: SystemUnit? = null

	var key: Any? = null

	private val daoUtil = DaoUtil()

	private var siteSettings = GeneralUtil.getManagedBean("siteSettings") as SiteSettings

	var unitInstance: Any? = null

	var tabs: Collection<Tab>? = null
	var selectedTab: String? = null

	private val daoPackage = "com.jimwebguru.iberis.core.persistence."

	var primaryKeyProperty: UnitProperty? = null

	var primaryKeyProperty2: UnitProperty? = null

	var formLayout: FormLayout? = null

	val headerTitle: String
		get()
		{
			if (this.key != null)
			{
				if (this.unit!!.persistenceClass != null && this.unit!!.persistenceClass.isNotEmpty())
				{
					try
					{
						val displayProperty = this.daoUtil.find(UnitProperty::class.java, this.unit!!.lookupDisplayPropertyId)

						val persistenceClass = Class.forName(this.daoPackage + displayProperty!!.unit.persistenceClass)

						val fld = persistenceClass.getDeclaredField(displayProperty.propertyName)

						fld!!.isAccessible = true

						return fld.get(this.unitInstance).toString()
					}
					catch (ex: Exception)
					{
						GeneralUtil.logError(ex)
					}

				}
				else if (this.unit!!.tableName != null && this.unit!!.tableName.isNotEmpty())
				{
					val arrayRecord = this.unitInstance as HashMap<*, *>?

					val displayProperty = this.daoUtil.find(UnitProperty::class.java, this.unit!!.lookupDisplayPropertyId)

					return arrayRecord!![displayProperty!!.propertyName].toString()
				}
			}

			return "<< NEW >>"
		}

	val allUnits: List<SystemUnit>?
		get() = daoUtil.find("SELECT OBJECT(o) FROM SystemUnit o ORDER BY o.unitName")

	@PostConstruct
	fun postInitialization()
	{
		try
		{
			val unitIdParam = GeneralUtil.getQueryStringValue("unitId")
			val keysParam = GeneralUtil.getQueryStringValue("keys")

			if (unitIdParam != null)
			{
				this.unitId = unitIdParam.toLong()
			}

			if (this.unitId != null && this.unitId!! > 0)
			{
				this.unit = this.daoUtil.find(SystemUnit::class.java, this.unitId!!)
				this.unit!!.pluralIcon = this.unit!!.pluralIcon.replace(" ".toRegex(), "%20")
				this.unit!!.singularIcon = this.unit!!.singularIcon.replace(" ".toRegex(), "%20")

				this.primaryKeyProperty = this.daoUtil.find(UnitProperty::class.java, this.unit!!.lookupPropertyPrimaryKey)

				var primaryKey2: Any? = null

				if (this.unit!!.lookupPropertyPrimaryKey2 != null)
				{
					primaryKey2 = this.daoUtil.find(UnitProperty::class.java, this.unit!!.lookupPropertyPrimaryKey2)
				}

				if (primaryKey2 != null)
				{
					this.primaryKeyProperty2 = primaryKey2 as UnitProperty?
				}

				val form = this.daoUtil.find(Form::class.java, this.unit!!.defaultFormId)
				this.tabs = form!!.tabs
				this.formLayout = FormLayout(form.layoutXml)

				if (keysParam != null)
				{
					val keys = keysParam.split(",".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()

					if (keys.isNotEmpty())
					{
						if (this.unit!!.persistenceClass != null && this.unit!!.persistenceClass.isNotEmpty())
						{
							this.selectWithPersistenceClass(keys)
						}
						else if (this.unit!!.tableName != null && this.unit!!.tableName.isNotEmpty())
						{
							this.selectNative(keys)
						}
					}
					else
					{
						this.setNewInstance()
					}
				}
				else
				{
					this.setNewInstance()
				}
			}

		}
		catch (ex: Exception)
		{
			GeneralUtil.logError(ex)
		}

	}

	fun save()
	{
		if (this.key != null)
		{
			if (this.unit!!.persistenceClass != null && this.unit!!.persistenceClass.isNotEmpty())
			{
				this.updateWithPersistenceClass()
			}
			else if (this.unit!!.tableName != null && this.unit!!.tableName.isNotEmpty())
			{
				this.updateNative()
			}
		}
		else
		{
			if (this.unit!!.persistenceClass != null && this.unit!!.persistenceClass.isNotEmpty())
			{
				this.createWithPersistenceClass()
			}
			else if (this.unit!!.tableName != null && this.unit!!.tableName.isNotEmpty())
			{
				this.createNative()
			}
		}
	}

	private fun getNativeParameterValue(arrayRecord: HashMap<*, *>, propertyName: String, propertyType: UnitPropertyType): Any?
	{
		return if ((propertyType.typeId.toInt() == PropertyType.FLEXIBLE_LOOKUP.propertyTypeId || propertyType.typeId.toInt() == PropertyType.LOOKUP.propertyTypeId) && (arrayRecord[propertyName] == null || arrayRecord[propertyName].toString().isEmpty()))
		{
			0
		}
		else
		{
			arrayRecord[propertyName]
		}
	}

	fun delete()
	{
		if (this.key != null)
		{
			try
			{
				if (this.unit!!.persistenceClass != null && this.unit!!.persistenceClass.isNotEmpty())
				{
					this.deleteWithPersistenceClass()
				}
				else if (this.unit!!.tableName != null && this.unit!!.tableName.isNotEmpty())
				{
					this.deleteNative()
				}

			}
			catch (ex: Exception)
			{
				GeneralUtil.logError(ex)
			}

		}
	}

	private fun setNewInstance()
	{
		try
		{
			if (this.unit!!.persistenceClass != null && this.unit!!.persistenceClass.isNotEmpty())
			{
				val persistenceClass = Class.forName(this.daoPackage + this.unit!!.persistenceClass)

				this.unitInstance = persistenceClass.newInstance()

				this.setInstanceMembers()
			}
			else if (this.unit!!.tableName != null && this.unit!!.tableName.isNotEmpty())
			{
				val arrayRecord = HashMap<String,Any?>()

				val unitProperties = this.daoUtil.find<UnitProperty>("SELECT OBJECT(o) FROM UnitProperty AS o WHERE o.unit.unitID = " + this.unit!!.unitID!!)

				for (property in unitProperties!!)
				{
					arrayRecord.put(property.propertyName, null)

					if (property.propertyType.typeId.toInt() == PropertyType.FLEXIBLE_LOOKUP.propertyTypeId)
					{
						arrayRecord.put(property.propertyName + "InstanceId", null)
					}
				}

				this.unitInstance = arrayRecord
			}
		}
		catch (ex: Exception)
		{
			GeneralUtil.logError(ex)
		}

	}

	private fun setInstanceMembers()
	{
		try
		{
			val persistenceClass = Class.forName(this.daoPackage + this.unit!!.persistenceClass)

			for (section in this.formLayout!!.sections)
			{
				for (field in section.fields)
				{
					val property = this.daoUtil.find(UnitProperty::class.java, field.propertyId!!)

					if (property != null && !property.isFakeLookup && property.propertyType.typeId.toInt() == PropertyType.LOOKUP.propertyTypeId)
					{
						val lookupUnit = this.daoUtil.find(SystemUnit::class.java, property.lookupUnitId)

						val lookupField = persistenceClass.getDeclaredField(property.propertyName)

						lookupField.isAccessible = true

						val fieldValue = lookupField.get(this.unitInstance)

						if (fieldValue == null)
						{
							lookupField.set(this.unitInstance, Class.forName(this.daoPackage + lookupUnit!!.persistenceClass).newInstance())
						}
					}
				}
			}
		}
		catch (ex: Exception)
		{
			GeneralUtil.logError(ex)
		}

	}

	private fun clearInstanceMembers()
	{
		try
		{
			val persistenceClass = Class.forName(this.daoPackage + this.unit!!.persistenceClass)

			for (section in this.formLayout!!.sections)
			{
				for (field in section.fields)
				{
					val property = this.daoUtil.find(UnitProperty::class.java, field.propertyId!!)

					if (property!!.propertyType.typeId.toInt() == PropertyType.LOOKUP.propertyTypeId)
					{
						var removeLookupNode = false

						val lookupUnit = this.daoUtil.find(SystemUnit::class.java, property.lookupUnitId)

						val lookupPrimaryKeyProperty = this.daoUtil.find(UnitProperty::class.java, lookupUnit!!.lookupPropertyPrimaryKey)

						val lookupField = persistenceClass.getDeclaredField(property.propertyName)

						lookupField.isAccessible = true

						val fieldValue = lookupField.get(this.unitInstance)

						if (fieldValue != null)
						{
							val lookupClass = Class.forName(this.daoPackage + lookupUnit.persistenceClass)

							val primaryKeyField = lookupClass.getDeclaredField(lookupPrimaryKeyProperty!!.propertyName)

							primaryKeyField.isAccessible = true

							val primaryKeyValue = primaryKeyField.get(fieldValue)

							if (primaryKeyValue == null || primaryKeyValue.toString() == "0" || primaryKeyValue.toString() == "")
							{
								removeLookupNode = true
							}

							val lookupPrimaryKeyProperty2Obj = this.daoUtil.find(UnitProperty::class.java, lookupUnit.lookupPropertyPrimaryKey2)

							if (lookupPrimaryKeyProperty2Obj != null)
							{

								val primaryKey2Field = lookupClass.getDeclaredField(lookupPrimaryKeyProperty2Obj.propertyName)

								primaryKey2Field.isAccessible = true

								val primaryKey2Value = primaryKey2Field.get(fieldValue)

								if (primaryKey2Value == null || primaryKey2Value.toString() == "0" || primaryKey2Value.toString() == "")
								{
									removeLookupNode = true
								}
							}

							if (removeLookupNode)
							{
								lookupField.set(this.unitInstance, null)
							}
						}
					}
				}
			}
		}
		catch (ex: Exception)
		{
			GeneralUtil.logError(ex)
		}

	}

	private fun selectWithPersistenceClass(keys: Array<String>)
	{
		try
		{
			val persistenceClass = Class.forName(this.daoPackage + this.unit!!.persistenceClass)

			this.unitInstance = this.daoUtil.find(persistenceClass, keys[0].toLong())

			this.key = java.lang.Long.valueOf(keys[0])


			this.setInstanceMembers()
		}
		catch (ex: Exception)
		{
			GeneralUtil.logError(ex)
		}

	}

	private fun createWithPersistenceClass()
	{
		this.clearInstanceMembers()

		this.daoUtil.create(unitInstance!!)

		try
		{
			val persistenceClass = Class.forName(this.daoPackage + this.unit!!.persistenceClass)

			val field = persistenceClass.getDeclaredField(this.primaryKeyProperty!!.propertyName)

			field.isAccessible = true

			this.key = field.get(this.unitInstance)
		}
		catch (ex: Exception)
		{
			GeneralUtil.logError(ex)
		}

		if (this.key != null)
		{
			GeneralUtil.addMessage("The " + this.siteSettings.bundle!!.getString(this.unit!!.singularName) + " has been created.", FacesMessage.SEVERITY_INFO)
		}
		else
		{
			GeneralUtil.addMessage("The " + this.siteSettings.bundle!!.getString(this.unit!!.singularName) + " could not be created.", FacesMessage.SEVERITY_ERROR)
		}

		this.setInstanceMembers()
	}

	private fun updateWithPersistenceClass()
	{
		this.clearInstanceMembers()

		if (this.daoUtil.update(this.unitInstance!!))
		{
			GeneralUtil.addMessage("The " + this.siteSettings.bundle!!.getString(this.unit!!.singularName) + " has been updated.", FacesMessage.SEVERITY_INFO)
		}
		else
		{
			GeneralUtil.addMessage("The " + this.siteSettings.bundle!!.getString(this.unit!!.singularName) + " could not be updated.", FacesMessage.SEVERITY_ERROR)
		}

		this.setInstanceMembers()
	}

	private fun deleteWithPersistenceClass()
	{
		if (this.daoUtil.delete(this.unitInstance!!))
		{
			GeneralUtil.addMessage("The " + this.siteSettings.bundle!!.getString(this.unit!!.singularName) + " has been deleted.", FacesMessage.SEVERITY_INFO)

			this.key = null

			this.setNewInstance()
		}
		else
		{
			GeneralUtil.addMessage("The " + this.siteSettings.bundle!!.getString(this.unit!!.singularName) + " has not been deleted.", FacesMessage.SEVERITY_ERROR)
		}
	}

	private fun selectNative(keys: Array<String>)
	{
		var query = "SELECT * FROM " + this.unit!!.tableName + " WHERE " + this.primaryKeyProperty!!.propertyName + " = ?"

		val queryParameters = ArrayList<QueryParameter>()

		var queryParameter = QueryParameter()
		queryParameter.parameterName = this.primaryKeyProperty!!.propertyName
		queryParameter.parameterValue = java.lang.Long.valueOf(keys[0])

		queryParameters.add(queryParameter)

		if (this.primaryKeyProperty2 != null)
		{
			query += " AND ${this.primaryKeyProperty2!!.propertyName} = ?"

			queryParameter = QueryParameter()
			queryParameter.parameterName = this.primaryKeyProperty2!!.propertyName
			queryParameter.parameterValue = java.lang.Long.valueOf(keys[1])

			queryParameters.add(queryParameter)
		}

		val arrayRecord = this.daoUtil.findNativeSingle(query, queryParameters, true) as ArrayRecord

		val hashMap = HashMap<Any?,Any?>()

		for (entryObject in arrayRecord.entries)
		{
			val entry = entryObject as kotlin.collections.Map.Entry<*, *>

			val key = entry.key
			val value = entry.value

			hashMap.put((key as DatabaseField).name, value)
		}

		this.unitInstance = hashMap

		this.key = keys[0].toLong()
	}

	private fun createNative()
	{
		if(this.unitInstance !is HashMap<*,*>) return

		val insertBuilder = StringBuilder()

		insertBuilder.append("INSERT INTO ")
		insertBuilder.append(this.unit!!.tableName)
		insertBuilder.append(" (")

		val columnBuilder = StringBuilder()
		val valueBuilder = StringBuilder()

		val arrayRecord = this.unitInstance as HashMap<Any, Any?>?
		val queryParameters = ArrayList<QueryParameter>()

		var prefix = ""

		val unitProperties = this.daoUtil.find<UnitProperty>("SELECT OBJECT(o) FROM UnitProperty AS o WHERE o.unit.unitID = " + this.unit!!.unitID!!)

		for (property in unitProperties!!)
		{
			if (property.propertyName != this.primaryKeyProperty!!.propertyName &&
					arrayRecord!!.containsKey(property.propertyName) && arrayRecord[property.propertyName] != null &&
					(this.primaryKeyProperty2 == null || this.primaryKeyProperty2 != null && this.primaryKeyProperty2!!.propertyName != property.propertyName))
			{
				columnBuilder.append(prefix)
				columnBuilder.append(property.propertyName)

				if (property.propertyType.typeId.toInt() == PropertyType.FLEXIBLE_LOOKUP.propertyTypeId)
				{
					if (arrayRecord[property.propertyName + "InstanceId"] != null)
					{
						columnBuilder.append(",")
						columnBuilder.append(property.propertyName)
						columnBuilder.append("InstanceId")
					}
				}

				valueBuilder.append(prefix)
				prefix = ","
				valueBuilder.append("?")

				var queryParameter = QueryParameter()
				queryParameter.parameterName = property.propertyName
				queryParameter.parameterValue = this.getNativeParameterValue(arrayRecord, property.propertyName, property.propertyType)
				queryParameters.add(queryParameter)

				if (property.propertyType.typeId.toInt() == PropertyType.FLEXIBLE_LOOKUP.propertyTypeId)
				{
					if (arrayRecord[property.propertyName + "InstanceId"] != null)
					{
						valueBuilder.append(",?")

						queryParameter = QueryParameter()
						queryParameter.parameterName = property.propertyName
						queryParameter.parameterValue = this.getNativeParameterValue(arrayRecord, property.propertyName + "InstanceId", property.propertyType)

						queryParameters.add(queryParameter)
					}
				}
			}
		}

		insertBuilder.append(columnBuilder.toString())
		insertBuilder.append(")")
		insertBuilder.append(" VALUES (")
		insertBuilder.append(valueBuilder.toString())
		insertBuilder.append(")")

		if (queryParameters.size > 0)
		{
			this.key = this.daoUtil.insertNative(SchemaDB(), insertBuilder.toString(), queryParameters)

			if (this.key != null)
			{
				if (this.key as Long > 0)
				{
					arrayRecord!!.put(this.primaryKeyProperty!!.propertyName, this.key)

					GeneralUtil.addMessage("The " + this.siteSettings!!.bundle!!.getString(this.unit!!.singularName) + " has been created.", FacesMessage.SEVERITY_INFO)
				}
				else
				{
					GeneralUtil.addMessage("The " + this.siteSettings!!.bundle!!.getString(this.unit!!.singularName) + " could not be created.", FacesMessage.SEVERITY_ERROR)
				}
			}
		}
	}

	private fun updateNative()
	{
		val updateBuilder = StringBuilder()
		updateBuilder.append("UPDATE ")
		updateBuilder.append(this.unit!!.tableName)
		updateBuilder.append(" SET ")

		val arrayRecord = this.unitInstance as HashMap<*, *>?
		val queryParameters = ArrayList<QueryParameter>()

		var prefix = ""

		val unitProperties = this.daoUtil.find<UnitProperty>("SELECT OBJECT(o) FROM UnitProperty AS o WHERE o.unit.unitID = " + this.unit!!.unitID!!)

		for (property in unitProperties!!)
		{
			if (property.propertyName != this.primaryKeyProperty!!.propertyName &&
					arrayRecord!!.containsKey(property.propertyName) && arrayRecord[property.propertyName] != null &&
					(this.primaryKeyProperty2 == null || this.primaryKeyProperty2 != null && this.primaryKeyProperty2!!.propertyName != property.propertyName))
			{
				updateBuilder.append(prefix)
				prefix = ","
				updateBuilder.append(property.propertyName)
				updateBuilder.append(" = ?")

				if (property.propertyType.typeId.toInt() == PropertyType.FLEXIBLE_LOOKUP.propertyTypeId)
				{
					if (arrayRecord[property.propertyName + "InstanceId"] != null)
					{
						updateBuilder.append(",")
						updateBuilder.append(property.propertyName)
						updateBuilder.append("InstanceId = ?")
					}
				}

				var queryParameter = QueryParameter()
				queryParameter.parameterName = property.propertyName
				queryParameter.parameterValue = this.getNativeParameterValue(arrayRecord, property.propertyName, property.propertyType)

				queryParameters.add(queryParameter)

				if (property.propertyType.typeId.toInt() == PropertyType.FLEXIBLE_LOOKUP.propertyTypeId)
				{
					if (arrayRecord[property.propertyName + "InstanceId"] != null)
					{
						queryParameter = QueryParameter()
						queryParameter.parameterName = property.propertyName + "InstanceId"
						queryParameter.parameterValue = this.getNativeParameterValue(arrayRecord, property.propertyName + "InstanceId", property.propertyType)

						queryParameters.add(queryParameter)
					}
				}
			}
		}

		updateBuilder.append(" WHERE ")
		updateBuilder.append(this.primaryKeyProperty!!.propertyName)
		updateBuilder.append(" = ?")

		var queryParameter = QueryParameter()
		queryParameter.parameterName = this.primaryKeyProperty!!.propertyName
		queryParameter.parameterValue = java.lang.Long.valueOf(arrayRecord!![this.primaryKeyProperty!!.propertyName].toString())

		queryParameters.add(queryParameter)

		if (this.primaryKeyProperty2 != null)
		{
			updateBuilder.append(" AND ")
			updateBuilder.append(this.primaryKeyProperty2!!.propertyName)
			updateBuilder.append(" = ?")

			queryParameter = QueryParameter()
			queryParameter.parameterName = this.primaryKeyProperty2!!.propertyName
			queryParameter.parameterValue = java.lang.Long.valueOf(arrayRecord[this.primaryKeyProperty2!!.propertyName].toString())

			queryParameters.add(queryParameter)
		}

		if (queryParameters.size > 0)
		{
			val updateCount = this.daoUtil.updateNative(updateBuilder.toString(), queryParameters)

			if (updateCount > 0)
			{
				GeneralUtil.addMessage("The " + this.siteSettings!!.bundle!!.getString(this.unit!!.singularName) + " has been updated.", FacesMessage.SEVERITY_INFO)
			}
			else
			{
				GeneralUtil.addMessage("The " + this.siteSettings!!.bundle!!.getString(this.unit!!.singularName) + " could not be updated.", FacesMessage.SEVERITY_ERROR)
			}
		}
	}

	private fun deleteNative()
	{
		val arrayRecord = this.unitInstance as HashMap<*, *>?

		val deleteBuilder = StringBuilder()
		deleteBuilder.append("DELETE FROM ")
		deleteBuilder.append(this.unit!!.tableName)
		deleteBuilder.append(" WHERE ")
		deleteBuilder.append(this.primaryKeyProperty!!.propertyName)
		deleteBuilder.append(" = ?")

		val queryParameters = ArrayList<QueryParameter>()

		var queryParameter = QueryParameter()
		queryParameter.parameterName = this.primaryKeyProperty!!.propertyName
		queryParameter.parameterValue = java.lang.Long.valueOf(arrayRecord!![this.primaryKeyProperty!!.propertyName].toString())

		queryParameters.add(queryParameter)

		if (this.primaryKeyProperty2 != null)
		{
			deleteBuilder.append(" AND ")
			deleteBuilder.append(this.primaryKeyProperty2!!.propertyName)
			deleteBuilder.append(" = ?")

			queryParameter = QueryParameter()
			queryParameter.parameterName = this.primaryKeyProperty2!!.propertyName
			queryParameter.parameterValue = java.lang.Long.valueOf(arrayRecord[this.primaryKeyProperty2!!.propertyName].toString())

			queryParameters.add(queryParameter)
		}

		val deleteCount = this.daoUtil.updateNative(deleteBuilder.toString(), queryParameters)

		if (deleteCount > 0)
		{
			GeneralUtil.addMessage("The " + this.siteSettings!!.bundle!!.getString(this.unit!!.singularName) + " has been deleted.", FacesMessage.SEVERITY_INFO)

			this.key = null

			this.setNewInstance()
		}
		else
		{
			GeneralUtil.addMessage("The " + this.siteSettings!!.bundle!!.getString(this.unit!!.singularName) + " has not been deleted.", FacesMessage.SEVERITY_ERROR)
		}
	}

	fun setUnitId(unitId: Long?)
	{
		this.unitId = unitId
	}
}
