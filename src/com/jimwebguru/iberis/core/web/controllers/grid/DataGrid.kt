/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jimwebguru.iberis.core.web.controllers.grid

import java.io.Serializable
import java.util.ArrayList

import javax.annotation.PostConstruct
import javax.faces.application.FacesMessage
import javax.faces.view.ViewScoped
import javax.inject.Named

import com.jimwebguru.iberis.core.loaders.GenericLazyLoader
import com.jimwebguru.iberis.core.loaders.LazyLoader
import com.jimwebguru.iberis.core.loaders.NativeLazyLoader
import com.jimwebguru.iberis.core.persistence.*
import com.jimwebguru.iberis.core.utils.db.DaoUtil
import com.jimwebguru.iberis.core.utils.db.QueryParameter
import com.jimwebguru.iberis.core.web.utils.GeneralUtil
import org.primefaces.event.SelectEvent
import org.primefaces.event.ToggleSelectEvent
import org.primefaces.event.UnselectEvent
import org.primefaces.event.data.PageEvent

import com.jimwebguru.iberis.core.web.SiteSettings


/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
@Named
@ViewScoped
class DataGrid : Serializable
{
	var unit: SystemUnit? = null

	private var referencedProperty: UnitProperty? = null
	private var primaryKeyProperty: UnitProperty? = null
	private var primaryKey2Property: UnitProperty? = null
	private var displayProperty: UnitProperty? = null

	private var flexibleUnitReference: Any? = null
	private var referenceValue: Any? = null
	var selectedObject: Any? = null

	private var referenceTargetUnitId: Long? = null
	var unitId: Long? = null
	private var joinTabId: Long? = null

	var selectedObjects: ArrayList<Any?>? = null
	var multiSelectedObjects: Array<Any?>? = null

	private val daoPackage = "com.jimwebguru.iberis.core.persistence."

	var selectionDisplayValue: String? = null
	var selectionPrimaryKeyValue: String? = null
	var selectionPrimaryKey2Value: String? = null
	var referencedDisplayField: String? = null
	var referencedPrimaryKeyField: String? = null
	var referencedPrimaryKeyField2: String? = null
	var selectionMode = "single"
	var callbackMethod = ""
	private var filter = ""

	private val editable = false
	var isSelectionCarryOver = false
	var isLookupMode = false

	private val daoUtil = DaoUtil()

	private var siteSettings = GeneralUtil.getManagedBean("siteSettings") as SiteSettings

	private var joinTab: Tab? = null

	var lazyLoader: LazyLoader? = null

	private val lookupReferenceProperty: UnitProperty?
		get()
		{
			var refLookupPrimaryKeyProperty: UnitProperty? = null

			if (this.referencedProperty != null && this.referencedProperty!!.propertyType.typeId.toInt() == PropertyType.LOOKUP.propertyTypeId && !this.referencedProperty!!.isFakeLookup)
			{
				if ((this.referenceTargetUnitId == null || this.referenceTargetUnitId == 0L) && this.referencedProperty!!.lookupUnitId != null && this.referenceValue != null)
				{
					this.referenceTargetUnitId = this.referencedProperty!!.lookupUnitId
				}

				if (this.referenceTargetUnitId != null && this.referenceTargetUnitId != 0L)
				{
					val referencedTargetUnit = this.daoUtil.find(SystemUnit::class.java, this.referenceTargetUnitId!!)

					val lookupPrimaryKeyPropertyId = referencedTargetUnit!!.lookupPropertyPrimaryKey

					refLookupPrimaryKeyProperty = this.daoUtil.find(UnitProperty::class.java, lookupPrimaryKeyPropertyId)
				}
			}

			return refLookupPrimaryKeyProperty
		}

	val formUrl: String
		get()
		{
			val unitKeys = GeneralUtil.getValueOfExpression(this.unitPrimaryKeyExpression, String::class.java)

			return this.siteSettings!!.siteUrl + "/unit-form/" + this.unitId + "/" + unitKeys
		}

	val cmsFormUrl: String
		get()
		{
			val unitKeys = GeneralUtil.getValueOfExpression(this.unitPrimaryKeyExpression, String::class.java)

			return this.siteSettings!!.siteUrl + "/cms-unit-form/" + this.unitId + "/" + unitKeys
		}

	val cmsReadOnlyFormUrl: String
		get()
		{
			val unitKeys = GeneralUtil.getValueOfExpression(this.unitPrimaryKeyExpression, String::class.java)

			return this.siteSettings!!.siteUrl + "/cms-read-unit-form/" + this.unitId + "/" + unitKeys
		}

	private val unitPrimaryKeyExpression: String
		get()
		{
			val primaryKeyProperty = this.daoUtil.find(UnitProperty::class.java, this.unit!!.lookupPropertyPrimaryKey)

			var primaryKey2Property: UnitProperty? = null

			if (this.unit!!.lookupPropertyPrimaryKey2 != null)
			{
				primaryKey2Property = this.daoUtil.find(UnitProperty::class.java, this.unit!!.lookupPropertyPrimaryKey2)
			}

			var primaryKeyExpression = "#{item." + primaryKeyProperty!!.propertyName

			if (primaryKey2Property != null)
			{
				primaryKeyExpression += "},#{item." + primaryKey2Property.propertyName
			}

			primaryKeyExpression += "}"

			return primaryKeyExpression
		}

	@PostConstruct
	fun postInitialization()
	{
		try
		{
			this.instantiateVariablesFromUrl()

			this.selectedObjects = ArrayList<Any?>()

			if (this.unitId != null && this.unitId!! > 0)
			{
				if (this.joinTabId != null && this.joinTabId!! > 0)
				{
					this.joinTab = this.daoUtil.find(Tab::class.java, this.joinTabId!!)

					if (this.joinTab!!.referencedUnitProperty != null)
					{
						this.referencedProperty = this.daoUtil.find(UnitProperty::class.java, this.joinTab!!.referencedUnitProperty)
					}
				}

				this.unit = this.daoUtil.find(SystemUnit::class.java, this.unitId!!)
				this.unit!!.pluralIcon = this.unit!!.pluralIcon.replace(" ".toRegex(), "%20")
				this.unit!!.singularIcon = this.unit!!.singularIcon.replace(" ".toRegex(), "%20")

				this.primaryKeyProperty = this.daoUtil.find(UnitProperty::class.java, this.unit!!.lookupPropertyPrimaryKey)
				this.displayProperty = this.daoUtil.find(UnitProperty::class.java, this.unit!!.lookupDisplayPropertyId)

				if (this.unit!!.lookupPropertyPrimaryKey2 != null)
				{
					this.primaryKey2Property = this.daoUtil.find(UnitProperty::class.java, this.unit!!.lookupPropertyPrimaryKey2)
				}

				try
				{
					if (this.unit!!.persistenceClass != null && !this.unit!!.persistenceClass.isEmpty())
					{
						val persistenceClass = Class.forName(this.daoPackage + this.unit!!.persistenceClass)

						this.lazyLoader = GenericLazyLoader(persistenceClass)
					}
					else if (this.unit!!.tableName != null && !this.unit!!.tableName.isEmpty())
					{
						this.lazyLoader = NativeLazyLoader(this.unit!!.tableName, this.primaryKeyProperty!!.propertyName, if (this.primaryKey2Property != null) this.primaryKey2Property!!.propertyName else null)
					}

					val refLookupPrimaryKeyProperty = this.lookupReferenceProperty

					this.buildAdditionalQueryParameters(refLookupPrimaryKeyProperty)
				}
				catch (ex: Exception)
				{
					GeneralUtil.logError(ex)
				}

			}
		}
		catch (ex: Exception)
		{
			GeneralUtil.logError(ex)
		}

	}

	private fun instantiateVariablesFromUrl()
	{
		if (GeneralUtil.getQueryStringValue("unitId") != null && GeneralUtil.getQueryStringValue("unitId")!!.isNotEmpty())
		{
			this.unitId = java.lang.Long.valueOf(GeneralUtil.getQueryStringValue("unitId"))
		}
		else if (GeneralUtil.getQueryStringValue("urlAlias") != null && GeneralUtil.getQueryStringValue("urlAlias")!!.isNotEmpty())
		{
			val content = this.daoUtil.find<Content>("SELECT OBJECT(o) FROM Content o WHERE o.urlAlias = '" + GeneralUtil.getQueryStringValue("urlAlias") + "'")

			if (content != null)
			{
				val item = content[0]

				if (item.type == "unit_grid")
				{
					this.unitId = item.unit.unitID
				}
			}

		}

		if (GeneralUtil.getQueryStringValue("referenceValue") != null && GeneralUtil.getQueryStringValue("referenceValue")!!.isNotEmpty())
		{
			this.referenceValue = GeneralUtil.getQueryStringValue("referenceValue")
		}

		if (GeneralUtil.getQueryStringValue("flexibleUnitReference") != null && GeneralUtil.getQueryStringValue("flexibleUnitReference")!!.isNotEmpty())
		{
			this.flexibleUnitReference = GeneralUtil.getQueryStringValue("flexibleUnitReference")
		}

		if (GeneralUtil.getQueryStringValue("joinTabId") != null && GeneralUtil.getQueryStringValue("joinTabId")!!.isNotEmpty())
		{
			this.joinTabId = java.lang.Long.valueOf(GeneralUtil.getQueryStringValue("joinTabId"))
		}

		if (GeneralUtil.getQueryStringValue("referencedDisplayField") != null && GeneralUtil.getQueryStringValue("referencedDisplayField")!!.isNotEmpty())
		{
			this.referencedDisplayField = GeneralUtil.getQueryStringValue("referencedDisplayField")
		}

		if (GeneralUtil.getQueryStringValue("referencedPrimaryKeyField") != null && GeneralUtil.getQueryStringValue("referencedPrimaryKeyField")!!.isNotEmpty())
		{
			this.referencedPrimaryKeyField = GeneralUtil.getQueryStringValue("referencedPrimaryKeyField")
		}

		if (GeneralUtil.getQueryStringValue("referencedPrimaryKeyField2") != null && GeneralUtil.getQueryStringValue("referencedPrimaryKeyField2")!!.isNotEmpty())
		{
			this.referencedPrimaryKeyField2 = GeneralUtil.getQueryStringValue("referencedPrimaryKeyField2")
		}

		if (GeneralUtil.getQueryStringValue("selectionMode") != null && GeneralUtil.getQueryStringValue("selectionMode")!!.isNotEmpty())
		{
			this.selectionMode = GeneralUtil.getQueryStringValue("selectionMode").toString()
		}

		if (GeneralUtil.getQueryStringValue("lookupMode") != null && GeneralUtil.getQueryStringValue("lookupMode")!!.isNotEmpty())
		{
			this.isLookupMode = GeneralUtil.getQueryStringValue("lookupMode")!!.toBoolean()
		}

		if (GeneralUtil.getQueryStringValue("callbackMethod") != null && GeneralUtil.getQueryStringValue("callbackMethod")!!.isNotEmpty())
		{
			this.callbackMethod = GeneralUtil.getQueryStringValue("callbackMethod")!!
		}

		if (GeneralUtil.getQueryStringValue("filter") != null && GeneralUtil.getQueryStringValue("filter")!!.isNotEmpty())
		{
			this.filter = GeneralUtil.getQueryStringValue("filter")!!
		}
	}

	private fun buildAdditionalQueryParameters(refLookupPrimaryKeyProperty: UnitProperty?)
	{
		val queryParameters = ArrayList<QueryParameter>()

		if (this.referencedProperty != null && this.referenceValue != null)
		{
			if (this.referencedProperty!!.propertyType.typeId.toInt() == PropertyType.FLEXIBLE_LOOKUP.propertyTypeId)
			{
				var queryParameter = QueryParameter()

				queryParameter.objectProperty = this.referencedProperty!!.propertyName
				queryParameter.conditionOperator = "="
				queryParameter.parameterName = this.referencedProperty!!.propertyName
				queryParameter.parameterValue = java.lang.Long.valueOf(this.flexibleUnitReference!!.toString())

				queryParameters.add(queryParameter)

				queryParameter = QueryParameter()
				queryParameter.objectProperty = this.referencedProperty!!.propertyName + "InstanceId"
				queryParameter.conditionOperator = "="
				queryParameter.parameterName = this.referencedProperty!!.propertyName + "InstanceId"
				queryParameter.parameterValue = java.lang.Long.valueOf(this.referenceValue!!.toString())

				queryParameters.add(queryParameter)
			}
			else
			{
				val queryParameter = QueryParameter()
				queryParameter.objectProperty = this.referencedProperty!!.propertyName

				if (refLookupPrimaryKeyProperty != null)
				{
					queryParameter.objectProperty = queryParameter.objectProperty + "." + refLookupPrimaryKeyProperty.propertyName
				}

				queryParameter.conditionOperator = "="
				queryParameter.parameterName = this.referencedProperty!!.propertyName
				queryParameter.parameterValue = java.lang.Long.valueOf(this.referenceValue!!.toString())

				queryParameters.add(queryParameter)
			}
		}
		else if (this.joinTab != null && this.joinTab!!.customQuery != null && this.joinTab!!.customQuery.isNotEmpty() && this.referenceValue != null)
		{
			val query = String.format(this.joinTab!!.customQuery, this.referenceValue!!.toString())

			val querySelectPart = query.substring(0, query.indexOf(" FROM "))

			val selectParts = querySelectPart.split(" ".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()

			val newSelectPart = "SELECT COUNT(" + selectParts[1] + ")"

			val countQuery = query.replaceFirst(querySelectPart.toRegex(), newSelectPart)

			this.lazyLoader!!.customQuery = query
			this.lazyLoader!!.customCountQuery = countQuery
		}
		else if (this.joinTab != null && this.joinTab!!.customQuery != null)
		{
			val query = this.joinTab!!.customQuery

			val querySelectPart = query.substring(0, query.indexOf(" FROM "))

			val selectParts = querySelectPart.split(" ".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()

			val newSelectPart = "SELECT COUNT(" + selectParts[1] + ")"

			val countQuery = query.replaceFirst(querySelectPart.toRegex(), newSelectPart)

			this.lazyLoader!!.customQuery = query
			this.lazyLoader!!.customCountQuery = countQuery
		}

		if (this.filter.isNotEmpty())
		{
			val queryParameter = QueryParameter()

			if (this.filter.contains("JOIN") && this.filter.contains("WHERE"))
			{
				queryParameter.joinClause = this.filter.substring(0, this.filter.indexOf(" WHERE ") + 1)

				this.filter = this.filter.replaceFirst(this.filter.substring(0, this.filter.indexOf(" WHERE ") + 7).toRegex(), "")
			}

			queryParameter.explicitCondition = this.filter

			queryParameters.add(queryParameter)
		}

		if (queryParameters.size > 0)
		{
			this.lazyLoader!!.additionalQueryParams = queryParameters
		}
	}

	fun delete()
	{
		var allRecordsDeleted = true

		if (this.selectionMode == "multiple")
		{
			if (this.selectedObjects != null && this.selectedObjects!!.size > 0)
			{
				for (unitObj in this.selectedObjects!!)
				{
					if (!this.daoUtil.delete(unitObj!!))
					{
						allRecordsDeleted = false
					}
				}

				this.selectedObjects!!.clear()
				this.multiSelectedObjects = this.selectedObjects!!.toTypedArray()
			}
		}
		else if (this.selectedObject != null)
		{
			if (!this.daoUtil.delete(this.selectedObject!!))
			{
				allRecordsDeleted = false
			}
			else
			{
				this.selectedObject = null
			}
		}

		if (allRecordsDeleted)
		{
			GeneralUtil.addMessage("Records have been deleted.", FacesMessage.SEVERITY_INFO)
		}
		else
		{
			GeneralUtil.addMessage("Some records were not deleted.", FacesMessage.SEVERITY_WARN)
		}
	}

	fun onRowSelect(event: SelectEvent)
	{
		if (this.selectionMode == "multiple")
		{
			if (!this.selectedObjects!!.contains(event.`object`))
			{
				this.selectedObjects!!.add(event.`object`)

				this.multiSelectedObjects = this.selectedObjects!!.toTypedArray()
			}
		}
		else
		{
			this.selectedObject = event.`object`
		}

		if (this.isLookupMode)
		{
			this.populateLookupValues()
		}
	}

	fun onRowUnselect(event: UnselectEvent)
	{
		if (this.selectionMode == "multiple")
		{
			if (this.selectedObjects!!.contains(event.`object`))
			{
				this.selectedObjects!!.remove(event.`object`)

				this.multiSelectedObjects = this.selectedObjects!!.toTypedArray()
			}
		}
		else
		{
			this.selectedObject = null
		}

		if (this.isLookupMode)
		{
			this.clearLookupValues()
		}
	}

	fun onRowToggle(event: ToggleSelectEvent)
	{
		if (event.isSelected)
		{
			for (dataObject in this.lazyLoader!!.resultList!!)
			{
				if (!this.selectedObjects!!.contains(dataObject))
				{
					this.selectedObjects!!.add(dataObject)
				}
			}
		}
		else
		{
			this.selectedObject = null
			this.selectedObjects!!.clear()
		}

		this.multiSelectedObjects = this.selectedObjects!!.toTypedArray()
	}

	fun onPageChange(event: PageEvent)
	{
		if (!this.isSelectionCarryOver)
		{
			this.selectedObject = null
			this.selectedObjects!!.clear()
		}

		this.multiSelectedObjects = this.selectedObjects!!.toTypedArray()
	}

	private fun populateLookupValues()
	{
		if (this.selectedObject != null)
		{
			try
			{
				val persistenceClass = Class.forName(this.daoPackage + this.unit!!.persistenceClass)

				val displayField = persistenceClass.getDeclaredField(this.displayProperty!!.propertyName)

				val primaryField = persistenceClass.getDeclaredField(this.primaryKeyProperty!!.propertyName)

				displayField.isAccessible = true
				primaryField.isAccessible = true

				var displayValue: Any? = displayField.get(this.selectedObject)

				if (this.displayProperty!!.propertyType.typeId == java.lang.Long.valueOf("3"))
				{
					if (displayValue != null)
					{
						val lookupUnit = this.daoUtil.find(SystemUnit::class.java, this.displayProperty!!.lookupUnitId)

						val lookupDisplayProperty = this.daoUtil.find(UnitProperty::class.java, lookupUnit!!.lookupDisplayPropertyId)

						val lookupClass = Class.forName(this.daoPackage + lookupUnit.persistenceClass)

						val lookupDisplayField = lookupClass.getDeclaredField(lookupDisplayProperty!!.propertyName)

						lookupDisplayField.isAccessible = true

						displayValue = lookupDisplayField.get(displayValue)
					}
				}

				val primaryKeyValue = primaryField.get(this.selectedObject)

				if (displayValue != null)
				{
					this.selectionDisplayValue = displayValue.toString()
				}

				this.selectionPrimaryKeyValue = primaryKeyValue.toString()

				if (this.primaryKey2Property != null)
				{
					val primary2Field = persistenceClass.getDeclaredField(this.primaryKey2Property!!.propertyName)
					primary2Field.isAccessible = true

					val primaryKey2Value = primary2Field.get(this.selectedObject)

					if (primaryKey2Value != null)
					{
						this.selectionPrimaryKey2Value = primaryKey2Value.toString()
					}
				}
			}
			catch (ex: Exception)
			{
				GeneralUtil.logError(ex)
			}

		}
	}

	private fun clearLookupValues()
	{
		this.selectionDisplayValue = ""
		this.selectionPrimaryKeyValue = ""
		this.selectionPrimaryKey2Value = ""
	}

	fun refresh()
	{
		//An empty method so that the datatable can be refreshed via ajax
		this.multiSelectedObjects = this.selectedObjects!!.toTypedArray()
	}

	fun enableLookupMode()
	{
		this.isLookupMode = true
	}

	fun enableMultiple()
	{
		this.selectionMode = "multiple"
	}
}
