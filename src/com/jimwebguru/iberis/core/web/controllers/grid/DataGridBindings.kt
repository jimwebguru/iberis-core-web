/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jimwebguru.iberis.core.web.controllers.grid

import java.io.Serializable
import java.util.ArrayList

import javax.annotation.PostConstruct
import javax.enterprise.context.RequestScoped
import javax.faces.component.UIComponent
import javax.inject.Named

import com.jimwebguru.iberis.core.persistence.*
import com.jimwebguru.iberis.core.utils.db.DaoUtil
import com.jimwebguru.iberis.core.web.utils.GeneralUtil
import org.primefaces.component.panel.Panel

import com.jimwebguru.iberis.core.web.SiteSettings
import com.jimwebguru.iberis.core.web.components.ComponentsUtil

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
@Named
@RequestScoped
class DataGridBindings : Serializable
{
	var searchPanel: Panel? = null

	private val daoUtil = DaoUtil()

	private var siteSettings = GeneralUtil.getManagedBean("siteSettings") as SiteSettings

	private var unit: SystemUnit? = null

	var grid: Grid? = null
		private set

	var gridColumns: Collection<GridColumn>? = null

	private val daoPackage = "com.jimwebguru.iberis.core.persistence."

	private var dataGrid: DataGrid? = null

	@PostConstruct
	fun postInitialization()
	{
		try
		{
			this.dataGrid = GeneralUtil.getManagedBean("dataGrid") as DataGrid

			this.unit = this.dataGrid!!.unit

			if (this.unit != null)
			{
				this.grid = this.daoUtil.find(Grid::class.java, unit!!.defaultGridId)

				this.gridColumns = this.grid!!.gridColumns

				this.searchPanel = Panel()
			}
		}
		catch (ex: Exception)
		{
			GeneralUtil.logError(ex)
		}

	}

	fun getDisplayedPropertyName(property: UnitProperty?): String
	{
		var propertyName = "." + property!!.propertyName

		if (property.propertyType.typeId.toInt() == PropertyType.LOOKUP.propertyTypeId)
		{
			val lookupUnit = this.daoUtil.find(SystemUnit::class.java, property.lookupUnitId)

			val displayProperty = this.daoUtil.find(UnitProperty::class.java, lookupUnit!!.lookupDisplayPropertyId)

			propertyName += this.getDisplayedPropertyName(displayProperty)
		}

		return propertyName
	}

	fun fakeLookupDisplayValue(unitId: Long, instanceId: Long): String
	{
		try
		{
			if (instanceId > 0)
			{
				val fakeLookupUnit = this.daoUtil.find(SystemUnit::class.java, unitId)

				val persistenceClass = Class.forName(this.daoPackage + fakeLookupUnit!!.persistenceClass)

				val displayProperty = this.daoUtil.find(UnitProperty::class.java, fakeLookupUnit.lookupDisplayPropertyId)

				val objectInstance = this.daoUtil.find(persistenceClass, instanceId)

				if (objectInstance != null)
				{
					val field = persistenceClass.getDeclaredField(displayProperty!!.propertyName)
					field.isAccessible = true

					val fieldValue = field.get(objectInstance)

					if (fieldValue != null)
					{
						return fieldValue.toString()
					}
				}
			}
		}
		catch (ex: Exception)
		{
			GeneralUtil.logError(ex)
		}

		return ""
	}

	fun booleanDisplayValue(booleanValue: String): String
	{
		return if (booleanValue == "true")
		{
			"Yes"
		}
		else "No"

	}

	fun flexibleLookupDisplayValue(unitId: Long): String?
	{
		if (unitId != 0L)
		{
			val flexibleLookupUnit = this.daoUtil.find(SystemUnit::class.java, unitId)

			return this.siteSettings!!.bundle!!.getString(flexibleLookupUnit!!.singularName)
		}

		return null
	}

	fun getColumnValue(gColumn: GridColumn): String
	{
		var columnValue: Any? = null

		if (gColumn.unitProperty.propertyType.typeId.toInt() == PropertyType.LOOKUP.propertyTypeId && gColumn.unitProperty.isFakeLookup!!)
		{
			columnValue = GeneralUtil.getValueOfExpression("#{dataGridBindings.fakeLookupDisplayValue(" + gColumn.unitProperty.lookupUnitId + ",item." + gColumn.unitProperty.propertyName + ")}", Any::class.java)
		}
		else if (gColumn.unitProperty.propertyType.typeId.toInt() == PropertyType.FLEXIBLE_LOOKUP.propertyTypeId)
		{
			columnValue = GeneralUtil.getValueOfExpression("#{dataGridBindings.fakeLookupDisplayValue(item." + gColumn.unitProperty.propertyName + ",item." + gColumn.unitProperty.propertyName + "InstanceId)}", Any::class.java)
		}
		else if (gColumn.unitProperty.propertyType.typeId.toInt() == PropertyType.BOOLEAN.propertyTypeId)
		{
			columnValue = GeneralUtil.getValueOfExpression("#{dataGridBindings.booleanDisplayValue(item." + gColumn.unitProperty.propertyName + ")}", Any::class.java)
		}
		else
		{
			columnValue = GeneralUtil.getValueOfExpression("#{item" + this.getDisplayedPropertyName(gColumn.unitProperty) + "}", Any::class.java)
		}

		return if (columnValue != null)
		{
			columnValue.toString()
		}
		else ""

	}

	fun search()
	{
		if (this.grid != null)
		{
			val queryConditions = ArrayList<String>()

			for (column in this.grid!!.searchColumns)
			{
				var searchPropertyName = column.unitProperty.propertyName

				var searchPropertyInstanceId: String? = null

				var searchValue: String?
				var searchInstanceValue: String? = null

				var comp: UIComponent?

				if (column.unitProperty.propertyType.typeId.toInt() == PropertyType.LOOKUP.propertyTypeId)
				{
					val lookupUnit = this.daoUtil.find(SystemUnit::class.java, column.unitProperty.lookupUnitId)

					val primaryKeyProperty = this.daoUtil.find(UnitProperty::class.java, lookupUnit!!.lookupPropertyPrimaryKey)

					searchPropertyName = searchPropertyName + "." + primaryKeyProperty!!.propertyName

					comp = GeneralUtil.getComponentById(this.searchPanel!!.parent.id + ":" + column.unitProperty.propertyName + "_" + primaryKeyProperty.propertyName)
				}
				else if (column.unitProperty.propertyType.typeId.toInt() == PropertyType.FLEXIBLE_LOOKUP.propertyTypeId)
				{
					comp = GeneralUtil.getComponentById(this.searchPanel!!.parent.id + ":" + column.unitProperty.propertyName + "UnitSelector")

					searchPropertyInstanceId = column.unitProperty.propertyName + "InstanceId"

					searchInstanceValue = ComponentsUtil.getComponentValue(GeneralUtil.getComponentById(this.searchPanel!!.parent.id + ":" + searchPropertyInstanceId)!!)
				}
				else
				{
					comp = GeneralUtil.getComponentById(this.searchPanel!!.parent.id + ":" + column.unitProperty.propertyName)
				}

				searchValue = ComponentsUtil.getComponentValue(comp!!)

				if (searchValue != null)
				{
					if (searchValue.isNotEmpty())
					{
						var queryCondition = "o." + searchPropertyName + " " + column.conditionOperator.operator + " "

						/*var columnDataType = column.unitProperty.dataType

						if (this.queryNeedsQuote(columnDataType))
						{
							queryCondition += "'"
						}

						queryCondition += searchValue

						if (column.conditionOperator.condition == "LIKE")
						{
							queryCondition += "%"
						}

						if (this.queryNeedsQuote(columnDataType))
						{
							queryCondition += "'"
						}

						if (searchPropertyInstanceId != null && searchInstanceValue != null)
						{
							queryCondition += " AND o." + searchPropertyInstanceId + " " + column.conditionOperator.operator + " "

							columnDataType = column.unitProperty.dataType

							if (this.queryNeedsQuote(columnDataType))
							{
								queryCondition += "'"
							}

							queryCondition += searchInstanceValue

							if (column.conditionOperator.condition == "LIKE")
							{
								queryCondition += "%"
							}

							if (this.queryNeedsQuote(columnDataType))
							{
								queryCondition += "'"
							}
						}  */

						queryConditions.add(queryCondition)
					}
				}
			}

			//this.dataGrid.setQueryConditions(queryConditions);
		}
	}
}
