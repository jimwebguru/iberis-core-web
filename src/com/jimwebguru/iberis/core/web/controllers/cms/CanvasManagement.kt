package com.jimwebguru.iberis.core.web.controllers.cms

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
import com.jimwebguru.iberis.core.loaders.GenericLazyLoader
import com.jimwebguru.iberis.core.persistence.*
import com.jimwebguru.iberis.core.utils.db.DaoUtil
import com.jimwebguru.iberis.core.web.utils.GeneralUtil
import com.jimwebguru.iberis.core.web.utils.UploadUtil
import javax.annotation.PostConstruct
import javax.faces.application.FacesMessage
import javax.faces.view.ViewScoped
import javax.inject.Named
import java.io.Serializable

@Named
@ViewScoped
class CanvasManagement : Serializable
{
	private var daoUtil = DaoUtil()

	var canvas: Canvas? = Canvas()

	var lazyLoader = GenericLazyLoader(Canvas::class.java)

	var imageLazyLoader = GenericLazyLoader(Image::class.java)

	var imageData: String? = null

	@PostConstruct
	fun postInit()
	{
		if (GeneralUtil.getRequestParameterValue("id") != null)
		{
			this.canvas = this.daoUtil.find(Canvas::class.java, GeneralUtil.getRequestParameterValue("id").toString().toLong())
		}
	}

	fun saveCanvas()
	{
		if (this.canvas!!.id != null && this.canvas!!.id > 0)
		{
			if (this.daoUtil.update(this.canvas!!))
			{
				GeneralUtil.addMessage("Canvas has been updated.", FacesMessage.SEVERITY_INFO)
			}
			else
			{
				GeneralUtil.addMessage("Canvas could not be updated.", FacesMessage.SEVERITY_ERROR)
			}
		}
		else
		{
			if (this.daoUtil.create(this.canvas!!))
			{
				GeneralUtil.addMessage("Canvas has been created.", FacesMessage.SEVERITY_INFO)
			}
			else
			{
				GeneralUtil.addMessage("Canvas could not be created.", FacesMessage.SEVERITY_ERROR)
			}
		}
	}

	fun deleteCanvas(c: Canvas)
	{
		if (c.id != null && c.id > 0)
		{
			this.daoUtil.delete(c)

			GeneralUtil.addMessage("Canvas has been deleted.", FacesMessage.SEVERITY_INFO)
		}

	}

	fun createCanvasImage()
	{
		UploadUtil.uploadImage(this.daoUtil, this.canvas!!.name + ".png", this.imageData)
	}
}