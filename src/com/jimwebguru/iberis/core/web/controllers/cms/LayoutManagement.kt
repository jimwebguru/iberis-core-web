package com.jimwebguru.iberis.core.web.controllers.cms

import com.jimwebguru.iberis.core.loaders.GenericLazyLoader
import com.jimwebguru.iberis.core.persistence.*
import com.jimwebguru.iberis.core.utils.db.DaoUtil
import com.jimwebguru.iberis.core.web.utils.GeneralUtil

import javax.annotation.PostConstruct
import javax.faces.application.FacesMessage
import javax.faces.view.ViewScoped
import javax.inject.Named
import java.io.Serializable
import java.util.ArrayList

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
@Named
@ViewScoped
class LayoutManagement : Serializable
{
	private val daoUtil = DaoUtil()

	var loader = GenericLazyLoader(Layout::class.java)

	var layout: Layout? = null

	var regionList: List<PageRegion>? = null

	var regionListValue: Long? = null

	@PostConstruct
	fun postInit()
	{
		val request = GeneralUtil.request

		val id = request!!.getParameter("id")

		if (id != null && id.isNotEmpty())
		{
			this.layout = this.daoUtil.find(Layout::class.java, id.toLong())
		}

		this.regionList = this.daoUtil.findAll(PageRegion::class.java)
	}

	fun newLayout()
	{
		this.layout = Layout()
	}

	fun selectLayout(item: Layout)
	{
		this.layout = item
	}

	fun saveLayout()
	{
		if (this.layout!!.id != null && this.layout!!.id > 0)
		{
			if (this.daoUtil.update(this.layout!!))
			{
				GeneralUtil.addMessage("Layout has been updated.", FacesMessage.SEVERITY_INFO)
			}
			else
			{
				GeneralUtil.addMessage("Layout could not be updated.", FacesMessage.SEVERITY_ERROR)
			}
		}
		else
		{
			if (this.daoUtil.create(this.layout!!))
			{
				GeneralUtil.addMessage("Layout has been created.", FacesMessage.SEVERITY_INFO)
			}
			else
			{
				GeneralUtil.addMessage("Layout could not be created.", FacesMessage.SEVERITY_ERROR)
			}
		}
	}

	fun deleteLayout(layout: Layout)
	{
		if (layout.id != null && layout.id > 0)
		{
			val success = this.daoUtil.delete(layout)

			if (success)
			{
				this.layout = null
			}

			GeneralUtil.addMessage("Layout has been deleted.", FacesMessage.SEVERITY_INFO)
		}
	}

	fun addPageRegion()
	{
		val region = this.daoUtil.find(PageRegion::class.java, this.regionListValue!!)

		if (region != null)
		{
			if (this.layout!!.pageRegions == null)
			{
				this.layout!!.pageRegions = ArrayList()
			}

			var regionExists = false

			for (r in this.layout!!.pageRegions)
			{
				if (r.id == region.id)
				{
					regionExists = true
				}
			}

			if (!regionExists)
			{
				this.layout!!.pageRegions.add(region)
			}
		}
	}

	fun clone(layout: Layout)
	{
		layout.name = layout.name + " (CLONE)"
		layout.id = null

		if (this.daoUtil.create(layout))
		{
			GeneralUtil.addMessage("Layout has been cloned.", FacesMessage.SEVERITY_INFO)
		}
		else
		{
			GeneralUtil.addMessage("Layout could not be cloned.", FacesMessage.SEVERITY_ERROR)
		}

	}

	fun removePageRegion(region: PageRegion)
	{
		this.layout!!.pageRegions.remove(region)
	}
}
