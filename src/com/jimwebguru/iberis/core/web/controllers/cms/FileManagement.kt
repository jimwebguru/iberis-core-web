package com.jimwebguru.iberis.core.web.controllers.cms

import com.jimwebguru.iberis.core.loaders.GenericLazyLoader
import com.jimwebguru.iberis.core.persistence.*
import com.jimwebguru.iberis.core.utils.db.DaoUtil
import com.jimwebguru.iberis.core.utils.db.QueryParameter
import com.jimwebguru.iberis.core.web.controllers.admin.security.Authorize
import com.jimwebguru.iberis.core.web.utils.GeneralUtil
import com.jimwebguru.iberis.core.web.utils.UploadUtil
import org.primefaces.event.FileUploadEvent
import org.primefaces.event.SelectEvent

import javax.annotation.PostConstruct
import javax.faces.application.FacesMessage
import javax.faces.view.ViewScoped
import javax.inject.Named
import java.io.Serializable
import java.util.ArrayList

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
@Named
@ViewScoped
class FileManagement : Serializable
{
	private val daoUtil = DaoUtil()

	var file: File? = null

	var genericLazyLoader = GenericLazyLoader(File::class.java)

	var siteList = ArrayList<Site>()

	var siteListValue: Long? = null

	@PostConstruct
	fun postInit()
	{
		val request = GeneralUtil.request

		val id = request!!.getParameter("id")

		if (id != null && id.isNotEmpty())
		{
			this.file = this.daoUtil.find(File::class.java, id.toLong())
		}

		this.genericLazyLoader.additionalQueryParams = CmsAuthUtil.getCmsSiteFilterParameters()

		val user = Authorize.getUser()

		if(user != null)
		{
			for (siteMembership in user.siteMemberships)
			{
				this.siteList.add(siteMembership.site)
			}
		}
	}

	fun newFile()
	{
		this.file = File()
		this.file!!.mimeType = MimeType()
		this.file!!.fileExtension = FileExtension()
	}

	fun save()
	{
		val success: Boolean

		if(this.siteList.size == 1)
		{
			if (this.file!!.sites == null)
			{
				this.file!!.sites = ArrayList()
			}

			if (this.file!!.sites.isEmpty())
			{
				this.file!!.sites.add(this.siteList[0])
			}
		}

		if(this.file!!.sites == null || this.file!!.sites.isEmpty())
		{
			GeneralUtil.addMessage("A site must be associated.", FacesMessage.SEVERITY_ERROR)
		}
		else
		{
			if (this.file!!.id != null && this.file!!.id > 0)
			{
				success = this.daoUtil.update(this.file!!)
			}
			else
			{
				success = this.daoUtil.create(this.file!!)
			}

			if (success)
			{
				GeneralUtil.addMessage("File has been saved.", FacesMessage.SEVERITY_INFO)
			}
			else
			{
				GeneralUtil.addMessage("File could not be saved.", FacesMessage.SEVERITY_ERROR)
			}
		}
	}

	fun handleFileUpload(event: FileUploadEvent)
	{
		val uploadedFile = event.file

		UploadUtil.uploadFile(this.daoUtil, uploadedFile)
	}

	fun delete(file: File)
	{
		val success = this.daoUtil.delete(file)

		if (success)
		{
			this.file = File()
			this.file!!.mimeType = MimeType()
			this.file!!.fileExtension = FileExtension()

			GeneralUtil.addMessage("File has been deleted.", FacesMessage.SEVERITY_INFO)
		}
		else
		{
			GeneralUtil.addMessage("File could not be deleted.", FacesMessage.SEVERITY_ERROR)
		}
	}

	fun selectFile(file: File)
	{
		this.file = file
	}

	fun onRowSelect(event: SelectEvent)
	{
		this.file = event.`object` as File
	}

	fun tagsAutoComplete(filter: String): List<VocabTerm>?
	{
		return this.daoUtil.find("SELECT OBJECT(o) FROM VocabTerm o WHERE LOWER(o.name) LIKE '" + filter.trim { it <= ' ' }.toLowerCase() + "%'")
	}

	fun addSite()
	{
		val site = this.daoUtil.find(Site::class.java, this.siteListValue!!)

		if (site != null)
		{
			if (this.file!!.sites == null)
			{
				this.file!!.sites = ArrayList()
			}

			var siteExists = false

			for (cSite in this.file!!.sites)
			{
				if (cSite.siteID == site.siteID)
				{
					siteExists = true
				}
			}

			if (!siteExists)
			{
				this.file!!.sites.add(site)
			}
		}
	}

	fun removeSite(site: Site)
	{
		this.file!!.sites.remove(site)
	}

	fun inSiteList(siteId: Long): Boolean
	{
		for(cSite in this.siteList)
		{
			if(cSite.siteID == siteId)
			{
				return true
			}
		}

		return false
	}
}
