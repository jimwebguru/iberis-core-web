package com.jimwebguru.iberis.core.web.controllers.cms

import com.jimwebguru.iberis.core.web.utils.GeneralUtil
import com.jimwebguru.iberis.core.loaders.GenericLazyLoader
import com.jimwebguru.iberis.core.utils.db.DaoUtil
import com.jimwebguru.iberis.core.persistence.FAQ

import javax.annotation.PostConstruct
import javax.faces.application.FacesMessage
import javax.faces.view.ViewScoped
import javax.inject.Named
import java.io.Serializable

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
@Named
@ViewScoped
class FaqManagement : Serializable
{

	private val daoUtil = DaoUtil()

	var faqs = GenericLazyLoader(FAQ::class.java)

	@PostConstruct
	fun postInit()
	{
	}

	fun deleteFaq(faqId: Long)
	{
		val faq = daoUtil.find(FAQ::class.java, faqId)

		if (faq != null)
		{
			val faqDeleted = daoUtil.delete(faq)

			if (faqDeleted)
			{
				GeneralUtil.addMessage("FAQ deleted successfully", FacesMessage.SEVERITY_INFO)
			}
			else
			{
				GeneralUtil.addMessage("FAQ could not be deleted", FacesMessage.SEVERITY_ERROR)

			}
		}
		else
		{
			GeneralUtil.addMessage("Invalid FAQ", FacesMessage.SEVERITY_ERROR)
		}
	}
}