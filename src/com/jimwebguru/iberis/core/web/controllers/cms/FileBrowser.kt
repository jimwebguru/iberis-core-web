package com.jimwebguru.iberis.core.web.controllers.cms

import com.jimwebguru.iberis.core.loaders.GenericLazyLoader
import com.jimwebguru.iberis.core.persistence.*
import com.jimwebguru.iberis.core.utils.db.DaoUtil
import com.jimwebguru.iberis.core.utils.db.QueryParameter
import com.jimwebguru.iberis.core.web.utils.GeneralUtil
import com.jimwebguru.iberis.core.web.utils.UploadUtil
import org.primefaces.event.FileUploadEvent

import javax.annotation.PostConstruct
import javax.faces.view.ViewScoped
import javax.inject.Named
import java.io.Serializable
import java.util.ArrayList

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
@Named
@ViewScoped
class FileBrowser : Serializable
{
	private val daoUtil = DaoUtil()

	var imageLoader = GenericLazyLoader(Image::class.java)

	var fileLoader: GenericLazyLoader<File>? = null

	var videoLoader: GenericLazyLoader<Video>? = null

	var searchText: String? = null

	var browserType: Int = 0

	@PostConstruct
	fun postInit()
	{
		val request = GeneralUtil.request

		if (request!!.getParameter("type") != null)
		{
			this.browserType = Integer.valueOf(request.getParameter("type"))!!
		}
		else
		{
			this.browserType = 0
		}

		this.imageLoader.additionalQueryParams = CmsAuthUtil.getCmsSiteFilterParameters()

		if (this.browserType != 1)
		{
			this.fileLoader = GenericLazyLoader(File::class.java)
			this.fileLoader!!.additionalQueryParams = CmsAuthUtil.getCmsSiteFilterParameters()

			val queryParameter2 = QueryParameter()
			queryParameter2.explicitCondition = "o.filePath IS NOT NULL AND o.filePath != ''"

			this.videoLoader = GenericLazyLoader(Video::class.java)
			this.videoLoader!!.additionalQueryParams = CmsAuthUtil.getCmsSiteFilterParameters()
			this.videoLoader!!.additionalQueryParams!!.add(queryParameter2)
		}
	}

	fun search()
	{
		var queryParameter = QueryParameter()
		queryParameter.parameterName = "name"
		queryParameter.objectProperty = "name"
		queryParameter.parameterValue = "%" + this.searchText!!.toLowerCase() + "%"
		queryParameter.conditionOperator = "LIKE"
		queryParameter.transformPropertyFunction = "LOWER"

		this.imageLoader.additionalQueryParams = CmsAuthUtil.getCmsSiteFilterParameters()
		this.imageLoader.additionalQueryParams!!.add(queryParameter)

		if (this.browserType != 1)
		{
			val queryParameter2 = QueryParameter()
			queryParameter2.explicitCondition = "o.filePath IS NOT NULL AND o.filePath != ''"

			this.videoLoader!!.additionalQueryParams = CmsAuthUtil.getCmsSiteFilterParameters()
			this.videoLoader!!.additionalQueryParams!!.add(queryParameter)
			this.videoLoader!!.additionalQueryParams!!.add(queryParameter2)

			queryParameter = QueryParameter()
			queryParameter.parameterName = "fileName"
			queryParameter.objectProperty = "fileName"
			queryParameter.parameterValue = "%" + this.searchText!!.toLowerCase() + "%"
			queryParameter.conditionOperator = "LIKE"
			queryParameter.transformPropertyFunction = "LOWER"

			this.fileLoader!!.additionalQueryParams = CmsAuthUtil.getCmsSiteFilterParameters()
			this.fileLoader!!.additionalQueryParams!!.add(queryParameter)
		}
	}

	fun handleImageUpload(event: FileUploadEvent)
	{
		val uploadedFile = event.file

		UploadUtil.uploadImage(this.daoUtil, uploadedFile)
	}

	fun handleFileUpload(event: FileUploadEvent)
	{
		val uploadedFile = event.file

		UploadUtil.uploadFile(this.daoUtil, uploadedFile)
	}

	fun handleVideoUpload(event: FileUploadEvent)
	{
		val uploadedFile = event.file

		UploadUtil.uploadVideo(this.daoUtil, uploadedFile)
	}
}
