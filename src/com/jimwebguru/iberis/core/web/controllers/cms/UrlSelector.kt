package com.jimwebguru.iberis.core.web.controllers.cms

import com.jimwebguru.iberis.core.loaders.GenericLazyLoader
import com.jimwebguru.iberis.core.persistence.Content
import com.jimwebguru.iberis.core.utils.db.QueryParameter
import com.jimwebguru.iberis.core.web.utils.GeneralUtil
import org.primefaces.event.SelectEvent

import javax.annotation.PostConstruct
import javax.faces.view.ViewScoped
import javax.inject.Named
import java.io.Serializable
import java.util.ArrayList

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
@Named
@ViewScoped
class UrlSelector : Serializable
{
	var referencedDisplayField: String? = null

	var selectedContent: Content? = null

	var contentGenericLazyLoader = GenericLazyLoader(Content::class.java)

	@PostConstruct
	fun postInitialization()
	{
		val params = ArrayList<QueryParameter>()
		val queryParameter = QueryParameter("type", "block")
		queryParameter.explicitCondition = "o.type != :type"
		params.add(queryParameter)

		this.contentGenericLazyLoader.additionalQueryParams = params

		if (GeneralUtil.getQueryStringValue("referencedDisplayField") != null)
		{
			this.referencedDisplayField = GeneralUtil.getQueryStringValue("referencedDisplayField")
		}
	}

	fun onRowSelect(event: SelectEvent)
	{
		this.selectedContent = event.`object` as Content
	}
}
