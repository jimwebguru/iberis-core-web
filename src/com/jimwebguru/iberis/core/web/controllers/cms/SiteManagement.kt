package com.jimwebguru.iberis.core.web.controllers.cms

import com.jimwebguru.iberis.core.loaders.GenericLazyLoader
import com.jimwebguru.iberis.core.persistence.*
import com.jimwebguru.iberis.core.utils.db.DaoUtil
import com.jimwebguru.iberis.core.utils.db.QueryParameter
import com.jimwebguru.iberis.core.web.utils.GeneralUtil

import javax.annotation.PostConstruct
import javax.faces.application.FacesMessage
import javax.faces.view.ViewScoped
import javax.inject.Named
import java.io.Serializable
import java.util.ArrayList

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
@Named
@ViewScoped
class SiteManagement : Serializable
{
	private val daoUtil = DaoUtil()

	var loader = GenericLazyLoader(Site::class.java)

	var site: Site? = null

	var unitSiteAssociation: UnitSiteAssociation? = null

	@PostConstruct
	fun postInit()
	{
		val request = GeneralUtil.request

		val id = request!!.getParameter("id")

		if (id != null && id.isNotEmpty())
		{
			this.site = this.daoUtil.find(Site::class.java, id.toLong())
			this.selectSite(this.site)
		}
	}

	fun newSite()
	{
		this.site = Site()
		this.site!!.siteType = SiteType()
		this.site!!.frontPageContent = Content()
		this.site!!.frontPageTemplate = Layout()
		this.site!!.innerPageTemplate = Layout()
	}

	fun selectSite(item: Site?)
	{
		this.site = item

		if (this.site!!.siteType == null)
		{
			this.site!!.siteType = SiteType()
		}

		if (this.site!!.frontPageContent == null)
		{
			this.site!!.frontPageContent = Content()
		}

		if (this.site!!.logo == null)
		{
			this.site!!.logo = Image()
		}

		if (this.site!!.frontPageTemplate == null)
		{
			this.site!!.frontPageTemplate = Layout()
		}
		else
		{
			//Added for unique class instantiation due to EL issue with more
			//than one property that could have the same entity reference.
			//JSF doesn't bind values well to objects when there is the same
			//object from JPA in more than one property of the same class
			val frontPageLayout = Layout()
			frontPageLayout.id = this.site!!.frontPageTemplate.id
			frontPageLayout.name = this.site!!.frontPageTemplate.name
			this.site!!.frontPageTemplate = frontPageLayout
		}

		if (this.site!!.innerPageTemplate == null)
		{
			this.site!!.innerPageTemplate = Layout()
		}
		else
		{
			//Added for unique class instantiation due to EL issue with more
			//than one property that could have the same entity reference.
			//JSF doesn't bind values well to objects when there is the same
			//object from JPA in more than one property of the same class
			val innerLayout = Layout()
			innerLayout.id = this.site!!.innerPageTemplate.id
			innerLayout.name = this.site!!.innerPageTemplate.name
			this.site!!.innerPageTemplate = innerLayout
		}
	}

	fun saveSite()
	{
		this.clearObjectForSave()

		if (this.site!!.siteID != null && this.site!!.siteID > 0)
		{
			if (this.daoUtil.update(this.site!!))
			{
				GeneralUtil.addMessage("Site has been updated.", FacesMessage.SEVERITY_INFO)
			}
			else
			{
				GeneralUtil.addMessage("Site could not be updated.", FacesMessage.SEVERITY_ERROR)
			}
		}
		else
		{
			if (this.daoUtil.create(this.site!!))
			{
				GeneralUtil.addMessage("Site has been created.", FacesMessage.SEVERITY_INFO)
			}
			else
			{
				GeneralUtil.addMessage("Site could not be created.", FacesMessage.SEVERITY_ERROR)
			}
		}

		this.resetObjectForUIBind()
	}

	fun deleteSite(site: Site)
	{
		if (site.siteID != null && site.siteID > 0)
		{
			val params = ArrayList<QueryParameter>()
			params.add(QueryParameter("siteId", site.siteID))

			val contentItems = this.daoUtil.find<Content>("SELECT DISTINCT OBJECT(o) FROM Content o JOIN o.sites s WHERE s.siteID = :siteId", params)

			for (content in contentItems!!)
			{
				if (content.sites != null)
				{
					var siteToRemove: Site? = null

					for (contentSite in content.sites)
					{
						if (contentSite.siteID === site.siteID)
						{
							siteToRemove = contentSite
						}
					}

					if (siteToRemove != null)
					{
						content.sites.remove(siteToRemove)

						this.daoUtil.update(content)
					}
				}
			}

			val success = this.daoUtil.delete(site)

			if (success)
			{
				this.site = null
			}

			GeneralUtil.addMessage("Site has been deleted.", FacesMessage.SEVERITY_INFO)
		}
	}

	fun newUnitSiteAssociation()
	{
		this.unitSiteAssociation = UnitSiteAssociation()
		this.unitSiteAssociation!!.unit = SystemUnit()
	}

	fun selectUnitSiteAssociation(item: UnitSiteAssociation)
	{
		this.unitSiteAssociation = item
	}

	fun saveUnitSiteAssociation()
	{
		this.unitSiteAssociation!!.site = this.site

		val queryParameters = ArrayList<QueryParameter>()

		var queryParameter = QueryParameter()
		queryParameter.parameterName = "site"
		queryParameter.parameterValue = this.unitSiteAssociation!!.site.siteID
		queryParameters.add(queryParameter)

		queryParameter = QueryParameter()
		queryParameter.parameterName = "unit"
		queryParameter.parameterValue = this.unitSiteAssociation!!.unit.unitID
		queryParameters.add(queryParameter)

		val count = this.daoUtil.count("SELECT COUNT(o) FROM UnitSiteAssociation o WHERE o.site.siteID = :site and o.unit.unitID = :unit", queryParameters)

		if (count > 0)
		{
			GeneralUtil.addMessage("That association already exists.", FacesMessage.SEVERITY_ERROR)
		}
		else
		{
			var existingSiteAssociation: UnitSiteAssociation? = null

			if (this.unitSiteAssociation!!.id != null && this.unitSiteAssociation!!.id > 0)
			{
				existingSiteAssociation = this.daoUtil.find(UnitSiteAssociation::class.java, this.unitSiteAssociation!!.id)
			}

			if (this.unitSiteAssociation!!.id != null && this.unitSiteAssociation!!.id > 0)
			{
				if (this.daoUtil.update(this.unitSiteAssociation!!))
				{
					GeneralUtil.addMessage("Unit Site Association has been updated.", FacesMessage.SEVERITY_INFO)
				}
				else
				{
					GeneralUtil.addMessage("Unit Site Association could not be updated.", FacesMessage.SEVERITY_ERROR)
				}
			}
			else
			{
				if (this.daoUtil.create(this.unitSiteAssociation!!))
				{
					GeneralUtil.addMessage("Unit Site Association has been created.", FacesMessage.SEVERITY_INFO)
				}
				else
				{
					GeneralUtil.addMessage("Unit Site Association could not be created.", FacesMessage.SEVERITY_ERROR)
				}
			}

			this.daoUtil.refresh(SystemUnit::class.java, this.unitSiteAssociation!!.unit.unitID)
			this.daoUtil.refresh(Site::class.java, this.site!!.siteID)

			if (existingSiteAssociation != null)
			{
				if (existingSiteAssociation.unit.unitID != this.unitSiteAssociation!!.unit.unitID)
				{
					this.daoUtil.refresh(SystemUnit::class.java, existingSiteAssociation.unit.unitID)
				}
			}

			this.site = this.daoUtil.find(Site::class.java, this.site!!.siteID)

			this.selectSite(this.site)
		}
	}

	fun deleteUnitSiteAssociation(item: UnitSiteAssociation)
	{
		if (item.id != null && item.id > 0)
		{
			this.daoUtil.delete(item)

			GeneralUtil.addMessage("Unit Site Selection has been deleted.", FacesMessage.SEVERITY_INFO)

			this.daoUtil.refresh(SystemUnit::class.java, this.unitSiteAssociation!!.unit.unitID)
			this.daoUtil.refresh(Site::class.java, this.site!!.siteID)

			this.site = this.daoUtil.find(Site::class.java, this.site!!.siteID)

			this.selectSite(this.site)
		}
	}

	private fun resetObjectForUIBind()
	{
		if (this.site!!.siteType == null)
		{
			this.site!!.siteType = SiteType()
		}

		if (this.site!!.frontPageContent == null)
		{
			this.site!!.frontPageContent = Content()
		}

		if (this.site!!.frontPageTemplate == null)
		{
			this.site!!.frontPageTemplate = Layout()
		}

		if (this.site!!.innerPageTemplate == null)
		{
			this.site!!.innerPageTemplate = Layout()
		}

		if (this.site!!.logo == null)
		{
			this.site!!.logo = Image()
		}
	}

	private fun clearObjectForSave()
	{
		if (this.site!!.siteType != null && (this.site!!.siteType.typeID == null || this.site!!.siteType.typeID == 0L))
		{
			this.site!!.siteType = null
		}

		if (this.site!!.frontPageContent != null && (this.site!!.frontPageContent.id == null || this.site!!.frontPageContent.id == 0L))
		{
			this.site!!.frontPageContent = null
		}

		if (this.site!!.frontPageTemplate != null && (this.site!!.frontPageTemplate.id == null || this.site!!.frontPageTemplate.id == 0L))
		{
			this.site!!.frontPageTemplate = null
		}

		if (this.site!!.innerPageTemplate != null && (this.site!!.innerPageTemplate.id == null || this.site!!.innerPageTemplate.id == 0L))
		{
			this.site!!.innerPageTemplate = null
		}

		if (this.site!!.logo != null && (this.site!!.logo.imageid == null || this.site!!.logo.imageid < 1))
		{
			this.site!!.logo = null
		}
	}
}
