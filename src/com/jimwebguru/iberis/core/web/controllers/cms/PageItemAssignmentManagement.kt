package com.jimwebguru.iberis.core.web.controllers.cms

import com.jimwebguru.iberis.core.loaders.GenericLazyLoader
import com.jimwebguru.iberis.core.persistence.*
import com.jimwebguru.iberis.core.utils.db.DaoUtil
import com.jimwebguru.iberis.core.utils.db.QueryParameter
import com.jimwebguru.iberis.core.web.utils.GeneralUtil
import org.primefaces.event.RowEditEvent

import javax.annotation.PostConstruct
import javax.faces.application.FacesMessage
import javax.faces.view.ViewScoped
import javax.inject.Named
import java.io.Serializable
import java.util.ArrayList

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
@Named
@ViewScoped
open class PageItemAssignmentManagement : Serializable
{
	protected var daoUtil = DaoUtil()

	var pageItemAssignment: PageItemAssignment? = null

	var lazyLoader = GenericLazyLoader(PageItemAssignment::class.java)

	val defaultPageItemFilter: String
		get()
		{
			if(this.pageItemAssignment != null)
			{
				if (this.pageItemAssignment?.pageItem != null && this.pageItemAssignment?.pageItem == 62L)
				{
					return "LEFT JOIN o.sites AS s WHERE o.availableGlobal = TRUE OR s.siteID = ${this.pageItemAssignment!!.site.siteID}"
				}
				else
				{
					return "LEFT JOIN o.sites AS s WHERE s.siteID = ${this.pageItemAssignment!!.site.siteID}"
				}
			}
			else
			{
				return "LEFT JOIN o.sites AS s WHERE s.siteID = 0"
			}
		}

	@PostConstruct
	open fun postInit()
	{
		if (GeneralUtil.getRequestParameterValue("id") != null)
		{
			this.pageItemAssignment = this.daoUtil.find(PageItemAssignment::class.java, GeneralUtil.getRequestParameterValue("id").toString().toLong())
		}
	}

	fun newPageItemAssignment()
	{
		this.pageItemAssignment = PageItemAssignment()
		this.pageItemAssignment!!.pageRegion = PageRegion()
		this.pageItemAssignment!!.site = Site()
	}

	fun selectPageItemAssignment(item: PageItemAssignment)
	{
		this.pageItemAssignment = item
	}

	open fun savePageItemAssignment()
	{
		if (this.pageItemAssignment!!.id != null && this.pageItemAssignment!!.id > 0)
		{
			if (this.daoUtil.update(this.pageItemAssignment!!))
			{
				this.saveUrls()
				this.rearrangeAssignmentSequence(this.pageItemAssignment)
				GeneralUtil.addMessage("Page Item Assignment has been updated.", FacesMessage.SEVERITY_INFO)
			}
			else
			{
				GeneralUtil.addMessage("Page Item Assignment could not be updated.", FacesMessage.SEVERITY_ERROR)
			}
		}
		else
		{
			if (this.daoUtil.create(this.pageItemAssignment!!))
			{
				this.saveUrls()
				this.rearrangeAssignmentSequence(this.pageItemAssignment)
				GeneralUtil.addMessage("Page Item Assignment has been created.", FacesMessage.SEVERITY_INFO)
			}
			else
			{
				GeneralUtil.addMessage("Page Item Assignment could not be created.", FacesMessage.SEVERITY_ERROR)
			}
		}

		this.daoUtil.refresh(PageItemAssignment::class.java, this.pageItemAssignment!!.id)

		this.pageItemAssignment = this.daoUtil.find(PageItemAssignment::class.java, this.pageItemAssignment!!.id)
	}

	private fun rearrangeAssignmentSequence(assignment: PageItemAssignment?)
	{
		val queryParameters = ArrayList<QueryParameter>()

		var queryParameter = QueryParameter()
		queryParameter.parameterName = "region"
		queryParameter.parameterValue = assignment!!.pageRegion.id

		queryParameters.add(queryParameter)

		queryParameter = QueryParameter()
		queryParameter.parameterName = "site"
		queryParameter.parameterValue = assignment.site.siteID

		queryParameters.add(queryParameter)

		val pageItemAssignments = this.daoUtil.find<PageItemAssignment>("SELECT OBJECT(o) FROM PageItemAssignment o WHERE o.site.siteID = :site AND o.pageRegion.id = :region ORDER BY o.sequence ASC", queryParameters)

		var counter = 0

		for (a in pageItemAssignments!!)
		{
			if (a.id != assignment!!.id)
			{
				if (counter == assignment.sequence)
				{
					counter++
				}

				a.sequence = counter

				this.daoUtil.update(a)
				counter++
			}
		}
	}

	private fun saveUrls()
	{
		if (this.pageItemAssignment!!.allowedUrls != null && this.pageItemAssignment!!.allowedUrls.isNotEmpty())
		{
			for (allowedUrl in this.pageItemAssignment!!.allowedUrls)
			{
				if (allowedUrl.id != null && allowedUrl.id > 0)
				{
					this.daoUtil.update(allowedUrl)
				}
			}
		}

		if (this.pageItemAssignment!!.deniedUrls != null && this.pageItemAssignment!!.deniedUrls.isNotEmpty())
		{
			for (deniedUrl in this.pageItemAssignment!!.deniedUrls)
			{
				if (deniedUrl.id != null && deniedUrl.id > 0)
				{
					this.daoUtil.update(deniedUrl)
				}
			}
		}
	}

	fun deletePageItemAssignment(assignment: PageItemAssignment)
	{
		if (assignment.id != null && assignment.id > 0)
		{
			if (assignment.allowedUrls != null)
			{
				for (allowedUrl in assignment.allowedUrls)
				{
					this.daoUtil.delete(allowedUrl)
				}
			}

			if (assignment.deniedUrls != null)
			{
				for (deniedUrl in assignment.deniedUrls)
				{
					this.daoUtil.delete(deniedUrl)
				}
			}

			this.daoUtil.delete(assignment)

			this.newPageItemAssignment()

			GeneralUtil.addMessage("Page Item Assignment has been deleted.", FacesMessage.SEVERITY_INFO)
		}

	}

	fun pageItemTitle(assignment: PageItemAssignment): String
	{
		if (assignment.pageItem == 33L)
		{
			val image = this.daoUtil.find(Image::class.java, assignment.pageItemInstanceId)

			return image!!.name
		}

		if (assignment.pageItem == 61L)
		{
			val presentation = this.daoUtil.find(Presentation::class.java, assignment.pageItemInstanceId)

			return presentation!!.name
		}

		if (assignment.pageItem == 62L)
		{
			val content = this.daoUtil.find(Content::class.java, assignment.pageItemInstanceId)

			return content!!.title
		}

		if (assignment.pageItem == 65L)
		{
			val gallery = this.daoUtil.find(Gallery::class.java, assignment.pageItemInstanceId)

			return gallery!!.name
		}

		if (assignment.pageItem == 66L)
		{
			val menu = this.daoUtil.find(Menu::class.java, assignment.pageItemInstanceId)

			return menu!!.name
		}

		if (assignment.pageItem == 75L)
		{
			val form = this.daoUtil.find(DynamicForm::class.java, assignment.pageItemInstanceId)

			return form!!.title
		}

		if (assignment.pageItem == 83L)
		{
			val catalog = this.daoUtil.find(ProductCatalog::class.java, assignment.pageItemInstanceId)

			return catalog!!.name
		}

		if (assignment.pageItem == 89L)
		{
			val video = this.daoUtil.find(Video::class.java, assignment.pageItemInstanceId)

			return video!!.name
		}

		return ""
	}

	fun addAllowedUrl()
	{
		if (this.pageItemAssignment!!.allowedUrls == null)
		{
			this.pageItemAssignment!!.allowedUrls = ArrayList()
		}

		val allowedUrl = AllowedUrl()
		allowedUrl.url = this.pageItemAssignment!!.allowedUrls_temp
		allowedUrl.pageItemAssignment = this.pageItemAssignment

		this.pageItemAssignment!!.allowedUrls.add(allowedUrl)
	}

	fun removeAllowedUrl(allowedUrl: AllowedUrl)
	{
		if (this.daoUtil.delete(allowedUrl))
		{
			this.pageItemAssignment!!.allowedUrls.remove(allowedUrl)
			this.daoUtil.refresh(PageItemAssignment::class.java, this.pageItemAssignment!!.id)
		}
	}

	fun onAllowedUrlsRowEdit(event: RowEditEvent)
	{

	}

	fun addDeniedUrl()
	{
		if (this.pageItemAssignment!!.deniedUrls == null)
		{
			this.pageItemAssignment!!.deniedUrls = ArrayList()
		}

		val deniedUrl = DeniedUrl()
		deniedUrl.url = this.pageItemAssignment!!.deniedUrls_temp
		deniedUrl.pageItemAssignment = this.pageItemAssignment

		this.pageItemAssignment!!.deniedUrls.add(deniedUrl)
	}

	fun removeDeniedUrl(deniedUrl: DeniedUrl)
	{
		if (this.daoUtil.delete(deniedUrl))
		{
			this.pageItemAssignment!!.deniedUrls.remove(deniedUrl)
			this.daoUtil.refresh(PageItemAssignment::class.java, this.pageItemAssignment!!.id)
		}
	}

	fun onDeniedUrlsRowEdit(event: RowEditEvent)
	{

	}

	fun refresh()
	{
		//UI Refresh
	}
}
