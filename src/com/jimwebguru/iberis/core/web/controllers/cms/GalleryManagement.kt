package com.jimwebguru.iberis.core.web.controllers.cms

import com.jimwebguru.iberis.core.loaders.GenericLazyLoader
import com.jimwebguru.iberis.core.persistence.*
import com.jimwebguru.iberis.core.utils.db.DaoUtil
import com.jimwebguru.iberis.core.web.controllers.admin.security.Authorize
import com.jimwebguru.iberis.core.web.utils.GeneralUtil
import com.jimwebguru.iberis.core.web.utils.UploadUtil
import org.primefaces.event.FileUploadEvent
import org.primefaces.event.SelectEvent

import javax.annotation.PostConstruct
import javax.faces.application.FacesMessage
import javax.faces.view.ViewScoped
import javax.inject.Named
import java.io.Serializable
import java.util.ArrayList

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
@Named
@ViewScoped
class GalleryManagement : Serializable
{
	var gallery: Gallery? = null

	var previewGallery: Gallery? = null

	var galleryImage: GalleryImage? = null

	var genericLazyLoader = GenericLazyLoader(Gallery::class.java)

	private val daoUtil = DaoUtil()

	var siteList = ArrayList<Site>()

	var siteListValue: Long? = null

	@PostConstruct
	fun postInit()
	{
		if (GeneralUtil.getRequestParameterValue("id") != null)
		{
			this.gallery = this.daoUtil.find(Gallery::class.java, GeneralUtil.getRequestParameterValue("id").toString().toLong())
		}

		this.genericLazyLoader.additionalQueryParams = CmsAuthUtil.getCmsSiteFilterParameters()

		val user = Authorize.getUser()

		if(user != null)
		{
			for (siteMembership in user.siteMemberships)
			{
				this.siteList.add(siteMembership.site)
			}
		}
	}

	fun save()
	{
		val success: Boolean

		if (this.gallery!!.type == 5 || this.gallery!!.type == 6)
		{
			if (this.gallery!!.imagesPerSlide > 1 && this.gallery!!.showThumbnails!!)
			{
				this.gallery!!.showThumbnails = false
				GeneralUtil.addMessage("Galleries cannot have thumbnail viewer with more than one image in slide.", FacesMessage.SEVERITY_INFO)
			}
		}

		if(this.gallery!!.flipSlides)
		{
			this.gallery!!.captionAsOverlay = false
		}

		if(this.siteList.size == 1)
		{
			if (this.gallery!!.sites == null)
			{
				this.gallery!!.sites = ArrayList()
			}

			if (this.gallery!!.sites.isEmpty())
			{
				this.gallery!!.sites.add(this.siteList[0])
			}
		}

		if(this.gallery!!.type == 5 && (this.gallery!!.slideMode == null || this.gallery!!.slideMode.isEmpty()))
		{
			this.gallery!!.slideMode = "fade"
		}

		if(this.gallery!!.sites == null || this.gallery!!.sites.isEmpty())
		{
			GeneralUtil.addMessage("A site must be associated.", FacesMessage.SEVERITY_ERROR)
		}
		else
		{
			if (this.gallery!!.id != null && this.gallery!!.id > 0)
			{
				success = this.daoUtil.update(this.gallery!!)
			}
			else
			{
				success = this.daoUtil.create(this.gallery!!)
			}

			if (success)
			{
				GeneralUtil.addMessage("Gallery has been saved.", FacesMessage.SEVERITY_INFO)
			}
			else
			{
				GeneralUtil.addMessage("Gallery could not be saved.", FacesMessage.SEVERITY_ERROR)
			}
		}
	}

	fun edit(gallery: Gallery)
	{
		this.gallery = gallery
	}

	fun delete(gallery: Gallery)
	{
		val success = this.daoUtil.delete(gallery)

		this.gallery = null

		if (previewGallery != null && previewGallery == gallery)
		{
			this.previewGallery = null
		}

		if (success)
		{
			GeneralUtil.addMessage("Gallery has been deleted.", FacesMessage.SEVERITY_INFO)
		}
		else
		{
			GeneralUtil.addMessage("Gallery could not be deleted.", FacesMessage.SEVERITY_ERROR)
		}
	}

	fun newGallery()
	{
		this.gallery = Gallery()
		this.gallery!!.imagesPerSlide = 1
		this.gallery!!.animationSpeed = 500
		this.gallery!!.pauseTime = 4000
	}

	fun onRowSelect(event: SelectEvent)
	{
		this.previewGallery = event.`object` as Gallery
		this.gallery = this.previewGallery
	}

	fun newGalleryImage()
	{
		this.galleryImage = GalleryImage()
		this.galleryImage!!.image = Image()
	}

	fun selectGalleryImage(gImage: GalleryImage)
	{
		this.galleryImage = gImage

		if (this.galleryImage!!.image == null)
		{
			this.galleryImage!!.image = Image()
		}
	}

	fun saveGalleryImage()
	{
		this.galleryImage!!.gallery = this.gallery

		val success: Boolean

		if (this.galleryImage!!.id != null && this.galleryImage!!.id > 0)
		{
			success = this.daoUtil.update(this.galleryImage!!)
		}
		else
		{
			success = this.daoUtil.create(this.galleryImage!!)
		}

		if (success)
		{
			this.reorderGalleryImages()

			this.daoUtil.refresh(Gallery::class.java, this.gallery!!.id)

			this.gallery = this.daoUtil.find(Gallery::class.java, this.gallery!!.id)

			GeneralUtil.addMessage("Gallery image has been saved.", FacesMessage.SEVERITY_INFO)
		}
		else
		{
			GeneralUtil.addMessage("Gallery image could not be saved.", FacesMessage.SEVERITY_ERROR)
		}
	}

	fun deleteGalleryImage(galleryImage: GalleryImage)
	{
		val success = this.daoUtil.delete(galleryImage)

		if (success)
		{
			this.daoUtil.refresh(Gallery::class.java, this.gallery!!.id)

			this.gallery = this.daoUtil.find(Gallery::class.java, this.gallery!!.id)

			GeneralUtil.addMessage("Gallery image has been deleted.", FacesMessage.SEVERITY_INFO)
		}
		else
		{
			GeneralUtil.addMessage("Gallery image could not be deleted.", FacesMessage.SEVERITY_ERROR)
		}
	}

	fun refreshGalleryImage()
	{
		if(this.galleryImage!!.image != null && this.galleryImage!!.image.imageid != null && this.galleryImage!!.image.imageid > 0)
		{
			this.galleryImage!!.image = this.daoUtil.find(Image::class.java, this.galleryImage!!.image.imageid)
		}
	}

	fun onRowGalleryImageSelect(event: SelectEvent)
	{
		this.galleryImage = event.`object` as GalleryImage
	}

	fun getGalleryTypeName(type: Int): String
	{
		val unitProperty = this.daoUtil.find(UnitProperty::class.java, 441L)

		for (unitPropertyListItem in unitProperty!!.unitPropertyListItems)
		{
			if (unitPropertyListItem.itemValue == type.toString())
			{
				return unitPropertyListItem.displayValue
			}
		}

		return type.toString()
	}

	private fun reorderGalleryImages()
	{
		this.gallery!!.galleryImages.remove(this.galleryImage)

		var counter = 0

		for (galleryImage in this.gallery!!.galleryImages)
		{
			if (counter == this.galleryImage!!.sequence)
			{
				counter++
			}

			galleryImage.sequence = counter

			this.daoUtil.update(galleryImage)

			counter++
		}
	}

	fun refresh()
	{
		//Used to bind data to managed beans and refresh ui
	}

	fun addSite()
	{
		val site = this.daoUtil.find(Site::class.java, this.siteListValue!!)

		if (site != null)
		{
			if (this.gallery!!.sites == null)
			{
				this.gallery!!.sites = ArrayList()
			}

			var siteExists = false

			for (cSite in this.gallery!!.sites)
			{
				if (cSite.siteID == site.siteID)
				{
					siteExists = true
				}
			}

			if (!siteExists)
			{
				this.gallery!!.sites.add(site)
			}
		}
	}

	fun removeSite(site: Site)
	{
		this.gallery!!.sites.remove(site)
	}

	fun inSiteList(siteId: Long): Boolean
	{
		for(cSite in this.siteList)
		{
			if(cSite.siteID == siteId)
			{
				return true
			}
		}

		return false
	}

	fun handleImageUpload(event: FileUploadEvent)
	{
		val uploadedFile = event.file

		UploadUtil.uploadImage(this.daoUtil, uploadedFile)

		val imageName = uploadedFile.fileName.replace(" ".toRegex(), "").toLowerCase()
		val images = this.daoUtil.find<Image>("SELECT OBJECT(o) FROM Image o WHERE o.name = $imageName")

		if(images != null && images.isNotEmpty())
		{
			this.galleryImage!!.image = images[0]
		}
	}
}
