package com.jimwebguru.iberis.core.web.controllers.cms

import com.jimwebguru.iberis.core.loaders.GenericLazyLoader
import com.jimwebguru.iberis.core.persistence.*
import com.jimwebguru.iberis.core.utils.db.DaoUtil
import com.jimwebguru.iberis.core.utils.db.QueryParameter
import com.jimwebguru.iberis.core.web.controllers.admin.security.Authorize
import com.jimwebguru.iberis.core.web.utils.GeneralUtil
import com.jimwebguru.iberis.core.web.utils.TextUtil
import com.jimwebguru.iberis.core.web.utils.UploadUtil
import org.primefaces.event.FileUploadEvent
import org.primefaces.event.SelectEvent

import javax.annotation.PostConstruct
import javax.faces.application.FacesMessage
import javax.faces.view.ViewScoped
import javax.inject.Named
import java.io.Serializable
import java.util.ArrayList
import java.util.regex.Pattern
/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
@Named
@ViewScoped
class PresentationManagement : Serializable
{
	var presentation: Presentation? = null

	var previewPresentation: Presentation? = null

	var presentationSlide: PresentationSlide? = null

	var currentSlide: Slide? = null

	private val daoUtil = DaoUtil()

	var genericLazyLoader = GenericLazyLoader(Presentation::class.java)

	var siteList = ArrayList<Site>()

	var siteListValue: Long? = null

	var videoUrl: String? = null

	var videoName: String? = null

	@PostConstruct
	fun postInit()
	{
		if (GeneralUtil.getRequestParameterValue("id") != null)
		{
			this.presentation = this.daoUtil.find(Presentation::class.java, GeneralUtil.getRequestParameterValue("id").toString().toLong())
		}

		this.genericLazyLoader.additionalQueryParams = CmsAuthUtil.getCmsSiteFilterParameters()

		val user = Authorize.getUser()

		if(user != null)
		{
			for (siteMembership in user.siteMemberships)
			{
				this.siteList.add(siteMembership.site)
			}
		}
	}

	fun save()
	{
		val success: Boolean

		if(this.siteList.size == 1)
		{
			if (this.presentation!!.sites == null)
			{
				this.presentation!!.sites = ArrayList()
			}

			if (this.presentation!!.sites.isEmpty())
			{
				this.presentation!!.sites.add(this.siteList[0])
			}
		}

		if(this.presentation!!.sites == null || this.presentation!!.sites.isEmpty())
		{
			GeneralUtil.addMessage("A site must be associated.", FacesMessage.SEVERITY_ERROR)
		}
		else
		{
			if (this.presentation!!.imageHeroStyle == null || this.presentation!!.imageHeroStyle.isEmpty())
			{
				this.presentation!!.imageHeroStyle = "width: 100%"
			}

			if (this.presentation!!.flipSlides!! && this.presentation!!.presentationType != 7)
			{
				this.presentation!!.flipSlides = false
			}

			if (this.presentation!!.orientation == "left" || this.presentation!!.orientation == "right")
			{
				if (this.presentation!!.scrollable!!)
				{
					this.presentation!!.scrollable = false
				}
			}

			if(this.presentation!!.startSlide == null)
			{
				this.presentation!!.startSlide = 0
			}

			if(this.presentation!!.presentationType == 5 && (this.presentation!!.slideMode == null || this.presentation!!.slideMode.isEmpty()))
			{
				this.presentation!!.slideMode = "fade"
			}

			if(this.presentation!!.presentationType == 2 || this.presentation!!.presentationType == 6 || this.presentation!!.presentationType == 8)
			{
				this.presentation!!.useOverlay = false
			}

			if(this.presentation!!.heroSliderMode && !this.presentation!!.useOverlay)
			{
				this.presentation!!.useOverlay = true
			}

			if (this.presentation!!.id != null && this.presentation!!.id > 0)
			{
				success = this.daoUtil.update(this.presentation!!)
			}
			else
			{
				success = this.daoUtil.create(this.presentation!!)
			}

			if (success)
			{
				if (this.previewPresentation != null && this.presentation != null && this.previewPresentation!!.id == this.presentation!!.id)
				{
					this.previewPresentation = this.presentation
				}

				GeneralUtil.addMessage("Presentation has been saved.", FacesMessage.SEVERITY_INFO)
			}
			else
			{
				GeneralUtil.addMessage("Presentation could not be saved.", FacesMessage.SEVERITY_ERROR)
			}
		}
	}

	fun edit(presentation: Presentation)
	{
		this.presentation = presentation
	}

	fun delete(presentation: Presentation)
	{
		val success = this.daoUtil.delete(presentation)

		this.presentation = null

		if (previewPresentation != null && previewPresentation == presentation)
		{
			this.previewPresentation = null
		}

		if (success)
		{
			GeneralUtil.addMessage("Presentation has been deleted.", FacesMessage.SEVERITY_INFO)
		}
		else
		{
			GeneralUtil.addMessage("Presentation could not be deleted.", FacesMessage.SEVERITY_ERROR)
		}
	}

	fun newPresentation()
	{
		this.presentation = Presentation()
		this.presentation!!.animationSpeed = 500
		this.presentation!!.pauseTime = 4000
		this.presentation!!.scrollable = false
		this.presentation!!.orientation = ""
		this.presentation!!.sectionsClosable = true
		this.presentation!!.toggleable = false
		this.presentation!!.showPager = false
		this.presentation!!.showControls = false
		this.presentation!!.autoSlide = false
		this.presentation!!.autoStart = false
	}

	fun newPresentationSlide()
	{
		this.presentationSlide = PresentationSlide()
		this.presentationSlide!!.slide = Slide()
	}

	fun selectPresentationSlide(pSlide: PresentationSlide)
	{
		this.presentationSlide = pSlide

		this.currentSlide = this.presentationSlide!!.slide

		if (this.currentSlide!!.image == null)
		{
			this.currentSlide!!.image = Image()
		}

		if (this.currentSlide!!.video == null)
		{
			this.currentSlide!!.video = Video()
		}
	}

	fun savePresentationSlide()
	{
		this.presentationSlide!!.presentation = this.presentation

		var success: Boolean

		if (this.presentationSlide!!.id != null && this.presentationSlide!!.id > 0)
		{
			success = this.daoUtil.update(this.presentationSlide!!.slide)

			if (success)
			{
				success = this.daoUtil.update(this.presentationSlide!!)
			}
		}
		else
		{
			this.presentationSlide!!.sequence = 0

			success = this.daoUtil.create(this.presentationSlide!!.slide)

			if (success)
			{
				success = this.daoUtil.create(this.presentationSlide!!)

				if (success)
				{
					this.reindexSlides()
				}
			}
		}

		if (success)
		{
			this.daoUtil.refresh(Presentation::class.java, this.presentation!!.id)

			this.presentation = this.daoUtil.find(Presentation::class.java, this.presentation!!.id)

			GeneralUtil.addMessage("Slide has been saved.", FacesMessage.SEVERITY_INFO)
		}
		else
		{
			GeneralUtil.addMessage("Slide could not be saved.", FacesMessage.SEVERITY_ERROR)
		}
	}

	fun deletePresentationSlide(presentationSlide: PresentationSlide)
	{
		val success = this.daoUtil.delete(presentationSlide)

		if (success)
		{
			this.daoUtil.refresh(Presentation::class.java, this.presentation!!.id)

			this.presentation = this.daoUtil.find(Presentation::class.java, this.presentation!!.id)

			GeneralUtil.addMessage("Slide has been deleted.", FacesMessage.SEVERITY_INFO)
		}
		else
		{
			GeneralUtil.addMessage("Slide could not be deleted.", FacesMessage.SEVERITY_ERROR)
		}
	}

	fun newSlide()
	{
		this.currentSlide = Slide()
		this.currentSlide!!.image = Image()
		this.currentSlide!!.video = Video()

		this.presentationSlide = PresentationSlide()
		this.presentationSlide!!.slide = this.currentSlide
		this.presentationSlide!!.sequence = 0
		this.presentationSlide!!.presentation = this.presentation
	}

	fun selectSlide(slide: Slide)
	{
		this.currentSlide = slide

		if (this.currentSlide!!.image == null)
		{
			this.currentSlide!!.image = Image()
		}

		if (this.currentSlide!!.video == null)
		{
			this.currentSlide!!.video = Video()
		}
	}

	fun saveSlide()
	{
		if (this.currentSlide!!.image != null && (this.currentSlide!!.image.imageid == null || this.currentSlide!!.image.imageid < 1))
		{
			this.currentSlide!!.image = null
		}

		if (this.currentSlide!!.video != null && (this.currentSlide!!.video.id == null || this.currentSlide!!.video.id < 1))
		{
			this.currentSlide!!.video = null
		}

		if (this.currentSlide!!.id != null && this.currentSlide!!.id > 0)
		{
			if (this.daoUtil.update(this.currentSlide!!))
			{
				GeneralUtil.addMessage("Slide has been updated.", FacesMessage.SEVERITY_INFO)
			}
			else
			{
				GeneralUtil.addMessage("Slide was not updated.", FacesMessage.SEVERITY_ERROR)
			}
		}
		else
		{
			if (this.daoUtil.create(this.currentSlide!!))
			{
				this.daoUtil.create(this.presentationSlide!!)

				GeneralUtil.addMessage("Slide has been created.", FacesMessage.SEVERITY_INFO)
			}
			else
			{
				GeneralUtil.addMessage("Slide was not created.", FacesMessage.SEVERITY_ERROR)
			}
		}

		if (this.currentSlide!!.image == null)
		{
			this.currentSlide!!.image = Image()
		}

		if (this.currentSlide!!.video == null)
		{
			this.currentSlide!!.video = Video()
		}

		this.daoUtil.refresh(Presentation::class.java, this.presentation!!.id)

		this.presentation = this.daoUtil.find(Presentation::class.java, this.presentation!!.id)
	}

	fun deleteSlide(slide: Slide)
	{
		val queryParameters = ArrayList<QueryParameter>()

		val parameter = QueryParameter()
		parameter.parameterName = "id"
		parameter.parameterValue = slide.id

		queryParameters.add(parameter)
		val presentationSlides = this.daoUtil.find<PresentationSlide>("SELECT OBJECT(o) FROM PresentationSlide o WHERE o.slide.id = :id", queryParameters)

		for (pSlide in presentationSlides!!)
		{
			this.daoUtil.delete(pSlide)
		}

		if (this.daoUtil.delete(slide))
		{
			GeneralUtil.addMessage("Slide has been deleted.", FacesMessage.SEVERITY_INFO)

			this.newSlide()
		}
		else
		{
			GeneralUtil.addMessage("Slide was not deleted.", FacesMessage.SEVERITY_ERROR)
		}
	}

	fun refreshSlide()
	{
		if(this.currentSlide!!.image != null && this.currentSlide!!.image.imageid != null && this.currentSlide!!.image.imageid > 0)
		{
			this.currentSlide!!.image = this.daoUtil.find(Image::class.java, this.currentSlide!!.image.imageid)
		}

		if(this.currentSlide!!.video != null && this.currentSlide!!.video.id != null && this.currentSlide!!.video.id > 0)
		{
			this.currentSlide!!.video = this.daoUtil.find(Video::class.java, this.currentSlide!!.video.id)
		}
	}

	private fun reindexSlides()
	{
		for (presentationSlide in this.presentation!!.presentationSlides)
		{
			presentationSlide.sequence = presentationSlide.sequence!! + 1

			this.daoUtil.update(presentationSlide)
		}


	}

	fun getPresentationTypeName(type: Int): String
	{
		val unitProperty = this.daoUtil.find(UnitProperty::class.java, java.lang.Long.valueOf(375))

		for (unitPropertyListItem in unitProperty!!.unitPropertyListItems)
		{
			if (unitPropertyListItem.itemValue == type.toString())
			{
				return unitPropertyListItem.displayValue
			}
		}

		return type.toString()
	}

	fun onRowSelect(event: SelectEvent)
	{
		this.previewPresentation = event.`object` as Presentation
		this.presentation = this.previewPresentation
	}

	fun refresh()
	{
		//Used to bind data to managed beans and refresh ui
	}

	fun onRowSlideSelect(event: SelectEvent)
	{
		this.presentationSlide = event.`object` as PresentationSlide
	}

	fun addSite()
	{
		val site = this.daoUtil.find(Site::class.java, this.siteListValue!!)

		if (site != null)
		{
			if (this.presentation!!.sites == null)
			{
				this.presentation!!.sites = ArrayList()
			}

			var siteExists = false

			for (cSite in this.presentation!!.sites)
			{
				if (cSite.siteID == site.siteID)
				{
					siteExists = true
				}
			}

			if (!siteExists)
			{
				this.presentation!!.sites.add(site)
			}
		}
	}

	fun removeSite(site: Site)
	{
		this.presentation!!.sites.remove(site)
	}

	fun inSiteList(siteId: Long): Boolean
	{
		for(cSite in this.siteList)
		{
			if(cSite.siteID == siteId)
			{
				return true
			}
		}

		return false
	}

	fun handleImageUpload(event: FileUploadEvent)
	{
		val uploadedFile = event.file

		UploadUtil.uploadImage(this.daoUtil, uploadedFile)

		val imageName = uploadedFile.fileName.replace(" ".toRegex(), "").toLowerCase()
		val images = this.daoUtil.find<Image>("SELECT OBJECT(o) FROM Image o WHERE o.name = $imageName")

		if(images != null && images.isNotEmpty())
		{
			this.currentSlide!!.image = images[0]
		}
	}

	fun handleVideoUpload(event: FileUploadEvent)
	{
		val uploadedFile = event.file

		UploadUtil.uploadVideo(this.daoUtil, uploadedFile)

		val videoName = uploadedFile.fileName.replace(" ".toRegex(), "").toLowerCase()
		val videos = this.daoUtil.find<Video>("SELECT OBJECT(o) FROM Video o WHERE o.name = $videoName")

		if(videos != null && videos.isNotEmpty())
		{
			this.currentSlide!!.video = videos[0]
		}
	}

	fun generateVideoFromUrl()
	{
		val textUtil = GeneralUtil.getManagedBean("textUtil") as TextUtil

		val video = Video()
		video.name = this.videoName
		video.customContent = textUtil.convertVideoUrlToCode(this.videoUrl!!)

		this.daoUtil.create(video)

		this.currentSlide!!.video = video
	}
}
