package com.jimwebguru.iberis.core.web.controllers.cms

import com.jimwebguru.iberis.core.persistence.File
import com.jimwebguru.iberis.core.utils.FileUtil
import com.jimwebguru.iberis.core.web.utils.GeneralUtil
import com.jimwebguru.iberis.core.loaders.GenericLazyLoader
import com.jimwebguru.iberis.core.persistence.*
import com.jimwebguru.iberis.core.utils.PersistenceUtil
import com.jimwebguru.iberis.core.utils.db.DaoUtil
import com.jimwebguru.iberis.core.utils.db.QueryParameter
import org.primefaces.event.FileUploadEvent
import org.primefaces.event.ReorderEvent
import org.primefaces.event.RowEditEvent

import javax.annotation.PostConstruct
import javax.faces.application.FacesMessage
import javax.faces.view.ViewScoped
import javax.inject.Named
import javax.imageio.ImageIO
import java.io.Serializable
import java.util.*

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
@Named
@ViewScoped
open class DynamicContentManagement : Serializable
{
	protected var daoUtil = DaoUtil()

	var loader = GenericLazyLoader(Content::class.java)

	var content: Content? = null

	var currentContentFile: ContentFile? = null

	var pageItemAssignment = PageItemAssignment()

	var pageItemAssignments: List<PageItemAssignment>? = ArrayList()

	var siteList: List<Site>? = null

	var siteListValue: Long? = null

	var existingFile: File? = File()

	var note = Note()

	var contentNotes: List<Note>? = ArrayList()

	var selectedSection: Long? = null

	var contentBlockSection: ContentBlockSection? = ContentBlockSection()

	var contentBlock: ContentBlock? = ContentBlock()

	val pageItemFilter: String
		get()
		{
			var conditionValue = "s.siteID IN ("

			if(this.content != null && this.content!!.sites != null)
			{
				for (site in this.content!!.sites)
				{
					conditionValue += site.siteID
					conditionValue += ","
				}
			}

			conditionValue = conditionValue.trimEnd(',')
			conditionValue += ")"

			if (this.pageItemAssignment.pageItem != null && this.pageItemAssignment.pageItem == 62L)
			{
				return "LEFT JOIN o.sites AS s WHERE o.availableGlobal = TRUE OR $conditionValue"
			}
			else
			{
				return "LEFT JOIN o.sites AS s WHERE $conditionValue"
			}
		}

	@PostConstruct
	open fun postInit()
	{
		val request = GeneralUtil.request

		val contentId = request!!.getParameter("contentId")

		if (contentId != null && contentId.isNotEmpty())
		{
			this.content = this.daoUtil.find(Content::class.java, contentId.toLong())

			this.retrievePageItemAssignments()
			this.retrieveContentNotes()
		}
		else
		{
			this.content = Content()
			this.content!!.type = "page"
			this.content!!.blocksSectionType = 3
			this.content!!.titleSize = "h1"
			this.content!!.blockTitleSize = "h2"
			this.content!!.showTitle = true
			this.content!!.showBlockTitle = true
			this.content!!.pageTemplate = Layout()
		}

		this.resetObjectForUIBind()

		this.siteList = this.daoUtil.findAll(Site::class.java)
	}

	open fun save()
	{
		if (this.content!!.type != "unit_grid" && (this.content!!.body == null || this.content!!.body.isEmpty()) && (this.content!!.customContentTemplate == null || this.content!!.customContentTemplate.isEmpty()))
		{
			GeneralUtil.addMessage("Body is required.", FacesMessage.SEVERITY_ERROR)
		}
		else if (this.content!!.type == "unit_grid" && (this.content!!.unit == null || this.content!!.unit.unitID < 1 || this.content!!.unitGrid == null || this.content!!.unitGrid.gridId < 1))
		{
			GeneralUtil.addMessage("Unit and Unit Grid are required for content with system unit grid.", FacesMessage.SEVERITY_ERROR)
		}
		else if (this.content!!.enableSocialSharing && !this.content!!.enableFacebook && !this.content!!.enableEmail && !this.content!!.enableTwitter && !this.content!!.enableLinkedin)
		{
			GeneralUtil.addMessage("One of the social media platforms has to be enabled to enable social sharing.", FacesMessage.SEVERITY_ERROR)
		}
		else
		{
			if (this.content!!.urlAlias == null || this.content!!.urlAlias.isEmpty())
			{
				val alias = this.createUrlAlias()

				this.content!!.urlAlias = alias
			}

			if(this.content!!.useCustomHtml)
			{
				this.content!!.contentLayout = ""
			}

			this.clearObjectForSave()

			if (this.content!!.id != null && this.content!!.id > 0)
			{
				this.daoUtil.update(this.content!!)

				GeneralUtil.addMessage("Content has been updated.", FacesMessage.SEVERITY_INFO)
			}
			else
			{
				this.daoUtil.create(this.content!!)

				GeneralUtil.addMessage("Content has been created.", FacesMessage.SEVERITY_INFO)
			}

			this.resetObjectForUIBind()

			if (this.content!!.bannerGraphic != null && this.content!!.bannerGraphic.imageid != null && this.content!!.bannerGraphic.imageid > 0)
			{
				this.content!!.bannerGraphic.filePath = this.daoUtil.find(Image::class.java, this.content!!.bannerGraphic.imageid)!!.filePath
			}
		}
	}

	private fun createUrlAlias(): String
	{
		val baseSlug = GeneralUtil.makeUrlSlug(this.content!!.title)

		var slug: String

		var index = 0

		do
		{
			slug = baseSlug

			if (index > 0)
			{
				slug = slug + "-" + index.toString()
			}

			index++

			val params = ArrayList<QueryParameter>()
			params.add(QueryParameter("alias", slug))
			val aliasCount = daoUtil.count("SELECT Count (o) FROM Content o WHERE o.urlAlias=:alias", params)

			val uniqueSlug = (aliasCount <= 0)

		} while (!uniqueSlug)

		return slug
	}

	fun delete(id: Long)
	{
		val contentItem = this.daoUtil.find(Content::class.java, id)

		if (contentItem != null)
		{
			contentItem.sites.clear()

			this.daoUtil.delete(contentItem)

			GeneralUtil.addMessage("Content has been deleted.", FacesMessage.SEVERITY_INFO)
		}
	}

	fun deleteFile(contentFile: ContentFile)
	{
		if (contentFile.id != null && contentFile.id > 0)
		{
			this.daoUtil.delete(contentFile)

			GeneralUtil.addMessage("File has been deleted.", FacesMessage.SEVERITY_INFO)
		}

		this.refreshContent()
	}

	fun handleBannerUpload(event: FileUploadEvent)
	{
		val uploadedFile = event.file

		try
		{
			val persistenceUtil = PersistenceUtil()

			val fileStorageSetting = this.daoUtil.find(SystemSetting::class.java, 1L)

			val directoryPath = fileStorageSetting!!.value + "/images/banners/"

			FileUtil.createFileAndDirectory(directoryPath, directoryPath + uploadedFile.fileName, uploadedFile.inputstream)

			val image = Image()
			image.name = uploadedFile.fileName
			image.filePath = "/image/banners/" + uploadedFile.fileName

			val bufferedImage = ImageIO.read(java.io.File(directoryPath + uploadedFile.fileName))
			image.width = bufferedImage.width.toLong()
			image.height = bufferedImage.height.toLong()
			image.mimeType = persistenceUtil.getMimeType(uploadedFile.contentType)

			val lastIndexOf = uploadedFile.fileName.lastIndexOf(".")
			image.fileExtension = persistenceUtil.getFileExtension(uploadedFile.fileName.substring(lastIndexOf))

			this.daoUtil.create(image)

			this.content!!.bannerGraphic = image

			GeneralUtil.addMessage("Banner has been uploaded. Content has been updated.", FacesMessage.SEVERITY_INFO)
		}
		catch (ex: Exception)
		{
			GeneralUtil.logError(ex)
		}

	}

	fun handleFilesUpload(event: FileUploadEvent)
	{
		val uploadedFile = event.file

		try
		{
			val persistenceUtil = PersistenceUtil()

			val fileStorageSetting = this.daoUtil.find(SystemSetting::class.java, 1L)

			val directoryPath = fileStorageSetting!!.value + "/files/documents/content/" + this.content!!.id + "/"

			val queryParameters = ArrayList<QueryParameter>()

			val queryParameter = QueryParameter()
			queryParameter.parameterName = "filePath"
			queryParameter.parameterValue = "/file/documents/content/" + this.content!!.id + "/" + uploadedFile.fileName

			queryParameters.add(queryParameter)

			val existingFiles = this.daoUtil.find<File>("SELECT OBJECT(o) FROM File o WHERE o.filePath = :filePath", queryParameters)

			var file: File? = null

			if (existingFiles == null || existingFiles.size == 0)
			{
				FileUtil.createFileAndDirectory(directoryPath, directoryPath + uploadedFile.fileName, uploadedFile.inputstream)

				file = File()
				file.fileName = uploadedFile.fileName
				file.displayName = uploadedFile.fileName
				file.filePath = "/file/documents/content/" + this.content!!.id + "/" + uploadedFile.fileName

				file.mimeType = persistenceUtil.getMimeType(uploadedFile.contentType)

				val lastIndexOf = uploadedFile.fileName.lastIndexOf(".")
				file.fileExtension = persistenceUtil.getFileExtension(uploadedFile.fileName.substring(lastIndexOf))

				this.daoUtil.create(file)
			}

			var fileExists = false

			if (this.content!!.contentFiles != null)
			{
				for (cFile in this.content!!.contentFiles)
				{
					if (cFile.file == file)
					{
						fileExists = true
					}
				}
			}

			if (!fileExists)
			{
				val contentFile = ContentFile()
				contentFile.content = this.content
				contentFile.file = file
				contentFile.sequence = 0

				this.daoUtil.create(contentFile)
			}

			GeneralUtil.addMessage("File(s) have been uploaded.", FacesMessage.SEVERITY_INFO)

			this.refreshContent()
		}
		catch (ex: Exception)
		{
			GeneralUtil.logError(ex)
		}

	}

	fun refresh()
	{
		if(this.content!!.bannerGraphic != null && this.content!!.bannerGraphic.imageid != null && this.content!!.bannerGraphic.imageid > 0)
		{
			 this.content!!.bannerGraphic = this.daoUtil.find(Image::class.java, this.content!!.bannerGraphic.imageid)
		}
	}

	fun refreshContent()
	{
		this.daoUtil.refresh(Content::class.java, this.content!!.id)

		this.content = this.daoUtil.find(Content::class.java, this.content!!.id)

		this.resetObjectForUIBind()
	}

	fun retrieveCurrentFile(contentFile: ContentFile)
	{
		this.currentContentFile = contentFile
	}

	fun onRowReorder(event: ReorderEvent)
	{
		val contentFile = event.source as ContentFile

		if (this.content!!.contentFiles.contains(contentFile))
		{
			this.content!!.contentFiles.remove(contentFile)
		}

		var counter = 0

		for (cFile in this.content!!.contentFiles)
		{
			if (counter == event.toIndex)
			{
				counter++
			}

			cFile.sequence = counter

			this.daoUtil.update(cFile)

			counter++
		}

		contentFile.sequence = event.toIndex
		this.daoUtil.update(contentFile)

		this.refreshContent()

	}

	fun saveFile()
	{
		this.daoUtil.update(this.currentContentFile!!.file)

		GeneralUtil.addMessage("File has been updated.", FacesMessage.SEVERITY_INFO)
	}

	fun newPageItemAssignment()
	{
		this.pageItemAssignment = PageItemAssignment()
		this.pageItemAssignment.pageRegion = PageRegion()
		this.pageItemAssignment.site = Site()
	}

	fun selectPageItemAssignment(item: PageItemAssignment)
	{
		this.pageItemAssignment = item
	}

	open fun savePageItemAssignment()
	{
		this.pageItemAssignment.pageItem = 62L
		this.pageItemAssignment.pageItemInstanceId = this.content!!.id

		if (this.pageItemAssignment.id != null && this.pageItemAssignment.id > 0)
		{
			if (this.daoUtil.update(this.pageItemAssignment))
			{
				this.saveUrls()
				GeneralUtil.addMessage("Page Item Assignment has been updated.", FacesMessage.SEVERITY_INFO)
			}
			else
			{
				GeneralUtil.addMessage("Page Item Assignment could not be updated.", FacesMessage.SEVERITY_ERROR)
			}
		}
		else
		{
			if (this.daoUtil.create(this.pageItemAssignment))
			{
				this.saveUrls()
				GeneralUtil.addMessage("Page Item Assignment has been created.", FacesMessage.SEVERITY_INFO)
			}
			else
			{
				GeneralUtil.addMessage("Page Item Assignment could not be created.", FacesMessage.SEVERITY_ERROR)
			}
		}

		this.daoUtil.refresh(PageItemAssignment::class.java, this.pageItemAssignment.id)

		this.retrievePageItemAssignments()
	}

	fun deletePageItemAssignment(assignment: PageItemAssignment)
	{
		if (assignment.id != null && assignment.id > 0)
		{
			this.daoUtil.delete(assignment)

			GeneralUtil.addMessage("Page Item Assignment has been deleted.", FacesMessage.SEVERITY_INFO)
		}

		this.refreshContent()
	}

	private fun retrievePageItemAssignments()
	{
		val queryParameters = ArrayList<QueryParameter>()

		var queryParameter = QueryParameter("pageItem", 62L)
		queryParameters.add(queryParameter)

		queryParameter = QueryParameter("instanceId", this.content!!.id)
		queryParameters.add(queryParameter)

		this.pageItemAssignments = this.daoUtil.find("SELECT OBJECT(o) FROM PageItemAssignment o WHERE o.pageItem = :pageItem AND o.pageItemInstanceId = :instanceId ORDER BY o.site.siteName,o.pageRegion.id", queryParameters)

	}

	private fun saveUrls()
	{
		if (this.pageItemAssignment.allowedUrls != null && this.pageItemAssignment.allowedUrls.isNotEmpty())
		{
			for (allowedUrl in this.pageItemAssignment.allowedUrls)
			{
				if (allowedUrl.id != null && allowedUrl.id > 0)
				{
					this.daoUtil.update(allowedUrl)
				}
			}
		}

		if (this.pageItemAssignment.deniedUrls != null && this.pageItemAssignment.deniedUrls.isNotEmpty())
		{
			for (deniedUrl in this.pageItemAssignment.deniedUrls)
			{
				if (deniedUrl.id != null && deniedUrl.id > 0)
				{
					this.daoUtil.update(deniedUrl)
				}
			}
		}
	}

	fun addAllowedUrl()
	{
		if (this.pageItemAssignment.allowedUrls == null)
		{
			this.pageItemAssignment.allowedUrls = ArrayList()
		}

		val allowedUrl = AllowedUrl()
		allowedUrl.url = this.pageItemAssignment.allowedUrls_temp
		allowedUrl.pageItemAssignment = this.pageItemAssignment

		this.pageItemAssignment.allowedUrls.add(allowedUrl)
	}

	fun removeAllowedUrl(allowedUrl: AllowedUrl)
	{
		if (this.daoUtil.delete(allowedUrl))
		{
			this.pageItemAssignment.allowedUrls.remove(allowedUrl)
			this.daoUtil.refresh(PageItemAssignment::class.java, this.pageItemAssignment.id)
		}
	}

	@SuppressWarnings("unused")
	fun onAllowedUrlsRowEdit(event: RowEditEvent)
	{

	}

	fun addDeniedUrl()
	{
		if (this.pageItemAssignment.deniedUrls == null)
		{
			this.pageItemAssignment.deniedUrls = ArrayList()
		}

		val deniedUrl = DeniedUrl()
		deniedUrl.url = this.pageItemAssignment.deniedUrls_temp
		deniedUrl.pageItemAssignment = this.pageItemAssignment

		this.pageItemAssignment.deniedUrls.add(deniedUrl)
	}

	fun removeDeniedUrl(deniedUrl: DeniedUrl)
	{
		if (this.daoUtil.delete(deniedUrl))
		{
			this.pageItemAssignment.deniedUrls.remove(deniedUrl)
			this.daoUtil.refresh(PageItemAssignment::class.java, this.pageItemAssignment.id)
		}
	}

	@SuppressWarnings("unused")
	fun onDeniedUrlsRowEdit(event: RowEditEvent)
	{

	}

	fun addSite()
	{
		val site = this.daoUtil.find(Site::class.java, this.siteListValue!!)

		if (site != null)
		{
			if (this.content!!.sites == null)
			{
				this.content!!.sites = ArrayList()
			}

			var siteExists = false

			for (cSite in this.content!!.sites)
			{
				if (cSite.siteID == site.siteID)
				{
					siteExists = true
				}
			}

			if (!siteExists)
			{
				this.content!!.sites.add(site)
			}
		}
	}

	fun addExistingFile()
	{
		if (this.existingFile != null && this.existingFile!!.id != null && this.existingFile!!.id > 0)
		{
			if (this.content!!.contentFiles == null)
			{
				this.content!!.contentFiles = ArrayList()
			}

			var addFile = true

			if (this.content!!.contentFiles.isNotEmpty())
			{
				for (contentFile in this.content!!.contentFiles)
				{
					if (contentFile.file.id == this.existingFile!!.id)
					{
						addFile = false
					}
				}
			}

			if (addFile)
			{
				val contentFile = ContentFile()
				contentFile.file = File()
				contentFile.file.id = this.existingFile!!.id
				contentFile.content = this.content
				contentFile.sequence = this.content!!.contentFiles.size

				this.daoUtil.create(contentFile)

				this.save()

				this.daoUtil.refresh(Content::class.java, this.content!!.id)

				this.content = this.daoUtil.find(Content::class.java, this.content!!.id)

				this.resetObjectForUIBind()
			}
		}
	}

	fun tagsAutoComplete(filter: String): List<VocabTerm>?
	{
		return this.daoUtil.find("SELECT OBJECT(o) FROM VocabTerm o WHERE LOWER(o.name) LIKE '" + filter.trim { it <= ' ' }.toLowerCase() + "%'")
	}

	fun newNote()
	{
		this.note = Note()
	}

	fun selectNote(item: Note)
	{
		this.note = item
	}

	fun saveNote()
	{
		this.note.systemUnit = this.daoUtil.find(SystemUnit::class.java, 62L)
		this.note.instanceId = this.content!!.id

		if (this.note.id != null && this.note.id > 0)
		{
			if (this.daoUtil.update(this.note))
			{
				GeneralUtil.addMessage("Note has been updated.", FacesMessage.SEVERITY_INFO)
			}
			else
			{
				GeneralUtil.addMessage("Note could not be updated.", FacesMessage.SEVERITY_ERROR)
			}
		}
		else
		{
			if (this.daoUtil.create(this.note))
			{
				GeneralUtil.addMessage("Note has been created.", FacesMessage.SEVERITY_INFO)
			}
			else
			{
				GeneralUtil.addMessage("Note could not be created.", FacesMessage.SEVERITY_ERROR)
			}
		}

		this.retrieveContentNotes()
	}

	fun deleteNote(item: Note)
	{
		if (item.id != null && item.id > 0)
		{
			if (this.daoUtil.delete(item))
			{
				GeneralUtil.addMessage("Note has been deleted.", FacesMessage.SEVERITY_INFO)

				this.retrieveContentNotes()
			}
		}
	}

	private fun retrieveContentNotes()
	{
		val queryParameters = ArrayList<QueryParameter>()

		var queryParameter = QueryParameter("systemUnit", 62L)
		queryParameters.add(queryParameter)

		queryParameter = QueryParameter("instanceId", this.content!!.id)
		queryParameters.add(queryParameter)

		this.contentNotes = this.daoUtil.find("SELECT OBJECT(o) FROM Note o WHERE o.systemUnit.unitID = :systemUnit AND o.instanceId = :instanceId ORDER BY o.id", queryParameters)

	}

	fun removeSite(site: Site)
	{
		this.content!!.sites.remove(site)
	}

	fun selectSection(section: ContentBlockSection)
	{
		this.contentBlockSection = section
	}

	fun saveSection()
	{
		if (this.contentBlockSection!!.id != null && this.contentBlockSection!!.id > 0)
		{
			this.daoUtil.update(this.contentBlockSection!!)

			GeneralUtil.addMessage("Content Block Section has been updated.", FacesMessage.SEVERITY_INFO)
		}
		else
		{
			this.contentBlockSection!!.content = this.content
			this.daoUtil.create(this.contentBlockSection!!)

			GeneralUtil.addMessage("Content Block Section has been created.", FacesMessage.SEVERITY_INFO)
		}

		this.refreshObjects()
	}

	fun deleteSection(section: ContentBlockSection)
	{
		for (cBlock in section.contentBlocks)
		{
			this.daoUtil.delete(cBlock)
		}

		val success = this.daoUtil.delete(section)

		if (success)
		{
			GeneralUtil.addMessage("Content Block Section has been deleted.", FacesMessage.SEVERITY_INFO)
		}
		else
		{
			GeneralUtil.addMessage("Content Block Section could not be deleted.", FacesMessage.SEVERITY_ERROR)
		}

		this.refreshObjects()
	}

	fun selectContentBlock(cBlock: ContentBlock)
	{
		this.contentBlock = cBlock
	}

	fun saveContentBlock()
	{
		if (this.contentBlock!!.id != null && this.contentBlock!!.id > 0)
		{
			this.daoUtil.update(this.contentBlock!!)

			GeneralUtil.addMessage("Content Block has been updated.", FacesMessage.SEVERITY_INFO)
		}
		else
		{
			if(this.contentBlock!!.contentBlockSection.contentBlocks == null)
			{
				this.contentBlock!!.sequence = 0
			}
			else
			{
				this.contentBlock!!.sequence = this.contentBlock!!.contentBlockSection.contentBlocks.size
			}

			this.daoUtil.create(this.contentBlock!!)

			GeneralUtil.addMessage("Content Block has been created.", FacesMessage.SEVERITY_INFO)
		}

		this.refreshObjects()
	}

	fun reorderBlocks(section: ContentBlockSection, cBlock: ContentBlock, sequenceIndex: Int)
	{
		section.contentBlocks.remove(cBlock)
		cBlock.sequence = sequenceIndex
		this.daoUtil.update(cBlock)

		var counter = 0

		for (f in section.contentBlocks)
		{
			if (counter == sequenceIndex)
			{
				counter++
			}

			f.sequence = counter

			this.daoUtil.update(f)

			counter++
		}

		this.refreshObjects()
	}

	fun addBlockToSection()
	{
		val section = this.daoUtil.find(ContentBlockSection::class.java, this.selectedSection!!)

		if (section != null)
		{
			this.contentBlock!!.contentBlockSection = section

			if (section.contentBlocks != null && section.contentBlocks.isNotEmpty())
			{
				var counter = 0

				for (f in section.contentBlocks)
				{
					f.sequence = counter

					this.daoUtil.update(f)

					counter++
				}

				this.contentBlock!!.sequence = section.contentBlocks.size
			}
			else
			{
				this.contentBlock!!.sequence = 0
			}

			this.daoUtil.update(this.contentBlock!!)

			this.refreshObjects()
		}
	}

	fun removeBlockFromSection(cBlock: ContentBlock)
	{
		//Query just incase stale from cache on field object
		val section = this.daoUtil.find(ContentBlockSection::class.java, cBlock.contentBlockSection.id)

		section!!.contentBlocks.remove(cBlock)

		this.daoUtil.delete(cBlock)

		var counter = 0

		if (section.contentBlocks.isNotEmpty())
		{
			for (f in section.contentBlocks)
			{
				f.sequence = counter

				this.daoUtil.update(f)

				counter++
			}
		}

		this.refreshObjects()
	}

	fun saveBlockLayout()
	{
		if (this.content!!.contentBlockSections != null && this.content!!.contentBlockSections.isNotEmpty())
		{
			for (section in this.content!!.contentBlockSections)
			{
				if (section.contentBlocks != null && section.contentBlocks.isNotEmpty())
				{
					for (cBlock in section.contentBlocks)
					{
						this.daoUtil.update(cBlock)
					}
				}
			}
		}
	}

	fun newSection()
	{
		this.contentBlockSection = ContentBlockSection()

		if(this.content!!.contentBlockSections != null && this.content!!.contentBlockSections.isNotEmpty())
		{
			this.contentBlockSection!!.sequence = this.content!!.contentBlockSections.size
		}
		else
		{
			this.contentBlockSection!!.sequence = 0
		}
	}

	fun newBlock(blockSection: ContentBlockSection)
	{
		this.contentBlock = ContentBlock()
		this.contentBlock!!.width = 12
		this.contentBlock!!.contentBlockSection = blockSection
	}

	fun getContentBlockItemType(pageItem: Long): String?
	{
		val contentUnit = this.daoUtil.find(SystemUnit::class.java, pageItem)

		return contentUnit?.unitName
	}

	fun getContentBlockItemIcon(pageItem: Long): String?
	{
		val contentUnit = this.daoUtil.find(SystemUnit::class.java, pageItem)

		return contentUnit?.singularIcon
	}

	fun getContentBlockItemTitle(pageItem: Long, instanceId: Long): String?
	{
		if(pageItem == 62L)
		{
			val item = this.daoUtil.find(Content::class.java, instanceId)

			return item?.title
		}

		if(pageItem == 61L)
		{
			val item = this.daoUtil.find(Presentation::class.java, instanceId)

			return item?.name
		}

		if(pageItem == 65L)
		{
			val item = this.daoUtil.find(Gallery::class.java, instanceId)

			return item?.name
		}

		if(pageItem == 66L)
		{
			val item = this.daoUtil.find(Menu::class.java, instanceId)

			return item?.name
		}

		if(pageItem == 33L)
		{
			val item = this.daoUtil.find(Image::class.java, instanceId)

			return item?.name
		}

		if(pageItem == 75L)
		{
			val item = this.daoUtil.find(DynamicForm::class.java, instanceId)

			return item?.title
		}

		if(pageItem == 83L)
		{
			val item = this.daoUtil.find(ProductCatalog::class.java, instanceId)

			return item?.name
		}

		if(pageItem == 89L)
		{
			val item = this.daoUtil.find(Video::class.java, instanceId)

			return item?.name
		}

		return ""
	}

	private fun refreshObjects()
	{
		this.daoUtil.refresh(Content::class.java, this.content!!.id)

		if (this.content!!.contentBlockSections != null && this.content!!.contentBlockSections.isNotEmpty())
		{
			for (section in this.content!!.contentBlockSections)
			{
				this.daoUtil.refresh(ContentBlockSection::class.java, section.id)
			}
		}

		this.content = this.daoUtil.find(Content::class.java, this.content!!.id)

		this.resetObjectForUIBind()

		this.contentBlockSection = null
	}

	private fun resetObjectForUIBind()
	{
		if (this.content!!.bannerGraphic == null)
		{
			this.content!!.bannerGraphic = Image()
		}

		if (this.content!!.pageTemplate == null)
		{
			this.content!!.pageTemplate = Layout()
		}

		if (this.content!!.gallery == null)
		{
			this.content!!.gallery = Gallery()
		}

		if (this.content!!.presentation == null)
		{
			this.content!!.presentation = Presentation()
		}

		if (this.content!!.unit == null)
		{
			this.content!!.unit = SystemUnit()
		}

		if (this.content!!.unitGrid == null)
		{
			this.content!!.unitGrid = Grid()
		}
	}

	private fun clearObjectForSave()
	{
		if (this.content!!.bannerGraphic != null && (this.content!!.bannerGraphic.imageid == null || this.content!!.bannerGraphic.imageid == 0L))
		{
			this.content!!.bannerGraphic = null
		}

		if (this.content!!.pageTemplate != null && (this.content!!.pageTemplate.id == null || this.content!!.pageTemplate.id == 0L))
		{
			this.content!!.pageTemplate = null
		}

		if (this.content!!.gallery != null && (this.content!!.gallery.id == null || this.content!!.gallery.id < 1))
		{
			this.content!!.gallery = null
		}

		if (this.content!!.presentation != null && (this.content!!.presentation.id == null || this.content!!.presentation.id < 1))
		{
			this.content!!.presentation = null
		}

		if (this.content!!.unit != null && (this.content!!.unit.unitID == null || this.content!!.unit.unitID < 1))
		{
			this.content!!.unit = null
		}

		if (this.content!!.unitGrid != null && (this.content!!.unitGrid.gridId == null || this.content!!.unitGrid.gridId < 1))
		{
			this.content!!.unitGrid = null
		}

		if(this.content!!.unit == null)
		{
			this.content!!.unitGrid = null
		}
	}
}
