package com.jimwebguru.iberis.core.web.controllers.cms

import com.jimwebguru.iberis.core.utils.db.QueryParameter
import com.jimwebguru.iberis.core.web.controllers.admin.security.Authorize
import java.util.ArrayList

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
object CmsAuthUtil
{
	fun getCmsSiteFilterParameters(): ArrayList<QueryParameter>
	{
		val user = Authorize.getUser()

		if(user != null && user.siteMemberships.isNotEmpty())
		{
			val roleValidated = Authorize.isUserInRole(arrayOf("Admin", "CMS_Admin"))

			if (roleValidated)
			{
				var conditionValue = "s.siteID IN ("

				for (siteMembership in user.siteMemberships)
				{
					conditionValue += siteMembership.site.siteID
					conditionValue += ","
				}

				conditionValue = conditionValue.trimEnd(',')
				conditionValue += ")"

				val queryParameters = ArrayList<QueryParameter>()

				val queryParameter = QueryParameter()
				queryParameter.joinClause = "JOIN o.sites s"
				queryParameter.explicitCondition = conditionValue

				queryParameters.add(queryParameter)

				return queryParameters
			}
			else
			{
				throw Exception("UNAUTHORIZED")
			}
		}
		else
		{
			throw Exception("UNAUTHORIZED")
		}
	}
}