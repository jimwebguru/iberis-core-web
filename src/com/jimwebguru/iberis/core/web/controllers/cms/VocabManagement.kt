package com.jimwebguru.iberis.core.web.controllers.cms

import com.jimwebguru.iberis.core.loaders.GenericLazyLoader
import com.jimwebguru.iberis.core.web.utils.GeneralUtil
import com.jimwebguru.iberis.core.utils.db.DaoUtil
import com.jimwebguru.iberis.core.persistence.VocabTerm
import com.jimwebguru.iberis.core.persistence.Vocabulary
import org.primefaces.event.NodeSelectEvent
import org.primefaces.event.SelectEvent
import org.primefaces.event.TreeDragDropEvent
import org.primefaces.model.DefaultTreeNode
import org.primefaces.model.TreeNode

import javax.annotation.PostConstruct
import javax.faces.application.FacesMessage
import javax.faces.view.ViewScoped
import javax.inject.Named
import java.io.Serializable

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
@Named
@ViewScoped
class VocabManagement : Serializable
{
	var genericLazyLoader = GenericLazyLoader(Vocabulary::class.java)

	var vocab: Vocabulary? = null
	var vocabTerm: VocabTerm? = null

	var selectedNode: TreeNode? = null

	private val daoUtil = DaoUtil()

	val vocabularyTree: DefaultTreeNode?
		get()
		{
			if (this.vocab == null)
			{
				return null
			}

			val rootNode = DefaultTreeNode(this.vocab!!.name, null)
			rootNode.isExpanded = true

			if (this.vocab!!.vocabTermCollection != null)
			{
				for (vocabTerm in this.vocab!!.vocabTermCollection)
				{
					this.createMenuTreeItems(vocabTerm, rootNode)
				}
			}

			return rootNode
		}

	@PostConstruct
	fun postInit()
	{
		val request = GeneralUtil.request

		val id = request!!.getParameter("id")

		if (id != null && id.isNotEmpty())
		{
			this.vocab = this.daoUtil.find(Vocabulary::class.java, id.toLong())
		}
	}

	fun saveVocabTerm()
	{
		var success: Boolean?

		if (this.vocabTerm!!.id != null && this.vocabTerm!!.id > 0)
		{
			success = this.daoUtil.update(this.vocabTerm!!)
		}
		else
		{
			this.vocabTerm!!.vocabulary = this.vocab

			success = this.daoUtil.create(this.vocabTerm!!)
		}

		if (success)
		{
			this.refreshVocab()

			GeneralUtil.addMessage("Tag has been saved.", FacesMessage.SEVERITY_INFO)
		}
		else
		{
			GeneralUtil.addMessage("Tag could not be saved.", FacesMessage.SEVERITY_ERROR)
		}

		this.daoUtil.refresh(Vocabulary::class.java, this.vocab!!.id)

		this.vocab = this.daoUtil.find(Vocabulary::class.java, this.vocab!!.id)

	}

	fun newVocabTerm()
	{
		this.vocabTerm = VocabTerm()
	}

	fun deleteSelectedTerm()
	{
		if(this.vocabTerm != null)
		{
			this.deleteVocabTerm(this.vocabTerm!!)
		}

		this.refreshVocab()

		this.vocab = this.daoUtil.find(Vocabulary::class.java, this.vocab!!.id)
	}

	private fun deleteVocabTerm(term: VocabTerm)
	{
		for (subTerm in term.subTerms)
		{
			this.deleteVocabTerm(subTerm)
		}

		this.daoUtil.delete(term)
	}

	fun saveVocabulary()
	{
		if (this.vocab != null)
		{
			val success: Boolean

			if (this.vocab!!.id != null && this.vocab!!.id > 0)
			{
				success = this.daoUtil.update(this.vocab!!)

				if (success)
				{
					GeneralUtil.addMessage("Tag group updated.", FacesMessage.SEVERITY_INFO)
				}
				else
				{
					GeneralUtil.addMessage("Tag group could not be updated.", FacesMessage.SEVERITY_ERROR)
				}
			}
			else
			{
				success = this.daoUtil.create(this.vocab!!)

				if (success)
				{
					GeneralUtil.addMessage("Tag group created.", FacesMessage.SEVERITY_INFO)

					this.vocabTerm = VocabTerm()
				}
				else
				{
					GeneralUtil.addMessage("Tag group could not be created.", FacesMessage.SEVERITY_ERROR)
				}
			}
		}
	}

	fun newVocabulary()
	{
		this.vocab = Vocabulary()
	}

	fun editVocabulary(voc: Vocabulary)
	{
		this.vocab = voc
		this.vocabTerm = VocabTerm()
	}

	fun deleteVocabulary(voc: Vocabulary)
	{
		for(item in voc.vocabTermCollection)
		{
			this.deleteVocabTerm(item)
		}

		this.daoUtil.delete(voc)

		if (this.vocab == voc)
		{
			this.vocab = null
		}
	}

	fun onRowSelect(event: SelectEvent)
	{
		this.vocab = event.`object` as Vocabulary
		this.vocabTerm = VocabTerm()
	}

	fun onDragDrop(event: TreeDragDropEvent)
	{
		val dragNode = event.dragNode
		val dropNode = event.dropNode

		this.selectedNode = dragNode

		val nodeTitle = dragNode.data.toString()

		var dragVocabTermId = nodeTitle.substring(nodeTitle.indexOf("{id:")).substring(4)
		dragVocabTermId = dragVocabTermId.substring(0, dragVocabTermId.length - 1)

		val dragVocabTerm = this.findVocabTermInVocab(dragVocabTermId.toLong())
		this.vocabTerm = dragVocabTerm

		val sequenceIndex: Int?

		if (dragNode.rowKey.lastIndexOf("_") > -1)
		{
			val rowKey = dragNode.rowKey.substring(dragNode.rowKey.lastIndexOf("_"))

			sequenceIndex = Integer.valueOf(rowKey.substring(1))
		}
		else
		{
			sequenceIndex = Integer.valueOf(dragNode.rowKey)
		}

		dragVocabTerm!!.sequenceIndex = sequenceIndex

		val parentNodeTitle = dropNode.data.toString()

		if (parentNodeTitle.indexOf("{id:") > -1)
		{
			var dropVocabTermId = parentNodeTitle.substring(parentNodeTitle.indexOf("{id:")).substring(4)
			dropVocabTermId = dropVocabTermId.substring(0, dropVocabTermId.length - 1)

			val dropMenuItem = this.findVocabTermInVocab(dropVocabTermId.toLong())

			dragVocabTerm.vocabulary = null
			dragVocabTerm.parentTerm = dropMenuItem

			if (dropMenuItem!!.subTerms != null)
			{
				this.reorderSubMenus(dropMenuItem.subTerms, dragVocabTerm, sequenceIndex)
			}
		}
		else
		{
			dragVocabTerm.vocabulary = this.vocab
			dragVocabTerm.parentTerm = null

			if (this.vocab!!.vocabTermCollection != null)
			{
				this.reorderSubMenus(this.vocab!!.vocabTermCollection, dragVocabTerm, sequenceIndex)
			}
		}

		this.daoUtil.update(dragVocabTerm)

		this.refreshVocab()

		this.vocab = this.daoUtil.find(Vocabulary::class.java, this.vocab!!.id)
	}

	private fun refreshVocab()
	{
		if (this.vocab!!.id != null && this.vocab!!.id > 0)
		{
			if (this.vocab!!.vocabTermCollection != null && this.vocab!!.vocabTermCollection.isNotEmpty())
			{
				for (item in this.vocab!!.vocabTermCollection)
				{
					this.refreshVocabTerm(item)
				}
			}

			this.daoUtil.refresh(Vocabulary::class.java, this.vocab!!.id)
		}
	}

	private fun refreshVocabTerm(vocabTerm: VocabTerm)
	{
		if (vocabTerm.id != null && vocabTerm.id > 0)
		{
			if (vocabTerm.subTerms != null)
			{
				for (item in vocabTerm.subTerms)
				{
					this.refreshVocabTerm(item)
				}
			}

			this.daoUtil.refresh(VocabTerm::class.java, vocabTerm.id)
		}
	}

	private fun reorderSubMenus(subTerms: MutableCollection<VocabTerm>, dragVocabTerm: VocabTerm, sequenceIndex: Int?)
	{
		if (subTerms.contains(dragVocabTerm))
		{
			subTerms.remove(dragVocabTerm)
		}

		var counter = 0

		for (vTerm in subTerms)
		{
			if (counter == sequenceIndex)
			{
				counter++
			}

			vTerm.sequenceIndex = counter

			this.daoUtil.update(vTerm)

			counter++
		}
	}

	fun onNodeSelect(event: NodeSelectEvent)
	{
		val node = event.treeNode
		val nodeTitle = node.data.toString()

		var menuItemId: String? = nodeTitle.substring(nodeTitle.indexOf("{id:")).substring(4)
		menuItemId = menuItemId!!.substring(0, menuItemId.length - 1)

		this.vocabTerm = this.findVocabTermInVocab(menuItemId.toLong())
	}

	private fun createMenuTreeItems(vocabTerm: VocabTerm, root: DefaultTreeNode)
	{
		val node = DefaultTreeNode(vocabTerm.name + " {id:" + vocabTerm.id + "}", root)

		node.isExpanded = false

		if (node == this.selectedNode)
		{
			node.isSelected = true

			this.expandTree(node.parent)
		}

		if (vocabTerm.subTerms != null && !vocabTerm.subTerms.isEmpty())
		{
			for (vTerm in vocabTerm.subTerms)
			{
				this.createMenuTreeItems(vTerm, node)
			}
		}
	}

	private fun expandTree(parent: TreeNode?)
	{
		if (parent != null)
		{
			parent.isExpanded = true

			this.expandTree(parent.parent)
		}
	}

	private fun findVocabTerm(item: VocabTerm, id: Long): VocabTerm?
	{
		var foundVocabTerm: VocabTerm?

		if (item.id == id)
		{
			return item
		}

		if (item.subTerms != null && item.subTerms.isNotEmpty())
		{
			for (sItem in item.subTerms)
			{
				foundVocabTerm = this.findVocabTerm(sItem, id)

				if (foundVocabTerm != null)
				{
					return foundVocabTerm
				}
			}
		}

		return null
	}

	private fun findVocabTermInVocab(id: Long): VocabTerm?
	{
		val targetMenuItem: VocabTerm? = null

		for (term in this.vocab!!.vocabTermCollection)
		{
			if (term.id == id)
			{
				return term
			}

			val t = this.findVocabTerm(term, id)

			if (t != null)
			{
				return t
			}
		}

		return targetMenuItem
	}
}
