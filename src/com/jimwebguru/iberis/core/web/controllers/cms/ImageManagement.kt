package com.jimwebguru.iberis.core.web.controllers.cms

import com.jimwebguru.iberis.core.loaders.GenericLazyLoader
import com.jimwebguru.iberis.core.persistence.*
import com.jimwebguru.iberis.core.utils.db.DaoUtil
import com.jimwebguru.iberis.core.web.controllers.admin.security.Authorize
import com.jimwebguru.iberis.core.web.utils.GeneralUtil
import com.jimwebguru.iberis.core.web.utils.UploadUtil
import org.primefaces.event.FileUploadEvent
import org.primefaces.event.SelectEvent
import org.primefaces.extensions.event.ImageAreaSelectEvent
import org.primefaces.model.CroppedImage

import javax.annotation.PostConstruct
import javax.faces.application.FacesMessage
import javax.faces.view.ViewScoped
import javax.inject.Named
import java.io.File
import java.io.Serializable
import java.util.ArrayList

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
@Named
@ViewScoped
class ImageManagement : Serializable
{
	private val daoUtil = DaoUtil()

	var image: Image? = null

	var croppedImageName: String? = null

	var sliceInfo: String? = null

	var genericLazyLoader = GenericLazyLoader(Image::class.java)

	var siteList = ArrayList<Site>()

	var siteListValue: Long? = null

	@PostConstruct
	fun postInit()
	{
		if (GeneralUtil.getRequestParameterValue("id") != null)
		{
			this.image = this.daoUtil.find(Image::class.java, GeneralUtil.getRequestParameterValue("id").toString().toLong())

			this.croppedImageName = this.image!!.name.replace("[^\\w\\s]".toRegex(), "").replace(" ".toRegex(), "").toLowerCase() + "_cropped"
		}

		this.genericLazyLoader.additionalQueryParams = CmsAuthUtil.getCmsSiteFilterParameters()

		val user = Authorize.getUser()

		if(user != null)
		{
			for (siteMembership in user.siteMemberships)
			{
				this.siteList.add(siteMembership.site)
			}
		}
	}

	fun newImage()
	{
		this.image = Image()
		this.image!!.mimeType = MimeType()
		this.image!!.fileExtension = FileExtension()
	}

	fun save()
	{
		if(this.siteList.size == 1)
		{
			if (this.image!!.sites == null)
			{
				this.image!!.sites = ArrayList()
			}

			if (this.image!!.sites.isEmpty())
			{
				this.image!!.sites.add(this.siteList[0])
			}
		}

		if(this.image!!.sites == null || this.image!!.sites.isEmpty())
		{
			GeneralUtil.addMessage("A site must be associated.", FacesMessage.SEVERITY_ERROR)
		}
		else
		{
			if(this.image!!.overlayColor == null || this.image!!.overlayColor.isEmpty())
			{
				this.image!!.overlayColor = "000000"
			}

			val success: Boolean

			if (this.image!!.imageid != null && this.image!!.imageid > 0)
			{
				success = this.daoUtil.update(this.image!!)
			}
			else
			{
				success = this.daoUtil.create(this.image!!)
			}

			if (success)
			{
				GeneralUtil.addMessage("Image has been saved.", FacesMessage.SEVERITY_INFO)
			}
			else
			{
				GeneralUtil.addMessage("Image could not be saved.", FacesMessage.SEVERITY_ERROR)
			}
		}
	}

	fun handleImageUpload(event: FileUploadEvent)
	{
		val uploadedFile = event.file

		UploadUtil.uploadImage(this.daoUtil, uploadedFile)
	}

	fun handleReplacementImageUpload(event: FileUploadEvent)
	{
		val uploadedFile = event.file

		UploadUtil.uploadReplacementImage(this.daoUtil, uploadedFile, this.image!!)

		this.image = this.daoUtil.find(Image::class.java, this.image!!.imageid)
	}

	fun delete(image: Image)
	{
		val success = this.daoUtil.delete(image)

		if (success)
		{
			this.image = Image()
			this.image!!.mimeType = MimeType()
			this.image!!.fileExtension = FileExtension()

			GeneralUtil.addMessage("Image has been deleted.", FacesMessage.SEVERITY_INFO)
		}
		else
		{
			GeneralUtil.addMessage("Image could not be deleted.", FacesMessage.SEVERITY_ERROR)
		}
	}

	fun saveCroppedImage()
	{
		if(this.sliceInfo != null && this.sliceInfo!!.isNotEmpty())
		{
			val slicedInfo = this.sliceInfo!!.split(",").map { it.toInt() }.toIntArray()

			UploadUtil.uploadCroppedImage(this.daoUtil, this.croppedImageName!!, this.image!!, slicedInfo)
		}
	}

	fun onRowSelect(event: SelectEvent)
	{
		this.image = event.`object` as Image
	}

	fun tagsAutoComplete(filter: String): List<VocabTerm>?
	{
		return this.daoUtil.find("SELECT OBJECT(o) FROM VocabTerm o WHERE LOWER(o.name) LIKE '" + filter.trim { it <= ' ' }.toLowerCase() + "%'")
	}

	fun addSite()
	{
		val site = this.daoUtil.find(Site::class.java, this.siteListValue!!)

		if (site != null)
		{
			if (this.image!!.sites == null)
			{
				this.image!!.sites = ArrayList()
			}

			var siteExists = false

			for (cSite in this.image!!.sites)
			{
				if (cSite.siteID == site.siteID)
				{
					siteExists = true
				}
			}

			if (!siteExists)
			{
				this.image!!.sites.add(site)
			}
		}
	}

	fun removeSite(site: Site)
	{
		this.image!!.sites.remove(site)
	}

	fun inSiteList(siteId: Long): Boolean
	{
		for(cSite in this.siteList)
		{
			if(cSite.siteID == siteId)
			{
				return true
			}
		}

		return false
	}
}
