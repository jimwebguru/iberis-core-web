package com.jimwebguru.iberis.core.web.controllers.cms

import com.jimwebguru.iberis.core.loaders.GenericLazyLoader
import com.jimwebguru.iberis.core.persistence.*
import com.jimwebguru.iberis.core.utils.db.DaoUtil
import com.jimwebguru.iberis.core.web.utils.GeneralUtil

import javax.annotation.PostConstruct
import javax.faces.application.FacesMessage
import javax.faces.view.ViewScoped
import javax.inject.Named
import java.io.Serializable

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
@Named
@ViewScoped
class PageRegionManagement : Serializable
{
	private var daoUtil = DaoUtil()

	var pageRegion: PageRegion? = null

	var lazyLoader = GenericLazyLoader(PageRegion::class.java)

	@PostConstruct
	fun postInit()
	{
		if (GeneralUtil.getRequestParameterValue("id") != null)
		{
			this.pageRegion = this.daoUtil.find(PageRegion::class.java, GeneralUtil.getRequestParameterValue("id").toString().toLong())
		}
	}

	fun newPageRegion()
	{
		this.pageRegion = PageRegion()
	}

	fun selectRegion(item: PageRegion)
	{
		this.pageRegion = item
	}

	fun savePageRegion()
	{
		if (this.pageRegion!!.id != null && this.pageRegion!!.id > 0)
		{
			if (this.daoUtil.update(this.pageRegion!!))
			{
				GeneralUtil.addMessage("Page Region has been updated.", FacesMessage.SEVERITY_INFO)
			}
			else
			{
				GeneralUtil.addMessage("Page Region could not be updated.", FacesMessage.SEVERITY_ERROR)
			}
		}
		else
		{
			if (this.daoUtil.create(this.pageRegion!!))
			{
				GeneralUtil.addMessage("Page Region has been created.", FacesMessage.SEVERITY_INFO)
			}
			else
			{
				GeneralUtil.addMessage("Page Region could not be created.", FacesMessage.SEVERITY_ERROR)
			}
		}
	}

	fun deletePageRegion(region: PageRegion)
	{
		if (region.id != null && region.id > 0)
		{
			this.daoUtil.delete(region)

			this.newPageRegion()

			GeneralUtil.addMessage("Page Region has been deleted.", FacesMessage.SEVERITY_INFO)
		}

	}
}
