package com.jimwebguru.iberis.core.web.controllers.cms

import com.jimwebguru.iberis.core.loaders.GenericLazyLoader
import com.jimwebguru.iberis.core.persistence.*
import com.jimwebguru.iberis.core.utils.db.DaoUtil
import com.jimwebguru.iberis.core.utils.db.QueryParameter
import com.jimwebguru.iberis.core.web.controllers.admin.security.Authorize
import com.jimwebguru.iberis.core.web.utils.GeneralUtil

import javax.annotation.PostConstruct
import javax.faces.application.FacesMessage
import javax.faces.view.ViewScoped
import javax.inject.Named
import java.io.Serializable
import java.util.ArrayList

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
@Named
@ViewScoped
class BlogManagement : Serializable
{
	private val daoUtil = DaoUtil()

	var loader = GenericLazyLoader(Blog::class.java)
	var blogEntryLoader = GenericLazyLoader(BlogEntry::class.java)

	var blog: Blog? = null

	var blogEntry: BlogEntry? = null

	var siteList = ArrayList<Site>()

	var siteListValue: Long? = null

	@PostConstruct
	fun postInit()
	{
		val request = GeneralUtil.request

		val id = request!!.getParameter("id")

		if (id != null && id.isNotEmpty())
		{
			this.blog = this.daoUtil.find(Blog::class.java, id.toLong())
		}

		val entryId = request.getParameter("entryId")

		if(entryId != null && entryId.isNotEmpty())
		{
			this.blogEntry = this.daoUtil.find(BlogEntry::class.java, entryId.toLong())
			this.blog = this.blogEntry!!.blog
		}

		if(this.blog != null)
		{
			val queryParameters = ArrayList<QueryParameter>()

			val queryParameter = QueryParameter("blogId", this.blog!!.id)
			queryParameter.explicitCondition = "o.blog.id = :blogId"

			queryParameters.add(queryParameter)

			this.blogEntryLoader.additionalQueryParams = queryParameters
		}

		val user = Authorize.getUser()

		if(user != null)
		{
			for (siteMembership in user.siteMemberships)
			{
				this.siteList.add(siteMembership.site)
			}
		}

		this.resetObjectForUIBind()
	}

	fun newBlog()
	{
		this.blog = Blog()
	}

	fun selectBlog(item: Blog)
	{
		this.blog = item
	}

	fun saveBlog()
	{
		if(this.blog!!.urlAlias == null || this.blog!!.urlAlias.isEmpty())
		{
			this.blog!!.urlAlias = GeneralUtil.makeUrlSlug(this.blog!!.name)
		}

		if(this.blog!!.readMoreLinkText == null || this.blog!!.readMoreLinkText.isEmpty())
		{
			this.blog!!.readMoreLinkText = "Read More"
		}

		if(this.blog!!.showListThumbnails && (this.blog!!.listThumbnailWidth == null || this.blog!!.listThumbnailWidth < 10))
		{
			this.blog!!.listThumbnailWidth = 25
		}

		if(this.siteList.size == 1)
		{
			if (this.blog!!.sites == null)
			{
				this.blog!!.sites = ArrayList()
			}

			if (this.blog!!.sites.isEmpty())
			{
				this.blog!!.sites.add(this.siteList[0])
			}
		}

		if(this.blog!!.sites == null || this.blog!!.sites.isEmpty())
		{
			GeneralUtil.addMessage("A site must be associated.", FacesMessage.SEVERITY_ERROR)
		}
		else
		{
			if (this.blog!!.id != null && this.blog!!.id > 0)
			{
				if (this.daoUtil.update(this.blog!!))
				{
					GeneralUtil.addMessage("Blog has been updated.", FacesMessage.SEVERITY_INFO)
				}
				else
				{
					GeneralUtil.addMessage("Blog could not be updated.", FacesMessage.SEVERITY_ERROR)
				}
			}
			else
			{
				if (this.daoUtil.create(this.blog!!))
				{
					GeneralUtil.addMessage("Blog has been created.", FacesMessage.SEVERITY_INFO)
				}
				else
				{
					GeneralUtil.addMessage("Blog could not be created.", FacesMessage.SEVERITY_ERROR)
				}
			}
		}
	}

	fun deleteBlog(blog: Blog)
	{
		if (blog.id != null && blog.id > 0)
		{
			val success = this.daoUtil.delete(blog)

			if (success && this.blog?.id == blog.id)
			{
				this.blog = null
			}

			GeneralUtil.addMessage("Blog has been deleted.", FacesMessage.SEVERITY_INFO)
		}
	}

	fun newBlogEntry()
	{
		this.blogEntry = BlogEntry()

		this.resetObjectForUIBind()
	}

	fun selectBlogEntry(item: BlogEntry)
	{
		this.blogEntry = item

		this.resetObjectForUIBind()
	}

	fun saveBlogEntry()
	{
		if(this.blogEntry!!.urlAlias == null || this.blogEntry!!.urlAlias.isEmpty())
		{
			this.blogEntry!!.urlAlias = GeneralUtil.makeUrlSlug(this.blogEntry!!.title)
		}

		this.clearObjectForSave()

		if (this.blogEntry!!.id != null && this.blogEntry!!.id > 0)
		{
			if (this.daoUtil.update(this.blogEntry!!))
			{
				GeneralUtil.addMessage("Blog Entry has been updated.", FacesMessage.SEVERITY_INFO)
			}
			else
			{
				GeneralUtil.addMessage("Blog Entry could not be updated.", FacesMessage.SEVERITY_ERROR)
			}
		}
		else
		{
			this.blogEntry!!.blog = this.blog

			if (this.daoUtil.create(this.blogEntry!!))
			{
				GeneralUtil.addMessage("Blog Entry has been created.", FacesMessage.SEVERITY_INFO)
			}
			else
			{
				GeneralUtil.addMessage("Blog Entry could not be created.", FacesMessage.SEVERITY_ERROR)
			}
		}

		this.resetObjectForUIBind()
	}

	fun deleteBlogEntry(blogEntry: BlogEntry)
	{
		if (blogEntry.id != null && blogEntry.id > 0)
		{
			val success = this.daoUtil.delete(blogEntry)

			if (success && this.blogEntry?.id == blogEntry.id)
			{
				this.blogEntry = null
			}

			GeneralUtil.addMessage("Blog Entry has been deleted.", FacesMessage.SEVERITY_INFO)
		}
	}

	private fun resetObjectForUIBind()
	{
		if(this.blogEntry != null && this.blogEntry!!.headerImage == null)
		{
			this.blogEntry!!.headerImage = Image()
		}

		if(this.blogEntry != null && this.blogEntry!!.bodyImage == null)
		{
			this.blogEntry!!.bodyImage = Image()
		}
	}

	private fun clearObjectForSave()
	{
		if(this.blogEntry != null && this.blogEntry!!.headerImage != null && (this.blogEntry!!.headerImage.imageid == null || this.blogEntry!!.headerImage.imageid < 1))
		{
			this.blogEntry!!.headerImage = null
		}

		if(this.blogEntry != null && this.blogEntry!!.bodyImage != null && (this.blogEntry!!.bodyImage.imageid == null || this.blogEntry!!.bodyImage.imageid < 1))
		{
			this.blogEntry!!.bodyImage = null
		}
	}

	fun refresh()
	{
		if(this.blogEntry!!.headerImage != null && this.blogEntry!!.headerImage.imageid != null && this.blogEntry!!.headerImage.imageid > 0)
		{
			this.blogEntry!!.headerImage = this.daoUtil.find(Image::class.java, this.blogEntry!!.headerImage.imageid)
		}

		if(this.blogEntry!!.bodyImage != null && this.blogEntry!!.bodyImage.imageid != null && this.blogEntry!!.bodyImage.imageid > 0)
		{
			this.blogEntry!!.bodyImage = this.daoUtil.find(Image::class.java, this.blogEntry!!.bodyImage.imageid)
		}
	}

	fun addSite()
	{
		val site = this.daoUtil.find(Site::class.java, this.siteListValue!!)

		if (site != null)
		{
			if (this.blog!!.sites == null)
			{
				this.blog!!.sites = ArrayList()
			}

			var siteExists = false

			for (cSite in this.blog!!.sites)
			{
				if (cSite.siteID == site.siteID)
				{
					siteExists = true
				}
			}

			if (!siteExists)
			{
				this.blog!!.sites.add(site)
			}
		}
	}

	fun removeSite(site: Site)
	{
		this.blog!!.sites.remove(site)
	}

	fun inSiteList(siteId: Long): Boolean
	{
		for(cSite in this.siteList)
		{
			if(cSite.siteID == siteId)
			{
				return true
			}
		}

		return false
	}
}
