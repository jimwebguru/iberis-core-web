package com.jimwebguru.iberis.core.web.controllers.cms

import com.jimwebguru.iberis.core.loaders.GenericLazyLoader
import com.jimwebguru.iberis.core.persistence.*
import com.jimwebguru.iberis.core.utils.db.DaoUtil
import com.jimwebguru.iberis.core.web.controllers.admin.security.Authorize
import com.jimwebguru.iberis.core.web.utils.GeneralUtil
import com.jimwebguru.iberis.core.web.utils.TextUtil
import com.jimwebguru.iberis.core.web.utils.UploadUtil
import org.primefaces.event.FileUploadEvent
import org.primefaces.event.SelectEvent

import javax.annotation.PostConstruct
import javax.faces.application.FacesMessage
import javax.faces.view.ViewScoped
import javax.inject.Named
import java.io.Serializable
import java.util.ArrayList

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
@Named
@ViewScoped
class VideoManagement : Serializable
{
	private val daoUtil = DaoUtil()

	var video: Video? = null

	var embedUrl: String? = null

	var genericLazyLoader = GenericLazyLoader(Video::class.java)

	var siteList = ArrayList<Site>()

	var siteListValue: Long? = null

	@PostConstruct
	fun postInit()
	{
		if (GeneralUtil.getRequestParameterValue("id") != null)
		{
			this.video = this.daoUtil.find(Video::class.java, GeneralUtil.getRequestParameterValue("id").toString().toLong())

			this.resetObjectForUIBind()
		}

		this.genericLazyLoader.additionalQueryParams = CmsAuthUtil.getCmsSiteFilterParameters()

		val user = Authorize.getUser()

		if(user != null)
		{
			for (siteMembership in user.siteMemberships)
			{
				this.siteList.add(siteMembership.site)
			}
		}
	}

	fun newVideo()
	{
		this.video = Video()
		this.video!!.mimeType = MimeType()
		this.video!!.fallbackImage = Image()
	}

	fun save()
	{
		if(this.siteList.size == 1)
		{
			if (this.video!!.sites == null)
			{
				this.video!!.sites = ArrayList()
			}

			if (this.video!!.sites.isEmpty())
			{
				this.video!!.sites.add(this.siteList[0])
			}
		}

		if(this.video!!.sites == null || this.video!!.sites.isEmpty())
		{
			GeneralUtil.addMessage("A site must be associated.", FacesMessage.SEVERITY_ERROR)
		}
		else if(this.video!!.videoAsBackground && (this.video!!.filePath == null || this.video!!.filePath.isEmpty()) && (this.video!!.videoPlatform == null || this.video!!.videoPlatform < 1 || this.video!!.platformVideoId == null || this.video!!.platformVideoId.isEmpty()))
		{
			GeneralUtil.addMessage("A video that is displayed as background must have a file path or be a video on a platform.", FacesMessage.SEVERITY_ERROR)
		}
		else
		{
			val success: Boolean

			this.clearObjectForSave()

			if (this.video!!.id != null && this.video!!.id > 0)
			{
				success = this.daoUtil.update(this.video!!)
			}
			else
			{
				success = this.daoUtil.create(this.video!!)
			}

			if (success)
			{
				GeneralUtil.addMessage("Video has been saved.", FacesMessage.SEVERITY_INFO)
			}
			else
			{
				GeneralUtil.addMessage("Video could not be saved.", FacesMessage.SEVERITY_ERROR)
			}

			this.resetObjectForUIBind()
		}
	}

	fun handleVideoUpload(event: FileUploadEvent)
	{
		val uploadedFile = event.file

		UploadUtil.uploadVideo(this.daoUtil, uploadedFile)
	}

	fun delete(vid: Video)
	{
		val success = this.daoUtil.delete(vid)

		if (success)
		{
			this.video = Video()
			this.video!!.mimeType = MimeType()

			GeneralUtil.addMessage("Video has been deleted.", FacesMessage.SEVERITY_INFO)
		}
		else
		{
			GeneralUtil.addMessage("Video could not be deleted.", FacesMessage.SEVERITY_ERROR)
		}
	}

	fun onRowSelect(event: SelectEvent)
	{
		this.video = event.`object` as Video

		this.resetObjectForUIBind()

		if (this.video!!.mimeType == null)
		{
			this.video!!.mimeType = MimeType()
		}
	}

	fun tagsAutoComplete(filter: String): List<VocabTerm>?
	{
		return this.daoUtil.find("SELECT OBJECT(o) FROM VocabTerm o WHERE LOWER(o.name) LIKE '" + filter.trim { it <= ' ' }.toLowerCase() + "%'")
	}

	fun generateEmbedCode()
	{
		val textUtil = GeneralUtil.getManagedBean("textUtil") as TextUtil

		this.video!!.customContent = textUtil.convertVideoUrlToCode(this.embedUrl!!)
	}

	private fun resetObjectForUIBind()
	{
		if (this.video!!.mimeType == null)
		{
			this.video!!.mimeType = MimeType()
		}
		
		if (this.video!!.fallbackImage == null)
		{
			this.video!!.fallbackImage = Image()
		}
	}

	private fun clearObjectForSave()
	{
		if (this.video!!.mimeType != null && (this.video!!.mimeType.typeid == null || this.video!!.mimeType.typeid < 1))
		{
			this.video!!.mimeType = null
		}
		
		if (this.video!!.fallbackImage != null && (this.video!!.fallbackImage.imageid == null || this.video!!.fallbackImage.imageid < 1))
		{
			this.video!!.fallbackImage = null
		}
	}

	fun addSite()
	{
		val site = this.daoUtil.find(Site::class.java, this.siteListValue!!)

		if (site != null)
		{
			if (this.video!!.sites == null)
			{
				this.video!!.sites = ArrayList()
			}

			var siteExists = false

			for (cSite in this.video!!.sites)
			{
				if (cSite.siteID == site.siteID)
				{
					siteExists = true
				}
			}

			if (!siteExists)
			{
				this.video!!.sites.add(site)
			}
		}
	}

	fun removeSite(site: Site)
	{
		this.video!!.sites.remove(site)
	}

	fun inSiteList(siteId: Long): Boolean
	{
		for(cSite in this.siteList)
		{
			if(cSite.siteID == siteId)
			{
				return true
			}
		}

		return false
	}

	fun refresh()
	{
		if(this.video!!.fallbackImage != null && this.video!!.fallbackImage.imageid != null && this.video!!.fallbackImage.imageid > 0)
		{
			this.video!!.fallbackImage = this.daoUtil.find(Image::class.java, this.video!!.fallbackImage.imageid)
		}
	}
}
