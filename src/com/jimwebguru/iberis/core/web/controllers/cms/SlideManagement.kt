package com.jimwebguru.iberis.core.web.controllers.cms

import com.jimwebguru.iberis.core.loaders.GenericLazyLoader
import com.jimwebguru.iberis.core.persistence.*
import com.jimwebguru.iberis.core.utils.db.DaoUtil
import com.jimwebguru.iberis.core.utils.db.QueryParameter
import com.jimwebguru.iberis.core.web.utils.GeneralUtil

import javax.annotation.PostConstruct
import javax.faces.application.FacesMessage
import javax.faces.view.ViewScoped
import javax.inject.Named
import java.io.Serializable
import java.util.ArrayList

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
@Named
@ViewScoped
class SlideManagement : Serializable
{
	var lazyLoader = GenericLazyLoader(Slide::class.java)

	var currentSlide: Slide? = null

	private val daoUtil = DaoUtil()

	@PostConstruct
	fun init()
	{

	}

	fun newSlide()
	{
		this.currentSlide = Slide()
		
		this.resetObjectForUIBind()
	}

	fun selectSlide(slide: Slide)
	{
		this.currentSlide = slide

		this.resetObjectForUIBind()
	}

	fun saveSlide()
	{
		this.clearObjectForSave()

		if (this.currentSlide!!.id != null && this.currentSlide!!.id > 0)
		{
			if (this.daoUtil.update(this.currentSlide!!))
			{
				GeneralUtil.addMessage("Slide has been updated.", FacesMessage.SEVERITY_INFO)
			}
			else
			{
				GeneralUtil.addMessage("Slide was not updated.", FacesMessage.SEVERITY_ERROR)
			}
		}
		else
		{
			if (this.daoUtil.create(this.currentSlide!!))
			{
				GeneralUtil.addMessage("Slide has been created.", FacesMessage.SEVERITY_INFO)
			}
			else
			{
				GeneralUtil.addMessage("Slide was not created.", FacesMessage.SEVERITY_ERROR)
			}
		}

		this.resetObjectForUIBind()
	}

	fun deleteSlide(slide: Slide)
	{
		val queryParameters = ArrayList<QueryParameter>()

		val parameter = QueryParameter()
		parameter.parameterName = "id"
		parameter.parameterValue = slide.id

		queryParameters.add(parameter)
		val presentationSlides = this.daoUtil.find<PresentationSlide>("SELECT OBJECT(o) FROM PresentationSlide o WHERE o.slide.id = :id", queryParameters)

		for (pSlide in presentationSlides!!)
		{
			this.daoUtil.delete(pSlide)
		}

		if (this.daoUtil.delete(slide))
		{
			GeneralUtil.addMessage("Slide has been deleted.", FacesMessage.SEVERITY_INFO)

			this.newSlide()
		}
		else
		{
			GeneralUtil.addMessage("Slide was not deleted.", FacesMessage.SEVERITY_ERROR)
		}
	}

	fun refreshSlide()
	{
		if(this.currentSlide!!.image != null && this.currentSlide!!.image.imageid != null && this.currentSlide!!.image.imageid > 0)
		{
			this.currentSlide!!.image = this.daoUtil.find(Image::class.java, this.currentSlide!!.image.imageid)
		}

		if(this.currentSlide!!.video != null && this.currentSlide!!.video.id != null && this.currentSlide!!.video.id > 0)
		{
			this.currentSlide!!.video = this.daoUtil.find(Video::class.java, this.currentSlide!!.video.id)
		}
	}

	fun slideFoundIn(slide: Slide): String
	{
		val presentations = ArrayList<Presentation>()

		val presentationSlides = this.daoUtil.find<PresentationSlide>("SELECT OBJECT(o) FROM PresentationSlide o WHERE o.slide.id = " + slide.id!!)

		for (presentationSlide in presentationSlides!!)
		{
			if (!presentations.contains(presentationSlide.presentation))
			{
				presentations.add(presentationSlide.presentation)
			}
		}

		val foundIn = StringBuilder()

		for (presentation in presentations)
		{
			foundIn.append(presentation.name)
			foundIn.append(",")
		}

		return if (foundIn.isEmpty())
		{
			foundIn.toString()
		}
		else foundIn.toString().substring(0, foundIn.toString().length - 1)

	}

	private fun resetObjectForUIBind()
	{
		if (this.currentSlide!!.image == null)
		{
			this.currentSlide!!.image = Image()
		}

		if(this.currentSlide!!.video == null)
		{
			this.currentSlide!!.video = Video()
		}
	}

	private fun clearObjectForSave()
	{
		if (this.currentSlide!!.image != null && (this.currentSlide!!.image.imageid == null || this.currentSlide!!.image.imageid < 1))
		{
			this.currentSlide!!.image = null
		}

		if (this.currentSlide!!.video != null && (this.currentSlide!!.video.id == null || this.currentSlide!!.video.id < 1))
		{
			this.currentSlide!!.video = null
		}
	}
}
