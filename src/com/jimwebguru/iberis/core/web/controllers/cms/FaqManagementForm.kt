package com.jimwebguru.iberis.core.web.controllers.cms

import com.jimwebguru.iberis.core.web.utils.GeneralUtil
import com.jimwebguru.iberis.core.utils.db.DaoUtil
import com.jimwebguru.iberis.core.persistence.FAQ
import com.jimwebguru.iberis.core.persistence.VocabTerm
import org.primefaces.model.CheckboxTreeNode
import org.primefaces.model.TreeNode

import javax.annotation.PostConstruct
import javax.faces.application.FacesMessage
import javax.faces.view.ViewScoped
import javax.inject.Named
import java.io.Serializable
import java.util.ArrayList

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
@Named
@ViewScoped
class FaqManagementForm : Serializable
{
	var faq: FAQ? = null
	private val daoUtil = DaoUtil()

	var root: TreeNode? = null

	var selectedCategories: Array<TreeNode>? = null

	private val faqCategories: List<VocabTerm>?
		get() = daoUtil.find("SELECT object(o) FROM VocabTerm o WHERE o.vocabulary = 1")

	@PostConstruct
	fun postInit()
	{
		val request = GeneralUtil.request

		val faqId = request!!.getParameter("faqid")

		if (faqId != null)
		{
			this.faq = daoUtil.find(FAQ::class.java, java.lang.Long.valueOf(faqId))
		}
		else
		{
			this.faq = FAQ()
		}

		val selectedCats = this.faq!!.vocabTermCollection


		root = CheckboxTreeNode("ROOT", null)

		val categories = this.faqCategories

		for (term in categories!!)
		{
			val parent = CheckboxTreeNode(term, root)
			parent.isExpanded = true

			val subTerms = term.subTerms
			if (subTerms != null)
			{
				for (sub in term.subTerms)
				{
					val child = CheckboxTreeNode(sub, parent)

					if (selectedCats != null)
					{
						for (selectedTerm in selectedCats)
						{
							if (selectedTerm.id == sub.id)
							{
								child.isSelected = true
							}
						}
					}
				}
			}

		}

	}

	fun saveFaq(): String
	{
		val faqSaved: Boolean

		val categories = ArrayList<VocabTerm>()

		for (node in this.selectedCategories!!)
		{
			categories.add(node.data as VocabTerm)
		}

		this.faq!!.vocabTermCollection = categories

		if (this.faq!!.id == null)
		{
			faqSaved = daoUtil.create(faq!!)
		}
		else
		{
			faqSaved = daoUtil.update(faq!!)
		}

		if (faqSaved)
		{
			GeneralUtil.addMessage("FAQ saved", FacesMessage.SEVERITY_INFO)
			return "pretty:faq-management"
		}
		else
		{
			GeneralUtil.addMessage("Could not save FAQ", FacesMessage.SEVERITY_ERROR)
			return ""
		}
	}
}
