package com.jimwebguru.iberis.core.web.controllers.cms

import com.jimwebguru.iberis.core.loaders.GenericLazyLoader
import com.jimwebguru.iberis.core.persistence.*
import com.jimwebguru.iberis.core.utils.db.DaoUtil
import com.jimwebguru.iberis.core.web.controllers.admin.security.Authorize
import com.jimwebguru.iberis.core.web.utils.GeneralUtil
import org.primefaces.event.NodeSelectEvent
import org.primefaces.event.SelectEvent
import org.primefaces.event.TreeDragDropEvent
import org.primefaces.model.DefaultTreeNode
import org.primefaces.model.TreeNode

import javax.annotation.PostConstruct
import javax.faces.application.FacesMessage
import javax.faces.view.ViewScoped
import javax.inject.Named
import java.io.Serializable
import java.util.ArrayList

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
@Named
@ViewScoped
class MenuManagement : Serializable
{
	var menu: Menu? = Menu()
	var menuItem: MenuItem? = MenuItem()
	var menuColumn: MenuColumn? = null

	var selectedNode: TreeNode? = null

	private val daoUtil = DaoUtil()

	var genericLazyLoader = GenericLazyLoader(Menu::class.java)

	var userRoles: List<Role>? = null

	var siteList = ArrayList<Site>()

	var siteListValue: Long? = null

	val menuTree: DefaultTreeNode?
		get()
		{
			if (this.menu == null)
			{
				return null
			}

			val rootNode = DefaultTreeNode(this.menu!!.name, null)
			rootNode.isExpanded = true

			if (this.menu!!.menuItemCollection != null)
			{
				for (menuItem in this.menu!!.menuItemCollection)
				{
					this.createMenuTreeItems(menuItem, rootNode)
				}
			}

			return rootNode
		}

	@PostConstruct
	fun postInit()
	{
		if (GeneralUtil.getRequestParameterValue("id") != null)
		{
			this.menu = this.daoUtil.find(Menu::class.java, GeneralUtil.getRequestParameterValue("id").toString().toLong())

			this.resetObjectForUIBind()
		}

		this.userRoles = daoUtil.find("SELECT Object(r) FROM Role r ORDER BY r.roleName ASC")

		this.genericLazyLoader.additionalQueryParams = CmsAuthUtil.getCmsSiteFilterParameters()

		val user = Authorize.getUser()

		if(user != null)
		{
			for (siteMembership in user.siteMemberships)
			{
				this.siteList.add(siteMembership.site)
			}
		}
	}

	fun saveMenuItem()
	{
		val success: Boolean

		if (this.menuItem!!.id != null && this.menuItem!!.id > 0)
		{
			success = this.daoUtil.update(this.menuItem!!)
		}
		else
		{
			this.menuItem!!.parentMenu = this.menu

			success = this.daoUtil.create(this.menuItem!!)
		}

		if (success)
		{
			this.refreshMenu()

			this.menu = this.daoUtil.find(Menu::class.java, this.menu!!.id)

			GeneralUtil.addMessage("MenuItem has been saved.", FacesMessage.SEVERITY_INFO)
		}
		else
		{
			GeneralUtil.addMessage("MenuItem could not be saved.", FacesMessage.SEVERITY_ERROR)
		}
	}


	fun deleteMenuItem()
	{
		val success: Boolean

		if (this.menuItem!!.id != null && this.menuItem!!.id > 0)
		{
			success = this.daoUtil.delete(this.menuItem!!)

			if (success)
			{
				this.refreshMenu()

				this.menu = this.daoUtil.find(Menu::class.java, this.menu!!.id)

				GeneralUtil.addMessage("MenuItem has been deleted.", FacesMessage.SEVERITY_INFO)
			}
			else
			{
				GeneralUtil.addMessage("MenuItem could not be deleted.", FacesMessage.SEVERITY_ERROR)
			}
		}
	}

	fun newMenu()
	{
		this.menu = Menu()
		this.menuItem = MenuItem()

		this.resetObjectForUIBind()
	}

	fun newMenuItem()
	{
		this.menuItem = MenuItem()
	}

	fun save()
	{
		if(this.siteList.size == 1)
		{
			if (this.menu!!.sites == null)
			{
				this.menu!!.sites = ArrayList()
			}

			if (this.menu!!.sites.isEmpty())
			{
				this.menu!!.sites.add(this.siteList[0])
			}
		}

		if(this.menu!!.sites == null || this.menu!!.sites.isEmpty())
		{
			GeneralUtil.addMessage("A site must be associated.", FacesMessage.SEVERITY_ERROR)
		}
		else
		{
			if (this.menu!!.type == 8 && (this.menu!!.buttonText == null || this.menu!!.buttonText.isEmpty()))
			{
				GeneralUtil.addMessage("Button text is required for menu of type Menu Button.", FacesMessage.SEVERITY_ERROR)
			}
			else
			{
				var success: Boolean?

				this.clearObjectForSave()

				if (this.menu!!.id != null && this.menu!!.id > 0)
				{
					success = this.daoUtil.update(this.menu!!)

					if (success)
					{
						GeneralUtil.addMessage("Menu updated.", FacesMessage.SEVERITY_INFO)
					}
					else
					{
						GeneralUtil.addMessage("Menu could not be updated.", FacesMessage.SEVERITY_ERROR)
					}
				}
				else
				{
					success = this.daoUtil.create(this.menu!!)

					if (success)
					{
						GeneralUtil.addMessage("Menu created.", FacesMessage.SEVERITY_INFO)
					}
					else
					{
						GeneralUtil.addMessage("Menu could not be created.", FacesMessage.SEVERITY_ERROR)
					}
				}

				this.resetObjectForUIBind()
			}
		}
	}

	fun editMenu(menu: Menu)
	{
		this.menu = menu
		this.menuItem = MenuItem()

		if (this.menu!!.menuToCrumb == null)
		{
			this.menu!!.menuToCrumb = Menu()
		}
	}

	fun deleteMenu(menu: Menu)
	{
		if (this.daoUtil.delete(menu))
		{
			if (this.menu == menu)
			{
				this.menu = Menu()
				this.menuItem = MenuItem()
			}

			GeneralUtil.addMessage("Menu deleted.", FacesMessage.SEVERITY_INFO)
		}
		else
		{
			GeneralUtil.addMessage("Menu could not be deleted.", FacesMessage.SEVERITY_ERROR)
		}

	}

	fun onRowSelect(event: SelectEvent)
	{
		this.menu = event.`object` as Menu
		this.menuItem = MenuItem()
	}

	fun onDragDrop(event: TreeDragDropEvent)
	{
		val dragNode = event.dragNode
		val dropNode = event.dropNode

		this.selectedNode = dragNode

		val nodeTitle = dragNode.data.toString()

		var dragMenuItemId = nodeTitle.substring(nodeTitle.indexOf("{id:")).substring(4)
		dragMenuItemId = dragMenuItemId.substring(0, dragMenuItemId.length - 1)

		val dragMenuItem = this.findMenuItemInMenu(dragMenuItemId.toLong())
		this.menuItem = dragMenuItem

		val sequenceIndex: Int?

		if (dragNode.rowKey.lastIndexOf("_") > -1)
		{
			val rowKey = dragNode.rowKey.substring(dragNode.rowKey.lastIndexOf("_"))

			sequenceIndex = Integer.valueOf(rowKey.substring(1))
		}
		else
		{
			sequenceIndex = Integer.valueOf(dragNode.rowKey)
		}

		dragMenuItem!!.sequenceIndex = sequenceIndex

		val parentNodeTitle = dropNode.data.toString()

		if (parentNodeTitle.indexOf("{id:") > -1)
		{
			var dropMenuItemId = parentNodeTitle.substring(parentNodeTitle.indexOf("{id:")).substring(4)
			dropMenuItemId = dropMenuItemId.substring(0, dropMenuItemId.length - 1)

			val dropMenuItem = this.findMenuItemInMenu(dropMenuItemId.toLong())

			dragMenuItem.parentMenu = null
			dragMenuItem.parentMenuItem = dropMenuItem

			this.daoUtil.update(dragMenuItem)

			this.reorderSubMenus(dropMenuItem!!.subMenuItems, dragMenuItem, sequenceIndex)
		}
		else
		{
			dragMenuItem.parentMenu = this.menu
			dragMenuItem.parentMenuItem = null

			this.daoUtil.update(dragMenuItem)

			this.reorderSubMenus(this.menu!!.menuItemCollection, dragMenuItem, sequenceIndex)
		}

		this.refreshMenu()

		this.menu = this.daoUtil.find(Menu::class.java, this.menu!!.id)
	}

	private fun refreshMenu()
	{
		if (this.menu!!.id != null && this.menu!!.id > 0)
		{
			if (this.menu!!.menuItemCollection != null && this.menu!!.menuItemCollection.isNotEmpty())
			{
				for (item in this.menu!!.menuItemCollection)
				{
					this.refreshMenuItem(item)
				}
			}

			this.daoUtil.refresh(Menu::class.java, menu!!.id)
		}
	}

	private fun refreshMenuItem(menuItem: MenuItem)
	{
		if (menuItem.id != null && menuItem.id > 0)
		{
			if (menuItem.subMenuItems != null)
			{
				for (item in menuItem.subMenuItems)
				{
					this.refreshMenuItem(item)
				}
			}

			this.daoUtil.refresh(MenuItem::class.java, menuItem.id)
		}
	}

	private fun reorderSubMenus(subMenus: MutableCollection<MenuItem>, dragMenuItem: MenuItem, sequenceIndex: Int?)
	{
		if (subMenus.contains(dragMenuItem))
		{
			subMenus.remove(dragMenuItem)
		}

		var counter = 0

		for (mItem in subMenus)
		{
			if (counter == sequenceIndex)
			{
				counter++
			}

			mItem.sequenceIndex = counter

			this.daoUtil.update(mItem)

			counter++
		}
	}

	fun onNodeSelect(event: NodeSelectEvent)
	{
		val node = event.treeNode
		val nodeTitle = node.data.toString()

		var menuItemId: String? = nodeTitle.substring(nodeTitle.indexOf("{id:")).substring(4)
		menuItemId = menuItemId!!.substring(0, menuItemId.length - 1)

		this.menuItem = this.findMenuItemInMenu(menuItemId.toLong())
	}

	private fun createMenuTreeItems(menuItem: MenuItem, root: DefaultTreeNode)
	{
		val node = DefaultTreeNode(menuItem.displayName + " {id:" + menuItem.id + "}", root)

		node.isExpanded = false

		if (node == this.selectedNode)
		{
			node.isSelected = true

			this.expandTree(node.parent)
		}

		if (menuItem.subMenuItems != null && !menuItem.subMenuItems.isEmpty())
		{
			for (m in menuItem.subMenuItems)
			{
				this.createMenuTreeItems(m, node)
			}
		}
	}

	private fun expandTree(parent: TreeNode?)
	{
		if (parent != null)
		{
			parent.isExpanded = true

			this.expandTree(parent.parent)
		}
	}

	private fun findMenuItem(item: MenuItem, id: Long): MenuItem?
	{
		var foundMenuItem: MenuItem?

		if (item.id == id)
		{
			return item
		}

		if (item.subMenuItems != null && item.subMenuItems.size > 0)
		{
			for (sItem in item.subMenuItems)
			{
				foundMenuItem = this.findMenuItem(sItem, id)

				if (foundMenuItem != null)
				{
					return foundMenuItem
				}
			}
		}

		return null
	}

	private fun findMenuItemInMenu(id: Long): MenuItem?
	{
		val targetMenuItem: MenuItem? = null

		for (m in this.menu!!.menuItemCollection)
		{
			if (m.id == id)
			{
				return m
			}

			val t = this.findMenuItem(m, id)

			if (t != null)
			{
				return t
			}
		}

		return targetMenuItem
	}

	/*public SelectItem[] userRoles()
	{

		List<Role> roles = daoUtil.find("SELECT Object(r) FROM Role r ORDER BY r.roleName ASC");

		SelectItem[] items = new SelectItem[roles.size()];

		int index = 0;
		for (Role r : roles)
		{
			items[index++] = new SelectItem(r.getId(), r.getRoleName());
		}

		return items;
	} */

	fun saveMegamenu()
	{
		this.daoUtil.update(this.menu!!)

		if (this.menu!!.menuItemCollection != null)
		{
			for (subMenu in this.menu!!.menuItemCollection)
			{
				this.daoUtil.update(subMenu)

				if (subMenu.subMenuColumns != null)
				{
					for (menuColumn in subMenu.subMenuColumns)
					{
						this.daoUtil.update(menuColumn)

						if (menuColumn.subMenuItems != null)
						{
							for (columnSubMenu in menuColumn.subMenuItems)
							{
								this.daoUtil.update(columnSubMenu)

								if (columnSubMenu.subMenuItems != null)
								{
									for (menuItem in columnSubMenu.subMenuItems)
									{
										this.daoUtil.update(menuItem)
									}
								}
							}
						}
					}
				}
			}
		}
	}

	private fun refreshMegaMenu()
	{
		this.saveMegamenu()

		this.daoUtil.refresh(Menu::class.java, this.menu!!.id)

		if (this.menu!!.menuItemCollection != null)
		{
			for (subMenu in this.menu!!.menuItemCollection)
			{
				this.daoUtil.refresh(MenuItem::class.java, subMenu.id)

				if (subMenu.subMenuColumns != null)
				{
					for (menuColumn in subMenu.subMenuColumns)
					{
						this.daoUtil.refresh(MenuColumn::class.java, menuColumn.id)

						if (menuColumn.subMenuItems != null)
						{
							for (columnSubMenu in menuColumn.subMenuItems)
							{
								this.daoUtil.refresh(MenuItem::class.java, columnSubMenu.id)
							}
						}
					}
				}
			}
		}

		this.menu = this.daoUtil.find(Menu::class.java, this.menu!!.id)
	}

	fun newMegamenuSection()
	{
		val subMenu = MenuItem()
		subMenu.displayName = "New Section"
		subMenu.subMenuGroup = true
		subMenu.parentMenu = this.menu

		if (this.menu!!.menuItemCollection != null)
		{
			subMenu.sequenceIndex = this.menu!!.menuItemCollection.size
		}
		else
		{
			this.menu!!.menuItemCollection = ArrayList()

			subMenu.sequenceIndex = 0
		}

		val success = this.daoUtil.create(subMenu)

		if (success)
		{
			this.menu!!.menuItemCollection.add(subMenu)
		}

		this.daoUtil.refresh(Menu::class.java, this.menu!!.id)

		this.menu = this.daoUtil.find(Menu::class.java, this.menu!!.id)
	}

	fun reorderMegamenuSection(subMenu: MenuItem, index: Int)
	{
		this.menu!!.menuItemCollection.remove(subMenu)

		subMenu.sequenceIndex = index
		this.daoUtil.update(subMenu)

		var counter = 0

		for (sMenu in this.menu!!.menuItemCollection)
		{
			if (index == counter)
			{
				counter++
			}

			sMenu.sequenceIndex = counter
			this.daoUtil.update(sMenu)

			counter++
		}

		this.daoUtil.refresh(Menu::class.java, this.menu!!.id)

		this.menu = this.daoUtil.find(Menu::class.java, this.menu!!.id)
	}

	fun removeMegamenuSection(subMenu: MenuItem)
	{
		if (subMenu.subMenuColumns != null)
		{
			for (menuColumn in subMenu.subMenuColumns)
			{
				if (menuColumn.subMenuItems != null)
				{
					for (columnSubMenu in menuColumn.subMenuItems)
					{
						if (columnSubMenu.subMenuItems != null)
						{
							for (menuItem in columnSubMenu.subMenuItems)
							{
								this.daoUtil.delete(menuItem)
							}
						}

						this.daoUtil.delete(columnSubMenu)
					}
				}

				this.daoUtil.delete(menuColumn)
			}
		}

		this.menu!!.menuItemCollection.remove(subMenu)

		this.daoUtil.delete(subMenu)

		var counter = 0

		for (subMenuItem in this.menu!!.menuItemCollection)
		{
			subMenuItem.sequenceIndex = counter
			this.daoUtil.update(subMenuItem)

			counter++
		}

		this.daoUtil.refresh(Menu::class.java, this.menu!!.id)

		this.menu = this.daoUtil.find(Menu::class.java, this.menu!!.id)
	}

	fun addSubmenuColumn(subMenu: MenuItem)
	{
		val menuColumn = MenuColumn()
		menuColumn.parentMenuItem = subMenu

		if (subMenu.subMenuColumns != null)
		{
			menuColumn.sequenceIndex = subMenu.subMenuColumns.size
		}
		else
		{
			subMenu.subMenuColumns = ArrayList()

			menuColumn.sequenceIndex = 0
		}

		val success = this.daoUtil.create(menuColumn)

		if (success)
		{
			subMenu.subMenuColumns.add(menuColumn)
		}

		this.daoUtil.refresh(MenuItem::class.java, subMenu.id)

		this.daoUtil.refresh(Menu::class.java, this.menu!!.id)

		this.menu = this.daoUtil.find(Menu::class.java, this.menu!!.id)
	}

	fun removeColumnFromMegamenuSection(subMenu: MenuItem, menuColumn: MenuColumn)
	{
		if (menuColumn.subMenuItems != null)
		{
			for (columnSubMenu in menuColumn.subMenuItems)
			{
				if (columnSubMenu.subMenuItems != null)
				{
					for (menuItem in columnSubMenu.subMenuItems)
					{
						this.daoUtil.delete(menuItem)
					}
				}

				this.daoUtil.delete(columnSubMenu)
			}
		}

		subMenu.subMenuColumns.remove(menuColumn)

		this.daoUtil.delete(menuColumn)

		var counter = 0

		for (mColumn in subMenu.subMenuColumns)
		{
			mColumn.sequenceIndex = counter
			this.daoUtil.update(mColumn)

			counter++
		}

		this.daoUtil.refresh(MenuItem::class.java, subMenu.id)

		this.daoUtil.refresh(Menu::class.java, this.menu!!.id)

		this.menu = this.daoUtil.find(Menu::class.java, this.menu!!.id)
	}

	fun reorderColumn(subMenu: MenuItem, menuColumn: MenuColumn, index: Int)
	{
		subMenu.subMenuColumns.remove(menuColumn)

		menuColumn.sequenceIndex = index

		this.daoUtil.update(menuColumn)

		var counter = 0

		for (mColumn in subMenu.subMenuColumns)
		{
			if (index == counter)
			{
				counter++
			}

			mColumn.sequenceIndex = counter

			this.daoUtil.update(mColumn)

			counter++
		}

		this.daoUtil.refresh(MenuItem::class.java, subMenu.id)

		this.daoUtil.refresh(Menu::class.java, this.menu!!.id)

		this.menu = this.daoUtil.find(Menu::class.java, this.menu!!.id)
	}

	fun addColumnSubMenu(menuColumn: MenuColumn)
	{
		val columnSubMenu = MenuItem()
		columnSubMenu.menuColumn = menuColumn
		columnSubMenu.subMenuGroup = true
		columnSubMenu.displayName = "New Sub Section"

		if (menuColumn.subMenuItems != null)
		{
			columnSubMenu.sequenceIndex = menuColumn.subMenuItems.size
		}
		else
		{
			menuColumn.subMenuItems = ArrayList()
			columnSubMenu.sequenceIndex = 0
		}

		val success = this.daoUtil.create(columnSubMenu)

		if (success)
		{
			menuColumn.subMenuItems.add(columnSubMenu)
		}

		this.daoUtil.refresh(MenuColumn::class.java, menuColumn.id)

		this.daoUtil.refresh(Menu::class.java, this.menu!!.id)

		this.menu = this.daoUtil.find(Menu::class.java, this.menu!!.id)
	}

	fun removeColumnSubMenu(menuColumn: MenuColumn, columnSubMenu: MenuItem)
	{
		if (columnSubMenu.subMenuItems != null)
		{
			for (menuItem in columnSubMenu.subMenuItems)
			{
				this.daoUtil.delete(menuItem)
			}
		}

		menuColumn.subMenuItems.remove(columnSubMenu)

		this.daoUtil.delete(columnSubMenu)

		var counter = 0

		for (subMenu in menuColumn.subMenuItems)
		{
			subMenu.sequenceIndex = counter

			this.daoUtil.update(subMenu)

			counter++
		}

		this.daoUtil.refresh(MenuColumn::class.java, menuColumn.id)

		this.daoUtil.refresh(Menu::class.java, this.menu!!.id)

		this.menu = this.daoUtil.find(Menu::class.java, this.menu!!.id)
	}

	fun reorderColumnSubMenu(menuColumn: MenuColumn, columnSubMenu: MenuItem, index: Int)
	{
		menuColumn.subMenuItems.remove(columnSubMenu)

		columnSubMenu.sequenceIndex = index

		this.daoUtil.update(columnSubMenu)

		var counter = 0

		for (sMenuItem in menuColumn.subMenuItems)
		{
			if (index == counter)
			{
				counter++
			}

			sMenuItem.sequenceIndex = counter

			this.daoUtil.update(sMenuItem)

			counter++
		}

		this.daoUtil.refresh(MenuColumn::class.java, menuColumn.id)

		this.daoUtil.refresh(Menu::class.java, this.menu!!.id)

		this.menu = this.daoUtil.find(Menu::class.java, this.menu!!.id)
	}

	fun addColumnSubMenuMenuItem(columnSubMenu: MenuItem)
	{
		val menuItem = MenuItem()
		menuItem.parentMenuItem = columnSubMenu
		menuItem.displayName = "New Menu Item"

		if (columnSubMenu.subMenuItems != null)
		{
			menuItem.sequenceIndex = columnSubMenu.subMenuItems.size
		}
		else
		{
			columnSubMenu.subMenuItems = ArrayList()
			menuItem.sequenceIndex = 0
		}

		val success = this.daoUtil.create(menuItem)

		if (success)
		{
			columnSubMenu.subMenuItems.add(menuItem)
		}

		this.daoUtil.refresh(MenuItem::class.java, columnSubMenu.id)

		this.daoUtil.refresh(Menu::class.java, this.menu!!.id)

		this.menu = this.daoUtil.find(Menu::class.java, this.menu!!.id)
	}

	fun removeColumnSubMenuMenuItem(columnSubMenu: MenuItem, menuItem: MenuItem)
	{
		columnSubMenu.subMenuItems.remove(menuItem)

		this.daoUtil.delete(menuItem)

		var counter = 0

		for (m in columnSubMenu.subMenuItems)
		{
			m.sequenceIndex = counter

			this.daoUtil.update(m)

			counter++
		}

		this.daoUtil.refresh(MenuItem::class.java, columnSubMenu.id)

		this.daoUtil.refresh(Menu::class.java, this.menu!!.id)

		this.menu = this.daoUtil.find(Menu::class.java, this.menu!!.id)
	}

	fun reorderColumnSubMenuMenuItem(columnSubMenu: MenuItem, menuItem: MenuItem, index: Int)
	{
		columnSubMenu.subMenuItems.remove(menuItem)

		menuItem.sequenceIndex = index

		this.daoUtil.update(menuItem)

		var counter = 0

		for (sMenuItem in columnSubMenu.subMenuItems)
		{
			if (index == counter)
			{
				counter++
			}

			sMenuItem.sequenceIndex = counter

			this.daoUtil.update(sMenuItem)

			counter++
		}

		this.daoUtil.refresh(MenuItem::class.java, columnSubMenu.id)

		this.daoUtil.refresh(Menu::class.java, this.menu!!.id)

		this.menu = this.daoUtil.find(Menu::class.java, this.menu!!.id)
	}

	fun saveMegamenuSection()
	{
		val success: Boolean

		success = this.daoUtil.update(this.menuItem!!)

		if (success)
		{
			this.menu = this.daoUtil.find(Menu::class.java, this.menu!!.id)

			GeneralUtil.addMessage("Section has been saved.", FacesMessage.SEVERITY_INFO)
		}
		else
		{
			GeneralUtil.addMessage("Section could not be saved.", FacesMessage.SEVERITY_ERROR)
		}
	}

	fun saveMegamenuItem()
	{
		val success: Boolean

		success = this.daoUtil.update(this.menuItem!!)

		if (success)
		{
			this.menu = this.daoUtil.find(Menu::class.java, this.menu!!.id)

			GeneralUtil.addMessage("Menu Item has been saved.", FacesMessage.SEVERITY_INFO)
		}
		else
		{
			GeneralUtil.addMessage("Menu Item could not be saved.", FacesMessage.SEVERITY_ERROR)
		}
	}

	fun saveMenuColumnContent()
	{
		val success: Boolean

		success = this.daoUtil.update(this.menuColumn!!)

		if (success)
		{
			this.menu = this.daoUtil.find(Menu::class.java, this.menu!!.id)

			GeneralUtil.addMessage("Column content has been saved.", FacesMessage.SEVERITY_INFO)
		}
		else
		{
			GeneralUtil.addMessage("Column content could not be saved.", FacesMessage.SEVERITY_ERROR)
		}
	}

	fun selectMenuItem(m: MenuItem)
	{
		this.menuItem = m
	}

	fun selectMenuColumn(m: MenuColumn)
	{
		this.menuColumn = m
	}

	fun refresh()
	{
		//UI Refresh
	}

	fun clearRoles()
	{
		if (this.menuItem!!.anonymousOnly!!)
		{
			this.menuItem!!.itemRoles = ArrayList()
		}
	}

	private fun resetObjectForUIBind()
	{
		if (this.menu!!.menuToCrumb == null)
		{
			this.menu!!.menuToCrumb = Menu()
		}
	}

	private fun clearObjectForSave()
	{
		if (this.menu!!.menuToCrumb != null && (this.menu!!.menuToCrumb.id == null || this.menu!!.menuToCrumb.id < 1))
		{
			this.menu!!.menuToCrumb = null
		}
	}

	fun addSite()
	{
		val site = this.daoUtil.find(Site::class.java, this.siteListValue!!)

		if (site != null)
		{
			if (this.menu!!.sites == null)
			{
				this.menu!!.sites = ArrayList()
			}

			var siteExists = false

			for (cSite in this.menu!!.sites)
			{
				if (cSite.siteID == site.siteID)
				{
					siteExists = true
				}
			}

			if (!siteExists)
			{
				this.menu!!.sites.add(site)
			}
		}
	}

	fun removeSite(site: Site)
	{
		this.menu!!.sites.remove(site)
	}

	fun inSiteList(siteId: Long): Boolean
	{
		for(cSite in this.siteList)
		{
			if(cSite.siteID == siteId)
			{
				return true
			}
		}

		return false
	}
}