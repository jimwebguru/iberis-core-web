package com.jimwebguru.iberis.core.web.controllers.cms

import com.jimwebguru.iberis.core.loaders.GenericLazyLoader
import com.jimwebguru.iberis.core.persistence.*
import com.jimwebguru.iberis.core.utils.db.DaoUtil
import com.jimwebguru.iberis.core.web.controllers.admin.security.Authorize
import com.jimwebguru.iberis.core.web.utils.GeneralUtil
import org.primefaces.event.RowEditEvent

import javax.annotation.PostConstruct
import javax.faces.application.FacesMessage
import javax.faces.view.ViewScoped
import javax.inject.Named
import javax.faces.context.FacesContext
import java.io.Serializable
import java.util.ArrayList

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
@Named
@ViewScoped
class DynamicFormManagement : Serializable
{
	private val daoUtil = DaoUtil()

	var selectedSection: Long? = null

	var form: DynamicForm? = DynamicForm()
	var formSection: DynamicFormSection? = DynamicFormSection()
	var formField: DynamicFormField? = DynamicFormField()
	var listItem = DynamicFieldListItem()

	var lazyLoader = GenericLazyLoader(DynamicForm::class.java)

	var siteList = ArrayList<Site>()

	var siteListValue: Long? = null

	@PostConstruct
	fun postInit()
	{
		val request = GeneralUtil.request

		val id = request!!.getParameter("id")

		if (id != null && id.isNotEmpty())
		{
			this.form = this.daoUtil.find(DynamicForm::class.java, id.toLong())

			this.resetObjectForUIBind()
		}
		else
		{
			this.form!!.pageTemplate = Layout()
			this.form!!.recipientEmailField = DynamicFormField()
		}

		this.lazyLoader.additionalQueryParams = CmsAuthUtil.getCmsSiteFilterParameters()

		val user = Authorize.getUser()

		if(user != null)
		{
			for (siteMembership in user.siteMemberships)
			{
				this.siteList.add(siteMembership.site)
			}
		}
	}

	fun saveForm()
	{
		if(this.siteList.size == 1)
		{
			if (this.form!!.sites == null)
			{
				this.form!!.sites = ArrayList()
			}

			if (this.form!!.sites.isEmpty())
			{
				this.form!!.sites.add(this.siteList[0])
			}
		}

		if(this.form!!.sites == null || this.form!!.sites.isEmpty())
		{
			GeneralUtil.addMessage("A site must be associated.", FacesMessage.SEVERITY_ERROR)
		}
		else
		{
			this.clearObjectForSave()

			if (this.form!!.id != null && this.form!!.id > 0)
			{
				this.daoUtil.update(this.form!!)

				GeneralUtil.addMessage("Form has been updated.", FacesMessage.SEVERITY_INFO)
			}
			else
			{
				this.daoUtil.create(this.form!!)

				GeneralUtil.addMessage("Form has been created.", FacesMessage.SEVERITY_INFO)
			}

			this.resetObjectForUIBind()
		}
	}

	fun deleteForm(form: DynamicForm)
	{
		val success = this.daoUtil.delete(form)

		if (success)
		{
			GeneralUtil.addMessage("Form has been deleted.", FacesMessage.SEVERITY_INFO)
		}
		else
		{
			GeneralUtil.addMessage("Form could not be deleted.", FacesMessage.SEVERITY_ERROR)
		}
	}

	fun selectSection(section: DynamicFormSection)
	{
		this.formSection = section
	}

	fun saveSection()
	{
		if (this.formSection!!.id != null && this.formSection!!.id > 0)
		{
			this.daoUtil.update(this.formSection!!)

			GeneralUtil.addMessage("Form Section has been updated.", FacesMessage.SEVERITY_INFO)
		}
		else
		{
			this.formSection!!.form = this.form
			this.daoUtil.create(this.formSection!!)

			GeneralUtil.addMessage("Form Section has been created.", FacesMessage.SEVERITY_INFO)
		}

		this.refreshObjects()
	}

	fun deleteSection(section: DynamicFormSection)
	{
		for (field in section.fields)
		{
			field.formSection = null

			this.daoUtil.update(field)
		}

		val success = this.daoUtil.delete(section)

		if (success)
		{
			GeneralUtil.addMessage("Form Section has been deleted.", FacesMessage.SEVERITY_INFO)
		}
		else
		{
			GeneralUtil.addMessage("Form Section could not be deleted.", FacesMessage.SEVERITY_ERROR)
		}

		this.refreshObjects()
	}

	fun selectFormField(field: DynamicFormField)
	{
		this.formField = field
	}

	fun saveFormField()
	{
		if (this.formField!!.required!! && (this.formField!!.requiredMessage == null || this.formField!!.requiredMessage.trim { it <= ' ' } == ""))
		{
			GeneralUtil.addMessage("Required field must have a required message.", FacesMessage.SEVERITY_ERROR)
			FacesContext.getCurrentInstance().validationFailed()
		}
		else
		{
			if(this.formField!!.lookupUnitId == 0L)
			{
				this.formField!!.lookupGridId = 0L
			}

			if(this.formField!!.fieldType.typeId != PropertyType.LOOKUP.propertyTypeId.toLong())
			{
				this.formField!!.lookupUnitId = 0L
				this.formField!!.lookupGridId = 0L
			}
			
			if (this.formField!!.id != null && this.formField!!.id > 0)
			{
				this.daoUtil.update(this.formField!!)

				if (this.formField!!.fieldType.typeId != 9L)
				{
					if (this.formField!!.fieldListItems != null && this.formField!!.fieldListItems.isNotEmpty())
					{
						for (item in this.formField!!.fieldListItems)
						{
							this.daoUtil.delete(item)
						}
					}
				}

				GeneralUtil.addMessage("Form Field has been updated.", FacesMessage.SEVERITY_INFO)
			}
			else
			{
				this.formField!!.form = this.form
				this.formField!!.sequence = 0
				this.daoUtil.create(this.formField!!)

				GeneralUtil.addMessage("Form Field has been created.", FacesMessage.SEVERITY_INFO)
			}

			this.refreshObjects()
		}
	}

	fun deleteFormField(field: DynamicFormField)
	{
		val success = this.daoUtil.delete(field)

		if (success)
		{
			GeneralUtil.addMessage("Form Field has been deleted.", FacesMessage.SEVERITY_INFO)
		}
		else
		{
			GeneralUtil.addMessage("Form Field could not be deleted.", FacesMessage.SEVERITY_ERROR)
		}

		this.refreshObjects()
	}

	fun reorderFields(section: DynamicFormSection, field: DynamicFormField, sequenceIndex: Int)
	{
		section.fields.remove(field)
		field.sequence = sequenceIndex
		this.daoUtil.update(field)

		var counter = 0

		for (f in section.fields)
		{
			if (counter == sequenceIndex)
			{
				counter++
			}

			f.sequence = counter

			this.daoUtil.update(f)

			counter++
		}

		this.refreshObjects()
	}

	fun addFieldToSection()
	{
		val section = this.daoUtil.find(DynamicFormSection::class.java, this.selectedSection!!)

		if (section != null)
		{
			this.formField!!.formSection = section

			if (section.fields != null && section.fields.isNotEmpty())
			{
				var counter = 0

				for (f in section.fields)
				{
					f.sequence = counter

					this.daoUtil.update(f)

					counter++
				}

				this.formField!!.sequence = section.fields.size
			}
			else
			{
				this.formField!!.sequence = 0
			}

			this.daoUtil.update(this.formField!!)

			this.refreshObjects()
		}
	}

	fun updateFieldToSection()
	{
		val request = GeneralUtil.request

		val sectionId = request!!.getParameter("sectionId")

		if (sectionId != null && sectionId != "0")
		{
			val section = this.daoUtil.find(DynamicFormSection::class.java, sectionId.toLong())

			val field = this.daoUtil.find(DynamicFormField::class.java, java.lang.Long.valueOf(request.getParameter("fieldId")))

			if (section != null && field != null)
			{
				field.formSection = section
				field.sequence = Integer.valueOf(request.getParameter("index"))

				this.daoUtil.update(field)

				this.refreshObjects()
			}
		}
		else
		{
			val field = this.daoUtil.find(DynamicFormField::class.java, request.getParameter("fieldId").toLong())
			field!!.formSection = null

			this.daoUtil.update(field)

			this.refreshObjects()
		}
	}

	fun removeFieldFromSection(field: DynamicFormField)
	{
		//Query just incase stale from cache on field object
		val section = this.daoUtil.find(DynamicFormSection::class.java, field.formSection.id)

		section!!.fields.remove(field)

		field.formSection = null

		this.daoUtil.update(field)

		var counter = 0

		if (section.fields.isNotEmpty())
		{
			for (f in section.fields)
			{
				f.sequence = counter

				this.daoUtil.update(f)

				counter++
			}
		}

		this.refreshObjects()
	}

	fun saveLayout()
	{
		if (this.form!!.formSections != null && this.form!!.formSections.isNotEmpty())
		{
			for (section in this.form!!.formSections)
			{
				if (section.fields != null && section.fields.isNotEmpty())
				{
					for (field in section.fields)
					{
						this.daoUtil.update(field)
					}
				}
			}
		}
	}

	fun newListItem()
	{
		this.listItem = DynamicFieldListItem()
	}

	fun editListItem(event: RowEditEvent)
	{
		val item = event.`object` as DynamicFieldListItem

		this.daoUtil.update(item)

		this.listItemRefreshObjects()

		GeneralUtil.addMessage("List item modified.", FacesMessage.SEVERITY_INFO)
	}

	fun saveListItem()
	{
		if (this.listItem.id != null && this.listItem.id > 0)
		{
			this.listItem.field = this.formField

			this.daoUtil.update(this.listItem)

			GeneralUtil.addMessage("List item has been updated.", FacesMessage.SEVERITY_INFO)
		}
		else
		{
			this.listItem.field = this.formField

			this.daoUtil.create(this.listItem)

			GeneralUtil.addMessage("List item has been created.", FacesMessage.SEVERITY_INFO)
		}

		this.listItemRefreshObjects()
	}

	fun selectListItem(item: DynamicFieldListItem)
	{
		this.listItem = item
	}

	fun deleteListItem(item: DynamicFieldListItem)
	{
		this.daoUtil.delete(item)

		this.listItemRefreshObjects()

		GeneralUtil.addMessage("List item has been deleted.", FacesMessage.SEVERITY_INFO)
	}

	fun clearListItems()
	{
		if (this.formField!!.fieldListItems != null && this.formField!!.fieldListItems.isNotEmpty())
		{
			for (item in this.formField!!.fieldListItems)
			{
				this.daoUtil.delete(item)
			}
		}

		this.listItemRefreshObjects()
	}

	private fun refreshObjects()
	{
		this.daoUtil.refresh(DynamicForm::class.java, this.form!!.id)

		if (this.form!!.formSections != null && this.form!!.formSections.isNotEmpty())
		{
			for (section in this.form!!.formSections)
			{
				this.daoUtil.refresh(DynamicFormSection::class.java, section.id)
			}
		}

		this.form = this.daoUtil.find(DynamicForm::class.java, this.form!!.id)

		this.resetObjectForUIBind()

		this.formSection = null
	}

	private fun listItemRefreshObjects()
	{
		this.daoUtil.update(this.formField!!)

		this.daoUtil.refresh(DynamicFormField::class.java, this.formField!!.id)

		this.formField = this.daoUtil.find(DynamicFormField::class.java, this.formField!!.id)

		this.daoUtil.refresh(DynamicForm::class.java, this.form!!.id)

		this.form = this.daoUtil.find(DynamicForm::class.java, this.form!!.id)

		this.resetObjectForUIBind()
	}

	fun newSection()
	{
		this.formSection = DynamicFormSection()
	}

	fun newField()
	{
		this.formField = DynamicFormField()
		this.formField!!.fieldType = UnitPropertyType()
		this.formField!!.width = 12
	}

	fun getFormType(sectionType: Int?): String?
	{
		val unitProperty = this.daoUtil.find(UnitProperty::class.java, 496L)

		for (listItem in unitProperty!!.unitPropertyListItems)
		{
			if (listItem.itemValue == sectionType!!.toString())
			{
				return listItem.displayValue
			}
		}

		return null
	}

	fun refresh()
	{

	}

	private fun resetObjectForUIBind()
	{
		if (this.form!!.pageTemplate == null)
		{
			this.form!!.pageTemplate = Layout()
		}

		if (this.form!!.recipientEmailField == null)
		{
			this.form!!.recipientEmailField = DynamicFormField()
		}
	}

	private fun clearObjectForSave()
	{
		if (this.form!!.pageTemplate != null && (this.form!!.pageTemplate.id == null || this.form!!.pageTemplate.id == 0L))
		{
			this.form!!.pageTemplate = null
		}

		if (this.form!!.recipientEmailField != null && (this.form!!.recipientEmailField.id == null || this.form!!.recipientEmailField.id == 0L))
		{
			this.form!!.recipientEmailField = null
		}
	}

	fun addSite()
	{
		val site = this.daoUtil.find(Site::class.java, this.siteListValue!!)

		if (site != null)
		{
			if (this.form!!.sites == null)
			{
				this.form!!.sites = ArrayList()
			}

			var siteExists = false

			for (cSite in this.form!!.sites)
			{
				if (cSite.siteID == site.siteID)
				{
					siteExists = true
				}
			}

			if (!siteExists)
			{
				this.form!!.sites.add(site)
			}
		}
	}

	fun removeSite(site: Site)
	{
		this.form!!.sites.remove(site)
	}

	fun inSiteList(siteId: Long): Boolean
	{
		for(cSite in this.siteList)
		{
			if(cSite.siteID == siteId)
			{
				return true
			}
		}

		return false
	}
}
