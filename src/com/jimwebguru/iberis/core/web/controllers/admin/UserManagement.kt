package com.jimwebguru.iberis.core.web.controllers.admin

import com.jimwebguru.iberis.core.loaders.GenericLazyLoader
import com.jimwebguru.iberis.core.persistence.*
import com.jimwebguru.iberis.core.utils.db.DaoUtil
import com.jimwebguru.iberis.core.web.utils.GeneralUtil
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder

import javax.annotation.PostConstruct
import javax.faces.application.FacesMessage
import javax.faces.view.ViewScoped
import javax.inject.Named
import java.io.Serializable
import java.util.ArrayList

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
@Named
@ViewScoped
open class UserManagement : Serializable
{
	protected var daoUtil = DaoUtil()

	var genericLazyLoader = GenericLazyLoader(User::class.java)

	var user: User? = User()

	var userGroup = UserGroup()
	var siteMembership = SiteMembership()
	var role = Role()

	var password: String? = null
	var passwordConfirm: String? = null

	@PostConstruct
	open fun postInit()
	{
		val request = GeneralUtil.request

		val userId = request!!.getParameter("id")

		if (userId != null)
		{
			this.user = daoUtil.find(User::class.java, userId.toLong())
		}
	}

	open fun save()
	{
		if (this.user!!.userId != null && this.user!!.userId > 0)
		{
			if (this.daoUtil.update(this.user!!))
			{
				GeneralUtil.addMessage("User has been updated.", FacesMessage.SEVERITY_INFO)
			}
			else
			{
				GeneralUtil.addMessage("User could not be updated.", FacesMessage.SEVERITY_ERROR)
			}
		}
		else
		{
			val userCount = this.daoUtil.count("SELECT COUNT(o) FROM User o WHERE o.userName = '" + this.user!!.userName + "'")

			if (userCount == 0L)
			{
				if (this.daoUtil.create(this.user!!))
				{
					this.password = "ChangeMe"
					this.passwordConfirm = "ChangeMe"
					this.savePassword()

					GeneralUtil.addMessage("User has been created.", FacesMessage.SEVERITY_INFO)
				}
				else
				{
					GeneralUtil.addMessage("User could not be created.", FacesMessage.SEVERITY_ERROR)
				}
			}
			else
			{
				GeneralUtil.addMessage("Username already taken.", FacesMessage.SEVERITY_ERROR)
			}
		}
	}

	fun delete(user: User)
	{
		if (this.daoUtil.delete(user))
		{
			GeneralUtil.addMessage("User has been deleted.", FacesMessage.SEVERITY_INFO)
		}
		else
		{
			GeneralUtil.addMessage("User could not be deleted.", FacesMessage.SEVERITY_ERROR)
		}
	}

	fun addUserGroup()
	{
		if (this.userGroup.groupId != null && this.userGroup.groupId > 0)
		{
			var groupExists = false

			if (this.user!!.groups != null)
			{
				for (g in this.user!!.groups)
				{
					if (g.groupId == this.userGroup.groupId)
					{
						groupExists = true
					}
				}
			}

			if (!groupExists)
			{
				if (this.user!!.groups == null)
				{
					this.user!!.groups = ArrayList()
				}

				val newUserGroup = UserGroup()
				newUserGroup.groupId = this.userGroup.groupId
				newUserGroup.groupName = this.userGroup.groupName

				this.user!!.groups.add(newUserGroup)

				this.daoUtil.update(this.user!!)
			}
		}
	}

	fun removeUserGroup(group: UserGroup)
	{
		this.user!!.groups.remove(group)

		this.daoUtil.update(this.user!!)
	}

	fun addRole()
	{
		if (this.role.id != null && this.role.id > 0)
		{
			var roleExists = false

			if (this.siteMembership.roles != null)
			{
				for (r in this.siteMembership.roles)
				{
					if (r.id == this.role.id)
					{
						roleExists = true
					}
				}
			}

			if (!roleExists)
			{
				if (this.siteMembership.roles == null)
				{
					this.siteMembership.roles = ArrayList()
				}

				val newRole = Role()
				newRole.id = this.role.id
				newRole.roleName = this.role.roleName

				this.siteMembership.roles.add(newRole)

				this.daoUtil.update(this.siteMembership)
			}
		}
	}

	fun removeRole(role: Role)
	{
		this.siteMembership.roles.remove(role)

		this.daoUtil.update(this.siteMembership)
	}

	fun newSiteMembership()
	{
		this.siteMembership = SiteMembership()
		this.siteMembership.site = Site()

	}

	fun editSiteMembership(membership: SiteMembership)
	{
		this.siteMembership = membership
	}

	fun saveSiteMembership()
	{
		this.siteMembership.user = this.user

		if (this.siteMembership.membershipID != null && this.siteMembership.membershipID > 0)
		{
			if (this.daoUtil.update(this.siteMembership))
			{
				GeneralUtil.addMessage("Membership has been updated.", FacesMessage.SEVERITY_INFO)
			}
			else
			{
				GeneralUtil.addMessage("Membership could not be updated.", FacesMessage.SEVERITY_ERROR)
			}
		}
		else
		{
			if (this.daoUtil.create(this.siteMembership))
			{
				GeneralUtil.addMessage("Membership has been created.", FacesMessage.SEVERITY_INFO)
			}
			else
			{
				GeneralUtil.addMessage("Membership could not be created.", FacesMessage.SEVERITY_ERROR)
			}
		}

		this.daoUtil.refresh(User::class.java, this.user!!.userId)

		this.user = this.daoUtil.find(User::class.java, this.user!!.userId)
	}

	fun deleteSiteMembership(siteMembership: SiteMembership)
	{
		if (this.daoUtil.delete(siteMembership))
		{
			GeneralUtil.addMessage("Membership has been deleted.", FacesMessage.SEVERITY_INFO)

			this.user!!.siteMemberships.remove(siteMembership)
		}
		else
		{
			GeneralUtil.addMessage("Membership could not be deleted.", FacesMessage.SEVERITY_ERROR)
		}
	}

	fun savePassword()
	{
		if (this.password == this.passwordConfirm)
		{
			val passwordEncoder = BCryptPasswordEncoder()
			val encryptedPassword = passwordEncoder.encode(this.password)

			this.user!!.password = encryptedPassword

			if (this.daoUtil.update(this.user!!))
			{
				GeneralUtil.addMessage("Password updated.", FacesMessage.SEVERITY_INFO)
			}
			else
			{
				GeneralUtil.addMessage("Password could not be updated.", FacesMessage.SEVERITY_ERROR)
			}
		}
	}
}
