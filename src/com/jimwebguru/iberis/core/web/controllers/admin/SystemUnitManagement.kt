package com.jimwebguru.iberis.core.web.controllers.admin

import com.jimwebguru.iberis.core.web.SiteSettings
import com.jimwebguru.iberis.core.persistence.*
import com.jimwebguru.iberis.core.utils.db.DaoUtil
import com.jimwebguru.iberis.core.utils.db.QueryParameter
import com.jimwebguru.iberis.core.utils.db.SchemaDB
import com.jimwebguru.iberis.core.web.utils.GeneralUtil

import javax.annotation.PostConstruct
import javax.faces.application.FacesMessage
import javax.faces.view.ViewScoped
import javax.inject.Named
import java.io.Serializable
import java.util.ArrayList

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
@Named
@ViewScoped
class SystemUnitManagement : Serializable
{
	var selectedUnitId: Long? = null

	private val daoUtil = DaoUtil()

	var selectedUnit: SystemUnit? = null

	var systemUnits: List<SystemUnit>? = null

	private var forms: MutableList<Form>? = null

	private var grids: MutableList<Grid>? = null

	private var unitProperties: MutableList<UnitProperty>? = null

	private val schemaDB = SchemaDB()

	var unitSiteAssociation: UnitSiteAssociation? = null

	private var siteSettings = GeneralUtil.getManagedBean("siteSettings") as SiteSettings

	@PostConstruct
	fun postInit()
	{
		val unitParam = GeneralUtil.request!!.getParameter("systemUnitId")

		if (unitParam != null)
		{
			this.selectedUnitId = java.lang.Long.valueOf(unitParam.toString())

			this.loadSystemUnitData()
		}

		this.systemUnits = this.daoUtil.find("SELECT OBJECT(o) FROM SystemUnit o ORDER BY o.unitName")
	}

	fun changeSystemUnit()
	{
		this.loadSystemUnitData()
	}

	private fun loadSystemUnitData()
	{
		if (this.selectedUnitId != 0L)
		{
			this.selectedUnit = this.daoUtil.find(SystemUnit::class.java, this.selectedUnitId!!)

			val queryParameters = ArrayList<QueryParameter>()
			queryParameters.add(QueryParameter("unit", this.selectedUnit!!))

			this.forms = this.daoUtil.find<Form>("SELECT OBJECT(o) FROM Form AS o WHERE o.unit = :unit", queryParameters)?.toMutableList()
			this.unitProperties = this.daoUtil.find<UnitProperty>("SELECT OBJECT(o) FROM UnitProperty AS o WHERE o.unit = :unit", queryParameters)?.toMutableList()

			queryParameters.clear()

			queryParameters.add(QueryParameter("unitId", this.selectedUnit!!.unitID))

			this.grids = this.daoUtil.find<Grid>("SELECT OBJECT(o) FROM Grid AS o WHERE o.unitId = :unitId", queryParameters)?.toMutableList()
		}
		else
		{
			this.selectedUnit = null
		}
	}

	fun save()
	{
		this.selectedUnit!!.unitName = this.selectedUnit!!.unitName.toUpperCase().replace("[^a-zA-Z0-9 -]".toRegex(), "")

		val oldTableName = this.selectedUnit!!.tableName

		if (this.selectedUnit!!.persistenceClass == null || this.selectedUnit!!.persistenceClass.trim { it <= ' ' } == "")
		{
			this.selectedUnit!!.tableName = "dynamic_" + this.selectedUnit!!.unitName.toLowerCase()
		}
		else
		{
			this.selectedUnit!!.tableName = ""
		}

		if (this.daoUtil.update(this.selectedUnit!!))
		{
			this.schemaDB.renameUnitTable(this.selectedUnit!!, oldTableName)

			if (this.selectedUnit!!.tableName == "")
			{
				this.schemaDB.removeUnitTable(oldTableName)
			}

			if ((oldTableName == null || oldTableName.trim { it <= ' ' } == "") && this.selectedUnit!!.tableName != null && this.selectedUnit!!.tableName.trim { it <= ' ' } != "")
			{
				val primaryProperty = this.daoUtil.find(UnitProperty::class.java, this.selectedUnit!!.lookupPropertyPrimaryKey)
				val primaryProperty2 = this.daoUtil.find(UnitProperty::class.java, this.selectedUnit!!.lookupPropertyPrimaryKey2)

				this.schemaDB.createUnitTable(this.selectedUnit!!, ArrayList(this.unitProperties!!), primaryProperty!!, primaryProperty2)
			}

			GeneralUtil.addMessage("The system unit has been updated.", FacesMessage.SEVERITY_INFO)
		}
		else
		{
			GeneralUtil.addMessage("The system unit could not be updated.", FacesMessage.SEVERITY_ERROR)
		}
	}

	fun delete()
	{
		for (form in this.forms!!)
		{
			this.deleteForm(form)
		}

		for (grid in this.grids!!)
		{
			this.deleteGrid(grid)
		}

		for (unitProperty in this.unitProperties!!)
		{
			this.deleteUnitProperty(unitProperty)
		}

		if (this.daoUtil.delete(this.selectedUnit!!))
		{
			this.schemaDB.removeUnitTable(this.selectedUnit!!.tableName)

			this.selectedUnit = null
			this.forms!!.clear()
			this.grids!!.clear()
			this.unitProperties!!.clear()

			GeneralUtil.addMessage("The " + this.siteSettings.bundle?.getString(this.selectedUnit!!.singularName) + " has been deleted.", FacesMessage.SEVERITY_INFO)
		}
		else
		{
			GeneralUtil.addMessage("The " + this.siteSettings.bundle?.getString(this.selectedUnit!!.singularName) + " could not be deleted.", FacesMessage.SEVERITY_ERROR)
		}
	}

	fun deleteGrid(grid: Grid)
	{
		if (grid.gridId == this.selectedUnit!!.defaultGridId)
		{
			GeneralUtil.addMessage("A default grid for a system unit cannot be deleted. Please make a different grid the default first.", FacesMessage.SEVERITY_ERROR)
			GeneralUtil.addCallbackParam("validationFailed", true)
			return
		}

		if (this.grids!!.size < 2)
		{
			GeneralUtil.addMessage("A system unit must have at least one grid.", FacesMessage.SEVERITY_ERROR)
			GeneralUtil.addCallbackParam("validationFailed", true)
			return
		}

		var success = true

		for (gridColumn in grid.gridColumns)
		{
			success = this.daoUtil.delete(gridColumn)
		}

		for (searchColumn in grid.searchColumns)
		{
			success = this.daoUtil.delete(searchColumn)
		}

		if (success)
		{
			grid.gridColumns.clear()
			grid.searchColumns.clear()

			if (this.daoUtil.delete(grid))
			{
				GeneralUtil.addMessage("The grid has been deleted", FacesMessage.SEVERITY_INFO)
				this.loadSystemUnitData()
			}
			else
			{
				GeneralUtil.addMessage("The grid could not be deleted.", FacesMessage.SEVERITY_ERROR)
				GeneralUtil.addCallbackParam("validationFailed", true)
			}
		}
	}

	fun deleteForm(form: Form)
	{
		if (form.formId == this.selectedUnit!!.defaultFormId)
		{
			GeneralUtil.addMessage("A default form for a system unit cannot be deleted. Please make a different form the default first.", FacesMessage.SEVERITY_ERROR)
			GeneralUtil.addCallbackParam("validationFailed", true)
			return
		}

		if (this.forms!!.size < 2)
		{
			GeneralUtil.addMessage("A system unit must have at least one form.", FacesMessage.SEVERITY_ERROR)
			GeneralUtil.addCallbackParam("validationFailed", true)
			return
		}

		var success = true

		if (form.tabs != null && form.tabs.isNotEmpty())
		{
			for (tab in form.tabs)
			{
				success = this.daoUtil.delete(tab)
			}
		}

		if (success)
		{
			if (form.tabs != null)
			{
				form.tabs.clear()
			}

			if (this.daoUtil.delete(form))
			{
				GeneralUtil.addMessage("The form has been deleted", FacesMessage.SEVERITY_INFO)
				this.loadSystemUnitData()
			}
			else
			{
				GeneralUtil.addMessage("The form could not be deleted.", FacesMessage.SEVERITY_ERROR)
				GeneralUtil.addCallbackParam("validationFailed", true)
			}
		}
	}

	fun deleteUnitProperty(unitProperty: UnitProperty)
	{
		if (unitProperty.propertyId == this.selectedUnit!!.lookupDisplayPropertyId ||
				unitProperty.propertyId == this.selectedUnit!!.lookupPropertyPrimaryKey ||
				unitProperty.propertyId == this.selectedUnit!!.lookupPropertyPrimaryKey2)
		{
			GeneralUtil.addMessage("A unit property that is a lookup display or part of a primary key cannot be deleted.", FacesMessage.SEVERITY_ERROR)
			GeneralUtil.addCallbackParam("validationFailed", true)
			return
		}

		var success = true

		val queryParameters = ArrayList<QueryParameter>()

		var queryParameter = QueryParameter()
		queryParameter.parameterName = "unitProperty"
		queryParameter.parameterValue = unitProperty

		queryParameters.add(queryParameter)

		val gridColumns = this.daoUtil.find<GridColumn>("SELECT OBJECT(o) FROM GridColumn o WHERE o.unitProperty = :unitProperty", queryParameters)

		if (gridColumns != null && gridColumns.isNotEmpty())
		{
			for (gridColumn in gridColumns)
			{
				success = this.daoUtil.delete(gridColumn)
			}
		}

		queryParameters.clear()

		queryParameter = QueryParameter()
		queryParameter.parameterName = "unitPropertyId"
		queryParameter.parameterValue = unitProperty.propertyId

		queryParameters.add(queryParameter)

		val tabs = this.daoUtil.find<Tab>("SELECT OBJECT(o) FROM Tab o WHERE o.referencedUnitProperty = :unitPropertyId", queryParameters)

		if (tabs != null && tabs.size > 0)
		{
			for (tab in tabs)
			{
				success = this.daoUtil.delete(tab)
			}
		}

		if (unitProperty.unitPropertyListItems != null)
		{
			for (unitPropertyListItem in unitProperty.unitPropertyListItems)
			{
				success = this.daoUtil.delete(unitPropertyListItem)
			}
		}

		if (success)
		{
			if (unitProperty.unitPropertyListItems != null)
			{
				unitProperty.unitPropertyListItems.clear()
			}

			if (this.daoUtil.delete(unitProperty))
			{
				this.schemaDB.removeUnitProperty(this.selectedUnit!!, unitProperty)

				this.loadSystemUnitData()

				GeneralUtil.addMessage("The unit property has been deleted", FacesMessage.SEVERITY_INFO)
			}
			else
			{
				GeneralUtil.addMessage("The unit property could not be deleted.", FacesMessage.SEVERITY_ERROR)
				GeneralUtil.addCallbackParam("validationFailed", true)
			}
		}
	}

	fun newUnitSiteAssociation()
	{
		this.unitSiteAssociation = UnitSiteAssociation()
		this.unitSiteAssociation!!.site = Site()
	}

	fun selectUnitSiteAssociation(item: UnitSiteAssociation)
	{
		this.unitSiteAssociation = item
	}

	fun saveUnitSiteAssociation()
	{
		this.unitSiteAssociation!!.unit = this.selectedUnit

		val queryParameters = ArrayList<QueryParameter>()

		var queryParameter = QueryParameter()
		queryParameter.parameterName = "site"
		queryParameter.parameterValue = this.unitSiteAssociation!!.site.siteID
		queryParameters.add(queryParameter)

		queryParameter = QueryParameter()
		queryParameter.parameterName = "unit"
		queryParameter.parameterValue = this.unitSiteAssociation!!.unit.unitID
		queryParameters.add(queryParameter)

		val count = this.daoUtil.count("SELECT COUNT(o) FROM UnitSiteAssociation o WHERE o.site.siteID = :site and o.unit.unitID = :unit", queryParameters)

		if (count > 0)
		{
			GeneralUtil.addMessage("That association already exists.", FacesMessage.SEVERITY_ERROR)
		}
		else
		{
			var existingSiteAssociation: UnitSiteAssociation? = null

			if (this.unitSiteAssociation!!.id != null && this.unitSiteAssociation!!.id > 0)
			{
				existingSiteAssociation = this.daoUtil.find(UnitSiteAssociation::class.java, this.unitSiteAssociation!!.id)
			}

			if (this.unitSiteAssociation!!.id != null && this.unitSiteAssociation!!.id > 0)
			{
				if (this.daoUtil.update(this.unitSiteAssociation!!))
				{
					GeneralUtil.addMessage("Unit Site Association has been updated.", FacesMessage.SEVERITY_INFO)
				}
				else
				{
					GeneralUtil.addMessage("Unit Site Association could not be updated.", FacesMessage.SEVERITY_ERROR)
				}
			}
			else
			{
				if (this.daoUtil.create(this.unitSiteAssociation!!))
				{
					GeneralUtil.addMessage("Unit Site Association has been created.", FacesMessage.SEVERITY_INFO)
				}
				else
				{
					GeneralUtil.addMessage("Unit Site Association could not be created.", FacesMessage.SEVERITY_ERROR)
				}
			}

			this.daoUtil.refresh(SystemUnit::class.java, this.selectedUnit!!.unitID)
			this.daoUtil.refresh(Site::class.java, this.unitSiteAssociation!!.site.siteID)

			if (existingSiteAssociation != null)
			{
				if (existingSiteAssociation.site.siteID != this.unitSiteAssociation!!.site.siteID)
				{
					this.daoUtil.refresh(Site::class.java, existingSiteAssociation.site.siteID)
				}
			}

			this.selectedUnit = this.daoUtil.find(SystemUnit::class.java, this.selectedUnit!!.unitID)
		}
	}

	fun deleteUnitSiteAssociation(item: UnitSiteAssociation)
	{
		if (item.id != null && item.id > 0)
		{
			this.daoUtil.delete(item)

			GeneralUtil.addMessage("Unit Site Selection has been deleted.", FacesMessage.SEVERITY_INFO)

			this.daoUtil.refresh(SystemUnit::class.java, this.selectedUnit!!.unitID)
			this.daoUtil.refresh(Site::class.java, this.unitSiteAssociation!!.site.siteID)

			this.selectedUnit = this.daoUtil.find(SystemUnit::class.java, this.selectedUnit!!.unitID)
		}
	}

	fun getForms(): List<Form>?
	{
		return forms
	}

	fun setForms(forms: MutableList<Form>)
	{
		this.forms = forms
	}

	fun getGrids(): List<Grid>?
	{
		return grids
	}

	fun setGrids(grids: MutableList<Grid>)
	{
		this.grids = grids
	}

	fun getUnitProperties(): List<UnitProperty>?
	{
		return unitProperties
	}

	fun setUnitProperties(unitProperties: MutableList<UnitProperty>)
	{
		this.unitProperties = unitProperties
	}
}
