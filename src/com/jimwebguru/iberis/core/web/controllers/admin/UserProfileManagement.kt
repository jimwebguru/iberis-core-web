package com.jimwebguru.iberis.core.web.controllers.admin

import com.jimwebguru.iberis.core.web.controllers.admin.security.Authorize
import javax.annotation.PostConstruct
import javax.faces.view.ViewScoped
import javax.inject.Named

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
@Named
@ViewScoped
class UserProfileManagement : UserManagement()
{
	@PostConstruct
	override fun postInit()
	{
		this.user = Authorize.getUser()
	}
}