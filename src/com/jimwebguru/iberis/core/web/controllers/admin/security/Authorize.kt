package com.jimwebguru.iberis.core.web.controllers.admin.security

import com.jimwebguru.iberis.core.persistence.User
import com.jimwebguru.iberis.core.utils.db.DaoUtil
import com.jimwebguru.iberis.core.web.utils.GeneralUtil
import com.jimwebguru.iberis.core.web.utils.GeneralUtil.request

import javax.servlet.http.HttpServletRequest

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
object Authorize
{
	val isAuthenticated: Boolean
		get()
		{
			val request = GeneralUtil.request

			return request!!.remoteUser != null && request.remoteUser.isNotEmpty()
		}


	fun isUserInRole(roles: Array<String>): Boolean
	{
		val request = GeneralUtil.request

		var authorized = false

		if (request!!.remoteUser != null && request.remoteUser != "")
		{
			for (role in roles)
			{
				if (request.isUserInRole(role))
				{
					authorized = true
				}
			}
		}

		return authorized
	}

	fun getUser(): User?
	{
		val request = GeneralUtil.request
		val daoUtil = DaoUtil()

		return if (request!!.remoteUser != null && request.remoteUser != "")
		{
			val userList = daoUtil.find<User>("SELECT OBJECT(o) FROM User o WHERE o.userName = '${request.remoteUser}'")

			userList?.get(0)
		}
		else
		{
			null
		}
	}
}
