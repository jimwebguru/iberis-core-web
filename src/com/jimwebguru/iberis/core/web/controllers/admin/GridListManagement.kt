package com.jimwebguru.iberis.core.web.controllers.admin

import com.jimwebguru.iberis.core.loaders.GenericLazyLoader
import com.jimwebguru.iberis.core.utils.db.DaoUtil
import com.jimwebguru.iberis.core.persistence.Grid
import com.jimwebguru.iberis.core.web.utils.GeneralUtil

import javax.faces.application.FacesMessage
import javax.faces.view.ViewScoped
import javax.inject.Named
import java.io.Serializable

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
@Named
@ViewScoped
class GridListManagement : Serializable
{
	private val daoUtil = DaoUtil()

	var gridGenericLazyLoader = GenericLazyLoader(Grid::class.java)

	fun delete(grid: Grid)
	{
		var success = true

		for (gridColumn in grid.gridColumns)
		{
			success = this.daoUtil.delete(gridColumn)
		}

		for (searchColumn in grid.searchColumns)
		{
			success = this.daoUtil.delete(searchColumn)
		}

		if (success)
		{
			grid.gridColumns.clear()
			grid.searchColumns.clear()

			if (this.daoUtil.delete(grid))
			{
				GeneralUtil.addMessage("Grid has been deleted.", FacesMessage.SEVERITY_INFO)
			}
			else
			{
				GeneralUtil.addMessage("Grid cannot be deleted.", FacesMessage.SEVERITY_ERROR)
				GeneralUtil.addCallbackParam("validationFailed", true)
			}
		}
	}
}
