package com.jimwebguru.iberis.core.web.controllers.admin

import com.jimwebguru.iberis.core.loaders.GenericLazyLoader
import com.jimwebguru.iberis.core.utils.db.DaoUtil
import com.jimwebguru.iberis.core.persistence.Icon
import com.jimwebguru.iberis.core.web.utils.GeneralUtil
import org.primefaces.event.RowEditEvent
import org.primefaces.event.SelectEvent

import javax.annotation.PostConstruct
import javax.faces.application.FacesMessage
import javax.faces.view.ViewScoped
import javax.inject.Named
import java.io.Serializable

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
@Named
@ViewScoped
class IconManagement : Serializable
{
	private val daoUtil = DaoUtil()

	var icon = Icon()
	var selectedIcon: Icon? = null

	var referencedDisplayField: String? = null

	var lazyLoader = GenericLazyLoader(Icon::class.java)

	@PostConstruct
	fun postInitialization()
	{
		if (GeneralUtil.getQueryStringValue("referencedDisplayField") != null)
		{
			this.referencedDisplayField = GeneralUtil.getQueryStringValue("referencedDisplayField")
		}
	}

	fun addIcon()
	{
		if (this.icon.name != null && this.icon.name.isNotEmpty())
		{
			val success = this.daoUtil.create(this.icon)

			if (success)
			{
				this.icon = Icon()

				GeneralUtil.addMessage("Icon added and resource bundle reloaded.", FacesMessage.SEVERITY_INFO)
			}
			else
			{
				GeneralUtil.addMessage("Could not add Icon.", FacesMessage.SEVERITY_ERROR)
			}
		}
	}

	fun onRowSelect(event: SelectEvent)
	{
		this.selectedIcon = event.`object` as Icon
	}

	fun onRowEdit(event: RowEditEvent)
	{
		val success = this.daoUtil.update(event.`object`)

		if (success)
		{
			GeneralUtil.addMessage("Icon modified.", FacesMessage.SEVERITY_INFO)
		}
	}

	@SuppressWarnings("unused")
	fun onRowCancel(event: RowEditEvent)
	{

	}
}
