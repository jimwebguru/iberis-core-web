package com.jimwebguru.iberis.core.web.controllers.admin

import com.jimwebguru.iberis.core.persistence.PropertyType
import com.jimwebguru.iberis.core.persistence.UnitProperty
import com.jimwebguru.iberis.core.web.utils.GeneralUtil

import javax.faces.application.FacesMessage

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
class UnitPropertyValidation
{
	fun validate(unitProperty: UnitProperty, persistenceClass: String?): Boolean
	{
		if (unitProperty.propertyType.typeId.toInt() == PropertyType.TEXT.propertyTypeId && (persistenceClass == null || persistenceClass.isEmpty()))
		{
			if (unitProperty.textLength < 1)
			{
				GeneralUtil.addMessage(unitProperty.propertyName + " needs to have a text length greater than zero.", FacesMessage.SEVERITY_ERROR)

				return false
			}
		}

		if (unitProperty.propertyType.typeId.toInt() == PropertyType.SELECTION.propertyTypeId)
		{
			if (unitProperty.unitPropertyListItems == null || unitProperty.unitPropertyListItems.isEmpty())
			{
				GeneralUtil.addMessage(unitProperty.propertyName + " must have at least one select item.", FacesMessage.SEVERITY_ERROR)

				return false
			}
		}

		return true
	}
}
