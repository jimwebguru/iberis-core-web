package com.jimwebguru.iberis.core.web.controllers.admin

import com.jimwebguru.iberis.core.loaders.GenericLazyLoader
import com.jimwebguru.iberis.core.utils.db.DaoUtil
import com.jimwebguru.iberis.core.persistence.UserGroup
import com.jimwebguru.iberis.core.web.utils.GeneralUtil

import javax.annotation.PostConstruct
import javax.faces.application.FacesMessage
import javax.faces.view.ViewScoped
import javax.inject.Named
import java.io.Serializable

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
@Named
@ViewScoped
class UserGroupManagement : Serializable
{
	protected var daoUtil = DaoUtil()

	var userGroup: UserGroup? = null

	var lazyLoader = GenericLazyLoader(UserGroup::class.java)

	@PostConstruct
	fun postInit()
	{
		if (GeneralUtil.getRequestParameterValue("id") != null)
		{
			this.userGroup = this.daoUtil.find(UserGroup::class.java, java.lang.Long.valueOf(GeneralUtil.getRequestParameterValue("id").toString()))

			this.resetObjectForUIBind()
		}
	}

	fun newUserGroup()
	{
		this.userGroup = UserGroup()
		this.userGroup!!.parentGroup = UserGroup()
	}

	fun selectUserGroup(item: UserGroup)
	{
		this.userGroup = item

		this.resetObjectForUIBind()
	}

	fun saveUserGroup()
	{
		this.clearObjectForSave()

		if (this.userGroup!!.groupId != null && this.userGroup!!.groupId > 0)
		{
			if (this.daoUtil.update(this.userGroup!!))
			{
				GeneralUtil.addMessage("User Group has been updated.", FacesMessage.SEVERITY_INFO)
			}
			else
			{
				GeneralUtil.addMessage("User Group could not be updated.", FacesMessage.SEVERITY_ERROR)
			}
		}
		else
		{
			if (this.daoUtil.create(this.userGroup!!))
			{
				GeneralUtil.addMessage("User Group has been created.", FacesMessage.SEVERITY_INFO)
			}
			else
			{
				GeneralUtil.addMessage("User Group could not be created.", FacesMessage.SEVERITY_ERROR)
			}
		}

		this.resetObjectForUIBind()
	}

	fun deleteUserGroup(group: UserGroup)
	{
		if (group.groupId != null && group.groupId > 0)
		{
			this.daoUtil.delete(group)

			this.newUserGroup()

			GeneralUtil.addMessage("User Group has been deleted.", FacesMessage.SEVERITY_INFO)
		}

	}

	private fun resetObjectForUIBind()
	{
		if (this.userGroup!!.parentGroup == null)
		{
			this.userGroup!!.parentGroup = UserGroup()
		}
	}

	private fun clearObjectForSave()
	{
		if (this.userGroup!!.parentGroup != null && (this.userGroup!!.parentGroup.groupId == null || this.userGroup!!.parentGroup.groupId < 1))
		{
			this.userGroup!!.parentGroup = null
		}
	}
}
