package com.jimwebguru.iberis.core.web.controllers.admin

import com.jimwebguru.iberis.core.loaders.GenericLazyLoader
import com.jimwebguru.iberis.core.utils.db.DaoUtil
import com.jimwebguru.iberis.core.persistence.ResourceBundleItem
import com.jimwebguru.iberis.core.web.utils.GeneralUtil
import com.sun.faces.application.ApplicationAssociate
import org.primefaces.event.RowEditEvent
import org.primefaces.event.SelectEvent


import javax.annotation.PostConstruct
import javax.faces.application.FacesMessage
import javax.faces.view.ViewScoped
import javax.inject.Named
import java.io.Serializable
import java.util.ResourceBundle

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
@Named
@ViewScoped
class BundleManagement : Serializable
{
	private val daoUtil = DaoUtil()

	var bundleItem = ResourceBundleItem()
	var selectedBundleItem: ResourceBundleItem? = null

	var referencedDisplayField: String? = null

	var lazyLoader = GenericLazyLoader(ResourceBundleItem::class.java)

	@PostConstruct
	fun postInitialization()
	{
		if (GeneralUtil.getQueryStringValue("referencedDisplayField") != null)
		{
			this.referencedDisplayField = GeneralUtil.getQueryStringValue("referencedDisplayField")
		}
	}

	fun addBundleItem()
	{
		if (this.bundleItem.bundleKey != null && this.bundleItem.bundleKey.isNotEmpty()
				&& this.bundleItem.bundleValue != null && this.bundleItem.bundleValue.isNotEmpty())
		{
			this.bundleItem.countryCode = "US"
			this.bundleItem.languageCode = "en"
			this.bundleItem.bundleName = "iberisbundle"
			this.bundleItem.variant = ""

			val success = this.daoUtil.create(this.bundleItem)

			if (success)
			{
				this.reloadResourceBundle()

				this.bundleItem = ResourceBundleItem()

				GeneralUtil.addMessage("Bundle item added and resource bundle reloaded.", FacesMessage.SEVERITY_INFO)
			}
			else
			{
				GeneralUtil.addMessage("Could not add bundle item.", FacesMessage.SEVERITY_ERROR)
			}
		}
	}

	private fun reloadResourceBundle()
	{
		ResourceBundle.clearCache(Thread.currentThread().contextClassLoader)

		val appBundle = ApplicationAssociate.getCurrentInstance().resourceBundles["bundle"] as Any

		val resources = this.getFieldValue(appBundle, "resources") as MutableMap<*,*>?

		resources!!.clear()
	}

	fun onRowSelect(event: SelectEvent)
	{
		this.selectedBundleItem = event.`object` as ResourceBundleItem
	}

	fun onRowEdit(event: RowEditEvent)
	{
		val success = this.daoUtil.update(event.`object`)

		if (success)
		{
			GeneralUtil.addMessage("Bundle item modified.", FacesMessage.SEVERITY_INFO)
		}

		this.reloadResourceBundle()
	}

	@SuppressWarnings("unused")
	fun onRowCancel(event: RowEditEvent)
	{

	}

	private fun getFieldValue(`object`: Any, fieldName: String): Any?
	{
		try
		{
			val field = `object`.javaClass.getDeclaredField(fieldName)
			field.isAccessible = true

			return field.get(`object`)
		}
		catch (e: Exception)
		{
			return null
		}

	}
}
