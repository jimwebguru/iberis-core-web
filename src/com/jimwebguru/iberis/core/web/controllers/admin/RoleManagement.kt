package com.jimwebguru.iberis.core.web.controllers.admin

import com.jimwebguru.iberis.core.loaders.GenericLazyLoader
import com.jimwebguru.iberis.core.persistence.*
import com.jimwebguru.iberis.core.utils.db.DaoUtil
import com.jimwebguru.iberis.core.utils.db.QueryParameter
import com.jimwebguru.iberis.core.web.utils.GeneralUtil

import javax.annotation.PostConstruct
import javax.faces.application.FacesMessage
import javax.faces.view.ViewScoped
import javax.inject.Named
import javax.faces.context.FacesContext
import java.io.Serializable
import java.util.ArrayList

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
@Named
@ViewScoped
class RoleManagement : Serializable
{
	protected var daoUtil = DaoUtil()

	var role: Role? = null

	var permission: Permission? = null

	var permissions: List<Permission>? = null

	var lazyLoader = GenericLazyLoader(Role::class.java)

	@PostConstruct
	fun postInit()
	{
		if (GeneralUtil.getRequestParameterValue("id") != null)
		{
			this.role = this.daoUtil.find(Role::class.java, java.lang.Long.valueOf(GeneralUtil.getRequestParameterValue("id").toString()))

			this.loadPermissions()
		}
	}

	fun newRole()
	{
		this.role = Role()
	}

	fun selectRole(item: Role)
	{
		this.role = item

		this.loadPermissions()
	}

	fun saveRole()
	{
		this.role!!.roleName = this.role!!.roleName.replace(" ".toRegex(), "_")

		if (this.role!!.id != null && this.role!!.id > 0)
		{
			if (this.daoUtil.update(this.role!!))
			{
				GeneralUtil.addMessage("Role has been updated.", FacesMessage.SEVERITY_INFO)
			}
			else
			{
				GeneralUtil.addMessage("Role could not be updated.", FacesMessage.SEVERITY_ERROR)
			}
		}
		else
		{
			if (this.daoUtil.create(this.role!!))
			{
				GeneralUtil.addMessage("Role has been created.", FacesMessage.SEVERITY_INFO)
			}
			else
			{
				GeneralUtil.addMessage("Role could not be created.", FacesMessage.SEVERITY_ERROR)
			}
		}
	}

	fun deleteRole(role: Role)
	{
		if (role.id != null && role.id > 0)
		{
			this.daoUtil.delete(role)

			this.newRole()

			GeneralUtil.addMessage("Role has been deleted.", FacesMessage.SEVERITY_INFO)
		}

	}

	fun newPermission()
	{
		this.permission = Permission()
		this.permission!!.unit = SystemUnit()
		this.permission!!.role = this.role
		this.permission!!.site = Site()
	}

	fun selectPermission(item: Permission)
	{
		this.permission = item

		if (this.permission!!.site == null)
		{
			this.permission!!.site = Site()
		}
	}

	fun savePermission()
	{
		if (this.permission!!.globalPermission!!)
		{
			this.permission!!.site.siteID = 0L
		}

		var countQuery = "SELECT COUNT(o) FROM Permission o WHERE o.role.id = " + this.role!!.id + " AND o.unit.unitID = " + this.permission!!.unit.unitID + " AND o.site.siteID = " + this.permission!!.site.siteID

		if (this.permission!!.id != null && this.permission!!.id > 0)
		{
			countQuery += " AND o.id != " + this.permission!!.id!!
		}

		val count = this.daoUtil.count(countQuery)

		if (count > 0)
		{
			GeneralUtil.addMessage("That permission record already exists.", FacesMessage.SEVERITY_ERROR)
		}
		else if (!this.permission!!.globalPermission && (this.permission!!.site == null || this.permission!!.site.siteID < 1))
		{
			GeneralUtil.addMessage("Permission needs to have a site when not global.", FacesMessage.SEVERITY_ERROR)
			FacesContext.getCurrentInstance().validationFailed()
		}
		else
		{
			if (this.permission!!.site != null && (this.permission!!.site.siteID == null || this.permission!!.site.siteID < 1))
			{
				this.permission!!.site = null
			}

			if (this.permission!!.id != null && this.permission!!.id > 0)
			{
				if (this.daoUtil.update(this.permission!!))
				{
					GeneralUtil.addMessage("Permission has been updated.", FacesMessage.SEVERITY_INFO)
				}
				else
				{
					GeneralUtil.addMessage("Permission could not be updated.", FacesMessage.SEVERITY_ERROR)
				}
			}
			else
			{
				if (this.daoUtil.create(this.permission!!))
				{
					GeneralUtil.addMessage("Permission has been created.", FacesMessage.SEVERITY_INFO)
				}
				else
				{
					GeneralUtil.addMessage("Permission could not be created.", FacesMessage.SEVERITY_ERROR)
				}
			}

			if (this.permission!!.site == null)
			{
				this.permission!!.site = Site()
			}

			this.loadPermissions()
		}
	}

	fun deletePermission(item: Permission)
	{
		if (item.id != null && item.id > 0)
		{
			this.daoUtil.delete(item)

			GeneralUtil.addMessage("Permission has been deleted.", FacesMessage.SEVERITY_INFO)

			this.loadPermissions()
		}
	}

	private fun loadPermissions()
	{
		val queryParameters = ArrayList<QueryParameter>()

		val queryParameter = QueryParameter()
		queryParameter.parameterName = "role"
		queryParameter.parameterValue = this.role!!.id

		queryParameters.add(queryParameter)

		this.permissions = this.daoUtil.find("SELECT OBJECT(o) FROM Permission o WHERE o.role.id = :role", queryParameters)
	}
}
