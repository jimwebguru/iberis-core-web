package com.jimwebguru.iberis.core.web.controllers.admin

import com.jimwebguru.iberis.core.layout.FormField
import com.jimwebguru.iberis.core.layout.FormLayout
import com.jimwebguru.iberis.core.layout.FormSection
import com.jimwebguru.iberis.core.persistence.*
import com.jimwebguru.iberis.core.utils.XmlUtil
import com.jimwebguru.iberis.core.utils.db.DaoUtil
import com.jimwebguru.iberis.core.utils.db.QueryParameter
import com.jimwebguru.iberis.core.web.utils.GeneralUtil

import javax.annotation.PostConstruct
import javax.faces.application.FacesMessage
import javax.faces.view.ViewScoped
import javax.inject.Named
import java.io.Serializable
import java.util.ArrayList

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
@Named
@ViewScoped
class FormManagement : Serializable
{
	private val daoUtil = DaoUtil()

	var form: Form? = Form()

	private var referencingProperties: MutableList<UnitProperty>? = null

	var referencePropertyId: Long? = null

	var tab = Tab()

	private val xmlUtil = XmlUtil()

	private var unitProperties: MutableList<UnitProperty>? = null

	var formLayout: FormLayout? = null

	@PostConstruct
	fun postInit()
	{
		if (GeneralUtil.getQueryStringValue("formId") != null)
		{
			this.form = this.daoUtil.find(Form::class.java, java.lang.Long.valueOf(GeneralUtil.getQueryStringValue("formId")))

			val prettyXml = this.xmlUtil.prettyPrint(this.form!!.layoutXml)

			if (prettyXml.isNotEmpty())
			{
				this.form!!.layoutXml = prettyXml
			}
		}

		if (GeneralUtil.getQueryStringValue("unitId") != null)
		{
			val unit = this.daoUtil.find(SystemUnit::class.java, java.lang.Long.valueOf(GeneralUtil.getQueryStringValue("unitId")))

			this.form!!.unit = unit
		}

		this.formInitialize()
	}

	private fun formInitialize()
	{
		if (this.form!!.unit != null && this.form!!.unit.unitID > 0)
		{
			val queryParameters = ArrayList<QueryParameter>()

			val queryParameter = QueryParameter()
			queryParameter.parameterName = "unit"
			queryParameter.parameterValue = this.form!!.unit

			queryParameters.add(queryParameter)

			this.unitProperties = this.daoUtil.find<UnitProperty>("SELECT OBJECT(o) FROM UnitProperty o WHERE o.unit = :unit", queryParameters)?.toMutableList()

			this.referencingProperties = this.daoUtil.find<UnitProperty>("SELECT OBJECT(o) FROM UnitProperty o WHERE o.propertyType.typeId = " + PropertyType.LOOKUP.propertyTypeId + " AND o.lookupUnitId = " + this.form!!.unit.unitID)?.toMutableList()
		}
		else
		{
			this.form!!.unit = SystemUnit()
		}


		if (this.form!!.formType == null)
		{
			this.form!!.formType = this.daoUtil.find(FormType::class.java, 1L)
		}

		if (this.form!!.formId == null || this.form!!.formId > 0 && (this.form!!.layoutXml == null || this.form!!.layoutXml.isEmpty()))
		{
			if (this.unitProperties != null)
			{
				val layoutBuilder = StringBuilder()
				layoutBuilder.append("<form><section name=\"General Information\"><fields>")

				for (unitProperty in this.unitProperties!!)
				{
					layoutBuilder.append("<field propertyId=\"")
					layoutBuilder.append(unitProperty.propertyId)
					layoutBuilder.append("\" required=\"")

					if (this.form!!.unit.lookupPropertyPrimaryKey == unitProperty.propertyId || unitProperty.unit.lookupPropertyPrimaryKey2 != null && unitProperty.unit.lookupPropertyPrimaryKey2 == unitProperty.propertyId)
					{
						layoutBuilder.append("true")
					}
					else
					{
						layoutBuilder.append("false")
					}

					layoutBuilder.append("\" width=\"12\" inline-label=\"true\"/>")

				}

				layoutBuilder.append("</fields></section></form>")

				this.form!!.layoutXml = layoutBuilder.toString()

				val prettyXml = this.xmlUtil.prettyPrint(this.form!!.layoutXml)

				if (prettyXml.isNotEmpty())
				{
					this.form!!.layoutXml = prettyXml
				}
			}
		}
	}

	fun save()
	{
		val prettyXml = this.xmlUtil.prettyPrint(this.form!!.layoutXml)

		if (prettyXml.isNotEmpty())
		{
			this.form!!.layoutXml = prettyXml
		}

		if (this.form!!.formId != null && this.form!!.formId > 0)
		{
			if (this.daoUtil.update(this.form!!))
			{
				GeneralUtil.addMessage("Form has been updated.", FacesMessage.SEVERITY_INFO)
			}
			else
			{
				GeneralUtil.addMessage("Form cannot be updated.", FacesMessage.SEVERITY_ERROR)
				GeneralUtil.addCallbackParam("validationFailed", true)
			}
		}
		else
		{
			this.form!!.unit = this.daoUtil.find(SystemUnit::class.java, this.form!!.unit.unitID)
			this.formInitialize()

			if (this.daoUtil.create(this.form!!))
			{
				GeneralUtil.addMessage("Form has been created.", FacesMessage.SEVERITY_INFO)
			}
			else
			{
				GeneralUtil.addMessage("Form cannot be created.", FacesMessage.SEVERITY_ERROR)
				GeneralUtil.addCallbackParam("validationFailed", true)
			}
		}
	}

	fun delete()
	{
		if (this.form!!.formId != null && this.form!!.formId > 0)
		{
			var success = true

			if (this.form!!.tabs != null && this.form!!.tabs.isNotEmpty())
			{
				for (tab in this.form!!.tabs)
				{
					success = this.daoUtil.delete(tab)
				}
			}

			if (success)
			{
				if (this.form!!.tabs != null)
				{
					this.form!!.tabs.clear()
				}

				if (this.daoUtil.delete(this.form!!))
				{
					GeneralUtil.addMessage("Form has been deleted.", FacesMessage.SEVERITY_INFO)

					this.form = Form()
					this.form!!.unit = SystemUnit()

					if (this.unitProperties != null)
					{
						this.unitProperties!!.clear()
					}

					if (this.referencingProperties != null)
					{
						this.referencingProperties!!.clear()
					}
				}
				else
				{
					GeneralUtil.addMessage("Form cannot be deleted.", FacesMessage.SEVERITY_ERROR)
					GeneralUtil.addCallbackParam("validationFailed", true)
				}
			}
		}
	}

	fun newTab()
	{
		this.tab = Tab()
		this.referencePropertyId = 0
	}

	fun loadTab(index: Int)
	{
		val tabs = ArrayList(this.form!!.tabs)

		this.tab = tabs[index]

		this.referencePropertyId = this.tab.referencedUnitProperty
	}

	fun saveTab()
	{
		this.tab.form = this.form

		if ((this.tab.referencedUnitProperty == null || this.tab.referencedUnitProperty < 1) && (this.tab.customQuery == null || this.tab.customQuery.isEmpty()))
		{
			GeneralUtil.addMessage("Referencing grid must have a custom query or referenced property.", FacesMessage.SEVERITY_ERROR)
			GeneralUtil.addCallbackParam("validationFailed", true)
		}
		else
		{
			if (this.tab.tabId != null && this.tab.tabId > 0)
			{
				if (this.daoUtil.update(this.tab))
				{
					GeneralUtil.addMessage("Related Grid has been updated.", FacesMessage.SEVERITY_INFO)
				}
				else
				{
					GeneralUtil.addMessage("Related Grid cannot be updated.", FacesMessage.SEVERITY_ERROR)
					GeneralUtil.addCallbackParam("validationFailed", true)
				}
			}
			else
			{
				if (this.daoUtil.create(this.tab))
				{
					if (this.form!!.tabs == null)
					{
						this.form!!.tabs = ArrayList()
					}

					this.form!!.tabs.add(this.tab)

					GeneralUtil.addMessage("Related Grid has been created.", FacesMessage.SEVERITY_INFO)
				}
				else
				{
					GeneralUtil.addMessage("Related Grid cannot be created.", FacesMessage.SEVERITY_ERROR)
					GeneralUtil.addCallbackParam("validationFailed", true)
				}
			}
		}
	}

	fun deleteTab(tab: Tab)
	{
		if (this.daoUtil.delete(tab))
		{
			GeneralUtil.addMessage("Related Grid has been deleted.", FacesMessage.SEVERITY_INFO)
			this.form!!.tabs.remove(tab)
		}
		else
		{
			GeneralUtil.addMessage("Related Grid cannot be deleted.", FacesMessage.SEVERITY_ERROR)
			GeneralUtil.addCallbackParam("validationFailed", true)
		}
	}

	fun referenceChange()
	{
		if(this.referencePropertyId != 0L)
		{
			for (unitProperty in this.referencingProperties!!)
			{
				if (unitProperty.propertyId == this.referencePropertyId)
				{
					this.tab.referencedUnitProperty = this.referencePropertyId
					this.tab.unitId = unitProperty.unit.unitID
					this.tab.gridId = unitProperty.unit.defaultGridId
				}
			}
		}
		else
		{
			this.tab.referencedUnitProperty = 0
			this.tab.unitId = 0
			this.tab.gridId = 0
		}
	}

	fun openLayoutDesigner()
	{
		this.formLayout = FormLayout(this.form!!.layoutXml)
	}

	fun addSection()
	{
		val section = FormSection()
		section.name = "New Section"

		this.formLayout!!.sections.add(section)
	}

	fun removeSection(section: FormSection)
	{
		this.formLayout!!.sections.remove(section)
	}

	fun addField(section: FormSection)
	{
		val formField = FormField()

		section.fields.add(formField)
	}

	fun removeField(section: FormSection, formField: FormField)
	{
		section.fields.remove(formField)
	}

	fun reorderFields(section: FormSection, field: FormField, sequenceIndex: Int)
	{
		section.fields.remove(field)
		section.fields.add(sequenceIndex, field)
	}

	fun saveLayout()
	{
		val layoutBuilder = StringBuilder()
		layoutBuilder.append("<form>")

		for (section in this.formLayout!!.sections)
		{
			layoutBuilder.append("<section name=\"")
			layoutBuilder.append(section.name)
			layoutBuilder.append("\"><fields>")

			for (formField in section.fields)
			{
				layoutBuilder.append("<field propertyId=\"")
				layoutBuilder.append(formField.propertyId)
				layoutBuilder.append("\" required=\"")
				layoutBuilder.append(formField.isRequired)
				layoutBuilder.append("\" width=\"")
				layoutBuilder.append(formField.width)
				layoutBuilder.append("\" inline-label=\"")
				layoutBuilder.append(formField.isInlineLabel)
				layoutBuilder.append("\"/>")
			}

			layoutBuilder.append("</fields></section>")
		}

		layoutBuilder.append("</form>")

		val prettyXml = this.xmlUtil.prettyPrint(layoutBuilder.toString())

		this.form!!.layoutXml = prettyXml
	}

	fun refresh()
	{

	}

	fun getReferencingProperties(): List<UnitProperty>?
	{
		return referencingProperties
	}

	fun getUnitProperties(): List<UnitProperty>?
	{
		return unitProperties
	}

	fun setUnitProperties(unitProperties: MutableList<UnitProperty>)
	{
		this.unitProperties = unitProperties
	}
}
