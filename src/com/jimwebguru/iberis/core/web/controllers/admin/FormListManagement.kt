package com.jimwebguru.iberis.core.web.controllers.admin

import com.jimwebguru.iberis.core.utils.db.DaoUtil

import com.jimwebguru.iberis.core.loaders.GenericLazyLoader
import com.jimwebguru.iberis.core.persistence.Form
import com.jimwebguru.iberis.core.utils.XmlUtil
import com.jimwebguru.iberis.core.web.utils.GeneralUtil

import javax.faces.application.FacesMessage
import javax.faces.view.ViewScoped
import javax.inject.Named
import java.io.Serializable

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
@Named
@ViewScoped
class FormListManagement : Serializable
{
	private val daoUtil = DaoUtil()

	var formGenericLazyLoader = GenericLazyLoader(Form::class.java)

	private val xmlUtil = XmlUtil()

	var form: Form? = null

	fun save()
	{
		val prettyXml = this.xmlUtil.prettyPrint(this.form!!.layoutXml)

		if (prettyXml.isNotEmpty())
		{
			this.form!!.layoutXml = prettyXml
		}

		if (this.daoUtil.update(this.form!!))
		{
			GeneralUtil.addMessage("Form has been updated.", FacesMessage.SEVERITY_INFO)
		}
		else
		{
			GeneralUtil.addMessage("Form cannot be updated.", FacesMessage.SEVERITY_ERROR)
			GeneralUtil.addCallbackParam("validationFailed", true)
		}
	}

	fun selectForm(form: Form)
	{
		this.form = form
	}

	fun delete(form: Form)
	{
		var success = true

		if (form.tabs != null && form.tabs.size > 0)
		{
			for (tab in form.tabs)
			{
				success = this.daoUtil.delete(tab)
			}
		}

		if (success)
		{
			if (this.daoUtil.delete(form))
			{
				GeneralUtil.addMessage("Form has been deleted.", FacesMessage.SEVERITY_INFO)
			}
			else
			{
				GeneralUtil.addMessage("Form cannot be deleted.", FacesMessage.SEVERITY_ERROR)
				GeneralUtil.addCallbackParam("validationFailed", true)
			}
		}
	}
}
