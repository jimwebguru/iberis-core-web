package com.jimwebguru.iberis.core.web.controllers.admin

import com.jimwebguru.iberis.core.persistence.*
import com.jimwebguru.iberis.core.utils.db.DaoUtil
import com.jimwebguru.iberis.core.utils.db.QueryParameter
import com.jimwebguru.iberis.core.web.utils.GeneralUtil

import javax.annotation.PostConstruct
import javax.faces.application.FacesMessage
import javax.faces.view.ViewScoped
import javax.inject.Named
import java.io.Serializable
import java.util.ArrayList

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
@Named
@ViewScoped
class GridManagement : Serializable
{
	var grid: Grid? = Grid()

	private val daoUtil = DaoUtil()

	var searchColumn = SearchColumn()

	var gridColumn = GridColumn()

	private var conditionOperators: List<ConditionOperator>? = null

	private var unitProperties: List<UnitProperty>? = null

	@PostConstruct
	fun postInit()
	{
		if (GeneralUtil.getQueryStringValue("gridId") != null)
		{
			this.grid = this.daoUtil.find(Grid::class.java, java.lang.Long.valueOf(GeneralUtil.getQueryStringValue("gridId")))
		}

		if (GeneralUtil.getQueryStringValue("unitId") != null)
		{
			val unitId = java.lang.Long.valueOf(GeneralUtil.getQueryStringValue("unitId"))

			this.grid!!.unitId = unitId

			val queryParameters = ArrayList<QueryParameter>()

			val queryParameter = QueryParameter()
			queryParameter.parameterName = "unitId"
			queryParameter.parameterValue = unitId

			queryParameters.add(queryParameter)

			this.unitProperties = this.daoUtil.find("SELECT OBJECT(o) FROM UnitProperty o WHERE o.unit.unitID = :unitId", queryParameters)
		}

		if (this.grid!!.gridId == null)
		{
			this.grid!!.gridColumns = ArrayList()
			this.grid!!.searchColumns = ArrayList()
		}

		this.conditionOperators = this.daoUtil.findAll(ConditionOperator::class.java)
	}

	fun save()
	{
		if (this.grid!!.gridId != null && this.grid!!.gridId > 0)
		{
			if (this.daoUtil.update(this.grid!!))
			{
				GeneralUtil.addMessage("Grid has been updated.", FacesMessage.SEVERITY_INFO)
			}
			else
			{
				GeneralUtil.addMessage("Grid cannot be updated.", FacesMessage.SEVERITY_ERROR)
				GeneralUtil.addCallbackParam("validationFailed", true)
			}
		}
		else
		{
			if (this.daoUtil.create(this.grid!!))
			{
				GeneralUtil.addMessage("Grid has been created.", FacesMessage.SEVERITY_INFO)

				if (this.unitProperties == null && this.grid!!.unitId != null && this.grid!!.unitId > 0)
				{
					val queryParameters = ArrayList<QueryParameter>()

					val queryParameter = QueryParameter()
					queryParameter.parameterName = "unitId"
					queryParameter.parameterValue = this.grid!!.unitId

					queryParameters.add(queryParameter)

					this.unitProperties = this.daoUtil.find("SELECT OBJECT(o) FROM UnitProperty o WHERE o.unit.unitID = :unitId", queryParameters)
				}
			}
			else
			{
				GeneralUtil.addMessage("Grid cannot be created.", FacesMessage.SEVERITY_ERROR)
				GeneralUtil.addCallbackParam("validationFailed", true)
			}
		}
	}

	fun delete()
	{
		var success = true

		for (gridColumn in this.grid!!.gridColumns)
		{
			success = this.daoUtil.delete(gridColumn)
		}

		for (searchColumn in this.grid!!.searchColumns)
		{
			success = this.daoUtil.delete(searchColumn)
		}

		if (success)
		{
			this.grid!!.gridColumns.clear()
			this.grid!!.searchColumns.clear()

			if (this.daoUtil.delete(this.grid!!))
			{
				GeneralUtil.addMessage("Grid has been deleted.", FacesMessage.SEVERITY_INFO)
				this.grid = Grid()
			}
			else
			{
				GeneralUtil.addMessage("Grid cannot be deleted.", FacesMessage.SEVERITY_ERROR)
				GeneralUtil.addCallbackParam("validationFailed", true)
			}
		}
	}

	fun newSearchColumn()
	{
		this.searchColumn = SearchColumn()
		this.searchColumn.unitProperty = UnitProperty()
		this.searchColumn.conditionOperator = ConditionOperator()
	}

	fun loadSearchColumn(index: Int)
	{
		val searchColumns = ArrayList(this.grid!!.searchColumns)

		this.searchColumn = searchColumns[index]
	}

	fun saveSearchColumn()
	{
		this.searchColumn.grid = this.grid

		if (this.searchColumn.searchID != null && this.searchColumn.searchID > 0)
		{
			if (this.daoUtil.update(this.searchColumn))
			{
				GeneralUtil.addMessage("Search Column has been updated.", FacesMessage.SEVERITY_INFO)
			}
			else
			{
				GeneralUtil.addMessage("Search Column cannot be updated.", FacesMessage.SEVERITY_ERROR)
				GeneralUtil.addCallbackParam("validationFailed", true)
			}
		}
		else
		{
			if (this.daoUtil.create(this.searchColumn))
			{
				if (this.grid!!.searchColumns == null)
				{
					this.grid!!.searchColumns = ArrayList()
				}

				this.grid!!.searchColumns.add(this.searchColumn)

				GeneralUtil.addMessage("Search Column has been created.", FacesMessage.SEVERITY_INFO)
			}
			else
			{
				GeneralUtil.addMessage("Search Column cannot be created.", FacesMessage.SEVERITY_ERROR)
				GeneralUtil.addCallbackParam("validationFailed", true)
			}
		}

		this.save()
	}

	fun deleteSearchColumn(searchColumn: SearchColumn)
	{
		if (this.daoUtil.delete(searchColumn))
		{
			GeneralUtil.addMessage("Search Column has been deleted.", FacesMessage.SEVERITY_INFO)

			this.grid!!.searchColumns.remove(searchColumn)

			this.save()
		}
		else
		{
			GeneralUtil.addMessage("Search Column cannot be deleted.", FacesMessage.SEVERITY_ERROR)
			GeneralUtil.addCallbackParam("validationFailed", true)
		}
	}

	fun newGridColumn()
	{
		this.gridColumn = GridColumn()
		this.gridColumn.unitProperty = UnitProperty()
	}

	fun loadGridColumn(index: Int)
	{
		val gridColumns = ArrayList(this.grid!!.gridColumns)

		this.gridColumn = gridColumns[index]
	}

	fun saveGridColumn()
	{
		this.gridColumn.grid = this.grid

		if (this.gridColumn.gridColumnId != null && this.gridColumn.gridColumnId > 0)
		{
			if (this.daoUtil.update(this.gridColumn))
			{
				GeneralUtil.addMessage("Grid Column has been updated.", FacesMessage.SEVERITY_INFO)
			}
			else
			{
				GeneralUtil.addMessage("Grid Column cannot be updated.", FacesMessage.SEVERITY_ERROR)
				GeneralUtil.addCallbackParam("validationFailed", true)
			}
		}
		else
		{
			if (this.daoUtil.create(this.gridColumn))
			{
				if (this.grid!!.gridColumns == null)
				{
					this.grid!!.gridColumns = ArrayList()
				}

				this.grid!!.gridColumns.add(this.gridColumn)

				GeneralUtil.addMessage("Grid Column has been created.", FacesMessage.SEVERITY_INFO)
			}
			else
			{
				GeneralUtil.addMessage("Grid Column cannot be created.", FacesMessage.SEVERITY_ERROR)
				GeneralUtil.addCallbackParam("validationFailed", true)
			}
		}

		this.save()
	}

	fun deleteGridColumn(gridColumn: GridColumn)
	{
		if (this.daoUtil.delete(gridColumn))
		{
			GeneralUtil.addMessage("Grid Column has been deleted.", FacesMessage.SEVERITY_INFO)
			this.grid!!.gridColumns.remove(gridColumn)

			this.save()
		}
		else
		{
			GeneralUtil.addMessage("Grid Column cannot be deleted.", FacesMessage.SEVERITY_ERROR)
			GeneralUtil.addCallbackParam("validationFailed", true)
		}
	}
}
