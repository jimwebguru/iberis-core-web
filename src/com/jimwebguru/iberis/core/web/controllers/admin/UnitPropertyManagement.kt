package com.jimwebguru.iberis.core.web.controllers.admin

import com.jimwebguru.iberis.core.persistence.*
import com.jimwebguru.iberis.core.utils.db.DaoUtil
import com.jimwebguru.iberis.core.utils.db.SchemaDB
import com.jimwebguru.iberis.core.web.utils.GeneralUtil
import org.apache.commons.lang3.StringUtils
import org.apache.commons.text.WordUtils
import org.primefaces.event.RowEditEvent

import javax.annotation.PostConstruct
import javax.faces.application.FacesMessage
import javax.faces.view.ViewScoped
import javax.inject.Named
import java.io.Serializable
import java.util.ArrayList

/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
@Named
@ViewScoped
class UnitPropertyManagement : Serializable
{
	private val daoUtil = DaoUtil()
	private val schemaDB = SchemaDB()
	private var oldPropertyName = ""

	var unitProperty: UnitProperty? = UnitProperty()
	var currentListItem = UnitPropertyListItem()

	@PostConstruct
	fun postInit()
	{
		if (GeneralUtil.getQueryStringValue("propertyId") != null)
		{
			this.unitProperty = this.daoUtil.find(UnitProperty::class.java, GeneralUtil.getQueryStringValue("propertyId")!!.toLong())
			this.oldPropertyName = this.unitProperty!!.propertyName
		}

		if (GeneralUtil.getQueryStringValue("unitId") != null)
		{
			this.unitProperty!!.unit = this.daoUtil.find(SystemUnit::class.java, GeneralUtil.getQueryStringValue("unitId")!!.toLong())
			this.unitProperty!!.propertyType = UnitPropertyType()
			this.unitProperty!!.unitPropertyListItems = ArrayList()
		}
	}

	fun save()
	{
		this.unitProperty!!.propertyName = this.formatPropertyName(this.unitProperty!!.propertyName)

		val unitPropertyValidation = UnitPropertyValidation()

		if (unitPropertyValidation.validate(this.unitProperty!!, this.unitProperty!!.unit.persistenceClass))
		{
			for (listItem in this.unitProperty!!.unitPropertyListItems)
			{
				if (listItem.unitProperty == null)
				{
					listItem.unitProperty = this.unitProperty
				}
			}

			if (this.unitProperty!!.propertyId != null && this.unitProperty!!.propertyId > 0)
			{
				if (this.daoUtil.update(this.unitProperty!!))
				{
					this.schemaDB.modifyUnitProperty(this.unitProperty!!.unit, this.unitProperty!!, this.oldPropertyName)

					this.oldPropertyName = this.unitProperty!!.propertyName

					GeneralUtil.addMessage("Unit Property has been updated.", FacesMessage.SEVERITY_INFO)
				}
				else
				{
					GeneralUtil.addMessage("Unit Property cannot be updated.", FacesMessage.SEVERITY_ERROR)
					GeneralUtil.addCallbackParam("validationFailed", true)
				}
			}
			else
			{
				if (this.daoUtil.create(this.unitProperty!!))
				{
					this.schemaDB.createUnitProperty(this.unitProperty!!.unit, this.unitProperty!!)

					this.oldPropertyName = this.unitProperty!!.propertyName

					GeneralUtil.addMessage("Unit Property has been created.", FacesMessage.SEVERITY_INFO)
				}
				else
				{
					GeneralUtil.addMessage("Unit Property cannot be created.", FacesMessage.SEVERITY_ERROR)
					GeneralUtil.addCallbackParam("validationFailed", true)
				}
			}
		}
	}

	private fun formatPropertyName(propertyN: String): String
	{
		var propertyName = propertyN
		propertyName = propertyName.replace("[^a-zA-Z0-9 -]".toRegex(), "")

		propertyName = WordUtils.capitalizeFully(propertyName)

		propertyName = StringUtils.uncapitalize(propertyName)

		propertyName = propertyName.replace(" ".toRegex(), "")

		return propertyName
	}

	fun editListItem(event: RowEditEvent)
	{
		val listItem = event.`object` as UnitPropertyListItem

		if (listItem.itemId != null && listItem.itemId > 0)
		{
			this.daoUtil.update(listItem)
		}

		GeneralUtil.addMessage("List item modified.", FacesMessage.SEVERITY_INFO)
	}

	fun addListItem()
	{
		this.unitProperty!!.unitPropertyListItems.add(this.currentListItem)
		this.currentListItem = UnitPropertyListItem()
	}

	fun removeListItem(listItem: UnitPropertyListItem)
	{
		if (listItem.itemId != null && listItem.itemId > 0)
		{
			this.daoUtil.delete(listItem)
		}

		this.unitProperty!!.unitPropertyListItems.remove(listItem)

		GeneralUtil.addMessage("List item removed.", FacesMessage.SEVERITY_INFO)
	}

	fun clearListItems()
	{
		for (listItem in this.unitProperty!!.unitPropertyListItems)
		{
			if (listItem.itemId != null && listItem.itemId > 0)
			{
				this.daoUtil.delete(listItem)
			}
		}

		this.unitProperty!!.unitPropertyListItems.clear()

		GeneralUtil.addMessage("List items cleared.", FacesMessage.SEVERITY_INFO)
	}

	fun lookupUnitChanged()
	{
		this.unitProperty!!.lookupGridId = java.lang.Long.valueOf(0)
	}

	fun refresh()
	{

	}
}
