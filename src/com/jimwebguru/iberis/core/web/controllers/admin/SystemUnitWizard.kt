package com.jimwebguru.iberis.core.web.controllers.admin

import com.jimwebguru.iberis.core.persistence.*
import com.jimwebguru.iberis.core.utils.XmlUtil
import com.jimwebguru.iberis.core.utils.db.DaoUtil
import com.jimwebguru.iberis.core.utils.db.SchemaDB
import com.jimwebguru.iberis.core.web.utils.GeneralUtil
import org.apache.commons.lang3.StringUtils
import org.apache.commons.text.WordUtils
import org.primefaces.event.FlowEvent
import org.primefaces.event.RowEditEvent
import org.primefaces.event.SelectEvent

import javax.faces.application.FacesMessage
import javax.faces.view.ViewScoped
import javax.inject.Named
import javax.faces.context.FacesContext
import javax.faces.model.SelectItem
import java.io.Serializable
import java.util.ArrayList


/**
 *
 * IBERIS CMS PROJECT
 * Created By: James May ( jimwebguru )
 *
 */
@Named
@ViewScoped
class SystemUnitWizard : Serializable
{
	private val daoUtil = DaoUtil()

	private val schemaDB = SchemaDB()

	var unit = SystemUnit()

	var form = Form()
	var grid = Grid()
	var unitProperties = ArrayList<UnitProperty>()

	var currentProperty = UnitProperty()
	var currentListItem = UnitPropertyListItem()

	private var newProperty = true

	var primaryKey: Int = 0
	var primaryKey2: Int = 0
	var displayPropertyId: Int? = null

	var propertySelectItems = ArrayList<SelectItem>()

	fun onFlowProcess(event: FlowEvent): String
	{
		if (event.newStep == "unit-properties" && this.unitProperties.size == 0)
		{
			val idProperty = UnitProperty()
			idProperty.propertyName = "id"
			idProperty.bundleLabelId = "idLabel"

			val propertyType = this.daoUtil.find(UnitPropertyType::class.java, 1L)
			idProperty.propertyType = propertyType

			this.unitProperties.add(idProperty)
		}

		if (event.oldStep == "unit-properties" && event.newStep == "keys")
		{
			if (this.unitProperties.size == 0)
			{
				GeneralUtil.addMessage("At least one property must be added.", FacesMessage.SEVERITY_ERROR)

				return event.oldStep
			}

			val unitPropertyValidation = UnitPropertyValidation()

			for (unitProperty in this.unitProperties)
			{
				if (!unitPropertyValidation.validate(unitProperty, this.unit.persistenceClass))
				{
					return event.oldStep
				}
			}

			this.propertySelectItems.clear()

			var counter = 0

			for (unitProperty in this.unitProperties)
			{
				this.propertySelectItems.add(SelectItem(counter, unitProperty.propertyName))
				counter++
			}
		}

		if (event.oldStep == "keys" && event.newStep == "confirmation")
		{
			this.form.name = WordUtils.capitalizeFully(this.unit.unitName) + " Default Form"

			val formType = FormType()
			formType.formTypeId = 1L

			this.form.formType = formType
			this.form.unit = this.unit

			this.grid.gridName = WordUtils.capitalizeFully(this.unit.unitName + " Default Grid")

		}

		return event.newStep
	}

	fun saveUnit(): String
	{
		this.unit.unitName = this.unit.unitName.toUpperCase().replace("[^a-zA-Z0-9 -]".toRegex(), "")

		if (this.unit.persistenceClass == null || this.unit.persistenceClass.trim { it <= ' ' } == "")
		{
			this.unit.tableName = "dynamic_" + this.unit.unitName.toLowerCase()
		}
		else
		{
			this.unit.tableName = ""
		}

		var success: Boolean

		if (this.unit.unitID != null && this.unit.unitID > 0)
		{
			success = this.daoUtil.update(this.unit)
		}
		else
		{
			success = this.daoUtil.create(this.unit)
		}

		if (!success)
		{
			GeneralUtil.addMessage("System Unit could not be created or updated.", FacesMessage.SEVERITY_ERROR)
			GeneralUtil.addCallbackParam("validationFailed", true)
			return ""
		}

		if (success)
		{
			for (unitProperty in this.unitProperties)
			{
				if (this.unit.tableName != null && this.unit.tableName.isNotEmpty() && unitProperty.propertyType.typeId.toInt() == PropertyType.LOOKUP.propertyTypeId)
				{
					unitProperty.isFakeLookup = true
				}

				unitProperty.unit = this.unit

				if (unitProperty.propertyId != null && unitProperty.propertyId > 0)
				{
					success = this.daoUtil.update(unitProperty)
				}
				else
				{
					success = this.daoUtil.create(unitProperty)
				}

				if (!success)
				{
					GeneralUtil.addMessage("Unit Property could not be created or updated.", FacesMessage.SEVERITY_ERROR)
					GeneralUtil.addCallbackParam("validationFailed", true)
					return ""
				}

				if (unitProperty.propertyType.typeId.toInt() == PropertyType.SELECTION.propertyTypeId)
				{
					for (unitPropertyListItem in unitProperty.unitPropertyListItems)
					{
						unitPropertyListItem.unitProperty = unitProperty

						if (unitPropertyListItem.itemId != null && unitPropertyListItem.itemId > 0)
						{
							success = this.daoUtil.update(unitPropertyListItem)
						}
						else
						{
							success = this.daoUtil.create(unitPropertyListItem)
						}

						if (!success)
						{
							GeneralUtil.addMessage("List item for " + unitProperty.propertyName + " could not be updated or created.", FacesMessage.SEVERITY_ERROR)
							GeneralUtil.addCallbackParam("validationFailed", true)
							return ""
						}
					}
				}
			}

			if (success)
			{
				val displayProperty = this.unitProperties[this.displayPropertyId!!]
				val primaryProperty = this.unitProperties[this.primaryKey]

				var primary2Property: UnitProperty? = null

				if (this.primaryKey2 > -1)
				{
					primary2Property = this.unitProperties[this.primaryKey2]
				}

				this.unit.lookupDisplayPropertyId = displayProperty.propertyId
				this.unit.lookupPropertyPrimaryKey = primaryProperty.propertyId

				if (primary2Property != null)
				{
					this.unit.lookupPropertyPrimaryKey2 = primary2Property.propertyId
				}

				success = this.daoUtil.update(this.unit)

				if (!success)
				{
					GeneralUtil.addMessage("System Unit could not be updated with keys.", FacesMessage.SEVERITY_ERROR)
					GeneralUtil.addCallbackParam("validationFailed", true)
					return ""
				}

				if (success)
				{
					this.grid.unitId = this.unit.unitID

					val gridColumns = ArrayList<GridColumn>()

					val layoutBuilder = StringBuilder()
					layoutBuilder.append("<form><section name=\"General Information\" columns=\"2\"><fields>")

					var counter = 0

					for (unitProperty in this.unitProperties)
					{
						layoutBuilder.append("<field propertyId=\"")
						layoutBuilder.append(unitProperty.propertyId)
						layoutBuilder.append("\" required=\"")

						if (counter == primaryKey || counter == primaryKey2)
						{
							layoutBuilder.append("true")
						}
						else
						{
							layoutBuilder.append("false")
						}

						layoutBuilder.append("\"/>")

						val gridColumn = GridColumn()
						gridColumn.unitProperty = unitProperty
						gridColumn.grid = this.grid

						gridColumns.add(gridColumn)

						counter++
					}

					layoutBuilder.append("</fields></section></form>")

					this.form.layoutXml = layoutBuilder.toString()

					val prettyXml = XmlUtil().prettyPrint(this.form.layoutXml)

					if (prettyXml.isNotEmpty())
					{
						this.form.layoutXml = prettyXml
					}

					if (this.form.formId != null)
					{
						success = this.daoUtil.update(this.form)
					}
					else
					{
						success = this.daoUtil.create(this.form)
					}

					if (!success)
					{
						GeneralUtil.addMessage("Form could not be created or updated.", FacesMessage.SEVERITY_ERROR)
						GeneralUtil.addCallbackParam("validationFailed", true)
						return ""
					}

					this.unit.defaultFormId = this.form.formId
					this.daoUtil.update(this.unit)

					if (success)
					{
						if (this.grid.gridId != null)
						{
							success = this.daoUtil.update(this.grid)
						}
						else
						{
							success = this.daoUtil.create(this.grid)
						}

						if (!success)
						{
							GeneralUtil.addMessage("Grid could not be created or updated.", FacesMessage.SEVERITY_ERROR)
							GeneralUtil.addCallbackParam("validationFailed", true)
							return ""
						}

						this.unit.defaultGridId = this.grid.gridId
						this.daoUtil.update(this.unit)

						if (success)
						{
							for (gridColumn in gridColumns)
							{
								gridColumn.grid = this.grid

								if (gridColumn.gridColumnId != null)
								{
									success = this.daoUtil.update(gridColumn)
								}
								else
								{
									success = this.daoUtil.create(gridColumn)
								}

								if (!success)
								{
									GeneralUtil.addMessage("Grid Column could not be created or updated.", FacesMessage.SEVERITY_ERROR)
									GeneralUtil.addCallbackParam("validationFailed", true)
									return ""
								}
							}

							this.daoUtil.refresh(Grid::class.java, this.grid.gridId)

							if (success && this.unit.tableName.trim { it <= ' ' } != "")
							{
								success = this.schemaDB.createUnitTable(this.unit, this.unitProperties, primaryProperty, primary2Property)

								if (!success)
								{
									GeneralUtil.addMessage("System unit table could not be created. Table may already exist.", FacesMessage.SEVERITY_ERROR)
									GeneralUtil.addCallbackParam("validationFailed", true)
									return ""
								}
							}

							if (success)
							{
								try
								{
									FacesContext.getCurrentInstance().externalContext.redirect(FacesContext.getCurrentInstance().externalContext.requestContextPath + "/admin/system-unit-wizard-confirm/" + this.unit.unitID)
								}
								catch (e: Exception)
								{
									GeneralUtil.logError(e)
								}

							}
						}
					}
				}
			}

		}

		return ""
	}

	private fun formatPropertyName(propertyN: String): String
	{
		var propertyName = propertyN
		propertyName = propertyName.replace("[^a-zA-Z0-9 -]".toRegex(), "")

		propertyName = WordUtils.capitalizeFully(propertyName)

		propertyName = StringUtils.uncapitalize(propertyName)

		propertyName = propertyName.replace(" ".toRegex(), "")

		return propertyName
	}

	fun refresh()
	{

	}

	fun saveProperty()
	{
		this.currentProperty.propertyName = this.formatPropertyName(this.currentProperty.propertyName)

		if (this.newProperty)
		{
			this.unitProperties.add(this.currentProperty)
		}
	}

	fun addUnitProperty()
	{
		this.currentProperty = UnitProperty()
		this.currentProperty.propertyType = UnitPropertyType()
		this.currentProperty.unitPropertyListItems = ArrayList()
		this.newProperty = true
	}

	fun editUnitProperty(index: Int)
	{
		this.currentProperty = this.unitProperties[index]
		this.newProperty = false
	}

	fun removeUnitProperty(index: Int)
	{
		this.unitProperties.removeAt(index)
	}

	@SuppressWarnings("unused")
	fun onPropertyRowSelect(event: SelectEvent)
	{

	}

	@SuppressWarnings("unused")
	fun editListItem(event: RowEditEvent)
	{
		GeneralUtil.addMessage("List item modified.", FacesMessage.SEVERITY_INFO)
	}

	fun addListItem()
	{
		this.currentProperty.unitPropertyListItems.add(this.currentListItem)
		this.currentListItem = UnitPropertyListItem()
	}

	fun removeListItem(listItem: UnitPropertyListItem)
	{
		this.currentProperty.unitPropertyListItems.remove(listItem)
	}

	fun clearListItems()
	{
		this.currentProperty.unitPropertyListItems.clear()
	}
}
