canvas.on('object:scaled', function(event)
{
	var scaledObject = event.target;
	
	if(scaledObject._text)
	{
		$("#txtSize").val((scaledObject.fontSize * scaledObject.scaleX).toFixed(0));
	}
});

function populateTextPropertyFields()
{
	var txtObject = canvas.getActiveObject();
					
	$("#txtDisplay").val(txtObject.text);
	$("#txtFont").val(txtObject.fontFamily);
	$("#txtSize").val((txtObject.fontSize * txtObject.scaleX).toFixed(0));
	
	if(txtObject.fontWeight == "bold")
	{
		$("#txtBold").prop("checked", true);
	}
	else
	{
		$("#txtBold").prop("checked", false);
	}
		
	if(txtObject.linethrough == true)
	{
		$("#txtLine").val("linethrough");
	}
	else if(txtObject.overline == true)
	{
		$("#txtLine").val("overline");
	}
	else if(txtObject.underline == true)
	{
		$("#txtLine").val("underline");
	}
	else
	{
		$("#txtLine").val("");
	}
	
	if(txtObject.shadow)
	{
		$("#txtShadow").prop("checked", true);
		$("#txtShadowOffset").val(txtObject.shadow.offsetX);
		
		var rgbArray = txtObject.shadow.color.split(",");
		
		var reg = /\d+/;
		var r = parseInt(rgbArray[0].match(reg)[0]);
		var g = parseInt(rgbArray[1].match(reg)[0]);
		var b = parseInt(rgbArray[2].match(reg)[0]);
		
		var hexColor = rgbToHex(r, g, b);
		
		$("#txtShadowColor").val(hexColor);
	}
	else
	{
		$("#txtShadowOffset").val(5);
		$("#txtShadowColor").val("#000000");
		$("#txtShadow").prop("checked", false);
	}
	
	$("#txtStyle").val(txtObject.fontStyle);
	$("#txtColor").val(txtObject.fill);
	
	if(txtObject.stroke)
	{
		$("#txtStroke").prop("checked", true);
		$("#txtStrokeColor").val(txtObject.stroke);
		$("#txtStrokeSize").val(txtObject.strokeWidth);
	}
	else
	{
		$("#txtStroke").prop("checked", false);
		$("#txtStrokeColor").val("#000000");
		$("#txtStrokeSize").val(2);
	}
}

function addTextObject()
{
	var text = new fabric.Text('Default Text', { left: 100, top: 100 });
	text['borderColor'] = 'black';
	text['cornerColor'] = 'black';
	text['cornerStrokeColor'] = 'black';
	text.setColor("#000000");
	//text.set('shadow', 'rgba(0,0,0,0.5) 5px 5px 5px');
	canvas.add(text);
	
	reloadObjectList();
	
	canvas.setActiveObject(canvas.item(canvas.getObjects().length - 1));
	canvas.renderAll();
	
	$("#txtDisplay").val(text.text);
	$("#txtFont").val(text.fontFamily);
	$("#txtSize").val(text.fontSize);
	$("#txtBold").prop("checked", false);
	$("#txtLine").val("");
	$("#txtShadowOffset").val(5);
	$("#txtShadowColor").val("#000000");
	$("#txtShadow").prop("checked", false);
	$("#txtStyle").val("normal");
	$("#txtColor").val("#000000");
	$("#txtStroke").prop("checked", false);
	$("#txtStrokeColor").val("#000000");
	$("#txtStrokeSize").val(2);
	
	$("#object-properties").show();
	$(".txtProperties").show();
	$(".imgProperties").hide();
}

function setText()
{
	canvas.getActiveObject().text = $("#txtDisplay").val();
	canvas.renderAll();
	
	reloadObjectList();
}

function setFont()
{
	canvas.getActiveObject().fontFamily = $("#txtFont").val();
	canvas.renderAll();
}

function setFontSize()
{
	canvas.getActiveObject().set('fontSize', $("#txtSize").val());
	canvas.renderAll();
}

function setFontColor()
{
	canvas.getActiveObject().setColor($("#txtColor").val());
	canvas.renderAll();
}

function setFontStyle()
{
	canvas.getActiveObject().fontStyle = $("#txtStyle").val();
	canvas.renderAll();
}

$("#txtBold").click(function()
{
	if($(this).is(":checked"))
	{
		canvas.getActiveObject().fontWeight = 'bold';
	}
	else
	{
		canvas.getActiveObject().fontWeight = 'normal';
	}
	
	canvas.renderAll();
});

function setFontLine()
{
	if($("#txtLine").val() == "linethrough")
	{
		canvas.getActiveObject().set('linethrough', true);
		canvas.getActiveObject().set('overline', false);
		canvas.getActiveObject().set('underline', false);
	}
	else if($("#txtLine").val() == "overline")
	{
		canvas.getActiveObject().set('linethrough', false);
		canvas.getActiveObject().set('overline', true);
		canvas.getActiveObject().set('underline', false);
	}
	else if($("#txtLine").val() == "underline")
	{
		canvas.getActiveObject().set('linethrough', false);
		canvas.getActiveObject().set('overline', false);
		canvas.getActiveObject().set('underline', true);
	}
	else
	{
		canvas.getActiveObject().set('linethrough', false);
		canvas.getActiveObject().set('overline', false);
		canvas.getActiveObject().set('underline', false);
	}
	
	canvas.renderAll();
}

$("#txtShadow").click(function()
{
	var txtObject = canvas.getActiveObject();
	
	if($(this).is(":checked"))
	{
		if(!txtObject.shadow)
		{
			txtObject.shadow = {};
		}
		
		var rgbColor = hexToRgb($("#txtShadowColor").val());
		
		txtObject.shadow.color = "rgba(" + rgbColor.r + "," + rgbColor.g + "," + rgbColor.b + ",0.5)";
		txtObject.shadow.blur = 5;
		txtObject.shadow.offsetX = $("#txtShadowOffset").val();
		txtObject.shadow.offsetY = $("#txtShadowOffset").val();
	}
	else
	{
		txtObject.shadow = null;
	}
	
	canvas.renderAll();
});

function setFontShadowColor()
{
	var txtObject = canvas.getActiveObject();
	
	if(txtObject.shadow)
	{
		var rgbColor = hexToRgb($("#txtShadowColor").val());
		
		txtObject.shadow.color = "rgba(" + rgbColor.r + "," + rgbColor.g + "," + rgbColor.b + ",0.5)";
		
		canvas.renderAll();
	}
}

function setFontShadowOffset()
{
	var txtObject = canvas.getActiveObject();
	
	if(txtObject.shadow)
	{
		txtObject.shadow.offsetX = $("#txtShadowOffset").val();
		txtObject.shadow.offsetY = $("#txtShadowOffset").val();
		
		canvas.renderAll();
	}
}

$("#txtStroke").click(function()
{
	var txtObject = canvas.getActiveObject();
	
	if($(this).is(":checked"))
	{
		txtObject.set('stroke', $("#txtStrokeColor").val());
		txtObject.set('strokeWidth', $("#txtStrokeSize").val());
	}
	else
	{
		//txtObject.shadow = null;
		
		txtObject.set('stroke', null);
		txtObject.set('strokeWidth', null);
	}
	
	canvas.renderAll();
});

function setFontStrokeColor()
{
	if($("#txtStroke").is(":checked"))
	{
		canvas.getActiveObject().set('stroke', $("#txtStrokeColor").val());
	}
	else
	{
		canvas.getActiveObject().set('stroke', null);
	}
	
	canvas.renderAll();
}

function setFontStrokeSize()
{
	if($("#txtStroke").is(":checked"))
	{
		canvas.getActiveObject().set('strokeWidth', $("#txtStrokeSize").val());
	}
	else
	{
		canvas.getActiveObject().set('strokeWidth', null);
	}
	
	canvas.renderAll();
}