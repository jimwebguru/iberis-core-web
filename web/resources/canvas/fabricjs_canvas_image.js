document.getElementById('file').addEventListener("change", function (e)
{
	var file = e.target.files[0];
	var reader = new FileReader();

	reader.onload = function (f)
	{
		var data = f.target.result;

		addCanvasImage(data);
	};

	reader.readAsDataURL(file);

	$('#file').val('');
});

function selectServerImage(url)
{
	addCanvasImage(url);
}

function addCanvasImage(data)
{
	fabric.Image.fromURL(data, function (img)
	{
		var oImg = img.set({left: 0, top: 0, angle: 0, transparentCorners: false});
		oImg['borderColor'] = 'black';
		oImg['cornerColor'] = 'black';
		oImg['cornerStrokeColor'] = 'black';
		oImg.scale(0.5);

		canvas.add(oImg);
		canvas.setActiveObject(canvas.item(canvas.getObjects().length - 1));
		canvas.renderAll();

		reloadObjectList();


		$("#brownie").prop("checked", false);
		$("#vintage").prop("checked", false);
		$("#technicolor").prop("checked", false);
		$("#polaroid").prop("checked", false);
		$("#kodachrome").prop("checked", false);
		$("#blackwhite").prop("checked", false);
		$("#grayscale").prop("checked", false);
		$("#invert").prop("checked", false);
		$("#sepia").prop("checked", false);
		$("#brightness").prop("checked", false);
		$("#brightness-value").val(0.1);
		$("#contrast").prop("checked", false);
		$("#contrast-value").val(0);
		$("#saturation").prop("checked", false);
		$("#saturation-value").val(0);
		$("#noise").prop("checked", false);
		$("#noise-value").val(100);
		$("#pixelate").prop("checked", false);
		$("#pixelate-value").val(4);
		$("#blur").prop("checked", false);
		$("#blur-value").val(0.1);
		$("#sharpen").prop("checked", false);
		$("#emboss").prop("checked", false);
		$("#hue").prop("checked", false);
		$("#hue-value").val(0);

		$("#object-properties").show();
		$(".imgProperties").show();
		$(".txtProperties").hide();
		//var dataURL = canvas.toDataURL({format: 'png', quality: 0.8});
	});
}

function populateImagePropertyFields()
{
	var imgObject = canvas.getActiveObject();

	$("#sepia").prop("checked", false);
	$("#brownie").prop("checked", false);
	$("#vintage").prop("checked", false);
	$("#technicolor").prop("checked", false);
	$("#polaroid").prop("checked", false);
	$("#kodachrome").prop("checked", false);
	$("#blackwhite").prop("checked", false);
	$("#grayscale").prop("checked", false);
	$("#invert").prop("checked", false);
	$("#brightness").prop("checked", false);
	$("#brightness-value").val(0.1);
	$("#contrast").prop("checked", false);
	$("#contrast-value").val(0);
	$("#saturation").prop("checked", false);
	$("#saturation-value").val(0);
	$("#noise").prop("checked", false);
	$("#noise-value").val(100);
	$("#pixelate").prop("checked", false);
	$("#pixelate-value").val(4);
	$("#blur").prop("checked", false);
	$("#blur-value").val(0.1);
	$("#sharpen").prop("checked", false);
	$("#emboss").prop("checked", false);
	$("#hue").prop("checked", false);
	$("#hue-value").val(0);

	if(imgObject.filters.length > 0)
	{
		imgObject.filters.forEach(function(filter)
		{
			if (filter.type == "Brownie")
			{
				$("#brownie").prop("checked", true);
			}

			if (filter.type == "Vintage")
			{
				$("#vintage").prop("checked", true);
			}

			if (filter.type == "Technicolor")
			{
				$("#technicolor").prop("checked", true);
			} 

			if (filter.type == "Polaroid")
			{
				$("#polaroid").prop("checked", true);
			}

			if (filter.type == "Kodachrome")
			{
				$("#kodachrome").prop("checked", true);
			}

			if (filter.type == "BlackWhite")
			{
				$("#blackwhite").prop("checked", true);
			}

			if (filter.type == "Grayscale")
			{
				$("#grayscale").prop("checked", true);
			}

			if (filter.type == "Invert")
			{
				$("#invert").prop("checked", true);
			}

			if (filter.type == "Sepia")
			{
				$("#sepia").prop("checked", true);
			}

			if (filter.type == "Brightness")
			{
				$("#brightness").prop("checked", true);
				$("#brightness-value").val(filter.brightness);
			}

			if (filter.type == "Contrast")
			{
				$("#contrast").prop("checked", true);
				$("#contrast-value").val(filter.contrast);
			}

			if (filter.type == "Saturation")
			{
				$("#saturation").prop("checked", true);
				$("#saturation-value").val(filter.saturation);
			}

			if (filter.type == "Noise")
			{
				$("#noise").prop("checked", true);
				$("#noise-value").val(filter.noise);
			}

			if (filter.type == "Pixelate")
			{
				$("#pixelate").prop("checked", true);
				$("#pixelate-value").val(filter.blocksize);
			}

			if (filter.type == "Blur")
			{
				$("#blur").prop("checked", true);
				$("#blur-value").val(filter.blur);
			}

			/*if (filter.type == "Convolute")
			{
				$("#sharpen").prop("checked", true);
			} */

			if (filter.type == "Convolute")
			{
				$("#emboss").prop("checked", true);
			}

			if (filter.type == "HueRotation")
			{
				$("#hue").prop("checked", true);
				$("#hue-value").val(filter.rotation);
			}
		});
	}
}

f = fabric.Image.filters;

function applyFilter(filterName, filter)
{
	var obj = canvas.getActiveObject();

	if(filter === false)
	{
		var newFilters = Array();

		obj.filters.forEach(function (f)
		{
			if(f.type !== filterName)
			{
				newFilters.push(f);
			}
		});
		
		obj.filters = newFilters;
	}
	else
	{
		obj.filters.push(filter);
	}

	//obj.filters[index] = filter;
	obj.applyFilters();
	
	canvas.renderAll();
}

/*function getFilter(index)
{
	var obj = canvas.getActiveObject();
	return obj.filters[index];
} */

function applyFilterValue(filterName, prop, value)
{
	var obj = canvas.getActiveObject();

	obj.filters.forEach(function (f)
	{
		if(f.type === filterName)
		{
			f[prop] = value;
			obj.applyFilters();
			canvas.renderAll();
		}
	});
}

$('#brownie').click(function() {
	applyFilter("Brownie", $(this).is(":checked") && new f.Brownie());
});
  
$('#vintage').click(function() {
	applyFilter("Vintage", $(this).is(":checked") && new f.Vintage());
});
  
$('#technicolor').click(function() {
	applyFilter("Technicolor", $(this).is(":checked") && new f.Technicolor());
});
  
$('#polaroid').click(function() {
	applyFilter("Polaroid", $(this).is(":checked") && new f.Polaroid());
});
  
$('#kodachrome').click(function() {
	applyFilter("Kodachrome", $(this).is(":checked") && new f.Kodachrome());
});
  
$('#blackwhite').click(function() {
	applyFilter("BlackWhite", $(this).is(":checked") && new f.BlackWhite());
});
  
$('#grayscale').click(function() {
	applyFilter("Grayscale", $(this).is(":checked") && new f.Grayscale());
});

$('#invert').click(function() {
	applyFilter("Invert", $(this).is(":checked") && new f.Invert());
});

$('#sepia').click(function() {
	applyFilter("Sepia", $(this).is(":checked") && new f.Sepia());
});

$('#brightness').click(function () {
	applyFilter("Brightness", $(this).is(":checked") && new f.Brightness({
	  brightness: parseFloat($('#brightness-value').val())
	}));
});
  
$('#brightness-value').change(function() {
	applyFilterValue("Brightness", 'brightness', parseFloat($(this).val()));
});

$('#contrast').click(function () {
	applyFilter("Contrast", $(this).is(":checked") && new f.Contrast({
	  contrast: parseFloat($('#contrast-value').val())
	}));
});

$('#contrast-value').change(function() {
	applyFilterValue("Contrast", 'contrast', parseFloat($(this).val()));
});
  
$('#saturation').click(function () {
	applyFilter("Saturation", $(this).is(":checked") && new f.Saturation({
	  saturation: parseFloat($('#saturation-value').val())
	}));
});
  
$('#saturation-value').change(function() {
	applyFilterValue("Saturation", 'saturation', parseFloat($(this).val()));
});
  
$('#noise').click(function () {
	applyFilter("Noise", $(this).is(":checked") && new f.Noise({
	  noise: parseInt($('#noise-value').val(), 10)
	}));
});
  
$('#noise-value').change(function() {
	applyFilterValue("Noise", 'noise', parseInt($(this).val(), 10));
});
  
$('#pixelate').click(function() {
	applyFilter("Pixelate", $(this).is(":checked") && new f.Pixelate({
	  blocksize: parseInt($('#pixelate-value').val(), 10)
	}));
});
  
$('#pixelate-value').change(function() {
	applyFilterValue("Pixelate", 'blocksize', parseInt($(this).val(), 10));
});
  
$('#blur').click(function() {
	applyFilter("Blur", $(this).is(":checked") && new f.Blur({
	  blur: parseFloat($('#blur-value').val())
	}));
});
  
$('#blur-value').change(function() {
	applyFilterValue("Blur", 'blur', parseFloat($(this).val(), 10));
});
  
/*$('#sharpen').click(function() {
	applyFilter("Convolute", $(this).is(":checked") && new f.Convolute({
	  matrix: [  0, -1,  0,
				-1,  5, -1,
				 0, -1,  0 ],
		ftype: 'Sharpen'
	}));
});*/
  
$('#emboss').click(function() {
	applyFilter("Convolute", $(this).is(":checked") && new f.Convolute({
	  matrix: [ 1,   1,  1,
				1, 0.7, -1,
			   -1,  -1, -1 ],
		ftype: 'Emboss'
	}));
});

$('#hue').click(function() {
	applyFilter("HueRotation", $(this).is(":checked") && new f.HueRotation({
	  rotation: $('#hue-value').val(),
	}));
});

$('#hue-value').change(function() {
	applyFilterValue("HueRotation", 'rotation', $(this).val());
});