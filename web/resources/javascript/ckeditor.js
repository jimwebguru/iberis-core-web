CKEDITOR.editorConfig = function( config )
{
    config.filebrowserBrowseUrl = '/file-browser',
    config.filebrowserImageBrowseUrl = '/file-browser?type=1',
    config.filebrowserWindowHeight = '500',
    config.filebrowserWindowWidth = ((screen.width > 800)? 800 : (screen.width - 10))
};
