var quickFormFrameId = "#quickFormIframe";
var lookupDialogFrameId = "#lookupDialogIframe";

/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
function openLookupDialog(unitId, primaryKeyField, primaryKey2Field, displayField, callbackMethod, filter)
{
    if(unitId != null && unitId != 0)
    {
            var queryString = "";

            if(callbackMethod != "")
            {
                queryString = "?callbackMethod=" + callbackMethod;
            }

            if(typeof filter !== 'undefined' && filter != "")
            {
                if(queryString != "")
                {
                    queryString = queryString + "&filter=" + filter;
                }
                else
                {
                    queryString = "?filter=" + filter;
                }
            }

            if(parent.PF('lookupDialog') != null)
            {
                if(primaryKey2Field != null && primaryKey2Field != '')
                {
                    $(lookupDialogFrameId,window.parent.document).attr("src", "/lookup-grid/" + unitId + "/" + displayField + "/" + primaryKeyField + "/" + primaryKey2Field + queryString);
                }
                else
                {
                    $(lookupDialogFrameId,window.parent.document).attr("src", "/lookup-grid/" + unitId + "/" + displayField + "/" + primaryKeyField + queryString);
                }
                
                parent.PF('lookupDialog').show();
            }
            else
            {
                if(primaryKey2Field != null && primaryKey2Field != '')
                {
                    $(lookupDialogFrameId).attr("src", "/lookup-grid/" + unitId + "/" + displayField + "/" + primaryKeyField + "/" + primaryKey2Field + queryString);
                }
                else
                {
                    $(lookupDialogFrameId).attr("src", "/lookup-grid/" + unitId + "/" + displayField + "/" + primaryKeyField + queryString);
                }
                
                PF('lookupDialog').show();
            }
    }
}

function closeLookupDialog(callbackMethod)
{
    if(parent.PF('lookupDialog') != null)
    {
        $(lookupDialogFrameId,window.parent.document).attr("src","about:blank");
        parent.PF('lookupDialog').hide();

        if(callbackMethod != "")
        {
            var fn = parent.window[callbackMethod];

            if(typeof fn === 'function')
            {
                fn([{name:'skipValidation',value:'1'}]);
            }
        }
    }
    else
    {
        $(lookupDialogFrameId).attr("src","about:blank");
        PF('lookupDialog').hide();

        if(callbackMethod != "")
        {
            var fn = window[callbackMethod];

            if(typeof fn === 'function')
            {
                fn([{name:'skipValidation',value:'1'}]);
            }
        }
    }
}

function fillInLookupValues(displayField, primaryKeyField, primaryKeyField2, displayValue, primaryKey, primaryKey2, callbackMethod)
{
    displayFieldId = PrimeFaces.escapeClientId(displayField);
    primaryKeyFieldId = PrimeFaces.escapeClientId(primaryKeyField);
    primaryKeyField2Id = null;

    if(primaryKeyField2 != null && primaryKeyField2 != '')
    {
        primaryKeyField2Id = PrimeFaces.escapeClientId(primaryKeyField2);
    }

    if(primaryKey != null && primaryKey != "")
    {
        if(parent.PF('lookupDialog') != null)
        {
            if(!$(displayFieldId,window.parent.document).length > 0)
            {
                var otherFrame = searchForFieldsInOtherIframes(displayFieldId);

                if(otherFrame != "")
                {
                    $(displayFieldId,$(otherFrame,window.parent.document).contents()).html(displayValue);
                    $(displayFieldId + "_value",$(otherFrame,window.parent.document).contents()).val(displayValue);

                    if($(otherFrame,window.parent.document)[0].contentWindow.PF(displayField).getJQ().hasClass('canactivate'))
                    {
                        $(otherFrame, window.parent.document)[0].contentWindow.PF(displayField).enable();
                    }
                    
                    $(primaryKeyFieldId,$(otherFrame,window.parent.document).contents()).val(primaryKey);

                    if (primaryKey2 != null && primaryKey2 != '')
                    {
                        $(primaryKeyField2Id,$(otherFrame,window.parent.document).contents()).val(primaryKey2);
                    }
                }
            }
            else
            {
                $(displayFieldId, window.parent.document).html(displayValue);
                $(displayFieldId + "_value", window.parent.document).val(displayValue);

                if(parent.PF(displayField).getJQ().hasClass('canactivate'))
                {
                    parent.PF(displayField).enable();
                }
                
                $(primaryKeyFieldId, window.parent.document).val(primaryKey);

                if (primaryKey2 != null && primaryKey2 != '')
                {
                    $(primaryKeyField2Id, window.parent.document).val(primaryKey2);
                }
            }
        }
        else
        {
            $(displayFieldId).html(displayValue);
            $(displayFieldId + "_value").val(displayValue);

            if(PF(displayField).getJQ().hasClass('canactivate'))
            {
                PF(displayField).enable();
            }
            
            $(primaryKeyFieldId).val(primaryKey);

            if(primaryKey2 != null && primaryKey2 != '')
            {
                $(primaryKeyField2Id).val(primaryKey2);
            }
        }
    }
    
    closeLookupDialog(callbackMethod);
}

function openLookupForm(unitId, formUrl, primaryKeyField, primaryKey2Field)
{
    if(primaryKeyField != null && primaryKeyField.val() != null && primaryKeyField.val() != "0" && primaryKeyField.val() != "")
    {
        var sUrl = "";

        if(formUrl != "")
        {
            sUrl += formUrl + primaryKeyField.val();
        }
        else
        {
            sUrl += "/unit-form/" + unitId + "/" + primaryKeyField.val();
        }

        if(primaryKey2Field != null && primaryKey2Field.val() != null && primaryKey2Field.val() != "0" && primaryKey2Field.val() != "")
        {
            sUrl += "," + primaryKey2Field.val();
        }
    
        window.open(sUrl);
    }
}

function clearLookupValues(displayField, primaryKeyField,primaryKeyField2)
{
    displayFieldId = PrimeFaces.escapeClientId(displayField);
    primaryKeyFieldId = PrimeFaces.escapeClientId(primaryKeyField);
    primaryKeyField2Id = null;
    
    if(primaryKeyField2 != null && primaryKeyField2 != '')
    {
        primaryKeyField2Id = PrimeFaces.escapeClientId(primaryKeyField2);
    }

    if(parent.PF('lookupDialog') != null)
    {
        if(!$(displayFieldId,window.parent.document).length > 0)
        {
            var otherFrame = searchForFieldsInOtherIframes(displayFieldId);

            if(otherFrame != "")
            {
                $(displayFieldId,$(otherFrame,window.parent.document).contents()).html('');
                $(displayFieldId + "_value",$(otherFrame,window.parent.document).contents()).val('');

                $(otherFrame,window.parent.document)[0].contentWindow.PF(displayField).disable();
                $(primaryKeyFieldId,$(otherFrame,window.parent.document).contents()).val('');

                if (primaryKeyField2 != null && primaryKeyField2 != '')
                {
                    $(primaryKeyField2Id,$(otherFrame,window.parent.document).contents()).val('');
                }
            }
        }
        else
        {
            $(displayFieldId, window.parent.document).html('');
            $(displayFieldId + "_value", window.parent.document).val('');

            parent.PF(displayField).disable();

            $(primaryKeyFieldId, window.parent.document).val('');

            if (primaryKeyField2 != null && primaryKeyField2 != '')
            {
                $(primaryKeyField2Id, window.parent.document).val('');
            }
        }
    }
    else
    {
        $(displayFieldId).html('');
        $(displayFieldId + "_value").val('');

        PF(displayField).disable();

        $(primaryKeyFieldId).val('');

        if(primaryKeyField2 != null && primaryKeyField2 != '')
        {
            $(primaryKeyField2Id).val('');
        }
    }
}

function searchForFieldsInOtherIframes(displayFieldId)
{
    if($(displayFieldId,$('#quick-pia-iframe',window.parent.document).contents()).length == 1)
    {
        return "#quick-pia-iframe";
    }

    if($(displayFieldId,$('#quick-content-iframe',window.parent.document).contents()).length == 1)
    {
        return "#quick-content-iframe";
    }

    return "";
}

function flexibleLookupOnChangeClear(displayField, primaryKeyField,primaryKeyField2)
{
    displayFieldId = PrimeFaces.escapeClientId(displayField);
    primaryKeyFieldId = PrimeFaces.escapeClientId(primaryKeyField);
    primaryKeyField2Id = null;

    if(primaryKeyField2 != null && primaryKeyField2 != '')
    {
        primaryKeyField2Id = PrimeFaces.escapeClientId(primaryKeyField2);
    }

    $(displayFieldId).html('');
    $(displayFieldId + "_value").val('');

    PF(displayField).disable();

    $(primaryKeyFieldId).val('');

    if(primaryKeyField2 != null && primaryKeyField2 != '')
    {
        $(primaryKeyField2Id).val('');
    }
}

function openBundleDialog(displayField)
{
    if(parent.PF('lookupDialog') != null)
    {
        $(lookupDialogFrameId,window.parent.document).attr("src", "/resource-bundle/" + displayField);

        parent.PF('lookupDialog').show();
    }
    else
    {
        $(lookupDialogFrameId).attr("src", "/resource-bundle/" + displayField);

        PF('lookupDialog').show();
    }
}

function fillInResourceBundle(displayField, displayValue)
{
    displayFieldId = PrimeFaces.escapeClientId(displayField);

    if(parent.PF('lookupDialog') != null)
    {
        if(!($(displayFieldId,window.parent.document).length > 0))
        {
            var otherFrame = searchForFieldsInOtherIframes(displayFieldId);

            if(otherFrame != "")
            {
                $(displayFieldId,$(otherFrame,window.parent.document).contents()).val(displayValue);
            }
        }
        else
        {
            $(displayFieldId,window.parent.document).val(displayValue);
        }
    }
    else
    {
        $(displayFieldId).val(displayValue);
    }

    closeLookupDialog();
}

function clearResourceBundleValue(displayField)
{
    displayFieldId = PrimeFaces.escapeClientId(displayField);

    if(parent.PF('lookupDialog') != null)
    {
        $(displayFieldId,window.parent.document).val('');
    }
    else
    {
        $(displayFieldId).val('');
    }
}

function openIconsDialog(displayField)
{
    if(parent.PF('lookupDialog') != null)
    {
        $(lookupDialogFrameId,window.parent.document).attr("src", "/icons/" + displayField);

        parent.PF('lookupDialog').show();
    }
    else
    {
        $(lookupDialogFrameId).attr("src", "/icons/" + displayField);

        PF('lookupDialog').show();
    }
}

function openUrlSelectDialog(displayField)
{
    if(parent.PF('lookupDialog') != null)
    {
        $(lookupDialogFrameId,window.parent.document).attr("src", "/urls/" + displayField);

        parent.PF('lookupDialog').show();
    }
    else
    {
        $(lookupDialogFrameId).attr("src", "/urls/" + displayField);

        PF('lookupDialog').show();
    }
}